﻿using Core.Events;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Ioc
{
    public class CoreServiceContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {


            services.AddScoped<INotificationService, NotificationService>();


        }

    }
}
