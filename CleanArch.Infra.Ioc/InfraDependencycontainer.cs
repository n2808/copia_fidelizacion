﻿using CleanArch.Domain.Interfaces;
using CleanArch.Domain.Interfaces.Notifications;
using Domain.Interfaces;
using Domain.Interfaces.Backoffice;
using Domain.Interfaces.Campaigns;
using Domain.Interfaces.Configurations;
using Domain.Interfaces.EvidenceGestion;
using Domain.Interfaces.Group;
using Domain.Interfaces.location;
using Domain.Interfaces.Operators;
using Domain.Interfaces.Plans;
using Domain.Interfaces.Products;
using Domain.Interfaces.ReSchedule;
using Domain.Interfaces.Quotes;
using Domain.Interfaces.Scheduling;
using Domain.Interfaces.Status;
using Domain.Interfaces.Surveys;
using Domain.Interfaces.User;
using Infra.Data.Repository;
using Infra.Data.Repository.Backoffice;
using Infra.Data.Repository.Campaigns;
using Infra.Data.Repository.Configurations;
using Infra.Data.Repository.EvidenceGestion;
using Infra.Data.Repository.Groups;
using Infra.Data.Repository.Location;
using Infra.Data.Repository.Notifications;
using Infra.Data.Repository.Operators;
//using Infra.Data.Repository.Plans;
using Infra.Data.Repository.Products;
using Infra.Data.Repository.ReSchedule;
using Infra.Data.Repository.Quotes;
using Infra.Data.Repository.Scheduling;
using Infra.Data.Repository.Status;
using Infra.Data.Repository.Surveys;
using Infra.Data.Repository.Users;
using Microsoft.Extensions.DependencyInjection;
using Domain.Interfaces.Skill;
using Infra.Data.Repository.Skill;
using Application.Services.SecondRingLoad;
using Infra.Data.Repository.SecondRingLoad;
using Domain.Interfaces.SecondRingLoad;
using Domain.Interfaces.Releases;
using Infra.Data.Repository.Releases;

namespace Infra.Ioc
{
    public static class InfraDependencycontainer
    {
        public static void RegisterServices(IServiceCollection services)
        {

            services.AddScoped<IDatabaseClientDetailRepository, DatabaseClientDetailRepository>();
            services.AddScoped<IGestionRepository, GestionRepository>();
            services.AddScoped<IServiceDialerRepository, ServiceDialerRepository>();
            services.AddScoped<IInformativeAgentFieldsRepository, InformativeAgentFieldsRepository>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<ITipificationRepository, TipificationRepository>();
            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<IAppNotificationRepository, AppNotificationRepository>();
            services.AddScoped<IGestionReportRepository, GestionReportsRepository>();
            services.AddScoped<IActiveGestionRepository, ActiveGestionRepository>();
            services.AddScoped<IDatabaseClientReportsRepository, DatabaseClientReportsRepository>();
            services.AddScoped<IGestionReportsOffersRepository, GestionReportsOffersRepository>();
            services.AddScoped<ISegmentAppNotificationRepository, SegmentAppNotificationRepository>();
            services.AddScoped<ISurveySegmentRepository, SurveySegmentRepository>();
            services.AddScoped<ISurveyRepository, SurveyRepository>();
            services.AddScoped<ISurveyResultAnswersRepository, SurveyResultAnswersRepository>();
            services.AddScoped<ISurveyResultGestionRepository, SurveyResultGestionRepository>();
            services.AddScoped<ISurveyResultQuestionRepository, SurveyResultQuestionRepository>();

            //Notifications
            services.AddScoped<INotificationAccountRepository, NotificationAccountRepository>();
            services.AddScoped<INotificationLogRepository, NotificationLogRepository>();


            //campaign
            services.AddScoped<ISegmentRepository, SegmentRepository>();
            services.AddScoped<ISubCampaignRepository, SubCampaignRepository>();

            //Location
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IStateRepository, StateRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();

            // Scheduling
            services.AddScoped<IScheduledAppointmentRepository, ScheduledAppointmentRepository>();

            // Backoffice
            services.AddScoped<IBackofficeGestionRepository, BackofficeGestionRepository>();

            // Configuration
            services.AddScoped<IConfigurationRepository, ConfigurationRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();


            //User
            services.AddScoped<IUserRepository, UserRepository>();

            //Plans
            //services.AddScoped<IPlanRepository, PlanRepository>();


            //Rol
            services.AddScoped<IRolRepository, RolRepository>();

            //Products
            services.AddScoped<IProductRepository, ProductRepository>();

            //Group
            services.AddScoped<IGroupRepository, GroupRepository>();

            //Operators
            services.AddScoped<IOperatorRepository, OperatorRepository>();

            //Quotes
            services.AddScoped<IQuoteRepository, QuoteRepository>();

            //UserRol
            services.AddScoped<IUserRolRepository, UserRolRepository>();

            //Status
            services.AddScoped<IStatusRepository, StatusRepository>();

            //EvidenceGestion
            services.AddScoped<IEvidenceGestionRepository, EvidenceGestionRepository>();

            //GestionReport
            services.AddScoped<IGestionReportRepository, GestionReportsRepository>();

            //GestionStructure
            services.AddScoped<IGestionStructureRepository, GestionStructureRepository>();


            #region Reschedule
            services.AddScoped<IGestionRescheduleRepository, GestionRescheduleRepository>();
            services.AddScoped<IRescheduleHistoryRepository, RescheduleHistoryRepository>();
            services.AddScoped<ITipificationRescheduleRepository, TipificationRescheduleRepository>();

            #endregion

            #region GenericRepository
            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            #endregion

            //ClaroUSer
            services.AddScoped<IClaroUserRepository, ClaroUserRepository>();

            //Skills
            services.AddScoped<ISkillRepository, SkillRepository>();

            //ManagementBackOffice
            services.AddScoped<IBackOfficeManagementsRepository, BackOfficeManagementsRepository>();

            //BackOfficeOperations
            services.AddScoped<IBackOfficeOperationsRepository, BackOfficeOperationsRepository>();

            //Department
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();

            //BackOfficeOperations
            services.AddScoped<INewCityRepository, NewCityRepository>();

            //SecondRingLoad
            services.AddScoped<ISecondRingLoadRepository, SecondRingLoadRepository>();

            //FetchFaceEvidence
            services.AddScoped<IFetchFaceEvidenceRepository, FetchFaceEvidenceRepository>();

            services.AddScoped<ISubCategoryTipification, SubCategoryTipification>();

            services.AddScoped<IReleaseRepository, ReleaseRepository>();


        }
    }
}
