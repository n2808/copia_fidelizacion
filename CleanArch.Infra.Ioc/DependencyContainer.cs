﻿using Application.Common.interfaces;
using CleanArch.Domain.Core.Bus;
using CleanArch.Infra.Bus;
using CleanArch.Infra.Data.Context;
using Core.Events;
using Infra.Ioc;
using Microsoft.Extensions.DependencyInjection;

namespace CleanArch.Infra.Ioc
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
 

             
            //CQRS MediatR
            services.AddScoped<IMediatorhandler, inMemoryBus>();

            //context services.
            services.AddScoped<IU27ApplicationDBContext>(provider => provider.GetService<U27ApplicationDBContext>());
            services.AddScoped<U27ApplicationDBContext>();

            //Core Services
            //services.AddScoped<ISMSNotification, SMSNotification>();
 
            //Application Layer.
            ApplicationDependencycontainer.RegisterServices(services);

            // Infra.Data Layer
            InfraDependencycontainer.RegisterServices(services);

            // Infra.Data Layer
            CoreServiceContainer.RegisterServices(services);


        }
    }
}
