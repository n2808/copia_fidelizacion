﻿using Application.Interfaces;
using Application.Interfaces.Backoffice;
using Application.Interfaces.BulkExcel;
using Application.Interfaces.Configurations;
using Application.Interfaces.EvidenceGestion;
using Application.Interfaces.Group;
using Application.Interfaces.Location;
using Application.Interfaces.Operators;
using Application.Interfaces.Plans;
using Application.Interfaces.Products;
using Application.Interfaces.Quotes;
using Application.Interfaces.Release;
using Application.Interfaces.Reschedule;
using Application.Interfaces.Rol;
using Application.Interfaces.Scheduling;
using Application.Interfaces.SecondRingLoad;
using Application.Interfaces.Skill;
using Application.Interfaces.Status;
using Application.Interfaces.Surveys;
using Application.Interfaces.User;
using Application.Services;
using Application.Services.Backoffice;
using Application.Services.BulkExcel;
using Application.Services.Configurations;
using Application.Services.EvidenceGestion;
using Application.Services.Group;
using Application.Services.Location;
using Application.Services.Operators;
using Application.Services.Plans;
using Application.Services.Products;
using Application.Services.Quotes;
using Application.Services.Releases;
using Application.Services.Reschedule;
using Application.Services.Rol;
using Application.Services.Scheduling;
using Application.Services.SecondRingLoad;
using Application.Services.Skill;
using Application.Services.Status;
using Application.Services.User;
using CleanArch.Application.Interfaces;
using CleanArch.Application.Interfaces.Auths;
using CleanArch.Application.Interfaces.Notifications;
using CleanArch.Application.Services;
using CleanArch.Application.Services.Auths;
using CleanArch.Application.Services.Notifications;
using CleanArch.Application.Services.Surveys;
using Microsoft.Extensions.DependencyInjection;

namespace Infra.Ioc
{
    public static class ApplicationDependencycontainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IDatabaseClientDetailService, DatabaseClientDetailService>();
            services.AddScoped<IGestionService, GestionService>();
            services.AddScoped<IServiceDialerService, ServiceDialerService>();
            services.AddScoped<IInformativeAgentFieldsService, InformativeAgentFieldsService>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<ITipificationService, TipificationService>();
            services.AddScoped<IGestionReportService, GestionReportService>();
            services.AddScoped<IActiveGestionService, ActiveGestionService>();
            services.AddScoped<ISMSService, SMSService>();
            services.AddScoped<IDatabaseClientReportsService, DatabaseClientReportsService>();
            services.AddScoped<IGestionReportsOffersService, GestionReportsOffersService>();
            services.AddScoped<ISegmentAppNotificationService, SegmentAppNotificationService>();
            services.AddScoped<ISurveySegmentService, SurveySegmentService>();
            //Notification
            services.AddScoped<INotificationAccountService, NotificationAccountService>();
            services.AddScoped<IAppNotificationService, AppNotificationService>();
            services.AddScoped<INotificationLogService, NotificationLogService>();


            //Auth
            services.AddScoped<IAuthService, AuthService>();

            //Surveys
            services.AddScoped<ISurveyService, SurveyService>();
            services.AddScoped<ISurveyResultGestionService, SurveyResultGestionService>();
            services.AddScoped<ISurveyResultQuestionService, SurveyResultQuestionService>();

            //campaign
            services.AddScoped<ISegmentService, SegmentService>();
            services.AddScoped<ISubCampaignService, SubCampaignService>();

            //Location.
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IStateService, StateService>();
            services.AddScoped<ICountryService, CountryService>();

            //Scheduling
            services.AddScoped<IScheduledAppointmentService, ScheduledAppointmentService>();

            //Backoffice
            services.AddScoped<IBackofficeGestionService, BackofficeGestionService>();

            //Configuration
            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<ICategoryService, CategoryService>();

            //User
            services.AddScoped<IUserService, UserService>();

            //Plans
            services.AddScoped<IPlanService, PlanService>();
            //Rol
            services.AddScoped<IRolService, RolService>();
            //Products
            services.AddScoped<IProductService, ProductService>();

            //Group
            services.AddScoped<IGroupService, GroupService>();

            //Operators
            services.AddScoped<IOperatorService, OperatorService>();

            //Quotes
            services.AddScoped<IQuoteService, QuoteService>();

            //UserRol
            services.AddScoped<IUserRolService, UserRolService>();

            //BulkExcel
            services.AddScoped<IBulkExcelService, BulkExcelService>();

            //Status
            services.AddScoped<IStatusService, StatusService>();

            //EvidenceGestion
            services.AddScoped<IEvidenceGestionService, EvidenceGestionService>();

            //GestionReport
            services.AddScoped<IGestionReportService, GestionReportService>();

            //GestionStructure
            services.AddScoped<IGestionStructureService, GestionStructureService>();

            #region Reschedule
            services.AddScoped<ITipificationRescheduleService, TipificationRescheduleService>();
            services.AddScoped<IGestionRescheduleService, GestionRescheduleService>();
            services.AddScoped<IRescheduleService, RescheduleService>();
            #endregion

            //ClaroUSer
            services.AddScoped<IClaroUserService, ClaroUserService>();

            //Skills
            services.AddScoped<ISkillService, SkillService>();

            //ManagementsBaackOffice
            services.AddScoped<IBackOfficeManagementsService, BackOfficeManagementsService>();

            //BackOfficeOperations
            services.AddScoped<IBackOfficeOperationsService, BackOfficeOperationsService>();

            //Department
            services.AddScoped<IDepartmentService, DepartmentService>();
            
            //NewCity
            services.AddScoped<INewCityService, NewCityService>();

            //SecondRingLoad
            services.AddScoped<ISecondRingLoad, SecondRingLoad>();

            //FetchFaceEvidence
            services.AddScoped<IFetchFaceEvidenceService, FetchFaceEvidenceService>();

            services.AddScoped<ISubCategoryTipificationService, SubCategoryTipificationService>();

            services.AddScoped<IRelease, ReleaseService>();

        }

    }
}
