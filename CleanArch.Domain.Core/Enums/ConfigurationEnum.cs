﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core.Enums
{
    public static class TypeQuestionEnum
    {
        public static Guid Single { get; set; } = new Guid("a2b2f0e6-5eed-4636-a178-23c82a94c519");
        public static Guid Multiple { get; set; } = new Guid("674e4605-6933-4f6e-a355-cbab4ec372b8");
    }
}
