﻿using Core.Events.SMS;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Core.Events
{
    public class SMSNotificationMaSivApp : INotification
    {


        private NotificationAccountViewModel _account;
        HttpClient client = new HttpClient();
        public SMSNotificationMaSivApp(NotificationAccountViewModel account)
        {
            _account = account;
        }


        public async Task Notifiy()
        {

            try
            {
                _account.phone = _account.phone.Length == 10 ? "57" + _account.phone : _account.phone;

                client.BaseAddress = new Uri(_account.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string RequestString =
                        _account.Method +
                        "recipient=" + _account.phone +
                        "&messagedata=" + _account.Message1.Trim() +
                        _account.Account +
                        _account.Apikey +
                        _account.customData1 +
                        _account.customData2 +
                        _account.customData3;

                HttpResponseMessage response = await client.GetAsync(RequestString);
                if (response.IsSuccessStatusCode)
                {
                    await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {

                
            }
         
        }
    }
}
