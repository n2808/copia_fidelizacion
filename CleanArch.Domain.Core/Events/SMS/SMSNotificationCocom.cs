﻿using Core.Events.SMS;
using Core.Helper;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Core.Events
{

    public class CocomBodyRequest
    {
        public string from { get; set; }
        public string to { get; set; }
        public string text { get; set; }
    }

    public class SMSNotificationCocom : INotification
    {

        HttpClient client = new HttpClient();
        NotificationAccountViewModel _account;
        public SMSNotificationCocom(NotificationAccountViewModel account)
        {
            _account = account;
        }

        public async Task Notifiy()
        {

            try
            {
                _account.phone = _account.phone.Length == 10 ? "57" + _account.phone : _account.phone;
                string token = Base64.Encode(_account.Account + ":" + _account.Apikey);
                client.BaseAddress = new Uri(_account.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);

                //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;



                CocomBodyRequest payload = new CocomBodyRequest()
                {
                    from = _account.Account,
                    to = _account.phone,
                    text = _account.Message1
                    

                };


                var todoItemJson = new StringContent(
                    JsonSerializer.Serialize(payload),
                    Encoding.UTF8,
                    "application/json");


                HttpResponseMessage response = await client.PostAsync(_account.Method,todoItemJson);
                response.EnsureSuccessStatusCode();

                var result = response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {


            }



        }
    }
}
