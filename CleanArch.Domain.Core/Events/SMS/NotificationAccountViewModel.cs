﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Events.SMS
{
    public class NotificationAccountViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int TypeNotification { get; set; }
        public string Message1 { get; set; }
        public string phone { get; set; }
        public string Message2 { get; set; }
        public string Account { get; set; }
        public string Apikey { get; set; }
        public string token { get; set; }
        public string Url { get; set; }
        public string Method { get; set; }
        public string customData1 { get; set; }
        public string customData2 { get; set; }
        public string customData3 { get; set; }
        public string customData4 { get; set; }
        public string customData5 { get; set; }
        public string customData6 { get; set; }
        public bool status { get; set; }


    }
}
