﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Events
{
    public interface INotificationService
    {
        Task Send(List<INotification> notifications);
    }
}