﻿using Application.Backoffice.Commands.DeleteBackoffice;
using Application.Backoffice.Commands.PostBackoffice;
using Application.Backoffice.Commands.PutBackoffice;
using Application.Backoffice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/back-office-operations")]
    [ApiController]
    [Authorize]
    public class BackOfficeOperationController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetBackOfficeOperation([FromQuery] GetBackOfficeOperationsQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBackOfficeOperationById(int id)
        {
            return Ok(await Mediator.Send(new GetBackOfficeOperationQueryById() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public async Task<IActionResult> StatusActivateBackOfficeOperationById(int id)
        {
            return Ok(await Mediator.Send(new PutBackOfficeOperationActivateCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public async Task<IActionResult> StatusDeActivateBackOfficeOperationById(int id)
        {
            return Ok(await Mediator.Send(new PutBackOfficeOperationsDeActivateCommand() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> PostBackOfficeOperation([FromBody] PostBackOfficeOperationCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutBackOfficeOperation(int id, PutBackOfficeOperationCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBackOfficeOperation(int id)
        {
            return Ok(await Mediator.Send(new DeleteBackOfficeOperationCommand() { Id = id }));
        }
    }
}
