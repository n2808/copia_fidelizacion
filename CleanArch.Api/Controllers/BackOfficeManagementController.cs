﻿using Application.BackOfficeManagements.Command.Post;
using Application.BackOfficeManagements.Command.Put;
using Application.BackOfficeManagements.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/back-office")]
    [ApiController]
    [Authorize]

    public class BackOfficeManagementController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetManagementsAvailableBackOfficeQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            return Ok(await Mediator.Send(new GetAvailableBackOfficeByIdQuery() { Id = id }));
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAll([FromQuery] GetBackOfficeAllQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("overview")]
        public async Task<IActionResult> GetOverView([FromQuery] GetBackOfficeOverViewQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("availables")]
        public async Task<IActionResult> Get([FromQuery] GetAvailableBackOfficeQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("assign")]
        public async Task<IActionResult> Post([FromQuery] AssignAgentManagementCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("{id}/start-management")]
        public async Task<IActionResult> ActiveteGroup(Guid id)
        {
            return Ok(await Mediator.Send(new StartdBackOfficeManagementCommand() { Id = id }));
        }

        [HttpPut("{id}/end-management")]
        public async Task<IActionResult> PutEndBackOfficeManagement(Guid id, PutEndBackOfficeManagementCommand command)
        {
            command.Id = id;
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}/end-management-validator")]
        public async Task<IActionResult> PutEndValidatorManagement(Guid id, PutEndValidatorManagementCommand command)
        {
            command.Id = id;
            return Ok(await Mediator.Send(command));
        }

        [HttpPost]
        public async Task<IActionResult> PostManagementBackOffice([FromBody] PostBackOfficeManagementsCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, PutBackOfficeManagementsCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
