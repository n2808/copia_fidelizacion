﻿using Application.Plans.Command;
using Application.Plans.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/plans")]
    [ApiController]
    [Authorize]
    public class PlansController : ApiControllerBase
    {

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetPlanQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PostPlanCommand command)
        {
            return Ok(await Mediator.Send(command));
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, PutPlanCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new DeletePlanCommand() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public async Task<IActionResult> ActivetePlan(Guid id)
        {
            return Ok(await Mediator.Send(new ActivatePlanCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public async Task<IActionResult> DeactivatePlan(Guid id)
        {
            return  Ok(await Mediator.Send(new DeactivatePlanCommand() { Id = id }));
        }
    }
}
