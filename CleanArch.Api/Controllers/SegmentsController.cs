﻿using Application.Segments.Command;
using Application.Segments.Command.Post;
using Application.Segments.Command.Put;
using Application.Segments.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/segments")]
    [ApiController]
    [Authorize]
    public class SegmentsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetSegment([FromQuery] GetSegmentQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSegmentById(int id)
        {
            return Ok(await Mediator.Send(new GetSegmentByIdQuery() { Id = id }));
        }

        

        [HttpGet("{id}/activate")]
        public async Task<IActionResult> StatusActivateSegmentById(int id)
        {
            return Ok(await Mediator.Send(new PutStatusActivateSegmentByIdCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public async Task<IActionResult> StatusDeActivateSegmentById(int id)
        {
            return Ok(await Mediator.Send(new PutStatusDeActivateSegmentByIdCommand() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> PostSegment([FromBody] PostSegmentCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutSegment(int id, PutSegmentCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSegment(int id)
        {
            return Ok(await Mediator.Send(new DeleteSegmentCommand() { Id = id }));

        }
    }
}
