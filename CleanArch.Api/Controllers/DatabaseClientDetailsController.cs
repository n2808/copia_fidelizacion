﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace ClaroFidelizacion.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DatabaseClientDetailsController : ControllerBase
    {
        private readonly U27ApplicationDBContext _context;

        public DatabaseClientDetailsController(U27ApplicationDBContext context)
        {
            _context = context;
        }

        // GET: api/DatabaseClientDetails
        [HttpGet]
        
        public async Task<ActionResult<IEnumerable<DatabaseClientDetail>>> GetDatabaseClientDetail()
        {
            return await _context.DatabaseClientDetail.ToListAsync();
        }

        // GET: api/DatabaseClientDetails/5
        [HttpGet("{id}")]
        [Authorize(Roles = "Manager")]
        public async Task<ActionResult<DatabaseClientDetail>> GetDatabaseClientDetail(int id)
        {
            var databaseClientDetail = await _context.DatabaseClientDetail.FindAsync(id);
            var Id = User.Identity;

            var identity = (ClaimsIdentity)User.Identity;
            var Id2 = identity.FindFirst("Id").Value;
            IEnumerable<Claim> claims = identity.Claims;

            if (databaseClientDetail == null)
            {
                return NotFound();
            }

            return databaseClientDetail;
        }

        // PUT: api/DatabaseClientDetails/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDatabaseClientDetail(int id, DatabaseClientDetail databaseClientDetail)
        {
            if (id != databaseClientDetail.Id)
            {
                return BadRequest();
            }

            _context.Entry(databaseClientDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DatabaseClientDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DatabaseClientDetails
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DatabaseClientDetail>> PostDatabaseClientDetail(DatabaseClientDetail databaseClientDetail)
        {
            _context.DatabaseClientDetail.Add(databaseClientDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDatabaseClientDetail", new { id = databaseClientDetail.Id }, databaseClientDetail);
        }

        // DELETE: api/DatabaseClientDetails/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DatabaseClientDetail>> DeleteDatabaseClientDetail(int id)
        {
            var databaseClientDetail = await _context.DatabaseClientDetail.FindAsync(id);
            if (databaseClientDetail == null)
            {
                return NotFound();
            }

            _context.DatabaseClientDetail.Remove(databaseClientDetail);
            await _context.SaveChangesAsync();

            return databaseClientDetail;
        }

        private bool DatabaseClientDetailExists(int id)
        {
            return _context.DatabaseClientDetail.Any(e => e.Id == id);
        }
    }
}
