﻿using Application.Status.Command;
using Application.Status.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/status")]
    [ApiController]
    //[Authorize]

    public class StatusController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetStatus([FromQuery] GetStatusQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("subCampaing")]
        public async Task<IActionResult> GetStatusBySubCampaing([FromQuery] GetStatusQuerySubCampaing query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PostStatusCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, PutStatusCommand command)
        {
            if(id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new DeleteStatusCommand() { Id = id }));
        }
    }
}
