﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.SubCampaign.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubCampaignController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetSubCampaign([FromQuery] GetSubCampaignsQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

    }
}
