﻿using Application.Surveys.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyController : ApiControllerBase
    {

        [HttpGet]
        [Route("GetSurveys")]
        public IActionResult GetSurveys([FromQuery] GetSurveysQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpPost]
        [Route("Send")]
        public IActionResult PostSurveyResponses([FromBody] PostSurveyResponseCommand command)
        {
            return Ok(Mediator.Send(command));
        }
    }
}
