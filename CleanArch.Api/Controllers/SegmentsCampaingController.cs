﻿using Application.Segments.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/segmentsCampaing")]
    [ApiController]
    //[Authorize]
    public class SegmentsCampaingController : ApiControllerBase
    {
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSegmentCampaingById(int id)
        {
            return Ok(await Mediator.Send(new GetSegmentByIdCampaingQuery() { Id = id }));
        }
    }
}
