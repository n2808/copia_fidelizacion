﻿using Application.FetchFaceEvidence.Commands;
using Application.FetchFaceEvidence.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class FetchFaceEvidenceController : ApiControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> PostFetchFaceEvidence([FromBody] PostFetchFaceEvidenceCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFetchFaceEvidence(int id)
        {
            return Ok(await Mediator.Send(new FetchFaceEvidenceCommand() { FetchFaceId = id }));
        }
    }
}
