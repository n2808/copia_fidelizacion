﻿using Application.Location.Queries.Departments;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/departments")]
    [ApiController]
    //[Authorize]
    public class DepartmentController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetDepartments([FromQuery] GetDepartmentsQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDepartmentById(int id)
        {
            return Ok(await Mediator.Send(new GetDepartmentsByIdQuery() { Id = id }));
        }
    }
}
