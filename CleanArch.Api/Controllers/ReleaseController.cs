﻿using Application.Releases.Command.Delete;
using Application.Releases.Command.Post;
using Application.Releases.Command.Put;
using Application.Releases.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/release")]
    [ApiController]
    [Authorize]
    public class ReleaseController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetRelease([FromQuery] GetReleaseQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetReleaseById(int id)
        {
            return Ok(await Mediator.Send(new GetReleaseQueryById() { Id = id }));
        }

        [HttpGet("by-subCampaignId")]
        public async Task<IActionResult> GetReleaseBySubCampaignId([FromQuery] GetReleaseBySubCampaignIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("{id}/activate")]
        public async Task<IActionResult> StatusActivateReleaseById(int id)
        {
            return Ok(await Mediator.Send(new PutReleasesActivateCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public async Task<IActionResult> StatusDeActivateReleaseById(int id)
        {
            return Ok(await Mediator.Send(new PutReleasesDeActivateCommand() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> PostRelease([FromBody] PostReleaseCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutRelease(int id, PutReleasesCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSkill(int id)
        {
            return Ok(await Mediator.Send(new DeleteSkillCommand() { Id = id }));

        }
    }
}
