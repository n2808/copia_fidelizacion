﻿using Application.Notification.Commands;
using Application.Notification.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SMSController : ApiControllerBase
    {

        [HttpPost]
        [Route("GetSms")]
        //[EnableCors(PolicyName = "MyAllowSpecificOrigins")]
        public IActionResult GetSms([FromBody] GetSMSQuery command)
        {
            return Ok(Mediator.Send(command));
        }

        [HttpPost]
        [Route("Send")]
        public async Task<IActionResult> Post([FromBody] SendSMSCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
