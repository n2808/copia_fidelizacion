﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Auth.Commands;
using Application.ViewModel.Auth;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/auth-token")]
    [ApiController]
    public class AuthTokenController : ApiControllerBase
    {

        [HttpPost]
        public async Task<IActionResult> Authentication([FromBody] PostLoginCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("inbound")]
        public async Task<IActionResult> AuthenticationIbound([FromBody] PostLoginInboundCommand command)
        {
            var data = Mediator.Send(command);
            if (!data.Result.status)
            {
                return Unauthorized(await data);
            }
            return Ok(await data);
        }

        [HttpPost("outbound")]
        public async Task<IActionResult> AuthenticationOutBound([FromBody] PostLoginOutboundCommand command)
        {
            var data = Mediator.Send(command);
            if (!data.Result.status)
            {
                return Unauthorized(await data);
            }
            return Ok(await data);
        }

    }
}
