﻿using Application.Rol.Commands.DeleteRol;
using Application.Rol.Commands.PostRol;
using Application.Rol.Commands.PutRol;
using Application.Rol.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/rols")]
    [ApiController]
    //[Authorize]
    public class RolsController : ApiControllerBase
    {
        [HttpGet]       
        public IActionResult GetRol([FromQuery] GetRolQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpPost]
        public IActionResult PostRol([FromBody] PostRolCommand command)
        {
            return Ok(Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public IActionResult PutRol(Guid id, PutRolCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteRol(Guid id)
        {
            return Ok(Mediator.Send(new DeleteRolCommand() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public IActionResult StatusActivateRolById(Guid id)
        {
            return Ok(Mediator.Send(new PutStatusActivateRolById() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public IActionResult StatusDeactivateRolById(Guid id)
        {
            return Ok(Mediator.Send(new PutStatusDeactivateRolById() { Id = id }));
        }
    }
}
