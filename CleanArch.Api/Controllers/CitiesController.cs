﻿using Application.Location.Queries;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ApiControllerBase
    {
        [HttpGet]        
        public IActionResult GetCities([FromQuery] GetCitiesQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [Route("ByState/{id}")]
        [HttpGet]
        public IActionResult GetCitiesByState(int id)
        {
            return Ok(Mediator.Send(new GetCitiesByStateIdQuery() { id = id }));
        }
    }
}
