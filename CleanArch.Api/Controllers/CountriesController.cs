﻿using Application.Location.Queries.Countries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/countries")]
    [ApiController]
    //[Authorize]
    public class CountriesController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetCountries([FromQuery] GetCountriesQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContriesById(Guid id)
        {
            return Ok(await Mediator.Send(new GetCountriesByIdQuery() { Id = id }));
        }
        [HttpGet("{code}/code")]
        public async Task<IActionResult> GetContriesByCode(string code)
        {
            return Ok(await Mediator.Send(new GetCountriesByCodeQuery() { Code = code }));
        }
       
        [HttpGet("{name}/name")]
        public async Task<IActionResult> GetContriesByName(string name)
        {
            return Ok(await Mediator.Send(new GetCountriesByNameQuery() { Name = name }));
        }
    }
}
