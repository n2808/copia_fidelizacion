﻿using Application.EvidenceGestion.Queries;
using Application.Gestions.Queries.GetByIdEvidencesGestion;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EvidencesController : ApiControllerBase
    {
        [HttpGet("{id}/move")]
        public async Task<IActionResult> EvidencesGestion(int id)
        {
            return Ok(await Mediator.Send(new EvidenceGestionCommand() { GestionReportsId = id }));
        }

        [HttpGet("{id}")]
        public IActionResult GetEvidencesGestion(int id)
        {
            return Ok(Mediator.Send(new GetByIdEvidencesGestionQuery() { GestionReportsId = id }));
        }
    }
}
