﻿using Application.SecondRingLoad.Command.Post;
using Application.SecondRingLoad.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api")]
    [ApiController]
    [Authorize]
    public class SecondRingLoadController : ApiControllerBase
    {
        [HttpPost("upload-base/second-ring")]
        public async Task<IActionResult> UploadExcel(IFormFile fileOne)
        {
            return Ok(await Mediator.Send(new SecondRingLoadExcelCommand() { File = fileOne }));
        }

        [HttpGet("loads/second-ring")]
        public async Task<IActionResult> GetSecongRing([FromQuery] GetSecondRingQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("second-ring/{LoadId}/to-assign")]
        public async Task<IActionResult> GetSecongRingAssign(Guid LoadId)
        {
            return Ok(await Mediator.Send(new GetSecondRingAssingQuery() { Id = LoadId }));
        }
    }
}
