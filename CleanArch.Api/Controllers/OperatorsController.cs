﻿using Application.Operators.Command;
using Application.Operators.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/operators")]
    [ApiController]
    [Authorize]
    public class OperatorsController : ApiControllerBase
    {
        [HttpGet]
        public IActionResult GetOperators([FromQuery] GetOperatorQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpPost]
        public IActionResult PostOperator([FromBody] PostOperatorCommand command)
        {
            return Ok(Mediator.Send(command));
        }


        [HttpPut("{id}")]
        public IActionResult PutOperator(Guid id, PutOperatorCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteOperator(Guid id)
        {
            return Ok(Mediator.Send(new DeleteOperatorCommand() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public IActionResult ActiveteOperator(Guid id)
        {
            return Ok(Mediator.Send(new ActivateOperatorCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public IActionResult DeactivateOperator(Guid id)
        {
            return Ok(Mediator.Send(new DeactivateOperatorCommand() { Id = id }));
        }
    }
}
