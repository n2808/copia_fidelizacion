﻿using Application.ClaroUser.Command.Post;
using Application.ClaroUser.Command.Put;
using Application.ClaroUser.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{

    [Route("api/claro-users")]
    [ApiController]
    [Authorize]
    public class ClaroUserController : ApiControllerBase
    {
        [HttpGet("{code}/code")]
        public async Task<IActionResult> ActivetePlan(string code)
        {
            return Ok(await Mediator.Send(new GetCodeClaroUserQuery() { Code = code }));
        }

        [HttpPost("excel")]
        public async Task<IActionResult> UploadExcel(IFormFile fileOne)
        {
            return Ok(await Mediator.Send(new ClaroUsersExcelCommand() { File = fileOne }));
        }

        [HttpPost]
        public async Task<IActionResult> PostBackOfficeOperation([FromBody] PostClaroUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{code}")]
        public async Task<IActionResult> PutBackOfficeOperation(string code, PutClaroUserCommand command)
        {
            if (code != command.Code)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
