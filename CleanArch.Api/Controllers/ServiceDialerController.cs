﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ServicesDialer.Queries;
using Application.SubCampaign.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceDialerController : ApiControllerBase
    {

        [HttpGet]
        [Authorize]
        public IActionResult GetServiceDialer([FromQuery] GetServiceDialerQuery query)
        {
            return Ok(Mediator.Send(query));
        }

    }
}
