﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Group.Command;
using Application.Group.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/groups")]
    [ApiController]
    [Authorize]
    public class GroupsController : ApiControllerBase
    {
        [HttpGet]
        public IActionResult Get([FromQuery] GetGroupQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpPost]
        public IActionResult Post([FromBody] PostGroupCommand command)
        {
            return Ok(Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, PutGroupCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));

        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            return Ok(Mediator.Send(new DeleteGroupCommand() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public IActionResult ActiveteGroup(Guid id)
        {
            return Ok(Mediator.Send(new ActivateGroupCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public IActionResult DeactivateGroup(Guid id)
        {
            return Ok(Mediator.Send(new DeactivateGroupCommand() { Id = id }));
        }
    }
}
