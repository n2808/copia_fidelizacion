﻿using Application.Backoffice.Commands.DeleteBackoffice;
using Application.Backoffice.Commands.PostBackoffice;
using Application.Backoffice.Commands.PutBackoffice;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BackofficeGestionsController : ApiControllerBase
    {
        [Authorize]
        [HttpPost]
        public IActionResult PostBackofficeGestion([FromBody] PostBackofficeGestionCommand command)
        {
            return Ok(Mediator.Send(command));
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult DeleteBackofficeGestion(Guid id)
        {
            return Ok(Mediator.Send(new DeleteBackofficeGestionCommand() { Id = id }));
        }

        [Authorize]
        [HttpPut("{id}")]
        public IActionResult PutBackofficeGestion(Guid id, PutBackofficeGestionCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));

        }
    }
}
