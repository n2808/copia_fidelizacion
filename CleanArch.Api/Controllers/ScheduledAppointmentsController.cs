﻿using Application.Scheduling.Commands.DeleteScheduled;
using Application.Scheduling.Commands.PostScheduled;
using Application.Scheduling.Commands.PutScheduled;
using Application.Scheduling.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/Scheduled")]
    [ApiController]
    public class ScheduledAppointmentsController : ApiControllerBase
    {

        //[Authorize]
        [HttpGet]
        public IActionResult ScheduledAppointment([FromQuery] ScheduledAppointmentQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        //[Authorize]
        [HttpPost]
        public IActionResult PostScheduledAppointment([FromBody] PostScheduledAppointmentCommand command)
        {
            return Ok(Mediator.Send(command));
        }

        //[Authorize]
        [HttpDelete("{id}")]  
        public IActionResult DeleteScheduledAppointment(Guid id)
        {           
            return Ok(Mediator.Send(new DeleteScheduledAppointmentByIdCommand() { Id = id }));
        }

        //[Authorize]
        [HttpPut("{id}")]
        public IActionResult ScheduledAppointment(Guid id, PutScheduledAppointmentCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));

        }
    }
}
