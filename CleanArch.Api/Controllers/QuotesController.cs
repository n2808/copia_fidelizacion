﻿using Application.Quotes.Command;
using Application.Quotes.Queries;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/quotes")]
    [ApiController]    

    public class QuotesController : ApiControllerBase
    {
        [HttpGet]
        public IActionResult GetQuotes([FromQuery] GetQuoteQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpPost]
        public IActionResult PostQuote([FromBody] PostQuoteCommand command)
        {
            return Ok(Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public IActionResult PutQuote(Guid id, PutQuoteCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteQuote(Guid id)
        {
            return Ok(Mediator.Send(new DeleteQuoteCommand() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public IActionResult ActiveteQuote(Guid id)
        {
            return Ok(Mediator.Send(new ActivateQuoteCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public IActionResult DeactivateQuote(Guid id)
        {
            return Ok(Mediator.Send(new DeactivateQuoteCommand() { Id = id }));
        }
    }
}
