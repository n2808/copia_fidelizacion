﻿using Application.Configurations.Categories.Command;
using Application.Configurations.Categories.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : ApiControllerBase
    {

        [HttpGet]
        [Authorize]
        public IActionResult Get([FromQuery] GetCategoryQuery query)
        {
            return Ok(Mediator.Send(query));             
        }

        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody] PostCategoryCommand command)
        {
            return Ok(Mediator.Send(command));
        }


        [HttpPut("{id}")]
        [Authorize]
        public IActionResult Put(Guid id, PutCategoryCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));

        }

        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(Guid id)
        {
            return Ok(Mediator.Send(new DeleteCategoryCommand() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        [Authorize]
        public IActionResult ActiveteCategory(Guid id)
        {
            return Ok(Mediator.Send(new ActivateCategoryCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        [Authorize]
        public IActionResult DeactivateCategory(Guid id)
        {
            return Ok(Mediator.Send(new DeactivateCategoryCommand() { Id = id }));
        }

    }
}
