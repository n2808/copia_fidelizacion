﻿using Application.Gestions.Commands;
using Application.Gestions.Queries.GetByIdActiveGestion;
using Application.Gestions.Queries.GetGestionReport;
using Application.Gestions.Queries.GetGestionReportTification;
using Application.Gestions.Queries.GetInboundInitialGestion;
using Application.Gestions.Queries.GetInitialGestion;
using Application.Gestions.Queries.GetRecoveryGestionQuery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GestionController : ApiControllerBase
    {


        [HttpPost]
        [Route("InitialGestion")]
        public async Task<IActionResult>  PostInitialGestion([FromBody] GetInitialGestionQuery query )
        {
            return Ok(await Mediator.Send(query));
        }


        [HttpPost]
        [Route("InitialInboundGestion")]
        [Authorize]
        public async Task<IActionResult> PostInitialInboundGestion([FromBody] GetInboundInitialGestionQuery query )
        {
            return Ok(await Mediator.Send(query));
        }


        [HttpGet("Recovery/{id}")]
        [Authorize]
        public async Task<IActionResult> PostRecoveryGestion(int id )
        {
            return Ok(await Mediator.Send(new GetRecoveryGestionQuery { Id = id }));
        }


        [HttpPost]
        [Route("EndGestion")]
        //[EnableCors(PolicyName = "MyAllowSpecificOrigins")]
        public async Task<IActionResult> PostEndGestion([FromBody] EndGestionCommand command )
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("managements/{id}")]
        public IActionResult GetActiveGestion(int id)
        {
            return Ok(Mediator.Send(new GetByIdActiveGestionQuery() { Id = id }));
        }

        [HttpGet("managements/segment/{id}")]
        public IActionResult GetGestionReport([FromQuery] GetGestionReportQuery query)
        {
            return Ok( Mediator.Send(query));
        }

        [HttpGet]
        [Route("managements/query")]
        [Authorize]
        public IActionResult Get([FromQuery] GetGestionReportTificationQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpGet("managements/by-phone/{phone}")]
        [Authorize]
        public async Task<IActionResult> GetGestionReportByPhone(string phone)
        {
            return Ok(await Mediator.Send(new GetGestionReportByPhoneQuery() { Phone = phone }));
        }

        [HttpGet("managements/by-account/{account}")]
        [Authorize]
        public async Task<IActionResult> GetGestionReportByAccount(string account)
        {
            return Ok(await Mediator.Send(new GetGestionReportByAccountQuery() { Account = account }));
        }

        [HttpGet("managements/by-identification/{identification}")]
        [Authorize]
        public async Task<IActionResult> GetGestionReportByIdentification(string identification)
        {
            return Ok(await Mediator.Send(new GetGestionReportByIdentificationQuery() { Identification = identification }));
        }
    }
}
