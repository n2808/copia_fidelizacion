﻿using Application.Configurations.Queries.GetAllConfigurations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationsController : ApiControllerBase
    {
    
        [HttpGet]
        public IActionResult Get([FromQuery] GetAllConfigurationQuery query)
        {
            return Ok(Mediator.Send(query));
        }


    }
}
