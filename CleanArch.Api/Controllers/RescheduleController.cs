﻿using Application.Reschedule.Comands.AssignAgentCommand;
using Application.Reschedule.Comands.EndRescheduleCommand;
using Application.Reschedule.Queries.GetAvailableRescheduleQuery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/reschedule")]
    [Authorize]
    [ApiController]
    public class RescheduleController : ApiControllerBase
    {


  

        [HttpGet()]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery] GetGestionsByAgentQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


        [HttpGet("availables")]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery]  GetAvailableRescheduleQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


        [HttpPost("assign")]
        public async Task<IActionResult> Post([FromBody] AssignAgentCommand command)
        {
            return Ok(await Mediator.Send(command));

        }

        [HttpPost()]
        public async Task<IActionResult> Post([FromBody] EndRescheduleCommand command)
        {
            return Ok(await Mediator.Send(command));

        }


    }
}
