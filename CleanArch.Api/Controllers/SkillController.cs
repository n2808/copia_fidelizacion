﻿using Application.Skills.Command.Delete;
using Application.Skills.Command.Post;
using Application.Skills.Command.Put;
using Application.Skills.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/skills")]
    [ApiController]
    [Authorize]
    public class SkillController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetSkills([FromQuery] GetSkillsQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSkillById(int id)
        {
            return Ok(await Mediator.Send(new GetSkillQueryById() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public async Task<IActionResult> StatusActivateSkillById(int id)
        {
            return Ok(await Mediator.Send(new PutSkillsActivateCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public async Task<IActionResult> StatusDeActivateSkillById(int id)
        {
            return Ok(await Mediator.Send(new PutSkillsDeActivateCommand() { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> PostSkill([FromBody] PostSkillCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutSkill(int id, PutSkillsCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSkill(int id)
        {
            return Ok(await Mediator.Send(new DeleteSkillCommand() { Id = id }));

        }
    }
}
