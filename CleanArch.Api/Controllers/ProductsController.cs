﻿using Application.Products.Command;
using Application.Products.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/products")]
    [ApiController]
    [Authorize]
    public class ProductsController : ApiControllerBase
    {
        [HttpGet]
        public IActionResult Get([FromQuery] GetProductQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpPost]
        public IActionResult Post([FromBody] PostProductCommand command)
        {
            return Ok(Mediator.Send(command));
        }


        [HttpPut("{id}")]
        public IActionResult Put(Guid id, PutProductCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            return Ok(Mediator.Send(command));

        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            return Ok(Mediator.Send(new DeleteProductCommand() { Id = id }));
        }

        [HttpGet("{id}/activate")]
        public IActionResult ActivetePlan(Guid id)
        {
            return Ok(Mediator.Send(new ActivateProductCommand() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public IActionResult DeactivatePlan(Guid id)
        {
            return Ok(Mediator.Send(new DeactivateProductCommand() { Id = id }));
        }
    }
}
