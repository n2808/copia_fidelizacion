﻿using Application.SubCategoryTipification.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/sub-category-tipification")]
    [ApiController]
    //[Authorize]
    public class SubCategoryTipificationController : ApiControllerBase
    {
        [HttpGet("{code}")]
        public async Task<IActionResult> GetSubCategory(int code)
        {
            return Ok(await Mediator.Send(new GetSubCategoryTipification() { Code = code }));
        }
    }
}
