﻿using Application.BulkExcel.command;
using Application.User.Commands.DeleteUser;
using Application.User.Commands.PostUser;
using Application.User.Commands.PutUser;
using Application.User.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ApiControllerBase
    {
        [HttpGet]
        public IActionResult GetUser([FromQuery] GetUserQuery query)
        {
            return Ok(Mediator.Send(query));
        }

        [HttpPost]      
        public IActionResult PostUser([FromBody] PostUserCommand command)
        {
            return Ok(Mediator.Send(command));
        }

        [HttpPost("move")]
        public async Task<IActionResult> PostMoveUser([FromBody] PostMoveUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }


        [HttpGet("{id}/activate")]
        public IActionResult StatusActivateUserById(Guid id)
        {
            return Ok(Mediator.Send(new PutStatusActivateUserById() { Id = id }));
        }

        [HttpGet("{id}/deactivate")]
        public IActionResult StatusDeactivateUserById(Guid id)
        {
            return Ok(Mediator.Send(new PutStatusDeactivateUserById() { Id = id }));        
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(Guid id, PutUserCommand command)
        {
            if(id != command.Id)
            {
                return BadRequest();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(Guid id)
        {
            return Ok(Mediator.Send(new DeleteUserCommand() { Id = id }));
        }

        [HttpPost("{subCampaignId}/excel")]
        public IActionResult UploadExcel(IFormFile fileOne, int subCampaignId)
        {
            return Ok(Mediator.Send(new BulkExcelCommand() { File = fileOne, SubCampaignId = subCampaignId }));
        }


    }

}
