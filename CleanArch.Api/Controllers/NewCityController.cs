﻿using Application.Location.Queries.NewCities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaroFidelizacion.Api.Controllers
{
    [Route("api/new-cities")]
    [ApiController]
    //[Authorize]
    public class NewCityController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetSkills([FromQuery] GetNewCitiesQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSkillById(int id)
        {
            return Ok(await Mediator.Send(new GetNewCitiesByIdQuery() { Id = id }));
        }
    }
}
