﻿using Domain.Models.Scheduling;
using System.Linq;

namespace Domain.Interfaces.Scheduling
{
    public interface IScheduledAppointmentRepository
    {
        IQueryable<ScheduledAppointment> Get();      
        ScheduledAppointment Post(ScheduledAppointment scheduledAppointment);
        ScheduledAppointment Put(ScheduledAppointment scheduledAppointment);
        bool Delete(ScheduledAppointment entity);
    }
}
