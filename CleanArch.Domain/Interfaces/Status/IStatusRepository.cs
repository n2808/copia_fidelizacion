﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Interfaces.Status
{
    public interface IStatusRepository
    {
        IQueryable<Domain.Models.Status.Status> Get();
        Task<Domain.Models.Status.Status> GetById(Guid id);
        Task<Domain.Models.Status.Status> Post(Domain.Models.Status.Status status);
        Task<Domain.Models.Status.Status> Put(Domain.Models.Status.Status status);
        Task<bool> Delete(Domain.Models.Status.Status status);
    }
}
