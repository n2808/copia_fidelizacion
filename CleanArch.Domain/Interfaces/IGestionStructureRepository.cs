﻿using Core.Models.Operation;
using System.Linq;

namespace CleanArch.Domain.Interfaces
{
    public interface IGestionStructureRepository
    {
        IQueryable<GestionStructure> GetBySegmentId(int Id);
    }
}
