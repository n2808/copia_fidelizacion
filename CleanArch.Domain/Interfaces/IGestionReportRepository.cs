﻿using Core.Models.Operation;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IGestionReportRepository
    {
        Task<GestionReports> PostGestion(GestionReports gestion);
        Task<GestionReports> Put(GestionReports entity);
        IQueryable<GestionReports> GetGestionReports();
        bool PutEvidencesGestionReports(GestionReports gestionReports);
        GestionReports GetGestionReportsById(int Id);
        IQueryable<GestionReports> GetGestionReportsSegmentId(int segmentId, string field, string value);
        IQueryable<GestionReports> Get();

    }
}
