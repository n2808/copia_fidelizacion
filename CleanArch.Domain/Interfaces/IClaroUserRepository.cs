﻿using Domain.Models.ClaroUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IClaroUserRepository
    {
        IQueryable<ClaroUser> GetUsersCodeAdviser();
        Task<ClaroUser> Post(ClaroUser claroUser);
        Task<ClaroUser> Put(ClaroUser claroUser);
    }
}
