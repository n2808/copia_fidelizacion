﻿using Domain.Models.location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Interfaces.location
{
    public interface IDepartmentRepository
    {
        IQueryable<Department> Get();
    }
}
