﻿using Core.Models.location;
using System.Linq;

namespace Domain.Interfaces.location
{
    public interface IStateRepository
    {
        IQueryable<State> Get();
    }
}
