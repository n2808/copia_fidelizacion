﻿using CleanArch.Domain.Models;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IDatabaseClientReportsRepository
    {
        Task<DatabaseClientReports> PostClient(DatabaseClientReports gestion);
        Task<DatabaseClientReports> PutClient(DatabaseClientReports Client);
        IQueryable<DatabaseClientReports> GetClient();
    }
}
