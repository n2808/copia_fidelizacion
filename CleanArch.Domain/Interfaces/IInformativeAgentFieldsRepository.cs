﻿using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Domain.Interfaces
{
    public interface IInformativeAgentFieldsRepository
    {
        IQueryable<InformativeAgentFields> GetBySegmentId(int Id);
       
    }
}
