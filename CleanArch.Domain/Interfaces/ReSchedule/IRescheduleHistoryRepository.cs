﻿using Domain.Models.ReSchedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.ReSchedule
{
    public interface IRescheduleHistoryRepository
    {

        /// <summary>
        /// Get IQueryable of RescheduleHistory
        /// </summary>
        /// <returns></returns>
        IQueryable<RescheduleHistory> Get();


        /// <summary>
        /// Add a RescheduleHistory
        /// </summary>
        /// <returns></returns>

        Task<RescheduleHistory> Post(RescheduleHistory entity);

        /// <summary>
        /// Add a range of RescheduleHistorys
        /// </summary>
        /// <returns></returns>

        Task<List<RescheduleHistory>> PostRange(List<RescheduleHistory> entity);
        /// <summary>
        /// delete of RescheduleHistorys
        /// </summary>
        /// <returns></returns>

        Task Delete(RescheduleHistory entity);
        /// <summary>
        /// delete a range of RescheduleHistory
        /// </summary>
        /// <returns></returns>

        Task DeleteRange(List<RescheduleHistory> entity);
        /// <summary>
        /// update a RescheduleHistory
        /// </summary>
        /// <returns></returns>

        Task<RescheduleHistory> Put(RescheduleHistory entity);
    }
}
