﻿using Domain.Models.ReSchedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.ReSchedule
{
    public interface IGestionRescheduleRepository
    {

        /// <summary>
        /// Get IQueryable of GestionReschedule
        /// </summary>
        /// <returns></returns>
        IQueryable<GestionReschedule> Get();


        /// <summary>
        /// Add a GestionReschedule
        /// </summary>
        /// <returns></returns>

        Task<GestionReschedule> Post(GestionReschedule entity);

        /// <summary>
        /// Add a range of GestionReschedules
        /// </summary>
        /// <returns></returns>

        Task<List<GestionReschedule>> PostRange(List<GestionReschedule> entity);
        /// <summary>
        /// delete of GestionReschedules
        /// </summary>
        /// <returns></returns>

        Task Delete(GestionReschedule entity);
        /// <summary>
        /// delete a range of GestionReschedule
        /// </summary>
        /// <returns></returns>

        Task DeleteRange(List<GestionReschedule> entity);
        /// <summary>
        /// update a GestionReschedule
        /// </summary>
        /// <returns></returns>

        Task<GestionReschedule> Put(GestionReschedule entity);
    }
}
