﻿using Domain.Models.ReSchedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.ReSchedule
{
    public interface ITipificationRescheduleRepository
    {

        /// <summary>
        /// Get IQueryable of TipificationReschedule
        /// </summary>
        /// <returns></returns>
        IQueryable<TipificationReschedule> Get();


        /// <summary>
        /// Add a TipificationReschedule
        /// </summary>
        /// <returns></returns>

        Task<TipificationReschedule> Post(TipificationReschedule entity);

        /// <summary>
        /// Add a range of TipificationReschedules
        /// </summary>
        /// <returns></returns>

        Task<List<TipificationReschedule>> PostRange(List<TipificationReschedule> entity);
        /// <summary>
        /// delete of TipificationReschedules
        /// </summary>
        /// <returns></returns>

        Task Delete(TipificationReschedule entity);
        /// <summary>
        /// delete a range of TipificationReschedule
        /// </summary>
        /// <returns></returns>

        Task DeleteRange(List<TipificationReschedule> entity);
        /// <summary>
        /// update a TipificationReschedule
        /// </summary>
        /// <returns></returns>

        Task<TipificationReschedule> Put(TipificationReschedule entity);
    }
}
