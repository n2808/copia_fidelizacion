﻿using Domain.Models.Operation;
using System.Linq;

namespace CleanArch.Domain.Interfaces
{
    public interface IServiceDialerRepository
    {
        IQueryable<ServicesDialer> Get();
    }
}
