﻿using Domain.Models.Apps;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using System.Linq;

namespace CleanArch.Domain.Interfaces
{
    public interface ISegmentAppNotificationRepository
    {
        IQueryable<SegmentAppNotification> GetApps();
    }
}
