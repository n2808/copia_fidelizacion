﻿using Domain.Models.Plans;
using System;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Plan> PlanRepository { get; }
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
