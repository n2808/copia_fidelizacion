﻿using CleanArch.Domain.Models;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IGestionRepository
    {
        IEnumerable<InformativeAgentFields> GetClientdata();

        bool ValidateDialerServiceCode(string Code);


    }
}
