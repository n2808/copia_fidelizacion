﻿using Core.Models.Operation;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Interfaces.Campaigns
{
    public interface ISegmentRepository
    {
        IQueryable<Segment> Get();
        Task<Segment> Post(Segment segment);
        Task<Segment> Put(Segment segment);
        Task<Segment> PutStatusActivateSegmentById(Segment segment);
        Task<Segment> PutStatusDeActivateSegmentById(Segment segment);
        Task<bool> Delete(Segment segment);
        Task<Segment> GetSegmentById(Segment segment);
        Task<Segment> GetSegmentCampaingById(Segment segment);
    }
}
