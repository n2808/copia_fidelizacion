﻿using Core.Models.Operation;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Interfaces.Campaigns
{
    public interface ISubCampaignRepository
    {
        IQueryable<SubCampaign> Get();
        Task<string> GetById(int? id);

    }
}
