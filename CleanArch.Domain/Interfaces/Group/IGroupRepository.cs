﻿using System.Linq;
using Domain.Models.Groups;

namespace Domain.Interfaces.Group
{
    public interface IGroupRepository
    {
        IQueryable<Domain.Models.Groups.Group> Get();
        Domain.Models.Groups.Group Post(Domain.Models.Groups.Group group);
        Domain.Models.Groups.Group Put(Domain.Models.Groups.Group group);
        bool Delete(Domain.Models.Groups.Group group);
        bool Activate(Domain.Models.Groups.Group group);
        bool Deactivate(Domain.Models.Groups.Group group);
    }
}
