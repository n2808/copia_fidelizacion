﻿using Domain.Models.Backoffice;
using System.Linq;

namespace Domain.Interfaces.Backoffice
{
    public interface IBackofficeGestionRepository
    {
        IQueryable<BackofficeGestion> Get();
        BackofficeGestion Post(BackofficeGestion backofficeGestion);
        BackofficeGestion Put(BackofficeGestion backofficeGestion);
        bool Delete(BackofficeGestion entity);
    }
}
