﻿using Domain.Models.Backoffice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Backoffice
{
    public interface IBackOfficeOperationsRepository
    {
        IQueryable<BackOfficeOperations> Get();
        Task<BackOfficeOperations> Post(BackOfficeOperations backOfficeOperations);
        Task<BackOfficeOperations> Put(BackOfficeOperations backOfficeOperations);
        Task<bool> Delete(BackOfficeOperations backOfficeOperations);
        Task<BackOfficeOperations> Activate(BackOfficeOperations backOfficeOperations);
        Task<BackOfficeOperations> Deactivate(BackOfficeOperations backOfficeOperations);
    }
}
