﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.SecondRingLoad
{
    public interface ISecondRingLoadRepository
    {
        Task<Models.SecondRingLoads.SecondRingLoad> Post(Models.SecondRingLoads.SecondRingLoad secondRingLoad);
        IQueryable<Models.SecondRingLoads.SecondRingLoad> Get();
        Task<bool> Put(Domain.Models.SecondRingLoads.SecondRingLoad secondRingLoad);
    }
}
