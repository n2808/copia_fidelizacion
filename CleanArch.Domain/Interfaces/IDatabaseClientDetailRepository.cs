﻿using CleanArch.Domain.Models;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Interfaces
{
    public interface IDatabaseClientDetailRepository
    {
        IQueryable<DatabaseClientDetail> GetClient(int? Id);
        IQueryable<DatabaseClientDetail> GetClient();

    }
}
