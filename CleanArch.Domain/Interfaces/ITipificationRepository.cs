﻿using Domain.Models.Operation;
using Domain.Models.Tipifications;
using System.Linq;

namespace CleanArch.Domain.Interfaces
{
    public interface ITipificationRepository
    {
        IQueryable<Tipification> GetTipifications();
        IQueryable<Tipification> GetTipificationsByName();
    }
}
