﻿using System.Linq;
using Domain.Models.Surveys;

namespace Domain.Interfaces.Surveys
{
    public interface ISurveyRepository
    {
        IQueryable<Survey> Get();
    }
}
