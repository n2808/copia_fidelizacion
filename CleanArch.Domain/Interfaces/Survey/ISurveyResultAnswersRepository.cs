﻿using Domain.Models.SurveysResults;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Interfaces.Surveys
{
    public interface ISurveyResultAnswersRepository
    {
        IQueryable<SurveyResultAnswers> Get();
        List<SurveyResultAnswers> PostRange(List<SurveyResultAnswers> surveyResultAnswers);
    }
}
