﻿using Domain.Models.Operation;
using Domain.Models.SurveysResults;
using Domain.Models.Tipifications;
using System.Linq;

namespace Domain.Interfaces.Surveys
{
    public interface ISurveyResultGestionRepository
    {
        IQueryable<SurveyResultGestion> Get();
        SurveyResultGestion Post(SurveyResultGestion surveyResultGestion);
    }
}
