﻿using Domain.Models.Operation;
using Domain.Models.SurveysResults;
using Domain.Models.Tipifications;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Interfaces.Surveys
{
    public interface ISurveyResultQuestionRepository
    {
        IQueryable<SurveyResultQuestion> Get();
        List<SurveyResultQuestion> PostRange(List<SurveyResultQuestion> surveyResultQuestions);
    }
}
