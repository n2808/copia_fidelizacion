﻿using Domain.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Domain.Interfaces.Notifications
{
    public interface INotificationLogRepository
    {
        IQueryable<NotificationLog> Get();

        NotificationLog Post(NotificationLog notificationLog);
    }
}
