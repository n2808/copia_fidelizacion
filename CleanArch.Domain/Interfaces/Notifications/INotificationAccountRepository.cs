﻿using Domain.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Domain.Interfaces.Notifications
{
    public interface INotificationAccountRepository
    {
        IQueryable<NotificationAccount> Get();
    }
}
