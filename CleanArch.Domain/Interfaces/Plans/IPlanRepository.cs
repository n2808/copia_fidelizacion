﻿using Domain.Models.Plans;
using System.Linq;

namespace Domain.Interfaces.Plans
{
    public interface IPlanRepository
    {
        IQueryable<Plan> Get();
        Plan Post(Plan plan);
        Plan Put(Plan plan);
        bool Delete(Plan plan);
        bool Activate(Plan plan);
        bool Deactivate(Plan plan);
    }
}