﻿using Domain.Models.Operators;
using System;
using System.Linq;

namespace Domain.Interfaces.Operators
{
    public interface IOperatorRepository
    {
        IQueryable<Operator> Get();
        Operator GetById(Guid id);
        Operator Post(Operator _operator);
        Operator Put(Operator _operator);
        bool Delete(Operator _operator);
        bool Activate(Operator _operator);
        bool Deactivate(Operator _operator);
    }
}
