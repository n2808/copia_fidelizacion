﻿using CleanArch.Domain.Models;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Domain.Interfaces
{
    public interface IActiveGestionRepository
    {
        IQueryable<ActiveGestion> GetActiveGestion();
        Task<ActiveGestion> PostGestion(ActiveGestion activeGestion);

        Task<ActiveGestion>  PutGestion(ActiveGestion activeGestion);
        IQueryable<ActiveGestion> GetById();
    }
}
