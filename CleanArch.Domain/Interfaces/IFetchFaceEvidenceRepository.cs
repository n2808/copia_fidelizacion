﻿using Domain.Models.FetchFaceEvidences;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IFetchFaceEvidenceRepository
    {
        IQueryable<FetchFaceEvidence> GetFetchFaceEvidence();
        Task<FetchFaceEvidence> PostFetchFaceEvidence(FetchFaceEvidence fetchFaceEvidence);
        Task<FetchFaceEvidence> PutFetchFaceEvidence(FetchFaceEvidence fetchFaceEvidence);
        IQueryable<FetchFaceEvidence> GetById();
    }
}
