﻿using Domain.Models.Operation;
using Domain.Models.Surveys;
using Domain.Models.Tipifications;
using System.Linq;

namespace CleanArch.Domain.Interfaces
{
    public interface ISurveySegmentRepository
    {
        IQueryable<SurveySegment> Get();
    }
}
