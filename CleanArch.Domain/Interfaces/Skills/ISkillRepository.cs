﻿using Domain.Models.Skill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Skill
{
    public interface ISkillRepository
    {
        IQueryable<Skills> Get();
        Task<Skills> GetSkillById(Skills skills);
        Task<Skills> Post(Skills skills);
        Task<Skills> Put(Skills skills);
        Task<bool> Delete(Skills skills);
        Task<Skills> Activate(Skills skills);
        Task<Skills> Deactivate(Skills skills);
    }
}
