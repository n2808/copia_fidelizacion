﻿using Domain.Models.Quotes;
using System;
using System.Linq;

namespace Domain.Interfaces.Quotes
{
    public interface IQuoteRepository
    {
        IQueryable<Quote> Get();
        Quote GetById(Guid id);
        Quote Post(Quote quote);
        Quote Put(Quote quote);
        bool Delete(Quote quote);
        bool Activate(Quote quote);
        bool Deactivate(Quote quote);

    }
}
