﻿using Domain.Models.Tipification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Interfaces
{
    public interface ISubCategoryTipification
    {
        IQueryable<SubCategoryTipification> GetByType();
    }
}
