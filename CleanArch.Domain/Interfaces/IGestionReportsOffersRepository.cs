﻿using Domain.Models.Offer;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArch.Domain.Interfaces
{
    public interface IGestionReportsOffersRepository
    {
        IQueryable<GestionReportsOffers> GetOffers();
        Task PostOffers(List<GestionReportsOffers> offers);
        Task DeleteOffers(List<GestionReportsOffers> offers);
    }
}
