﻿using Domain.Models.ManagementsBackoffice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IBackOfficeManagementsRepository
    {
        Task<BackOfficeManagements> PostManagementBackOffice(BackOfficeManagements managementsBackOffice);
        Task<BackOfficeManagements> PutManagementBackOffice(BackOfficeManagements managementsBackOffice);
        IQueryable<BackOfficeManagements> Get();
        IQueryable<BackOfficeManagementsHistories> GetAll();
        Task<BackOfficeManagementsHistories> PostManagementBackOfficeHistory(BackOfficeManagementsHistories managementsBackOfficeHistory);
    }
}
