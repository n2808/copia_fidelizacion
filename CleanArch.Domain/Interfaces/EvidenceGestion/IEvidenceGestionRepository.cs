﻿using System.Linq;
using System.Threading.Tasks;

namespace Domain.Interfaces.EvidenceGestion
{
    public interface IEvidenceGestionRepository
    {

        IQueryable<Domain.Models.Gestion.EvidenceGestion> GetEvidenceGestion();
        Task<Domain.Models.Gestion.EvidenceGestion> PostEvidenceGestion(Domain.Models.Gestion.EvidenceGestion evidenceGestion);
        bool PutEvidenceGestion(Domain.Models.Gestion.EvidenceGestion evidenceGestion);
        IQueryable<Domain.Models.Gestion.EvidenceGestion> Get();

    }
}
