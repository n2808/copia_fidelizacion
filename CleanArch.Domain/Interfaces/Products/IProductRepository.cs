﻿using Domain.Models.Products;
using System;
using System.Linq;

namespace Domain.Interfaces.Products
{
    public interface IProductRepository
    {
        IQueryable<Product> Get();
        Product GetById(Guid id);
        Product Post(Product product);
        Product Put(Product product);
        bool Delete(Product product);
        bool Activate(Product product);
        bool Deactivate(Product product);
    }
}
