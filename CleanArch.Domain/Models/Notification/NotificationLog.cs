﻿using Core.Models.Common;
using Domain.Models.Operation;
using Domain.Models.User;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Notification
{
    public class NotificationLog : EntityWithIntId
    {
        public string Name { get; set; }
        public string phone { get; set; }
        public string Sendby { get; set; }
        public string Message { get; set; }
        public string flash { get; set; }
        public string customdata1 { get; set; }
        public string customdata2 { get; set; }
        public string AccountName { get; set; }
        public bool status { get; set; }
        public Guid? UserId { get; set; }
        public int CampaignId { get; set; }
        public int SubCampaignId { get; set; }
        public int SegmentId { get; set; }

        [ForeignKey("CampaignId")]
        public Campaign Campaign { get; set; }


        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }

        [ForeignKey("SubCampaignId")]
        public SubCampaign SubCampaign { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }



    }
}
