﻿using Core.Models.Common;
using Core.Models.configuration;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Tipifications
{
    public class Tipification : EntityWithIntId
    {
        public string Name { get; set; }
        public string NameLinked { get; set; }
        public int SubCampaignId { get; set; }
        public string SegmentName { get; set; }
        public string Dialer1Code { get; set; }
        public string Dialer2Code { get; set; }
        public string Report1Code { get; set; }
        public string Report2Code { get; set; }
        public string Report3Code { get; set; }

        public string IsContacted { get; set; }
        public string IsEfective { get; set; }
        public bool? AcceptOffer { get; set; } = false;
        public Int16 GestionNextStatus { get; set; }
        public bool status { get; set; }
        
        public string priority { get; set; } 
        public string Group1 { get; set; }
        public string Group2 { get; set; }
        public string Group3 { get; set; }
        public string Group4 { get; set; }
        
        

        public Guid? TypeTipificationId { get; set;  }

        [ForeignKey("TypeTipificationId")]
        public Configuration TypeTipification { get; set; }

        [ForeignKey("SubCampaignId")]
        public SubCampaign Subcampaign { get; set; }

        public List<ServicesDialerTipification> servicesDialerTipifications { get; set; }

        public bool IsRetained { get; set; }

    }
}
