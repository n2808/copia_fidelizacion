﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Tipifications
{
    public class ServicesDialerTipification : Entity
    {
        public string ServiceDialerName { get; set; }
        public int ServiceDialerId { get; set; }
        public int TipicationId { get; set; }

        [ForeignKey("TipicationId")]
        public Tipification Tipification { get; set; }

        [ForeignKey("ServiceDialerId")]
        public ServicesDialer servicesDialer { get; set; }

    }
}
