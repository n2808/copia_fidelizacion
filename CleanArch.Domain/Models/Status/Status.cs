﻿using Core.Models.Common;

namespace Domain.Models.Status
{
    public class Status : Entity
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; } = true;
        public bool IsSale { get; set; }
        public int SubcampaingId { get; set; }
    }
}
