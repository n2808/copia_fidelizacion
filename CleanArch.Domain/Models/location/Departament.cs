﻿using Core.Models.Common;
using Core.Models.location;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.location
{
    public class Department : EntityWithIntId
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; } = true;
        public Guid CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
        public List<City> Cities { get; set; }

    }
}
