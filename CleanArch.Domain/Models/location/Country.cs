﻿using Core.Models.Common;
using Domain.Models.location;
using System.Collections.Generic;

namespace Core.Models.location
{
    public class Country : Entity
	{
		public string Name { get; set; }
		public string DisplayName { get; set; }
		public bool Status { get; set; } = true;
        public string Code { get; set; }
        public List<Department> Departments { get; set; }
	}
}