﻿using Core.Models.Common;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.location
{
    public class NewCity : EntityWithIntId
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; } = true;
        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

    }
}
