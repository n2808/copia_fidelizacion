﻿using Core.Models.Common;
using Domain.Models.location;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.location
{
    public class City : EntityWithIntId
	{
		public string Name { get; set; }
		public string Code { get; set; }
        public bool Active { get; set; } = true;
		public int DepartmentId { get; set; }
		[ForeignKey("DepartmentId")]
		public Department Department { get; set; }

	}
}