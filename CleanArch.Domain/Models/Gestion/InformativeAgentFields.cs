﻿using Core.Models.Common;
using Core.Models.configuration;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Operation
{
	public class InformativeAgentFields : Entity
	{
        public string DisplayName { get; set; }
        public string DBFieldName { get; set; }
        public int Order { get; set; }
        public Guid InformationGroupId { get; set;  }


        public int SegmentId { get; set; }
        
        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }


        [ForeignKey("InformationGroupId")]
        public configuration.Configuration InformationGroup { get; set; }
    }
}