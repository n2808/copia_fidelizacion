﻿using Core.Models.Common;
using Core.Models.configuration;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Operation
{
	public class GestionStructure : EntityWithIntId
	{
        public string DatbaseField { get; set; }
        public string Name { get; set; }
        public string FrontendField { get; set; }
        public string ReportField { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public int? Order { get; set; } = 1;
        public int? Code { get; set; } = 1;

        public bool Status { get; set; }
        public int SegmentId { get; set; }

        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }
    }
}