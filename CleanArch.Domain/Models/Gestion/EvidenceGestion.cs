﻿using Core.Models.Common;
using Core.Models.Operation;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Gestion
{
    public class EvidenceGestion : Entity
    {
        public int GestionReportsId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

        [ForeignKey("GestionReportsId")]
        public GestionReports GestionReports { get; set; }
    }
}
