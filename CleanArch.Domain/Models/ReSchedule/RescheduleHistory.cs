﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.ReSchedule
{
    public class RescheduleHistory : EntityWithIntId
    {
        public Guid GestionRescheduleId { get; set; }
        public Guid TipificationId { get; set; }
        public string  TipificationName { get; set; }
        public string Observation { get; set; }


        [ForeignKey("GestionRescheduleId")]
        public GestionReschedule GestionReschedule { get; set; }

        [ForeignKey("TipificationId")]
        public Configuration Configuration { get; set; }




    }
}
