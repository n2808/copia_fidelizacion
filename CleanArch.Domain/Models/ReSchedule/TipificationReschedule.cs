﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.ReSchedule
{
    public class TipificationReschedule : Entity
    {
        public string TipificationCode { get; set; }
        public int? CampaignId { get; set; }
        public int? SubCampaignId { get; set; }
        public int? SegmentId { get; set; }
        public bool Hascall { get; set; } = false;
        public string Vdn { get; set; }
        public string load { get; set; }
        public string CustomData1 { get; set; }
        public string CustomData2 { get; set; }
        public string CustomData3 { get; set; }
        public string CustomData4 { get; set; }

        public bool status { get; set; }




        [ForeignKey("CampaignId")]
        public Campaign Campaign { get; set; }

        [ForeignKey("SubCampaignId")]
        public SubCampaign SubCampaign { get; set; }

        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }


    }
}
