﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.ReSchedule
{
    public class GestionReschedule : Entity
    {
        public int TipificationId { get; set; }
        public Guid? RecoveryTipification { get; set; }
        public string TipificationCode { get; set; }
        public int SegmentId { get; set; }
        public int GestionId { get; set; }
        public Guid? AgentId { get; set; }
        public int Count { get; set; } = 1;
        public bool status { get; set; } = true;

        [ForeignKey("TipificationId")]
        public Domain.Models.Tipifications.Tipification Tipification { get; set; }

        [ForeignKey("GestionId")]
        public GestionReports GestionReports { get; set; }

        [ForeignKey("SegmentId")]
        public Segment segment { get; set; }

        [ForeignKey("AgentId")]
        public User.User Agent { get; set; }


    }
}
