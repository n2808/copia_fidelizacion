﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Backoffice
{
    public class BackofficeGestion : Entity
    {
        public DateTime? ChangeDate { get; set; }
        public string BeforeTMCode { get; set; }
        public string BeforeValue { get; set; }
        public string BeforeName { get; set; }
        public string NewValue { get; set; }
        public string NewValueTax { get; set; }
        public string NewTMCode { get; set; }
        public string NewName { get; set; }
        public string Account { get; set; }
        public string Observation { get; set; }    
        public Guid? TypePlanId { get; set; }

        [ForeignKey("TypePlanId")]
        public Configuration TypePlan { get; set; }
    }
}
