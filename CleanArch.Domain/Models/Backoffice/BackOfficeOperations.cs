﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Backoffice
{
    public class BackOfficeOperations : EntityWithIntId
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string TypeOperation { get; set; }
        public string Operation { get; set; }
        public string Process { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
    }
}
