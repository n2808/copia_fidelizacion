﻿using Core.Models.Common;
using System;

namespace Domain.Models.SecondRingLoads
{
    public class SecondRingLoad : EntityWithIntId
    {
        public string account { get; set; }
        public string phone { get; set; }
        public string type { get; set; }
        public string assignedLogin { get; set; }
        public Guid assignedId { get; set; }
        public Guid loadId { get; set; }
        public string loadName { get; set; }
        public bool status { get; set; }
    }
}
