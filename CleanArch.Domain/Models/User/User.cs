﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;



namespace Domain.Models.User
{
	public class User : Entity
	{
		public string Document { get; set; }
        public string Names { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; } = true;
        public string PassWord { get; set; }
        public string BlogNotas { get; set; }
        public string login { get; set; }
        public bool Active { get; set; } = true;
        
        public List<UserRol> UserRol { get; set; }

        public Guid? CoordinadorId { get; set; }

        public User Coordinador { get; set; }

        public Guid? GerenteId { get; set; }

        public int? SubCampaignId { get; set; }

        [ForeignKey("SubCampaignId")]
        public SubCampaign SubCampaign { get; set; }

        

    }
}