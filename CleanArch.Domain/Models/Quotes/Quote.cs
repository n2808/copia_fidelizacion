﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using Domain.Models.Operators;
using Domain.Models.Plans;
using Domain.Models.Products;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Quotes
{
    public class Quote : Entity
    {
        public int? SubCampaignId { get; set; }
        public Guid? ProductId { get; set; }
        public Guid? OperatorId { get; set; }
        public Guid? PlanId { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; } = true;

        [ForeignKey("SubCampaignId")]
        public SubCampaign SubCampaign { get; set; }

        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        [ForeignKey("OperatorId")]
        public Operator Operator { get; set; }

        [ForeignKey("PlanId")]
        public Plan Plan { get; set; }
    }
}
