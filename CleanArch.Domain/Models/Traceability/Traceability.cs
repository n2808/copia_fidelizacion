﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Operation
{
	public class TraceabilityStatus : Entity
	{
        public Guid StatusId { get; set; }
        public Guid NewStatusId { get; set; }
        public int? SubCampaignId { get; set; }
        public int? SegmentId { get; set; }
    }
}