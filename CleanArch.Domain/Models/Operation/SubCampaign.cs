﻿using Core.Models.Common;
using Domain.Notification;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Operation
{
    public class SubCampaign : EntityWithIntId
	{
        public string Name { get; set; }

        public bool Status { get; set; }
        public bool IsInbound { get; set; } = false;
        public bool IsOutbound { get; set; } = true;
        public bool IsDigital { get; set; } = false;

        public string Description { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }

        public string Code { get; set; }
        public int? intId { get; set; }
        public Guid? GuidId { get; set; }
        public int order { get; set; }

        public int CampaignId { get; set; }


        [ForeignKey("CampaignId")]
        public Campaign Campaign { get; set; }


        public int? NotificationSMSAccountId { get; set; }

        [ForeignKey("NotificationSMSAccountId")]
        public NotificationAccount NotificationAccount { get; set; }


    }
}