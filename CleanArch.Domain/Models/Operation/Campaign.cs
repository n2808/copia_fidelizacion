﻿using Core.Models.Common;
using System;

namespace Domain.Models.Operation
{
    public class Campaign : EntityWithIntId
	{
        public string Name { get; set; }
        
        public bool Status { get; set; }
        
        public string Description { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }

        public string Code { get; set; }
        public int? intId { get; set; }
        public Guid? GuidId { get; set; }
        
        public int order { get; set; }

    }
}