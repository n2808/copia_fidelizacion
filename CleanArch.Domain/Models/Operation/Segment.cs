﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Operation
{
    public class Segment : EntityWithIntId
	{
        public string Name { get; set; }
        public bool Status { get; set; }
        public bool IsInbound { get; set; } = false;
        public bool IsOutbound { get; set; } = true;
        public bool IsDigital { get; set; } = false;
        public string Description { get; set; }
        public string ValidationScript { get; set; }
        public string LoadScript { get; set; }
        public string Scritp1 { get; set; }
        public string Scritp2 { get; set; }
        public string GreetingDialog { get; set; }
        public string FarewelDialog { get; set; }
        public int SubCampaignId { get; set; }
        [ForeignKey("SubCampaignId")]
        public SubCampaign SubCampaign { get; set; }
    }
}