﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Operation
{
    public class ServicesDialer : EntityWithIntId
    {
        public string VDN { get; set;  }
        public string DialerName { get; set;  }
        public string DisplayName { get; set;  }
        public string DisplayReportName { get; set;  }
        public string Description { get; set;  }
        public string CallTypeName { get; set;  }
        public string Name { get; set; }
        public int CampaignId { get; set; }
        public int? SubCampaignId { get; set; }
        public int? SegmentId { get; set; }
        
        public bool status { get; set; } = true;


        [ForeignKey("CampaignId")]
        public Campaign Campaign { get; set; }


        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }

        [ForeignKey("SubCampaignId")]
        public SubCampaign SubCampaign { get; set; }
    }
}
