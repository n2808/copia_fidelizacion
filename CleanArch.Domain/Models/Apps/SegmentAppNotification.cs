﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using Domain.Notification;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Apps
{
    public class SegmentAppNotification : EntityWithIntId
    {
        public int AppId { get; set; }
        public int SegmentId { get; set; }
        public int? NotificationAccountId { get; set; }
        public string Message { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public string CustomName { get; set; }
        public string CustomData1 { get; set; }
        public string CustomData2 { get; set; }

        [ForeignKey("AppId")]
        public AppNotification AppNotification { get; set; }

        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }

        [ForeignKey("NotificationAccountId")]
        public NotificationAccount NotificationAccount { get; set; }

    }
}
