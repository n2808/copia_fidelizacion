﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Apps
{
    public class AppNotification : EntityWithIntId
    {
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Description { get; set; }
        public string feature1 { get; set; }
        public string feature2 { get; set; }
        public string feature3 { get; set; }
        public bool status { get; set; }
    }
}
