﻿using Core.Models.Common;

namespace Domain.Models.Products
{
    public class Product : Entity
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string GeneralLegalText { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; } = true;
    }
}
