﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.ManagementsBackoffice
{
    public class BackOfficeManagements : Entity
    {
        public int? ManagementId { get; set; }
        [ForeignKey("ManagementId")]
        public GestionReports Management { get; set; }
        public int? TypificationId { get; set; }
        [ForeignKey("TypificationId")]
        public Domain.Models.Tipifications.Tipification Typification { get; set; }
        public string TypificationName { get; set; }
        public string TypeOperation { get; set; }
        public string Operation { get; set; }
        public string Observation { get; set; }
        public Guid? UserId { get; set; }
        public Guid? UserValidationId { get; set; }
        public int? TypificationBackValidationId { get; set; }
        public int? TypificationBackId { get; set; }
        [ForeignKey("TypificationBackId")]
        public Domain.Models.Tipifications.Tipification TypificationBack { get; set; }
        public string TypificationNameBack { get; set; }
        public string ObservationBack { get; set; }
        public string ObservationValidator { get; set; }
        public int SegmentId { get; set; }
        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }
        public DateTime? InitValidation { get; set; }
        public DateTime? EndValidation { get; set; }
        public string Process { get; set; }
        public bool Active { get; set; }
        public int Count { get; set; }
        public Guid? StatusId { get; set; }
        [ForeignKey("StatusId")]
        public Domain.Models.Status.Status Status { get; set; }
        public string StatusName { get; set; }
        public List<BackOfficeManagementsHistories> Histories { get; set; }

        public int? SecondRingLoadId { get; set; }
        [ForeignKey("SecondRingLoadId")]
        public Domain.Models.SecondRingLoads.SecondRingLoad SecondRingLoad { get; set; }
        public string? StateEnd { get; set; }

    }
}
