﻿using Core.Models.Common;
using Core.Models.Operation;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.FetchFaceEvidences
{
    public class FetchFaceEvidence : EntityWithIntId
    {
        public string Document { get; set; }
        public bool Status { get; set; }
        public int ActiveGestionId { get; set; }
        [ForeignKey("ActiveGestionId")]
        public ActiveGestion ActiveGestions { get; set; }
        public string URL { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
