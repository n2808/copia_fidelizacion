﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Models.ClaroUser
{
    public class ClaroUser : EntityWithIntId
    {
        public string Code { get; set; }
        public string Charge { get; set; }
        public string Location { get; set; }
        public string Region { get; set; }
        public string Ring { get; set; }
        public string Supervisor { get; set; }
        public string IdentificationSupervisor { get; set; }
        public string Coordinator { get; set; }
        public string IdentificationCoordinator { get; set; }
    }
}
