﻿using Core.Models.Common;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Skill
{
    public class Skills : EntityWithIntId
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public string Segment { get; set; }
        public int? SubCampaignId { get; set; }
        [ForeignKey("SubCampaignId")]
        public SubCampaign SubCampaign { get; set; }
    }
}
