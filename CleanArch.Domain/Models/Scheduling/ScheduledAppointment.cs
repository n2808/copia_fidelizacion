﻿using Core.Models.Common;
using Core.Models.Operation;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Scheduling
{
    public class ScheduledAppointment : Entity
    {
        public string WorkDay { get; set; }
        public string AppointmentDate { get; set; }
        public string Observation { get; set; }
        public int ActiveGestionId { get; set; }
        [ForeignKey("ActiveGestionId")]
        public ActiveGestion ActiveGestion { get; set; }
        public string AccountNumber { get; set; }
        public string CodeFailure { get; set; }
        public string EmailOfReceive { get; set; }
        public string NameOfReceive { get; set; }
        public string IncidenceTD { get; set; }
        public string PhoneOfReceive { get; set; }
        public bool ParentAccount { get; set; }
        public bool Active { get; set; }
        public string DateOne { get; set; }
        public string DateTwo { get; set; }
        public string DateThree { get; set; }
    }
}
