﻿using Core.Models.Common;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Scheduling
{
    public class ScheduledAppointmentReport : Entity
    {
        public string Ot { get; set; }
        public string WorkDay { get; set; }
        public DateTime AppointmentDate { get; set; }
        public string Observation { get; set; }
        public int GestionReportId { get; set; }

        [ForeignKey("GestionReportId")]
        public GestionReports GestionReports { get; set; }
    }
}
