﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.SurveysResults
{
    public class SurveyResultAnswers : Entity
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderId { get; set; } = 1;

        public Guid SurveyResultQuestionId { get; set; }


        [ForeignKey("SurveyResultQuestionId")]
        public SurveyResultQuestion SurveyResultQuestion { get; set; }

    }
}
