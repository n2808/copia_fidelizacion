﻿using Core.Models.Common;
using Core.Models.Operation;
using Domain.Models.Operation;
using Domain.Models.Surveys;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.SurveysResults
{
    public class SurveyResultGestion : Entity
    {
        public int SurveyId { get; set; }
        public int GestionId { get; set; }
        public string SurveyName { get; set; }
        public int SegmentId { get; set; }
        public int OrderId { get; set; } = 1;


        [ForeignKey("SurveyId")]
        public Survey Survey { get; set; }

        
        [ForeignKey("GestionId")]
        public GestionReports GestionReports { get; set; }


        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }
    }
}
