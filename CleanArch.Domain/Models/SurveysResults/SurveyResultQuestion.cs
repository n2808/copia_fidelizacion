﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.SurveysResults
{
    public class SurveyResultQuestion : Entity
    {

        public Guid SurveyResultGestionId { get; set; }
        
        
        [ForeignKey("SurveyResultGestionId")]
        public SurveyResultGestion SurveyResultGestion { get; set;  }
        public List<SurveyResultAnswers> SurveyResultAnswers { get; set;  }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? Type { get; set; }
        
        public int OrderId { get; set; } = 1;
        
    }
}
