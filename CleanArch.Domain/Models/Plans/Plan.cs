﻿using Core.Models.Common;

namespace Domain.Models.Plans
{
    public class Plan : Entity
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public double PriceWithTax { get; set; }
        public string Currency { get; set; }
        public string Tax { get; set; }
        public string MinutesIncluded { get; set; }
        public string MinutesIncludedLDI { get; set; }
        public string SMSIncluded { get; set; }
        public string SMSIncludedLDI { get; set; }
        public string APPSIncluided { get; set; }
        public string TMCode { get; set; }
        public string MessagePrice { get; set; }
        public string Description { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; } = true;
    }
}
