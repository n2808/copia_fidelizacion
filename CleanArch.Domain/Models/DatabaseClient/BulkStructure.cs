﻿using Core.Models.Common;
using Core.Models.configuration;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Operation
{
	public class BulkStructure : EntityWithIntId
	{
        public string ExcelField { get; set; }
        public string DatbaseField { get; set; }
        public int? Order { get; set; } = 1;
        public string Name { get; set; }

        public bool Status { get; set; }

        public string Description { get; set; }

        public int SegmentId { get; set; }

        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }
    }
}