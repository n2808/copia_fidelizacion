﻿using Core.Models.Common;
using Domain.Models.Operation;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.Operation
{
    public class DatabasesClientLoadOperation : EntityWithIntId
	{
        public string Name { get; set; }

        public bool Reported { get; set; } = false;
        public bool Status { get; set; }
        public string Description { get; set; }
        public int? RowsUploaded { get; set; }
        public string ReportYear { get; set; }
        public string ReportMonth { get; set; }

        public int DatabaseClientLoadId { get; set; }

        [ForeignKey("DatabaseClientLoadId")]
        public DatabasesClientLoad DatabaseClientLoad { get; set; }


        public int SegmentId { get; set; }

        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }


        public int SubcampaignId { get; set; }

        [ForeignKey("SubcampaignId")]
        public SubCampaign subCampaign { get; set; }

        public int CampaignId { get; set; }
        [ForeignKey("CampaignId")]
        public Campaign Campaign { get; set; }

    }
}