﻿using Core.Models.Common;
using Core.Models.configuration;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Core.Models.Operation
{
	public class DatabasesClientLoad : EntityWithIntId
	{
        public string Name { get; set; }
        public int SegmentId { get; set; }

        public bool Reported { get; set; } = false;
        public bool Status { get; set; }
        public string Description { get; set; }
        public int? RowsUploaded { get; set; }
        public string ReportYear { get; set; }
        public string ReportMonth { get; set; }
        public string RowsDuplicate { get; set; }
        public string RowsUnsuitable { get; set; }


        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }
    }
}