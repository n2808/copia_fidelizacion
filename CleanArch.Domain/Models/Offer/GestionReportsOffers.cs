﻿using Core.Models.Common;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Offer
{
    public class GestionReportsOffers : EntityWithIntId
    {
        public int GestionReportsId { get; set; }
        public int DatabaseClientReportsId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? value { get; set; }
        public bool? status { get; set; } = true;
        [ForeignKey("GestionReportsId")]
        public GestionReports GestionReports { get; set; }
        [ForeignKey("DatabaseClientReportsId")]
        public DatabaseClientReports DatabaseClientReports { get; set; }
    }
}
