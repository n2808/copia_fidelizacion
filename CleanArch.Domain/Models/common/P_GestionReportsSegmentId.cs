﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.common
{
    public class P_GestionReportsSegmentId
    {
        public string Phone { get; set; }
        public string Skill { get; set; }


        #region auditabledata
        public string Ip { get; set; }
        public string MacchineName { get; set; }
        public string TeamLeader { get; set; }
        public string Manager { get; set; }




        #endregion

        #region DialerInformation
        public int? tipificacionId { get; set; }

        public string DialerServiceId { get; set; }
        public string DialerServiceClientId { get; set; }

        public string DialerServiceTypeCall { get; set; }
        public string Dialercode { get; set; }
        #endregion

        #region ClientData
        public string ClientName { get; set; }
        public string CustomerName { get; set; }
        public string Customerdocument { get; set; }
        #endregion

        #region GestionData
        public string entrydocument { get; set; }
        public string entryName { get; set; }
        public string EntryContact1 { get; set; }
        public string EntryContact2 { get; set; }
        public string entryCity { get; set; }
        public string entrystate { get; set; }
        public string entryObservation { get; set; }
        public string entryObservation2 { get; set; }
        public string CurrentIncome { get; set; }
        public string NextIncome { get; set; }
        public string CurrentProduct { get; set; }
        public string NextProduct { get; set; }




        public string EntryData1 { get; set; }
        public string EntryData2 { get; set; }
        public string EntryData3 { get; set; }
        public string EntryData4 { get; set; }
        public string EntryData5 { get; set; }
        public string EntryData6 { get; set; }
        public string EntryData7 { get; set; }
        public string EntryData8 { get; set; }
        public string EntryData9 { get; set; }
        public string EntryData10 { get; set; }
        public string EntryData11 { get; set; }
        public string EntryData12 { get; set; }
        public string EntryData13 { get; set; }
        public string EntryData14 { get; set; }
        public string EntryData15 { get; set; }
        public string EntryData16 { get; set; }
        public string EntryData17 { get; set; }
        public string EntryData18 { get; set; }
        public string EntryData19 { get; set; }
        public string EntryData20 { get; set; }
        public string EntryData21 { get; set; }
        public string EntryData22 { get; set; }
        public string EntryData23 { get; set; }
        public string EntryData24 { get; set; }
        public string EntryData25 { get; set; }
        public string EntryData26 { get; set; }
        public string EntryData27 { get; set; }
        public string EntryData28 { get; set; }
        public string EntryData29 { get; set; }
        public string EntryData30 { get; set; }





        public DateTime? EndAt { get; set; }
        public string loadName { get; set; }

        public int? loadId { get; set; }

        #endregion

        #region CampaignData


        public string CampaignName { get; set; }
        public string SubCampaignName { get; set; }
        public string SegmentName { get; set; }

        public int SegmentId { get; set; }


        public int SubcampaignId { get; set; }

        public int CampaignId { get; set; }
        public int? DatabaseClientReportsId { get; set; }

        #endregion

        public int status { get; set; } = 1;
    }
}
