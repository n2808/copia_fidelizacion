﻿using Core.Models.Common;
using Domain.Models.Operation;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Surveys
{
    public class SurveySegment : Entity
    {

        public int SurveyId { get; set; }
        [ForeignKey("SurveyId")]
        public Survey Survey { get; set; }


        public int SegmentId { get; set; }
        [ForeignKey("SegmentId")]
        public Segment Segment { get; set; }

        public bool status { get; set; } = true;

    }
}
