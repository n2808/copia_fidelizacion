﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Surveys
{
    public class Survey : EntityWithIntId
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool status { get; set; }

        public List<SurveyQuestion> SurveyQuestions { get; set; }

    }
}
