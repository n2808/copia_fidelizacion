﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Surveys
{
    public class SurveyQuestion : EntityWithIntId
    {
        public string Name { get; set; }
        public string Description { get; set; }


        public Guid TypeId { get; set; }

        [ForeignKey("TypeId")]
        public Configuration Type { get; set; }

        public bool status { get; set; }

        public int Order { get; set; } = 1;

        public bool required { get; set; }

        public int SurveyId { get; set;  }

        [ForeignKey("SurveyId")]
        public Survey Survey { get; set; }

        public List<SurveyResponse> SurveyResponses { get; set; }

    }
}
