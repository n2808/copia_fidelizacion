﻿using Core.Models.Common;
using Core.Models.configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Surveys
{
    public class SurveyResponse : EntityWithIntId
    {
        public string Name { get; set; }
        public string Description { get; set; }


        public bool status { get; set; }
         
        public int Order { get; set; } = 1;

        public int SurveyQuestionId { get; set;  }

        [ForeignKey("SurveyQuestionId")]
        public SurveyQuestion SurveyQuestion { get; set; }

        
    }
}
