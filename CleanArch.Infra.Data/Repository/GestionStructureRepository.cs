﻿using CleanArch.Domain.Interfaces;
using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using System.Linq;

namespace Infra.Data.Repository
{
    public class GestionStructureRepository : IGestionStructureRepository
    {
        private U27ApplicationDBContext _ctx;

        public GestionStructureRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<GestionStructure> GetBySegmentId(int Id)
        {
            return _ctx.GestionStructure.Where(x => x.SegmentId == Id);
        }
    }
}
