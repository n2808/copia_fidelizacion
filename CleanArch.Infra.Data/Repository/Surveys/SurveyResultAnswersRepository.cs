﻿using Domain.Interfaces.Surveys;
using CleanArch.Infra.Data.Context;
using Domain.Models.SurveysResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Application.Core.Exceptions;

namespace Infra.Data.Repository.Surveys
{
    public class SurveyResultAnswersRepository : ISurveyResultAnswersRepository
    {
        private U27ApplicationDBContext _ctx;

        public SurveyResultAnswersRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<SurveyResultAnswers> Get()
        {
            return _ctx.SurveyResultAnswers;
        }

        public List<SurveyResultAnswers> PostRange(List<SurveyResultAnswers> surveyResultAnswers)
        {

            _ctx.SurveyResultAnswers.AddRange(surveyResultAnswers);

            try
            {
                _ctx.SaveChanges();
                return surveyResultAnswers;
            }
            catch (Exception ex)
            {

                throw new BadRequestException("No se ha podido agregar las respuestas");
            }
        }
    }
}
