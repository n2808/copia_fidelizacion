﻿using Domain.Interfaces.Surveys;
using CleanArch.Infra.Data.Context;
using Domain.Models.Surveys;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository.Surveys
{
    public class SurveyRepository : ISurveyRepository
    {
        private U27ApplicationDBContext _ctx;

        public SurveyRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<Survey> Get()
        {
            return _ctx.Surveys;
        }
    }
}
