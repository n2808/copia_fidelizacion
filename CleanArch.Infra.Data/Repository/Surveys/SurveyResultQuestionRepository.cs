﻿using Domain.Interfaces.Surveys;
using CleanArch.Infra.Data.Context;
using Domain.Models.SurveysResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Application.Core.Exceptions;

namespace Infra.Data.Repository.Surveys
{
    public class SurveyResultQuestionRepository : ISurveyResultQuestionRepository
    {
        private U27ApplicationDBContext _ctx;

        public SurveyResultQuestionRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<SurveyResultQuestion> Get()
        {
            return _ctx.SurveyResultQuestions;
        }


        public List<SurveyResultQuestion> PostRange(List<SurveyResultQuestion> surveyResultQuestions)
        {

            _ctx.SurveyResultQuestions.AddRange(surveyResultQuestions);

            try
            {
                _ctx.SaveChanges();
                return surveyResultQuestions;
            }
            catch (Exception ex)
            {

                throw new BadRequestException("No se ha podido agregar las preguntas");
            }
        }
    }
}
