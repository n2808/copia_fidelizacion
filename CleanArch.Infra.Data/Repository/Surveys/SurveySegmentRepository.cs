﻿using CleanArch.Domain.Interfaces;
using CleanArch.Domain.Models;
using CleanArch.Infra.Data.Context;
using Domain.Models.Operation;
using Domain.Models.Surveys;
using Domain.Models.Tipifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository.Surveys
{
    public class SurveySegmentRepository : ISurveySegmentRepository
    {
        private U27ApplicationDBContext _ctx;

        public SurveySegmentRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<SurveySegment> Get()
        {
            return _ctx.SurveySegments;
        }
    }
}
