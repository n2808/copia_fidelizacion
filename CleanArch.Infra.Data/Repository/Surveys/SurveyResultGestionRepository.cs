﻿using Domain.Interfaces.Surveys;
using CleanArch.Infra.Data.Context;
using Domain.Models.SurveysResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository.Surveys
{
    public class SurveyResultGestionRepository : ISurveyResultGestionRepository
    {
        private U27ApplicationDBContext _ctx;

        public SurveyResultGestionRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<SurveyResultGestion> Get()
        {
            return _ctx.SurveyResultGestions;
        }

        public SurveyResultGestion Post(SurveyResultGestion surveyResultGestion)
        {
            _ctx.SurveyResultGestions.Add(surveyResultGestion);
            _ctx.SaveChanges();

            return surveyResultGestion;
        }

    }
}
