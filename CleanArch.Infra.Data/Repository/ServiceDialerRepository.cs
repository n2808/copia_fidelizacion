﻿using CleanArch.Domain.Interfaces;
using CleanArch.Domain.Models;
using CleanArch.Infra.Data.Context;
using Domain.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository
{
    public class ServiceDialerRepository : IServiceDialerRepository
    {
        private U27ApplicationDBContext _ctx;

        public ServiceDialerRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<ServicesDialer> Get()
        {
            return _ctx.ServicesDialer;
        }
    }
}
