﻿using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using Domain.Interfaces;
using System.Linq;

namespace Infra.Data.Repository
{
    public class DatabaseClientDetailRepository : IDatabaseClientDetailRepository
    {
        private U27ApplicationDBContext _ctx;

        public DatabaseClientDetailRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<DatabaseClientDetail> GetClient(int? Id)
        {
            return _ctx.DatabaseClientDetail.Where(x => x.Id == Id);
        }

        
        public IQueryable<DatabaseClientDetail> GetClient()
        {
            return _ctx.DatabaseClientDetail;
                    
                 
        }

       

    }
}
