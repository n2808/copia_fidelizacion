﻿using CleanArch.Domain.Interfaces;
using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using System.Linq;

namespace Infra.Data.Repository
{
    public class InformativeAgentFieldsRepository : IInformativeAgentFieldsRepository
    {
        private U27ApplicationDBContext _ctx;

        public InformativeAgentFieldsRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<InformativeAgentFields> GetBySegmentId(int Id)
        {
            return _ctx.InformativeAgentFields.Where(x => x.SegmentId == Id);
        }

   
    }
}
