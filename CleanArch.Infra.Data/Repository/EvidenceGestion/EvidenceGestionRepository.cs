﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.EvidenceGestion;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository.EvidenceGestion
{
    public class EvidenceGestionRepository : IEvidenceGestionRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public EvidenceGestionRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<Domain.Models.Gestion.EvidenceGestion> PostEvidenceGestion(Domain.Models.Gestion.EvidenceGestion evidenceGestion)
        {
            _ctx.EvidenceGestions.Add(evidenceGestion);
            try
            {
               await _ctx.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new NotFoundException("Evidencias", "No se insertaron las evidencias");
            }

            return evidenceGestion;
        }


        public   IQueryable<Domain.Models.Gestion.EvidenceGestion> GetEvidenceGestion()
        {
            return  _ctx.EvidenceGestions;
        }

        public bool PutEvidenceGestion(Domain.Models.Gestion.EvidenceGestion evidenceGestion)
        {
            _ctx.Entry(evidenceGestion).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new NotFoundException("Evidencias", "Las evidencias no fue modificada");
            }

            return true;
        }

        public IQueryable<Domain.Models.Gestion.EvidenceGestion> Get()
        {
            return _ctx.EvidenceGestions;
        }
    }
}
