﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces;
using Domain.Models.FetchFaceEvidences;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class FetchFaceEvidenceRepository : IFetchFaceEvidenceRepository
    {
        private U27ApplicationDBContext _ctx;

        public FetchFaceEvidenceRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<FetchFaceEvidence> GetFetchFaceEvidence()
        {
            return _ctx.FetchFaceEvidence;

        }
        public async Task<FetchFaceEvidence> PostFetchFaceEvidence(FetchFaceEvidence fetchFaceEvidence)
        {
            _ctx.FetchFaceEvidence.Add(fetchFaceEvidence);

            await _ctx.SaveChangesAsync();

            return fetchFaceEvidence;
        }
        public async Task<FetchFaceEvidence> PutFetchFaceEvidence(FetchFaceEvidence fetchFaceEvidence)
        {

            _ctx.Entry(fetchFaceEvidence).State = EntityState.Modified;


            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("FetchFaceEvidence", fetchFaceEvidence.Id);
            }

            return fetchFaceEvidence;
        }

        public IQueryable<FetchFaceEvidence> GetById()
        {
            return _ctx.FetchFaceEvidence;
        }
    }
}
