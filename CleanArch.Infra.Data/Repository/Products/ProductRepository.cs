﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Products;
using Domain.Models.Products;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infra.Data.Repository.Products
{
    public class ProductRepository : IProductRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public ProductRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<Product> Get()
        {
            return _ctx.products;
        }

        public Product GetById(Guid id)
        {
            return _ctx.products.Find(id);
        }

        public Product Post(Product product)
        {
            _ctx.products.Add(product);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Producto", "El Producto no fué agregado");
            }

            return product;
        }

        public Product Put(Product product)
        {
            _ctx.Entry(product).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Product", "El Producto no fué modificado");
            }

            return product;
        }

        public bool Activate(Product product)
        {
            product.Status = true;
            _ctx.Entry(product).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Producto", "El Producto no fué modificado");
            }

            return true;
        }

        public bool Deactivate(Product product)
        {
            product.Status = false;
            _ctx.Entry(product).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Producto", "El Producto no fué modificado");
            }

            return true;
        }

        public bool Delete(Product product)
        {
            _ctx.Remove(product);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Producto", "El Producto no fue eliminado");
            }

            return true;
        }
    }
}
