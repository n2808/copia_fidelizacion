﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces;
using Domain.Models.Plans;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly U27ApplicationDBContext _ctx;
        private readonly IRepository<Plan> _planRepository;

        public UnitOfWork(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IRepository<Plan> PlanRepository => _planRepository ?? new BaseRepository<Plan>(_ctx);

        public void Dispose()
        {
            if (_ctx != null)
            {
                _ctx.Dispose();
            }
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _ctx.SaveChangesAsync();
        }
    }
}
