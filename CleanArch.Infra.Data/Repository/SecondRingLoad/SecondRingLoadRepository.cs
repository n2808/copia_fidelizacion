﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.SecondRingLoad;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository.SecondRingLoad
{
    public class SecondRingLoadRepository : ISecondRingLoadRepository
    {
        private U27ApplicationDBContext _ctx;

        public SecondRingLoadRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Domain.Models.SecondRingLoads.SecondRingLoad> Get()
        {
            return _ctx.SecondRingLoads;
        }

        public async Task<Domain.Models.SecondRingLoads.SecondRingLoad> Post(Domain.Models.SecondRingLoads.SecondRingLoad secondRingLoad)
        {
            //_ctx.Database.SetCommandTimeout(1000);
            _ctx.SecondRingLoads.Add(secondRingLoad);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("secondRingLoad", "La carga no fue registrada");
            }

            return secondRingLoad;
        }

        public async Task<bool> Put(Domain.Models.SecondRingLoads.SecondRingLoad secondRingLoad)
        {
            //_ctx.Database.SetCommandTimeout(1000);
            _ctx.Entry(secondRingLoad).State = EntityState.Modified;

            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new NotFoundException("SecondAssing", "Se ha asignado correctamente");
            }

            return true;
        }
    }
}
