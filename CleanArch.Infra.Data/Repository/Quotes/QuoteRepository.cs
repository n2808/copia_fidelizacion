﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Quotes;
using Domain.Models.Quotes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infra.Data.Repository.Quotes
{
    public class QuoteRepository : IQuoteRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public QuoteRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<Quote> Get()
        {
            return _ctx.Quotes;
        }

        public Quote GetById(Guid id)
        {
            return _ctx.Quotes.Find(id);
        }

        public Quote Post(Quote quote)
        {
            _ctx.Quotes.Add(quote);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Cita", "La Cita no fué agregada");
            }

            return quote;
        }

        public Quote Put(Quote quote)
        {
            _ctx.Entry(quote).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Cita", "La Cita no fué modificada");
            }

            return quote;
        }

        public bool Activate(Quote quote)
        {
            quote.Status = true;
            _ctx.Entry(quote).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Cita", "La Cita no fué modificada");
            }

            return true;
        }

        public bool Deactivate(Quote quote)
        {
            quote.Status = false;
            _ctx.Entry(quote).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Cita", "La Cita no fué modificada");
            }

            return true;
        }

        public bool Delete(Quote quote)
        {
            _ctx.Remove(quote);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Cita", "La Cita no fue eliminada");
            }

            return true;
        }
    }
}
