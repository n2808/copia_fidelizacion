﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces.ReSchedule;
using Domain.Models.ReSchedule;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository.ReSchedule
{
    public class TipificationRescheduleRepository : ITipificationRescheduleRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public TipificationRescheduleRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public async Task Delete(TipificationReschedule entity)
        {
            _ctx.Remove(entity);
            await _ctx.SaveChangesAsync();

        }

        public async Task DeleteRange(List<TipificationReschedule> entity)
        {

            _ctx.RemoveRange(entity);
            await _ctx.SaveChangesAsync();

        }

        public IQueryable<TipificationReschedule> Get()
        {
            return _ctx.TipificationReschedules;
        }

        public async Task<TipificationReschedule> Post(TipificationReschedule entity)
        {
            _ctx.Add(entity);
            await _ctx.SaveChangesAsync();
            return entity;

        }

        public async Task<List<TipificationReschedule>> PostRange(List<TipificationReschedule> entity)
        {
            _ctx.AddRange(entity);
            await _ctx.SaveChangesAsync();
            return entity;


        }

        public async Task<TipificationReschedule> Put(TipificationReschedule entity)
        {
            _ctx.Entry(entity).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();


            return entity;
        }
    }
}
