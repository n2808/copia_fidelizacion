﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces.ReSchedule;
using Domain.Models.ReSchedule;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository.ReSchedule
{
    public class RescheduleHistoryRepository : IRescheduleHistoryRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public RescheduleHistoryRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public async Task Delete(RescheduleHistory entity)
        {
            _ctx.Remove(entity);
            await _ctx.SaveChangesAsync();

        }

        public async Task DeleteRange(List<RescheduleHistory> entity)
        {
            
            _ctx.RemoveRange(entity);
            await _ctx.SaveChangesAsync();

        }

        public IQueryable<RescheduleHistory> Get()
        {
            return _ctx.rescheduleHistories;
        }

        public async Task<RescheduleHistory> Post(RescheduleHistory entity)
        {
            _ctx.Add(entity);
            await _ctx.SaveChangesAsync();
            return entity;

        }

        public async Task<List<RescheduleHistory>> PostRange(List<RescheduleHistory> entity)
        {
            _ctx.AddRange(entity);
            await _ctx.SaveChangesAsync();
            return entity;


        }

        public async Task<RescheduleHistory> Put(RescheduleHistory entity)
        {
            _ctx.Entry(entity).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();


            return entity;
        }
    }
}
