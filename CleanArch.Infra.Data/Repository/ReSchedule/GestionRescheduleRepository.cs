﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces.ReSchedule;
using Domain.Models.ReSchedule;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository.ReSchedule
{
    public class GestionRescheduleRepository : IGestionRescheduleRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public GestionRescheduleRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public async Task Delete(GestionReschedule entity)
        {
            _ctx.Remove(entity);
            await _ctx.SaveChangesAsync();

        }

        public async Task DeleteRange(List<GestionReschedule> entity)
        {
            
            _ctx.RemoveRange(entity);
            await _ctx.SaveChangesAsync();

        }

        public IQueryable<GestionReschedule> Get()
        {
            return _ctx.gestionReschedules;
        }

        public async Task<GestionReschedule> Post(GestionReschedule entity)
        {
            _ctx.Add(entity);
            await _ctx.SaveChangesAsync();
            return entity;

        }

        public async Task<List<GestionReschedule>> PostRange(List<GestionReschedule> entity)
        {
            _ctx.AddRange(entity);
            await _ctx.SaveChangesAsync();
            return entity;


        }

        public async Task<GestionReschedule> Put(GestionReschedule entity)
        {
            _ctx.Entry(entity).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();


            return entity;
        }
    }
}
