﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Status;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository.Status
{
    public class StatusRepository : IStatusRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public StatusRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        
        public IQueryable<Domain.Models.Status.Status> Get()
        {
            return _ctx.Status;
        }

        public async Task<Domain.Models.Status.Status> GetById(Guid id)
        {
            return _ctx.Status.Find(id);
        }

        public async Task<Domain.Models.Status.Status> Post(Domain.Models.Status.Status status)
        {
            _ctx.Status.Add(status);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Status", "El estado no fue agregado");
            }
            return status;
        }

        public async Task<Domain.Models.Status.Status> Put(Domain.Models.Status.Status status)
        {
            _ctx.Entry(status).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)

            {
                throw new NotFoundException("Status", "El estado no fué modificado");
            }
            return status;
        }
        public async Task<bool> Delete(Domain.Models.Status.Status status)
        {
            _ctx.Remove(status);

            try
            {
               await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new NotFoundException("Status", "El estado no fue eliminado");
            }

            return true;
        }
    }
}
