﻿using CleanArch.Domain.Interfaces;
using CleanArch.Domain.Models;
using CleanArch.Infra.Data.Context;
using Domain.Models.Offer;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class GestionReportsOffersRepository : IGestionReportsOffersRepository
    {
        private U27ApplicationDBContext _ctx;

        public GestionReportsOffersRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<GestionReportsOffers> GetOffers()
        {
            return _ctx.GestionReportsOffers;
        }

        public async Task PostOffers (List<GestionReportsOffers> offers)
        {
            _ctx.AddRange(offers);
           await _ctx.SaveChangesAsync();
                
      
        }

        public async Task DeleteOffers(List<GestionReportsOffers> offers)
        {
            _ctx.RemoveRange(offers);
            await _ctx.SaveChangesAsync();


        }



    }
}
