﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces;
using Domain.Models.ClaroUser;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class ClaroUserRepository : IClaroUserRepository
    {
        private U27ApplicationDBContext _ctx;

        public ClaroUserRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<ClaroUser> GetUsersCodeAdviser()
        {
            return _ctx.ClaroUsers;
        }

        public async Task<ClaroUser> Post(ClaroUser claroUser)
        {
            _ctx.ClaroUsers.Add(claroUser);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("claroUser", "El claro user no fue crado");
            }

            return claroUser;
        }

        public async Task<ClaroUser> Put(ClaroUser claroUser)
        {
            _ctx.Entry(claroUser).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("claroUser", "El claro user no fue modificada");
            }
            return claroUser;
        }
    }
}
