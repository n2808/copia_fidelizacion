﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Campaigns;
using Domain.Models.Operation;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository.Campaigns
{
    public class SubCampaignRepository : ISubCampaignRepository
    {
        private U27ApplicationDBContext _ctx;

        public SubCampaignRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<SubCampaign> Get()
        {
            return _ctx.SubCampaigns;
        }

        public async Task<string> GetById(int? id)
        {
            var model = await _ctx.SubCampaigns.FindAsync(id);
            return model.Name;
        }

    }
}
