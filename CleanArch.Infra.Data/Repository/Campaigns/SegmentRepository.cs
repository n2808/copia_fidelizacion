﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using Domain.Interfaces.Campaigns;
using Domain.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository.Campaigns
{
    public class SegmentRepository : ISegmentRepository
    {
        private U27ApplicationDBContext _ctx;

        public SegmentRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Segment> Get()
        {
            return _ctx.Segments;
        }

        public async Task<Segment> GetSegmentById(Segment segment)
        {
            return segment;
        }

        public async Task<Segment> GetSegmentCampaingById(Segment segment)
        {
            return segment;
        }

        public async Task<Segment> Post(Segment segment)
        {
            _ctx.Segments.Add(segment);
            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Segment", "El segmento no fue insertado");
            }
            return segment;
        }

        public async Task<Segment> Put(Segment segment)
        {
            _ctx.Entry(segment).State = EntityState.Modified;
            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Segment", "El segmento no fue modificada");
            }
            return segment;
        }

        public async Task<Segment> PutStatusActivateSegmentById(Segment segment)
        {
            segment.Status = true;
            _ctx.Entry(segment).State = EntityState.Modified;
            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Segment", "El segmento no fue modificado");
            }
            return segment;
        }

        public async Task<Segment> PutStatusDeActivateSegmentById(Segment segment)
        {
            segment.Status = false;
            _ctx.Entry(segment).State = EntityState.Modified;
            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Segment", "El segmento no fue modificado");
            }
            return segment;
        }
        public async Task<bool> Delete(Segment segment)
        {
            _ctx.Remove(segment);
            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Segment", "El segmento no fue eliminado");
            }
            return true;
        }
    }
}
