﻿using Application.Core.Exceptions;
using CleanArch.Domain.Interfaces;
using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class ActiveGestionRepository : IActiveGestionRepository
    {
        private U27ApplicationDBContext _ctx;

        public ActiveGestionRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<ActiveGestion> GetActiveGestion()
        {
            return _ctx.ActiveGestions;

        }



        public async Task<ActiveGestion> PostGestion(ActiveGestion activeGestion)
        {
            _ctx.ActiveGestions.Add(activeGestion);

            await _ctx.SaveChangesAsync();

            return activeGestion;
        }



        public async Task<ActiveGestion> PutGestion(ActiveGestion activeGestion)
        {
            
            _ctx.Entry(activeGestion).State = EntityState.Modified; 
          

            try
            {
               await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Gestion",activeGestion.Id);
            }

            return activeGestion;
        }

        public IQueryable<ActiveGestion> GetById()
        {
            return _ctx.ActiveGestions;
        }
    }
}
