﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces;
using Domain.Models.ManagementsBackoffice;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class BackOfficeManagementsRepository : IBackOfficeManagementsRepository
    {
        private U27ApplicationDBContext _ctx;

        public BackOfficeManagementsRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<BackOfficeManagements> Get()
        {
            return _ctx.BackofficeManagements;
        }

        public IQueryable<BackOfficeManagementsHistories> GetAll()
        {
            return _ctx.BackOfficeManagementsHistories;
        }

        public async Task<BackOfficeManagements> PostManagementBackOffice(BackOfficeManagements managementsBackOffice)
        {
            _ctx.Database.SetCommandTimeout(1000);
            _ctx.BackofficeManagements.Add(managementsBackOffice);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("No se pudo registrar el backoffice-managements");
            }
            return managementsBackOffice;
        }

        public async Task<BackOfficeManagementsHistories> PostManagementBackOfficeHistory(BackOfficeManagementsHistories managementsBackOfficeHistory)
        {
            _ctx.Database.SetCommandTimeout(1000);
            _ctx.BackOfficeManagementsHistories.Add(managementsBackOfficeHistory);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
            return managementsBackOfficeHistory;
        }

        public async Task<BackOfficeManagements> PutManagementBackOffice(BackOfficeManagements managementsBackOffice)
        {
            _ctx.Entry(managementsBackOffice).State = EntityState.Modified;
            try
            {
                 await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("ManagementBack", "El managements no fue modificada");
            }

            return managementsBackOffice;
        }
    }
}
