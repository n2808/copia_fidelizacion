﻿using CleanArch.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Domain.Notification;
using CleanArch.Domain.Interfaces.Notifications;

namespace Infra.Data.Repository.Notifications
{
    public class NotificationAccountRepository : INotificationAccountRepository
    {
        private U27ApplicationDBContext _ctx;

        public NotificationAccountRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<NotificationAccount> Get()
        {
            return _ctx.NotificationAccount;

        }



    }
}
