﻿using CleanArch.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Domain.Notification;
using CleanArch.Domain.Interfaces.Notifications;
using Application.Core.Exceptions;

namespace Infra.Data.Repository.Notifications
{
    public class NotificationLogRepository : INotificationLogRepository
    {
        private U27ApplicationDBContext _ctx;

        public NotificationLogRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<NotificationLog> Get()
        {
            return _ctx.NotificationLog;

        }

        public NotificationLog Post(NotificationLog notificationLog)
        {
            _ctx.AddRange(notificationLog);

            try
            {
                _ctx.SaveChanges();
                return notificationLog;
            }
            catch (Exception ex)
            {
                throw new BadRequestException("NotificationLog");

            }
        }





    }
}
