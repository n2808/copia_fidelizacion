﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class GestionReportsRepository : IGestionReportRepository
    {
        private U27ApplicationDBContext _ctx;

        public GestionReportsRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<GestionReports> PostGestion(GestionReports gestion)
        {
            _ctx.GestionReports.Add(gestion);
            await _ctx.SaveChangesAsync();

            return gestion;
        }

        public async Task<GestionReports> Put(GestionReports entity)
        {
            _ctx.Entry(entity).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();


            return entity;
        }

        public IQueryable<GestionReports> GetGestionReports()
        {
            return _ctx.GestionReports;
        }

        public bool PutEvidencesGestionReports(GestionReports gestionReports)
        {
            _ctx.Entry(gestionReports).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new NotFoundException("Evidencias", "Las evidencias no fue modificada");
            }

            return true;
        }

        public GestionReports GetGestionReportsById(int Id)
        {
            return _ctx.GestionReports.Find(Id);
        }

        public IQueryable<GestionReports> GetGestionReportsSegmentId(int segmentId, string field, string value)
        {
            var reportGestions = _ctx.GestionReports.FromSqlRaw("SP_RETURN_FILTER @segmentId = {0}, @field = {1}, @value = {2}", segmentId, field, value);
                        
            return reportGestions;
        }

        public IQueryable<GestionReports> Get()
        {
            return _ctx.GestionReports;
        }
    }
}
