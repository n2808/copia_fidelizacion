﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Skill;
using Domain.Models.Skill;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository.Skill
{
    public class SkillRepository : ISkillRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public SkillRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Skills> Get()
        {
            return _ctx.Skills;
        }

        public async Task<Skills> Post(Skills skills)
        {
            _ctx.Skills.Add(skills);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Skills", "El skills no fue creado");
            }
            return skills;
        }

        public async Task<Skills> Put(Skills skills)
        {
            _ctx.Entry(skills).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Skills", "El skills no fue editado");
            }
            return skills;
        }
        public async Task<Skills> Activate(Skills skills)
        {
            _ctx.Entry(skills).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Skills", "El skills fue activado");
            }
            return skills;
        }

        public async Task<Skills> Deactivate(Skills skills)
        {
            _ctx.Entry(skills).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Skills", "El skills no fue activado");
            }
            return skills;
        }

        public async Task<bool> Delete(Skills skills)
        {
            _ctx.Remove(skills);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Skills", "El skills no fue eliminado");
            }
            return true;
        }

        public async Task<Skills> GetSkillById(Skills skills)
        {
            return skills;
        }
    }
}
