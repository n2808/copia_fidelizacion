﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces.location;
using Domain.Models.location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository.Location
{
    public class NewCityRepository : INewCityRepository
    {
        private readonly U27ApplicationDBContext _ctx;
        public NewCityRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<NewCity> Get()
        {
            return _ctx.NewCities;
        }
    }
}
