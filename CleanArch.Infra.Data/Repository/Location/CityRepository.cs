﻿using CleanArch.Infra.Data.Context;
using Core.Models.location;
using Domain.Interfaces.location;
using System.Linq;

namespace Infra.Data.Repository.Location
{
    public class CityRepository : ICityRepository
    {
        private readonly U27ApplicationDBContext _ctx;
        public CityRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<City> Get()
        {
            return _ctx.Cities;
        }
    }
}
