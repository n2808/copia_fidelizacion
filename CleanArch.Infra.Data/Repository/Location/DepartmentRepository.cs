﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces.location;
using Domain.Models.location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository.Location
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly U27ApplicationDBContext _ctx;
        public DepartmentRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<Department> Get()
        {
            return _ctx.Departments;
        }
    }
}
