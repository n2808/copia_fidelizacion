﻿using CleanArch.Infra.Data.Context;
using Core.Models.location;
using Domain.Interfaces.location;
using System.Linq;

namespace Infra.Data.Repository.Location
{
    public class StateRepository : IStateRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public StateRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<State> Get()
        {
            return _ctx.states;
        }
    }
}
