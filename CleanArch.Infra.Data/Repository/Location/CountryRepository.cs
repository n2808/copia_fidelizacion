﻿using CleanArch.Infra.Data.Context;
using Core.Models.location;
using Domain.Interfaces.location;
using System.Linq;

namespace Infra.Data.Repository.Location
{
    public class CountryRepository : ICountryRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public CountryRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Country> Get()
        {
            return _ctx.Countries;
        }
    }
}
