﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Group;
using Domain.Models.Groups;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Infra.Data.Repository.Groups
{
    public class GroupRepository : IGroupRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public GroupRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Group> Get()
        {
            return _ctx.Groups;
        }

        public Group Post(Group group)
        {
            _ctx.Groups.Add(group);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new NotFoundException("Group", "El grupo no fué agregado");
            }

            return group;
        }

        public Group Put(Group group)
        {
            _ctx.Entry(group).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new NotFoundException("Group", "El grupo no fué modificado");
            }

            return group;
        }

        public bool Activate(Group group)
        {
            group.Status = true;
            _ctx.Entry(group).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new NotFoundException("Group", "El grupo no fué modificado"); ;
            }

            return true;
        }

        public bool Deactivate(Group group)
        {
            group.Status = false;
            _ctx.Entry(group).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (System.Exception)
            {

                throw new NotFoundException("Group", "El grupo no fué modificado");
            }

            return true;
        }

        public bool Delete(Group group)
        {
            _ctx.Remove(group);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new NotFoundException("Group", "El grupo no fue eliminado");
            }

            return true;
        }

    }
}
