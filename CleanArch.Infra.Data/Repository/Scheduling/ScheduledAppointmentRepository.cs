﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Scheduling;
using Domain.Models.Scheduling;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Infra.Data.Repository.Scheduling
{
    public class ScheduledAppointmentRepository : IScheduledAppointmentRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public ScheduledAppointmentRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        /// <summary>
        /// Method that returns all appointments scheduled.
        /// </summary>
        /// <returns></returns>
        public IQueryable<ScheduledAppointment> Get()
        {
            return _ctx.ScheduledAppointments;
        }

  
        /// <summary>
        /// method that adds scheduled appointments.
        /// </summary>
        /// <returns></returns>
        public ScheduledAppointment Post(ScheduledAppointment scheduledAppointment)
        {
            _ctx.ScheduledAppointments.Add(scheduledAppointment);
            _ctx.SaveChanges();
            return scheduledAppointment;
        }

        /// <summary>
        /// Method for modifying the entity.
        /// </summary>
        /// <param name="scheduledAppointment"></param>
        /// <returns></returns>
        public ScheduledAppointment Put(ScheduledAppointment scheduledAppointment)
        {
            _ctx.Entry(scheduledAppointment).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("ScheduledAppointment", "La cita no fué encontrada");
            }
            
            return scheduledAppointment;
        }

        /// <summary>
        /// Method that eliminates the entity that enters by parameter.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Delete(ScheduledAppointment entity)
        {
           _ctx.Remove(entity);
           _ctx.SaveChanges();
 
            return true;

        }

    }
}
