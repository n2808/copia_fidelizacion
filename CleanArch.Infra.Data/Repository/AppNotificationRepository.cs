﻿using CleanArch.Domain.Interfaces;
using CleanArch.Domain.Models;
using CleanArch.Infra.Data.Context;
using Domain.Models.Apps;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository
{
    public class AppNotificationRepository : IAppNotificationRepository
    {
        private U27ApplicationDBContext _ctx;

        public AppNotificationRepository (U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }


        public IQueryable<AppNotification> GetApps()
        {
            return _ctx.AppNotification;
        }
    }
}
