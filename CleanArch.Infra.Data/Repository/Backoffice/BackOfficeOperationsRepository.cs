﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Backoffice;
using Domain.Models.Backoffice;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Repository.Backoffice
{
    public class BackOfficeOperationsRepository : IBackOfficeOperationsRepository
    {
        private readonly U27ApplicationDBContext _ctx;
        public BackOfficeOperationsRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<BackOfficeOperations> Get()
        {
            return _ctx.BackOfficeOperations;
        }

        public async Task<BackOfficeOperations> Post(BackOfficeOperations backOfficeOperations)
        {
            _ctx.BackOfficeOperations.Add(backOfficeOperations);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("backOfficeOperations", "La operación del back-office no fue crado");
            }

            return backOfficeOperations;
        }

        public async Task<BackOfficeOperations> Put(BackOfficeOperations backOfficeOperations)
        {
            _ctx.Entry(backOfficeOperations).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("backOfficeOperations", "La operación del back-office no fue modificada");
            }

            return backOfficeOperations;
        }
        public async Task<BackOfficeOperations> Activate(BackOfficeOperations backOfficeOperations)
        {
            _ctx.Entry(backOfficeOperations).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("backOfficeOperations", "La operación del back-office no fue activada");
            }

            return backOfficeOperations;
        }

        public async Task<BackOfficeOperations> Deactivate(BackOfficeOperations backOfficeOperations)
        {
            _ctx.Entry(backOfficeOperations).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("backOfficeOperations", "La operación del back-office no fue desactivada");
            }

            return backOfficeOperations;
        }

        public async Task<bool> Delete(BackOfficeOperations backOfficeOperations)
        {
            _ctx.Remove(backOfficeOperations);
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("backOfficeOperations", "La operación del back-office no fue eliminada");
            }

            return true;
        }

    }
}
