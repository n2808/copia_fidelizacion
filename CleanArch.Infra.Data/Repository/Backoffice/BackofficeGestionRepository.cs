﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Backoffice;
using Domain.Models.Backoffice;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Infra.Data.Repository.Backoffice
{
    public class BackofficeGestionRepository : IBackofficeGestionRepository
    {
        private readonly U27ApplicationDBContext _ctx;
        public BackofficeGestionRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        /// <summary>
        /// Method that returns all appointments scheduled.
        /// </summary>
        /// <returns></returns>
        public IQueryable<BackofficeGestion> Get()
        {
            return _ctx.BackofficeGestions;
        }

        /// <summary>
        /// Method that adds BackofficeGestion.
        /// </summary>
        /// <returns></returns>
        public BackofficeGestion Post(BackofficeGestion backofficeGestion)
        {
            _ctx.BackofficeGestions.Add(backofficeGestion);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("ScheduledAppointment", "La getión no fue insertada");
            }
          
            return backofficeGestion;
        }

        /// <summary>
        /// Method for modifying the entity.
        /// </summary>
        /// <param name="backofficeGestion"></param>
        /// <returns></returns>
        public BackofficeGestion Put(BackofficeGestion backofficeGestion)
        {
            _ctx.Entry(backofficeGestion).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("ScheduledAppointment", "La getión no fue encontrada");
            }

            return backofficeGestion;          
        }

        /// <summary>
        /// Method that eliminates the entity that enters by parameter.
        /// </summary>
        /// <param name="backofficeGestion"></param>
        /// <returns></returns>
        public bool Delete(BackofficeGestion backofficeGestion)
        {
            _ctx.Remove(backofficeGestion);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("ScheduledAppointment", "La getión no fue eliminada");
            }

            return true;
        }
    }
}
