﻿using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infra.Data.Repository
{
    public class GestionRepository : IGestionRepository
    {
        private U27ApplicationDBContext _ctx;

        public GestionRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IEnumerable<InformativeAgentFields> GetClientdata()
        {
            return _ctx.InformativeAgentFields.Where(x => x.SegmentId == 1);
        }


        public bool ValidateDialerServiceCode(string Code)
        {
            try
            {
                var SD = _ctx.ServicesDialer.Where(c => c.VDN == Code && c.status == true).FirstOrDefault();
                return SD == null ? false : true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
