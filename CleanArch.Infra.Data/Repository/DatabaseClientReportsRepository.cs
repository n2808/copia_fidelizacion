﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Core.Models.Operation;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Data.Repository
{
    public class DatabaseClientReportsRepository : IDatabaseClientReportsRepository
    {
        private U27ApplicationDBContext _ctx;

        public DatabaseClientReportsRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }



        public async Task<DatabaseClientReports> PostClient(DatabaseClientReports Client)
        {
            _ctx.databaseClientReports.Add(Client);

            await _ctx.SaveChangesAsync();

            return Client;
        }


        public IQueryable<DatabaseClientReports> GetClient()
        {
            return _ctx.databaseClientReports;
        }

        public async Task<DatabaseClientReports> PutClient(DatabaseClientReports Client)
        {

            _ctx.Entry(Client).State = EntityState.Modified;
            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new NotFoundException("Client", ex.Message);
            }

            return Client;
        }


    }
}
