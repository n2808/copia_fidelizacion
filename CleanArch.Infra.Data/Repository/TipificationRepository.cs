﻿using CleanArch.Domain.Interfaces;
using CleanArch.Domain.Models;
using CleanArch.Infra.Data.Context;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository
{
    public class TipificationRepository : ITipificationRepository
    {
        private U27ApplicationDBContext _ctx;

        public TipificationRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Tipification> GetTipifications()
        {
            return _ctx.Tipification;
        }
        public IQueryable<Tipification> GetTipificationsByName()
        {
            return _ctx.Tipification;
        }
    }
}
