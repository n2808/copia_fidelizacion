﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Operators;
using Domain.Models.Operators;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infra.Data.Repository.Operators
{
    public class OperatorRepository : IOperatorRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public OperatorRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<Operator> Get()
        {
            return _ctx.Operators;
        }

        public Operator GetById(Guid id)
        {
            return _ctx.Operators.Find(id);
        }

        public Operator Post(Operator _operator)
        {
            _ctx.Operators.Add(_operator);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Operador", "El Producto no fué agregado");
            }

            return _operator;
        }

        public Operator Put(Operator _operator)
        {
            _ctx.Entry(_operator).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Operador", "El Operador no fué modificado");
            }

            return _operator;
        }

        public bool Activate(Operator _operator)
        {
            _operator.Status = true;
            _ctx.Entry(_operator).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Operador", "El Operador no fué modificado");
            }

            return true;
        }

        public bool Deactivate(Operator _operator)
        {
            _operator.Status = false;
            _ctx.Entry(_operator).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Operador", "El Operador no fué modificado");
            }

            return true;
        }

        public bool Delete(Operator _operator)
        {
            _ctx.Remove(_operator);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Operador", "El Operador no fue eliminado");
            }

            return true;
        }
    }
}
