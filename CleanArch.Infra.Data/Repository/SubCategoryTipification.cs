﻿using CleanArch.Infra.Data.Context;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Data.Repository
{
    public class SubCategoryTipification : ISubCategoryTipification
    {
        private U27ApplicationDBContext _ctx;

        public SubCategoryTipification(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<Domain.Models.Tipification.SubCategoryTipification> GetByType()
        {
            return _ctx.SubCategoryTipification;
        }
    }
}
