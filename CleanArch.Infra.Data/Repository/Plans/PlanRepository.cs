﻿using Application.Core.Exceptions;
using CleanArch.Infra.Data.Context;
using Domain.Interfaces.Plans;
using Domain.Models.Plans;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infra.Data.Repository.Plans
{
    public class PlanRepository : IPlanRepository
    {
        private readonly U27ApplicationDBContext _ctx;

        public PlanRepository(U27ApplicationDBContext ctx)
        {
            _ctx = ctx;
        }
        public IQueryable<Plan> Get()
        {
            return _ctx.Plans;
        }

        public Plan Post(Plan plan)
        {
            _ctx.Plans.Add(plan);           

            try
            {  
                 _ctx.SaveChanges();                
            }
            catch (Exception ex)
            {
                throw new NotFoundException("Plan", "Id");
            }

            return plan;
        }

        public Plan Put(Plan plan)
        {
            _ctx.Entry(plan).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Plan", "El plan no fué modificado");
            }

            return plan;
        }

        public bool Activate(Plan plan)
        {
            plan.Status = true;
            _ctx.Entry(plan).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Plan", "El plan no fué modificado");
            }

            return true;
        }

        public bool Deactivate(Plan plan)
        {
            plan.Status = false;
            _ctx.Entry(plan).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Plan", "El plan no fué modificado");
            }

            return true;
        }

        public bool Delete(Plan plan)
        {
            _ctx.Remove(plan);

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new NotFoundException("Plan", "El plan no fue eliminado");
            }

            return true;
        }

    }
}
