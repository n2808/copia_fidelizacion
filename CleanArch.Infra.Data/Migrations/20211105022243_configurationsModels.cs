﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class configurationsModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Account",
                table: "GestionReports",
                maxLength: 25,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Account",
                table: "ActiveGestions",
                maxLength: 25,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Account",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "Account",
                table: "ActiveGestions");
        }
    }
}
