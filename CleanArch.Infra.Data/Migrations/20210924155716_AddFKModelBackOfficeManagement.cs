﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFKModelBackOfficeManagement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "BackOfficeManagementId",
                table: "BackOfficeManagementsHistories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BackOfficeManagementsHistories_BackOfficeManagementId",
                table: "BackOfficeManagementsHistories",
                column: "BackOfficeManagementId");

            migrationBuilder.AddForeignKey(
                name: "FK_BackOfficeManagementsHistories_BackofficeManagements_BackOfficeManagementId",
                table: "BackOfficeManagementsHistories",
                column: "BackOfficeManagementId",
                principalTable: "BackofficeManagements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackOfficeManagementsHistories_BackofficeManagements_BackOfficeManagementId",
                table: "BackOfficeManagementsHistories");

            migrationBuilder.DropIndex(
                name: "IX_BackOfficeManagementsHistories_BackOfficeManagementId",
                table: "BackOfficeManagementsHistories");

            migrationBuilder.DropColumn(
                name: "BackOfficeManagementId",
                table: "BackOfficeManagementsHistories");
        }
    }
}
