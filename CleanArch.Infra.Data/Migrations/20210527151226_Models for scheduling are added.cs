﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class Modelsforschedulingareadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ScheduledAppointmentReports",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Ot = table.Column<string>(nullable: true),
                    WorkDay = table.Column<string>(nullable: true),
                    AppointmentDate = table.Column<DateTime>(nullable: false),
                    Observation = table.Column<string>(nullable: true),
                    GestionReportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduledAppointmentReports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScheduledAppointmentReports_GestionReports_GestionReportId",
                        column: x => x.GestionReportId,
                        principalTable: "GestionReports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ScheduledAppointments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Ot = table.Column<string>(nullable: true),
                    WorkDay = table.Column<string>(nullable: true),
                    AppointmentDate = table.Column<DateTime>(nullable: false),
                    Observation = table.Column<string>(nullable: true),
                    ActiveGestionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduledAppointments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScheduledAppointments_ActiveGestions_ActiveGestionId",
                        column: x => x.ActiveGestionId,
                        principalTable: "ActiveGestions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledAppointmentReports_GestionReportId",
                table: "ScheduledAppointmentReports",
                column: "GestionReportId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduledAppointments_ActiveGestionId",
                table: "ScheduledAppointments",
                column: "ActiveGestionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScheduledAppointmentReports");

            migrationBuilder.DropTable(
                name: "ScheduledAppointments");
        }
    }
}
