﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddUserSubCampaignId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_campaigns_CampaignId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_CampaignId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CampaignId",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "SubCampaignId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubCampaignId",
                table: "Users",
                column: "SubCampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_SubCampaigns_SubCampaignId",
                table: "Users",
                column: "SubCampaignId",
                principalTable: "SubCampaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_SubCampaigns_SubCampaignId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubCampaignId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubCampaignId",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "CampaignId",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_CampaignId",
                table: "Users",
                column: "CampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_campaigns_CampaignId",
                table: "Users",
                column: "CampaignId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
