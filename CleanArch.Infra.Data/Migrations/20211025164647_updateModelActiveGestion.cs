﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class updateModelActiveGestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSale",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSale",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSale",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "IsSale",
                table: "ActiveGestions");
        }
    }
}
