﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddGreetingAndFerbellToSegment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FarewelDialog",
                table: "Segments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GreetingDialog",
                table: "Segments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FarewelDialog",
                table: "Segments");

            migrationBuilder.DropColumn(
                name: "GreetingDialog",
                table: "Segments");
        }
    }
}
