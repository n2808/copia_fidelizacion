﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class updateServiceDialerCampaign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServicesDialer_campaigns_SubCampaignId",
                table: "ServicesDialer");

            migrationBuilder.DropIndex(
                name: "IX_ServicesDialer_SubCampaignId",
                table: "ServicesDialer");

            migrationBuilder.DropColumn(
                name: "SubCampaignId",
                table: "ServicesDialer");

            migrationBuilder.CreateIndex(
                name: "IX_ServicesDialer_CampaignId",
                table: "ServicesDialer",
                column: "CampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServicesDialer_campaigns_CampaignId",
                table: "ServicesDialer",
                column: "CampaignId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServicesDialer_campaigns_CampaignId",
                table: "ServicesDialer");

            migrationBuilder.DropIndex(
                name: "IX_ServicesDialer_CampaignId",
                table: "ServicesDialer");

            migrationBuilder.AddColumn<int>(
                name: "SubCampaignId",
                table: "ServicesDialer",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServicesDialer_SubCampaignId",
                table: "ServicesDialer",
                column: "SubCampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServicesDialer_campaigns_SubCampaignId",
                table: "ServicesDialer",
                column: "SubCampaignId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
