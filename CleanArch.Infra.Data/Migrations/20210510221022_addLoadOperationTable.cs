﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addLoadOperationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DatabasesClientLoadOperation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Reported = table.Column<bool>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    RowsUploaded = table.Column<int>(nullable: true),
                    ReportYear = table.Column<string>(nullable: true),
                    ReportMonth = table.Column<string>(nullable: true),
                    DatabaseClientLoadId = table.Column<int>(nullable: false),
                    SegmentId = table.Column<int>(nullable: false),
                    SubcampaignId = table.Column<int>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatabasesClientLoadOperation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DatabasesClientLoadOperation_campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DatabasesClientLoadOperation_DatabasesClientLoads_DatabaseClientLoadId",
                        column: x => x.DatabaseClientLoadId,
                        principalTable: "DatabasesClientLoads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DatabasesClientLoadOperation_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DatabasesClientLoadOperation_SubCampaigns_SubcampaignId",
                        column: x => x.SubcampaignId,
                        principalTable: "SubCampaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DatabasesClientLoadOperation_CampaignId",
                table: "DatabasesClientLoadOperation",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_DatabasesClientLoadOperation_DatabaseClientLoadId",
                table: "DatabasesClientLoadOperation",
                column: "DatabaseClientLoadId");

            migrationBuilder.CreateIndex(
                name: "IX_DatabasesClientLoadOperation_SegmentId",
                table: "DatabasesClientLoadOperation",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DatabasesClientLoadOperation_SubcampaignId",
                table: "DatabasesClientLoadOperation",
                column: "SubcampaignId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DatabasesClientLoadOperation");
        }
    }
}
