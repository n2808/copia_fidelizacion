﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFieldsModelsActiveGestionAndGestionReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SkillAvaya",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SkillAvaya",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SkillAvaya",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "SkillAvaya",
                table: "ActiveGestions");
        }
    }
}
