﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFieldsRowsDuplicateandRowsUnsuitableinmodelDatabasesClientLoad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RowsDuplicate",
                table: "DatabasesClientLoads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RowsUnsuitable",
                table: "DatabasesClientLoads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RowsDuplicate",
                table: "DatabasesClientLoads");

            migrationBuilder.DropColumn(
                name: "RowsUnsuitable",
                table: "DatabasesClientLoads");
        }
    }
}
