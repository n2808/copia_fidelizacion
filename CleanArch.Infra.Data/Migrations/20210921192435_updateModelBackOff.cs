﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class updateModelBackOff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "InitValidation",
                table: "BackofficeManagements",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndValidation",
                table: "BackofficeManagements",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<Guid>(
                name: "StatusId",
                table: "BackofficeManagements",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_StatusId",
                table: "BackofficeManagements",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_BackofficeManagements_Status_StatusId",
                table: "BackofficeManagements",
                column: "StatusId",
                principalTable: "Status",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackofficeManagements_Status_StatusId",
                table: "BackofficeManagements");

            migrationBuilder.DropIndex(
                name: "IX_BackofficeManagements_StatusId",
                table: "BackofficeManagements");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "BackofficeManagements");

            migrationBuilder.AlterColumn<DateTime>(
                name: "InitValidation",
                table: "BackofficeManagements",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndValidation",
                table: "BackofficeManagements",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
