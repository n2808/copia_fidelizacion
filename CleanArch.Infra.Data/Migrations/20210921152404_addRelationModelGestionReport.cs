﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addRelationModelGestionReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_GestionReports_CreatedBy",
                table: "GestionReports",
                column: "CreatedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_GestionReports_Users_CreatedBy",
                table: "GestionReports",
                column: "CreatedBy",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GestionReports_Users_CreatedBy",
                table: "GestionReports");

            migrationBuilder.DropIndex(
                name: "IX_GestionReports_CreatedBy",
                table: "GestionReports");
        }
    }
}
