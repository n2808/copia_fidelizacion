﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class createTableIntermediateDepartments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Departaments_SubCampaigns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    DepartamentId = table.Column<int>(nullable: false),
                    SubCampaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departaments_SubCampaigns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departaments_SubCampaigns_Departments_DepartamentId",
                        column: x => x.DepartamentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Departaments_SubCampaigns_SubCampaigns_SubCampaignId",
                        column: x => x.SubCampaignId,
                        principalTable: "SubCampaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Departaments_SubCampaigns_DepartamentId",
                table: "Departaments_SubCampaigns",
                column: "DepartamentId");

            migrationBuilder.CreateIndex(
                name: "IX_Departaments_SubCampaigns_SubCampaignId",
                table: "Departaments_SubCampaigns",
                column: "SubCampaignId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Departaments_SubCampaigns");
        }
    }
}
