﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class EntryDatafieldfrom13to30isaddedinActiveGestionandGestionReportsmodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EntryData13",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData14",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData15",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData16",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData17",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData18",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData19",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData20",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData21",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData22",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData23",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData24",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData25",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData26",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData27",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData28",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData29",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData30",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData13",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData14",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData15",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData16",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData17",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData18",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData19",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData20",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData21",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData22",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData23",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData24",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData25",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData26",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData27",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData28",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData29",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData30",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntryData13",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData14",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData15",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData16",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData17",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData18",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData19",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData20",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData21",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData22",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData23",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData24",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData25",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData26",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData27",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData28",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData29",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData30",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData13",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData14",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData15",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData16",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData17",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData18",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData19",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData20",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData21",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData22",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData23",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData24",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData25",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData26",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData27",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData28",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData29",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData30",
                table: "ActiveGestions");
        }
    }
}
