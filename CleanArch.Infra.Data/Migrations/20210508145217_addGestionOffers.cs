﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addGestionOffers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AcceptOffer",
                table: "Tipification",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GestionReportsOffers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    GestionReportsId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    value = table.Column<int>(nullable: true),
                    status = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GestionReportsOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GestionReportsOffers_GestionReports_GestionReportsId",
                        column: x => x.GestionReportsId,
                        principalTable: "GestionReports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "SegmentAppNotifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    AppId = table.Column<int>(nullable: false),
                    SegmentId = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CustomName = table.Column<string>(nullable: true),
                    CustomData1 = table.Column<string>(nullable: true),
                    CustomData2 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegmentAppNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SegmentAppNotifications_AppNotification_AppId",
                        column: x => x.AppId,
                        principalTable: "AppNotification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SegmentAppNotifications_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GestionReportsOffers_GestionReportsId",
                table: "GestionReportsOffers",
                column: "GestionReportsId");

            migrationBuilder.CreateIndex(
                name: "IX_SegmentAppNotifications_AppId",
                table: "SegmentAppNotifications",
                column: "AppId");

            migrationBuilder.CreateIndex(
                name: "IX_SegmentAppNotifications_SegmentId",
                table: "SegmentAppNotifications",
                column: "SegmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GestionReportsOffers");

            migrationBuilder.DropTable(
                name: "SegmentAppNotifications");

            migrationBuilder.DropColumn(
                name: "AcceptOffer",
                table: "Tipification");
        }
    }
}
