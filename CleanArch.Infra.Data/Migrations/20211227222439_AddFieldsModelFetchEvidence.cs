﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFieldsModelFetchEvidence : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "FetchFaceEvidence",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "FetchFaceEvidence",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "FetchFaceEvidence");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "FetchFaceEvidence");
        }
    }
}
