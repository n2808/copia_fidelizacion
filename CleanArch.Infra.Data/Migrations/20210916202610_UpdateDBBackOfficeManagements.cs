﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateDBBackOfficeManagements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "BackofficeManagements",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    ManagementId = table.Column<int>(nullable: false),
                    TypificationId = table.Column<int>(nullable: true),
                    TypificationName = table.Column<string>(nullable: true),
                    TypeOperation = table.Column<string>(nullable: false),
                    Operation = table.Column<string>(nullable: false),
                    Observation = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false),
                    TypificationBackId = table.Column<int>(nullable: true),
                    TypificationdBackId = table.Column<int>(nullable: true),
                    TypificationNameBack = table.Column<string>(nullable: true),
                    ObservationBack = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    InitValidation = table.Column<DateTime>(nullable: false),
                    EndValidation = table.Column<DateTime>(nullable: false),
                    Process = table.Column<string>(nullable: true),
                    GestionId = table.Column<int>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackofficeManagements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BackofficeManagements_ActiveGestions_ManagementId",
                        column: x => x.ManagementId,
                        principalTable: "ActiveGestions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BackofficeManagements_GestionReports_ManagementId",
                        column: x => x.ManagementId,
                        principalTable: "GestionReports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BackofficeManagements_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BackofficeManagements_Tipification_TypificationId",
                        column: x => x.TypificationId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BackofficeManagements_Tipification_TypificationdBackId",
                        column: x => x.TypificationdBackId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_ManagementId",
                table: "BackofficeManagements",
                column: "ManagementId");

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_SegmentId",
                table: "BackofficeManagements",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_TypificationId",
                table: "BackofficeManagements",
                column: "TypificationId");

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_TypificationdBackId",
                table: "BackofficeManagements",
                column: "TypificationdBackId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BackofficeManagements");

            migrationBuilder.CreateTable(
                name: "ManagementsBackOffice",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedByName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndValidation = table.Column<DateTime>(type: "datetime2", nullable: false),
                    InitValidation = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ManagementId = table.Column<int>(type: "int", nullable: false),
                    Observation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ObservationBack = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Operation = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Process = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SegmentId = table.Column<int>(type: "int", nullable: false),
                    TypeOperation = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TypificationBackId = table.Column<int>(type: "int", nullable: true),
                    TypificationId = table.Column<int>(type: "int", nullable: true),
                    TypificationName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TypificationNameBack = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TypificationdBackId = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    UpdatedByName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManagementsBackOffice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManagementsBackOffice_ActiveGestions_ManagementId",
                        column: x => x.ManagementId,
                        principalTable: "ActiveGestions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ManagementsBackOffice_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ManagementsBackOffice_Tipification_TypificationId",
                        column: x => x.TypificationId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ManagementsBackOffice_Tipification_TypificationdBackId",
                        column: x => x.TypificationdBackId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ManagementsBackOffice_ManagementId",
                table: "ManagementsBackOffice",
                column: "ManagementId");

            migrationBuilder.CreateIndex(
                name: "IX_ManagementsBackOffice_SegmentId",
                table: "ManagementsBackOffice",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ManagementsBackOffice_TypificationId",
                table: "ManagementsBackOffice",
                column: "TypificationId");

            migrationBuilder.CreateIndex(
                name: "IX_ManagementsBackOffice_TypificationdBackId",
                table: "ManagementsBackOffice",
                column: "TypificationdBackId");
        }
    }
}
