﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addDatabaseReports : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "databaseClientReports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Account = table.Column<string>(nullable: true),
                    UniqueId = table.Column<string>(nullable: true),
                    AsignedId = table.Column<string>(nullable: true),
                    CustomerCode = table.Column<string>(nullable: true),
                    BaseName = table.Column<string>(nullable: true),
                    ParnerName = table.Column<string>(nullable: true),
                    AccountType = table.Column<string>(nullable: true),
                    AccountStatus = table.Column<string>(nullable: true),
                    Names = table.Column<string>(nullable: true),
                    LastNames = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    DocumentType = table.Column<string>(nullable: true),
                    Document = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    SocialClass = table.Column<string>(nullable: true),
                    DialerPhone1 = table.Column<string>(nullable: true),
                    DialerPhone2 = table.Column<string>(nullable: true),
                    DialerPhone3 = table.Column<string>(nullable: true),
                    DialerPhone4 = table.Column<string>(nullable: true),
                    DialerPhone5 = table.Column<string>(nullable: true),
                    DialerPhone6 = table.Column<string>(nullable: true),
                    DialerPhone7 = table.Column<string>(nullable: true),
                    DialerServiceCode = table.Column<string>(nullable: true),
                    LoadId = table.Column<int>(nullable: false),
                    LoadName = table.Column<string>(nullable: true),
                    Offer1 = table.Column<string>(nullable: true),
                    Offer2 = table.Column<string>(nullable: true),
                    Offer3 = table.Column<string>(nullable: true),
                    Offer4 = table.Column<string>(nullable: true),
                    Offer5 = table.Column<string>(nullable: true),
                    Offer6 = table.Column<string>(nullable: true),
                    Offer7 = table.Column<string>(nullable: true),
                    Field1 = table.Column<string>(nullable: true),
                    Field2 = table.Column<string>(nullable: true),
                    Field3 = table.Column<string>(nullable: true),
                    Field4 = table.Column<string>(nullable: true),
                    Field5 = table.Column<string>(nullable: true),
                    Field6 = table.Column<string>(nullable: true),
                    Field7 = table.Column<string>(nullable: true),
                    Field8 = table.Column<string>(nullable: true),
                    Field9 = table.Column<string>(nullable: true),
                    Field10 = table.Column<string>(nullable: true),
                    Field11 = table.Column<string>(nullable: true),
                    Field12 = table.Column<string>(nullable: true),
                    Field13 = table.Column<string>(nullable: true),
                    Field14 = table.Column<string>(nullable: true),
                    Field15 = table.Column<string>(nullable: true),
                    Field16 = table.Column<string>(nullable: true),
                    Field17 = table.Column<string>(nullable: true),
                    Field18 = table.Column<string>(nullable: true),
                    Field19 = table.Column<string>(nullable: true),
                    Field20 = table.Column<string>(nullable: true),
                    Field21 = table.Column<string>(nullable: true),
                    Field22 = table.Column<string>(nullable: true),
                    Field23 = table.Column<string>(nullable: true),
                    Field24 = table.Column<string>(nullable: true),
                    Field25 = table.Column<string>(nullable: true),
                    Field26 = table.Column<string>(nullable: true),
                    Field27 = table.Column<string>(nullable: true),
                    Field28 = table.Column<string>(nullable: true),
                    Field29 = table.Column<string>(nullable: true),
                    Field30 = table.Column<string>(nullable: true),
                    Field31 = table.Column<string>(nullable: true),
                    Field32 = table.Column<string>(nullable: true),
                    Field33 = table.Column<string>(nullable: true),
                    Field34 = table.Column<string>(nullable: true),
                    Field35 = table.Column<string>(nullable: true),
                    Field36 = table.Column<string>(nullable: true),
                    Field37 = table.Column<string>(nullable: true),
                    Field38 = table.Column<string>(nullable: true),
                    Field39 = table.Column<string>(nullable: true),
                    Field40 = table.Column<string>(nullable: true),
                    Field41 = table.Column<string>(nullable: true),
                    Field42 = table.Column<string>(nullable: true),
                    Field43 = table.Column<string>(nullable: true),
                    Field44 = table.Column<string>(nullable: true),
                    Field45 = table.Column<string>(nullable: true),
                    Field46 = table.Column<string>(nullable: true),
                    Field47 = table.Column<string>(nullable: true),
                    Field48 = table.Column<string>(nullable: true),
                    Field49 = table.Column<string>(nullable: true),
                    Field50 = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    SubcampaignId = table.Column<int>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_databaseClientReports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_databaseClientReports_campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_databaseClientReports_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_databaseClientReports_SubCampaigns_SubcampaignId",
                        column: x => x.SubcampaignId,
                        principalTable: "SubCampaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_databaseClientReports_CampaignId",
                table: "databaseClientReports",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_databaseClientReports_SegmentId",
                table: "databaseClientReports",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_databaseClientReports_SubcampaignId",
                table: "databaseClientReports",
                column: "SubcampaignId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "databaseClientReports");
        }
    }
}
