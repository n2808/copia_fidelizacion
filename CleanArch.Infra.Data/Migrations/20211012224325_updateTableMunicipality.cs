﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class updateTableMunicipality : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Municipalities_SubCampaigns_SubCampaignId",
                table: "Municipalities");

            migrationBuilder.DropIndex(
                name: "IX_Municipalities_SubCampaignId",
                table: "Municipalities");

            migrationBuilder.DropColumn(
                name: "SubCampaignId",
                table: "Municipalities");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubCampaignId",
                table: "Municipalities",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Municipalities_SubCampaignId",
                table: "Municipalities",
                column: "SubCampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_Municipalities_SubCampaigns_SubCampaignId",
                table: "Municipalities",
                column: "SubCampaignId",
                principalTable: "SubCampaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
