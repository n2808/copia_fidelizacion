﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class Modelofplansadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Plans",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    Price = table.Column<double>(maxLength: 20, nullable: false),
                    PriceWithTax = table.Column<double>(maxLength: 20, nullable: false),
                    Currency = table.Column<string>(maxLength: 3, nullable: false),
                    MinutesIncluded = table.Column<string>(nullable: true),
                    MinutesIncludedLDI = table.Column<string>(nullable: true),
                    SMSIncluded = table.Column<string>(nullable: true),
                    SMSIncludedLDI = table.Column<string>(nullable: true),
                    APPSIncluided = table.Column<string>(nullable: true),
                    TMCode = table.Column<string>(maxLength: 30, nullable: true),
                    MessagePrice = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    UserUpdate = table.Column<string>(maxLength: 255, nullable: true),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plans", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Plans");
        }
    }
}
