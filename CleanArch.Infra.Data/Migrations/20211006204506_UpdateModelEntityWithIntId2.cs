﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateModelEntityWithIntId2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "Tipification",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "Surveys",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "SurveyResponses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "SurveyQuestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "SubCampaigns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "Skills",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "ServicesDialer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "Segments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "SegmentAppNotifications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "rescheduleHistories",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "NotificationLog",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "NotificationAccount",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "GestionStructure",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "GestionReportsOffers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "DatabasesClientLoads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "DatabasesClientLoadOperation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "ClaroUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "campaigns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "BulkStructure",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "BackOfficeOperations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "AppNotification",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedByName",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Tipification");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Surveys");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SurveyResponses");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SurveyQuestions");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SubCampaigns");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Skills");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "ServicesDialer");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Segments");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SegmentAppNotifications");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "rescheduleHistories");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "NotificationLog");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "NotificationAccount");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "GestionStructure");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "GestionReportsOffers");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "DatabasesClientLoads");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "DatabasesClientLoadOperation");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "campaigns");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "BulkStructure");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "BackOfficeOperations");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "AppNotification");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "ActiveGestions");
        }
    }
}
