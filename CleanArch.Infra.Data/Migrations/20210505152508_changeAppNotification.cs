﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class changeAppNotification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Caracteristica1",
                table: "AppNotification");

            migrationBuilder.DropColumn(
                name: "Caracteristica2",
                table: "AppNotification");

            migrationBuilder.DropColumn(
                name: "Caracteristica3",
                table: "AppNotification");

            migrationBuilder.AddColumn<string>(
                name: "Name2",
                table: "AppNotification",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "feature1",
                table: "AppNotification",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "feature2",
                table: "AppNotification",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "feature3",
                table: "AppNotification",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name2",
                table: "AppNotification");

            migrationBuilder.DropColumn(
                name: "feature1",
                table: "AppNotification");

            migrationBuilder.DropColumn(
                name: "feature2",
                table: "AppNotification");

            migrationBuilder.DropColumn(
                name: "feature3",
                table: "AppNotification");

            migrationBuilder.AddColumn<string>(
                name: "Caracteristica1",
                table: "AppNotification",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Caracteristica2",
                table: "AppNotification",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Caracteristica3",
                table: "AppNotification",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
