﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addTableHistoryBackOfficeManagements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BackOfficeManagementsHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    ManagementId = table.Column<int>(nullable: true),
                    TypificationId = table.Column<int>(nullable: true),
                    TypificationName = table.Column<string>(nullable: true),
                    TypeOperation = table.Column<string>(nullable: true),
                    Operation = table.Column<string>(nullable: true),
                    Observation = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: true),
                    TypificationBackId = table.Column<int>(nullable: true),
                    TypificationNameBack = table.Column<string>(nullable: true),
                    ObservationBack = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    InitValidation = table.Column<DateTime>(nullable: true),
                    EndValidation = table.Column<DateTime>(nullable: true),
                    Process = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Count = table.Column<int>(nullable: false),
                    StatusId = table.Column<Guid>(nullable: true),
                    StatusName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackOfficeManagementsHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BackOfficeManagementsHistories_GestionReports_ManagementId",
                        column: x => x.ManagementId,
                        principalTable: "GestionReports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BackOfficeManagementsHistories_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BackOfficeManagementsHistories_Status_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Status",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BackOfficeManagementsHistories_Tipification_TypificationBackId",
                        column: x => x.TypificationBackId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BackOfficeManagementsHistories_Tipification_TypificationId",
                        column: x => x.TypificationId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BackOfficeManagementsHistories_ManagementId",
                table: "BackOfficeManagementsHistories",
                column: "ManagementId");

            migrationBuilder.CreateIndex(
                name: "IX_BackOfficeManagementsHistories_SegmentId",
                table: "BackOfficeManagementsHistories",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_BackOfficeManagementsHistories_StatusId",
                table: "BackOfficeManagementsHistories",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BackOfficeManagementsHistories_TypificationBackId",
                table: "BackOfficeManagementsHistories",
                column: "TypificationBackId");

            migrationBuilder.CreateIndex(
                name: "IX_BackOfficeManagementsHistories_TypificationId",
                table: "BackOfficeManagementsHistories",
                column: "TypificationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BackOfficeManagementsHistories");
        }
    }
}
