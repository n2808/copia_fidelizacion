﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddGestionData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EntryContact1",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryContact2",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData1",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData10",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData11",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData12",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData2",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData3",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData4",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData5",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData6",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData7",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData8",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData9",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryCity",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryName",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entrydocument",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entrystate",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryContact1",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryContact2",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData1",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData10",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData11",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData12",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData2",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData3",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData4",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData5",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData6",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData7",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData8",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData9",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryCity",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryName",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entrydocument",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entrystate",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntryContact1",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryContact2",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData1",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData10",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData11",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData12",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData2",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData3",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData4",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData5",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData6",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData7",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData8",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData9",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "entryCity",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "entryName",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "entrydocument",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "entrystate",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryContact1",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryContact2",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData1",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData10",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData11",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData12",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData2",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData3",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData4",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData5",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData6",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData7",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData8",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData9",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "entryCity",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "entryName",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "entrydocument",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "entrystate",
                table: "ActiveGestions");
        }
    }
}
