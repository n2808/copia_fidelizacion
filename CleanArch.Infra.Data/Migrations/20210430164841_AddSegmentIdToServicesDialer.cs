﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddSegmentIdToServicesDialer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SegmentId",
                table: "ServicesDialer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServicesDialer_SegmentId",
                table: "ServicesDialer",
                column: "SegmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServicesDialer_Segments_SegmentId",
                table: "ServicesDialer",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServicesDialer_Segments_SegmentId",
                table: "ServicesDialer");

            migrationBuilder.DropIndex(
                name: "IX_ServicesDialer_SegmentId",
                table: "ServicesDialer");

            migrationBuilder.DropColumn(
                name: "SegmentId",
                table: "ServicesDialer");
        }
    }
}
