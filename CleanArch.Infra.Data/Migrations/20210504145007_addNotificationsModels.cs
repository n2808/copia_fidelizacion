﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addNotificationsModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppNotification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Caracteristica1 = table.Column<string>(nullable: true),
                    Caracteristica2 = table.Column<string>(nullable: true),
                    Caracteristica3 = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppNotification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NotificationAccount",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TypeNotification = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    Account = table.Column<string>(nullable: true),
                    Apikey = table.Column<string>(nullable: true),
                    token = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    customData1 = table.Column<string>(nullable: true),
                    customData2 = table.Column<string>(nullable: true),
                    customData3 = table.Column<string>(nullable: true),
                    customData4 = table.Column<string>(nullable: true),
                    customData5 = table.Column<string>(nullable: true),
                    customData6 = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationAccount", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NotificationLogService",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    Sendby = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    flash = table.Column<string>(nullable: true),
                    customdata1 = table.Column<string>(nullable: true),
                    customdata2 = table.Column<string>(nullable: true),
                    AccountName = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    SubCampaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationLogService", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppNotification");

            migrationBuilder.DropTable(
                name: "NotificationAccount");

            migrationBuilder.DropTable(
                name: "NotificationLogService");
        }
    }
}
