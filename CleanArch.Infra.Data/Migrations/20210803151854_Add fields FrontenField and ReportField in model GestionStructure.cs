﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddfieldsFrontenFieldandReportFieldinmodelGestionStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FrontendField",
                table: "GestionStructure",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReportField",
                table: "GestionStructure",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FrontendField",
                table: "GestionStructure");

            migrationBuilder.DropColumn(
                name: "ReportField",
                table: "GestionStructure");
        }
    }
}
