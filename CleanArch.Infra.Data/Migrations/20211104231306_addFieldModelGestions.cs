﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addFieldModelGestions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GestionReportsHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(maxLength: 25, nullable: false),
                    Skill = table.Column<string>(nullable: true),
                    SkillAvaya = table.Column<string>(nullable: true),
                    IsRetained = table.Column<bool>(nullable: false),
                    IsSale = table.Column<bool>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    MacchineName = table.Column<string>(nullable: true),
                    TeamLeader = table.Column<string>(nullable: true),
                    Manager = table.Column<string>(nullable: true),
                    tipificacionId = table.Column<int>(nullable: true),
                    DialerServiceId = table.Column<string>(nullable: true),
                    DialerServiceClientId = table.Column<string>(nullable: true),
                    DialerServiceTypeCall = table.Column<string>(nullable: true),
                    Dialercode = table.Column<string>(nullable: true),
                    ClientName = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    Customerdocument = table.Column<string>(nullable: true),
                    entrydocument = table.Column<string>(nullable: true),
                    entryName = table.Column<string>(nullable: true),
                    EntryContact1 = table.Column<string>(nullable: true),
                    EntryContact2 = table.Column<string>(nullable: true),
                    entryCity = table.Column<string>(nullable: true),
                    entrystate = table.Column<string>(nullable: true),
                    entryObservation = table.Column<string>(nullable: true),
                    entryObservation2 = table.Column<string>(nullable: true),
                    CurrentIncome = table.Column<string>(nullable: true),
                    NextIncome = table.Column<string>(nullable: true),
                    CurrentProduct = table.Column<string>(nullable: true),
                    NextProduct = table.Column<string>(nullable: true),
                    EntryData1 = table.Column<string>(nullable: true),
                    EntryData2 = table.Column<string>(nullable: true),
                    EntryData3 = table.Column<string>(nullable: true),
                    EntryData4 = table.Column<string>(nullable: true),
                    EntryData5 = table.Column<string>(nullable: true),
                    EntryData6 = table.Column<string>(nullable: true),
                    EntryData7 = table.Column<string>(nullable: true),
                    EntryData8 = table.Column<string>(nullable: true),
                    EntryData9 = table.Column<string>(nullable: true),
                    EntryData10 = table.Column<string>(nullable: true),
                    EntryData11 = table.Column<string>(nullable: true),
                    EntryData12 = table.Column<string>(nullable: true),
                    EntryData13 = table.Column<string>(nullable: true),
                    EntryData14 = table.Column<string>(nullable: true),
                    EntryData15 = table.Column<string>(nullable: true),
                    EntryData16 = table.Column<string>(nullable: true),
                    EntryData17 = table.Column<string>(nullable: true),
                    EntryData18 = table.Column<string>(nullable: true),
                    EntryData19 = table.Column<string>(nullable: true),
                    EntryData20 = table.Column<string>(nullable: true),
                    EntryData21 = table.Column<string>(nullable: true),
                    EntryData22 = table.Column<string>(nullable: true),
                    EntryData23 = table.Column<string>(nullable: true),
                    EntryData24 = table.Column<string>(nullable: true),
                    EntryData25 = table.Column<string>(nullable: true),
                    EntryData26 = table.Column<string>(nullable: true),
                    EntryData27 = table.Column<string>(nullable: true),
                    EntryData28 = table.Column<string>(nullable: true),
                    EntryData29 = table.Column<string>(nullable: true),
                    EntryData30 = table.Column<string>(nullable: true),
                    EntryData31 = table.Column<string>(nullable: true),
                    EntryData32 = table.Column<string>(nullable: true),
                    EntryData33 = table.Column<string>(nullable: true),
                    EntryData34 = table.Column<string>(nullable: true),
                    EntryData35 = table.Column<string>(nullable: true),
                    EntryData36 = table.Column<string>(nullable: true),
                    EntryData37 = table.Column<string>(nullable: true),
                    EntryData38 = table.Column<string>(nullable: true),
                    EntryData39 = table.Column<string>(nullable: true),
                    EntryData40 = table.Column<string>(nullable: true),
                    EntryData41 = table.Column<string>(nullable: true),
                    EntryData42 = table.Column<string>(nullable: true),
                    EntryData43 = table.Column<string>(nullable: true),
                    EntryData44 = table.Column<string>(nullable: true),
                    EntryData45 = table.Column<string>(nullable: true),
                    EntryData46 = table.Column<string>(nullable: true),
                    EntryData47 = table.Column<string>(nullable: true),
                    EntryData48 = table.Column<string>(nullable: true),
                    EntryData49 = table.Column<string>(nullable: true),
                    EntryData50 = table.Column<string>(nullable: true),
                    EntryData51 = table.Column<string>(nullable: true),
                    EntryData52 = table.Column<string>(nullable: true),
                    EntryData53 = table.Column<string>(nullable: true),
                    EntryData54 = table.Column<string>(nullable: true),
                    EntryData55 = table.Column<string>(nullable: true),
                    EntryData56 = table.Column<string>(nullable: true),
                    EntryData57 = table.Column<string>(nullable: true),
                    EntryData58 = table.Column<string>(nullable: true),
                    EntryData59 = table.Column<string>(nullable: true),
                    EntryData60 = table.Column<string>(nullable: true),
                    EntryData61 = table.Column<string>(nullable: true),
                    EntryData62 = table.Column<string>(nullable: true),
                    EntryData63 = table.Column<string>(nullable: true),
                    EntryData64 = table.Column<string>(nullable: true),
                    EntryData65 = table.Column<string>(nullable: true),
                    EntryData66 = table.Column<string>(nullable: true),
                    EntryData67 = table.Column<string>(nullable: true),
                    EntryData68 = table.Column<string>(nullable: true),
                    EntryData69 = table.Column<string>(nullable: true),
                    EntryData70 = table.Column<string>(nullable: true),
                    EntryData71 = table.Column<string>(nullable: true),
                    EntryData72 = table.Column<string>(nullable: true),
                    EntryData73 = table.Column<string>(nullable: true),
                    EntryData74 = table.Column<string>(nullable: true),
                    EntryData75 = table.Column<string>(nullable: true),
                    EntryData76 = table.Column<string>(nullable: true),
                    EntryData77 = table.Column<string>(nullable: true),
                    EntryData78 = table.Column<string>(nullable: true),
                    EntryData79 = table.Column<string>(nullable: true),
                    EntryData80 = table.Column<string>(nullable: true),
                    EndAt = table.Column<DateTime>(nullable: true),
                    loadName = table.Column<string>(nullable: true),
                    loadId = table.Column<int>(nullable: true),
                    CampaignName = table.Column<string>(nullable: true),
                    SubCampaignName = table.Column<string>(nullable: true),
                    SegmentName = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    SubcampaignId = table.Column<int>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    DatabaseClientReportsId = table.Column<int>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    Process = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GestionReportsHistories", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GestionReportsHistories");

            migrationBuilder.DropColumn(
                name: "Account",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "Account",
                table: "ActiveGestions");
        }
    }
}
