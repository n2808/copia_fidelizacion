﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addFieldNotificationAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NotificationAccountId",
                table: "SegmentAppNotifications",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SegmentAppNotifications_NotificationAccountId",
                table: "SegmentAppNotifications",
                column: "NotificationAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_SegmentAppNotifications_NotificationAccount_NotificationAccountId",
                table: "SegmentAppNotifications",
                column: "NotificationAccountId",
                principalTable: "NotificationAccount",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SegmentAppNotifications_NotificationAccount_NotificationAccountId",
                table: "SegmentAppNotifications");

            migrationBuilder.DropIndex(
                name: "IX_SegmentAppNotifications_NotificationAccountId",
                table: "SegmentAppNotifications");

            migrationBuilder.DropColumn(
                name: "NotificationAccountId",
                table: "SegmentAppNotifications");
        }
    }
}
