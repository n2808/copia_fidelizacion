﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addfieldstoDatabaseTemp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Field51",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field52",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field53",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field54",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field55",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field56",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field57",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field58",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field59",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field60",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field61",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field62",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field63",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field64",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field65",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field66",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field67",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field68",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field69",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field70",
                table: "Temp_DatabaseClientDetail",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Field51",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field52",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field53",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field54",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field55",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field56",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field57",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field58",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field59",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field60",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field61",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field62",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field63",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field64",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field65",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field66",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field67",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field68",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field69",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Field70",
                table: "Temp_DatabaseClientDetail");
        }
    }
}
