﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addFieldIdentificationModelGestions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Identification",
                table: "GestionReports",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Identification",
                table: "ActiveGestions",
                maxLength: 20,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Identification",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "Identification",
                table: "ActiveGestions");
        }
    }
}
