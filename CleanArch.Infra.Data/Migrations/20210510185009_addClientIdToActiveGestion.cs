﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addClientIdToActiveGestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DatabaseClientDetailId",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActiveGestions_DatabaseClientDetailId",
                table: "ActiveGestions",
                column: "DatabaseClientDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveGestions_DatabaseClientBase_DatabaseClientDetailId",
                table: "ActiveGestions",
                column: "DatabaseClientDetailId",
                principalTable: "DatabaseClientBase",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveGestions_DatabaseClientBase_DatabaseClientDetailId",
                table: "ActiveGestions");

            migrationBuilder.DropIndex(
                name: "IX_ActiveGestions_DatabaseClientDetailId",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "DatabaseClientDetailId",
                table: "ActiveGestions");
        }
    }
}
