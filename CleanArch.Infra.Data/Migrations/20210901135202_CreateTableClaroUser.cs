﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class CreateTableClaroUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClaroUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(nullable: true),
                    Charge = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    Ring = table.Column<string>(nullable: true),
                    Supervisor = table.Column<string>(nullable: true),
                    IdentificationSupervisor = table.Column<string>(nullable: true),
                    Coordinator = table.Column<string>(nullable: true),
                    IdentificationCoordinator = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaroUsers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClaroUsers");

            migrationBuilder.DropTable(
                name: "EvidenceGestions");

            migrationBuilder.DropColumn(
                name: "FrontendField",
                table: "GestionStructure");

            migrationBuilder.DropColumn(
                name: "ReportField",
                table: "GestionStructure");
        }
    }
}
