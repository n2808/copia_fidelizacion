﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateDataBaseGestions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "IsRetained",
            //    table: "GestionReports");

            //migrationBuilder.DropColumn(
            //    name: "IsRetained",
            //    table: "ActiveGestions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsRetained",
                table: "GestionReports",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsRetained",
                table: "ActiveGestions",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
