﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class ChangeModelBackOfficeManagement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackofficeManagements_ActiveGestions_ManagementId",
                table: "BackofficeManagements");

            migrationBuilder.DropColumn(
                name: "GestionId",
                table: "BackofficeManagements");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GestionId",
                table: "BackofficeManagements",
                type: "int",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BackofficeManagements_ActiveGestions_ManagementId",
                table: "BackofficeManagements",
                column: "ManagementId",
                principalTable: "ActiveGestions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
