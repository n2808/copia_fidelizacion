﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addTypeConfigurationRelationShipt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TypeTipificationId",
                table: "Tipification",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tipification_TypeTipificationId",
                table: "Tipification",
                column: "TypeTipificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tipification_Configurations_TypeTipificationId",
                table: "Tipification",
                column: "TypeTipificationId",
                principalTable: "Configurations",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tipification_Configurations_TypeTipificationId",
                table: "Tipification");

            migrationBuilder.DropIndex(
                name: "IX_Tipification_TypeTipificationId",
                table: "Tipification");

            migrationBuilder.DropColumn(
                name: "TypeTipificationId",
                table: "Tipification");
        }
    }
}
