﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addendAttogestionReports : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EndAt",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "loadId",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "loadName",
                table: "GestionReports",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndAt",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "loadId",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "loadName",
                table: "GestionReports");
        }
    }
}
