﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class changeTipificationRelationShip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tipification_campaigns_SubCampaignId",
                table: "Tipification");

            migrationBuilder.DropColumn(
                name: "CampaignId",
                table: "Tipification");

            migrationBuilder.AlterColumn<int>(
                name: "SubCampaignId",
                table: "Tipification",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tipification_SubCampaigns_SubCampaignId",
                table: "Tipification",
                column: "SubCampaignId",
                principalTable: "SubCampaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tipification_SubCampaigns_SubCampaignId",
                table: "Tipification");

            migrationBuilder.AlterColumn<int>(
                name: "SubCampaignId",
                table: "Tipification",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CampaignId",
                table: "Tipification",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Tipification_campaigns_SubCampaignId",
                table: "Tipification",
                column: "SubCampaignId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
