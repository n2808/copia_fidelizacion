﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateClaroUSerModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "ClaroUsers",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                table: "ClaroUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByName",
                table: "ClaroUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "ClaroUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "ClaroUsers",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedBy",
                table: "ClaroUsers",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "ClaroUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "CreatedByName",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "ClaroUsers");
        }
    }
}
