﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class updateModelStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "status",
                table: "Status");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Status",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Status");

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "Status",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
