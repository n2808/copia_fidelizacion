﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddfieldsEntryDatainmodelGestionActiveandGestionReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EntryData31",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData32",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData33",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData34",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData35",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData36",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData37",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData38",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData39",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData40",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData41",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData42",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData43",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData44",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData45",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData46",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData47",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData48",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData49",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData50",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData51",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData52",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData53",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData54",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData55",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData56",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData57",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData58",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData59",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData60",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData61",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData62",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData63",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData64",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData65",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData66",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData67",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData68",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData69",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData70",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData71",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData72",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData73",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData74",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData75",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData76",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData77",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData78",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData79",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData80",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData31",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData32",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData33",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData34",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData35",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData36",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData37",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData38",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData39",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData40",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData41",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData42",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData43",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData44",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData45",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData46",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData47",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData48",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData49",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData50",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData51",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData52",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData53",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData54",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData55",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData56",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData57",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData58",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData59",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData60",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData61",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData62",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData63",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData64",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData65",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData66",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData67",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData68",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData69",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData70",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData71",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData72",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData73",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData74",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData75",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData76",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData77",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData78",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData79",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EntryData80",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntryData31",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData32",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData33",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData34",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData35",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData36",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData37",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData38",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData39",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData40",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData41",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData42",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData43",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData44",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData45",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData46",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData47",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData48",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData49",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData50",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData51",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData52",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData53",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData54",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData55",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData56",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData57",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData58",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData59",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData60",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData61",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData62",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData63",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData64",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData65",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData66",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData67",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData68",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData69",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData70",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData71",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData72",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData73",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData74",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData75",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData76",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData77",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData78",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData79",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData80",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "EntryData31",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData32",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData33",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData34",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData35",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData36",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData37",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData38",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData39",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData40",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData41",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData42",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData43",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData44",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData45",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData46",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData47",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData48",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData49",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData50",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData51",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData52",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData53",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData54",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData55",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData56",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData57",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData58",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData59",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData60",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData61",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData62",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData63",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData64",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData65",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData66",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData67",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData68",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData69",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData70",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData71",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData72",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData73",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData74",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData75",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData76",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData77",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData78",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData79",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "EntryData80",
                table: "ActiveGestions");
        }
    }
}
