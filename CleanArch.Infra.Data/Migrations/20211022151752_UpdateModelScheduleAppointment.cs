﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateModelScheduleAppointment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Ot",
                table: "ScheduledAppointments");

            migrationBuilder.AlterColumn<string>(
                name: "AppointmentDate",
                table: "ScheduledAppointments",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "AccountNumber",
                table: "ScheduledAppointments",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ScheduledAppointments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "CodeFailure",
                table: "ScheduledAppointments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailOfReceive",
                table: "ScheduledAppointments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IncidenceTD",
                table: "ScheduledAppointments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameOfReceive",
                table: "ScheduledAppointments",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ParentAccount",
                table: "ScheduledAppointments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PhoneOfReceive",
                table: "ScheduledAppointments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "VisitingDays",
                table: "ScheduledAppointments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountNumber",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "CodeFailure",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "EmailOfReceive",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "IncidenceTD",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "NameOfReceive",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "ParentAccount",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "PhoneOfReceive",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "VisitingDays",
                table: "ScheduledAppointments");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AppointmentDate",
                table: "ScheduledAppointments",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ot",
                table: "ScheduledAppointments",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
