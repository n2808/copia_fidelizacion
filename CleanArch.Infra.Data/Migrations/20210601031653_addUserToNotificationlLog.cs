﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addUserToNotificationlLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "NotificationLog",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NotificationLog_CampaignId",
                table: "NotificationLog",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_NotificationLog_SegmentId",
                table: "NotificationLog",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_NotificationLog_SubCampaignId",
                table: "NotificationLog",
                column: "SubCampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_NotificationLog_UserId",
                table: "NotificationLog",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_NotificationLog_campaigns_CampaignId",
                table: "NotificationLog",
                column: "CampaignId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_NotificationLog_Segments_SegmentId",
                table: "NotificationLog",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_NotificationLog_SubCampaigns_SubCampaignId",
                table: "NotificationLog",
                column: "SubCampaignId",
                principalTable: "SubCampaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_NotificationLog_Users_UserId",
                table: "NotificationLog",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NotificationLog_campaigns_CampaignId",
                table: "NotificationLog");

            migrationBuilder.DropForeignKey(
                name: "FK_NotificationLog_Segments_SegmentId",
                table: "NotificationLog");

            migrationBuilder.DropForeignKey(
                name: "FK_NotificationLog_SubCampaigns_SubCampaignId",
                table: "NotificationLog");

            migrationBuilder.DropForeignKey(
                name: "FK_NotificationLog_Users_UserId",
                table: "NotificationLog");

            migrationBuilder.DropIndex(
                name: "IX_NotificationLog_CampaignId",
                table: "NotificationLog");

            migrationBuilder.DropIndex(
                name: "IX_NotificationLog_SegmentId",
                table: "NotificationLog");

            migrationBuilder.DropIndex(
                name: "IX_NotificationLog_SubCampaignId",
                table: "NotificationLog");

            migrationBuilder.DropIndex(
                name: "IX_NotificationLog_UserId",
                table: "NotificationLog");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "NotificationLog");
        }
    }
}
