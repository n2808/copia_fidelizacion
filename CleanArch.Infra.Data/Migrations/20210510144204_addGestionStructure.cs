﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addGestionStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_bulkStructures_Segments_SegmentId",
                table: "bulkStructures");

            migrationBuilder.DropPrimaryKey(
                name: "PK_bulkStructures",
                table: "bulkStructures");

            migrationBuilder.RenameTable(
                name: "bulkStructures",
                newName: "BulkStructure");

            migrationBuilder.RenameIndex(
                name: "IX_bulkStructures_SegmentId",
                table: "BulkStructure",
                newName: "IX_BulkStructure_SegmentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BulkStructure",
                table: "BulkStructure",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "GestionStructure",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    DatbaseField = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Description1 = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    Code = table.Column<int>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    SegmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GestionStructure", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GestionStructure_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GestionStructure_SegmentId",
                table: "GestionStructure",
                column: "SegmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_BulkStructure_Segments_SegmentId",
                table: "BulkStructure",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BulkStructure_Segments_SegmentId",
                table: "BulkStructure");

            migrationBuilder.DropTable(
                name: "GestionStructure");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BulkStructure",
                table: "BulkStructure");

            migrationBuilder.RenameTable(
                name: "BulkStructure",
                newName: "bulkStructures");

            migrationBuilder.RenameIndex(
                name: "IX_BulkStructure_SegmentId",
                table: "bulkStructures",
                newName: "IX_bulkStructures_SegmentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_bulkStructures",
                table: "bulkStructures",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_bulkStructures_Segments_SegmentId",
                table: "bulkStructures",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
