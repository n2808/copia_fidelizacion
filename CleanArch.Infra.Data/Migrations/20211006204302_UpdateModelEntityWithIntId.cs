﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateModelEntityWithIntId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Tipification");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Surveys");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SurveyResponses");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SurveyQuestions");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SubCampaigns");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Skills");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "ServicesDialer");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "Segments");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "SegmentAppNotifications");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "rescheduleHistories");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "NotificationLog");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "NotificationAccount");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "GestionStructure");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "GestionReportsOffers");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "DatabasesClientLoads");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "DatabasesClientLoadOperation");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "ClaroUsers");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "campaigns");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "BulkStructure");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "BackOfficeOperations");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "AppNotification");

            migrationBuilder.DropColumn(
                name: "UpdatedByName",
                table: "ActiveGestions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "Tipification",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "Temp_DatabaseClientDetail",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "Surveys",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "SurveyResponses",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "SurveyQuestions",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "SubCampaigns",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "Skills",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "ServicesDialer",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "Segments",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "SegmentAppNotifications",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "rescheduleHistories",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "NotificationLog",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "NotificationAccount",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "GestionStructure",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "GestionReportsOffers",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "GestionReports",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "DatabasesClientLoads",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "DatabasesClientLoadOperation",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "databaseClientReports",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "DatabaseClientBase",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "ClaroUsers",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "campaigns",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "BulkStructure",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "BackOfficeOperations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "AppNotification",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UpdatedByName",
                table: "ActiveGestions",
                type: "uniqueidentifier",
                nullable: true);
        }
    }
}
