﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class fixdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_gestionReschedules_Segment_SegmentId",
            //    table: "gestionReschedules");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_TipificationReschedules_Campaign_CampaignId",
            //    table: "TipificationReschedules");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_TipificationReschedules_Segment_SegmentId",
            //    table: "TipificationReschedules");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_TipificationReschedules_SubCampaign_SubCampaignId",
            //    table: "TipificationReschedules");

            //migrationBuilder.DropTable(
            //    name: "Campaign");

            //migrationBuilder.DropTable(
            //    name: "Segment");

            //migrationBuilder.DropTable(
            //    name: "SubCampaign");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_gestionReschedules_Segments_SegmentId",
            //    table: "gestionReschedules",
            //    column: "SegmentId",
            //    principalTable: "Segments",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_TipificationReschedules_campaigns_CampaignId",
            //    table: "TipificationReschedules",
            //    column: "CampaignId",
            //    principalTable: "campaigns",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_TipificationReschedules_Segments_SegmentId",
            //    table: "TipificationReschedules",
            //    column: "SegmentId",
            //    principalTable: "Segments",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_TipificationReschedules_SubCampaigns_SubCampaignId",
            //    table: "TipificationReschedules",
            //    column: "SubCampaignId",
            //    principalTable: "SubCampaigns",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_gestionReschedules_Segments_SegmentId",
                table: "gestionReschedules");

            migrationBuilder.DropForeignKey(
                name: "FK_TipificationReschedules_campaigns_CampaignId",
                table: "TipificationReschedules");

            migrationBuilder.DropForeignKey(
                name: "FK_TipificationReschedules_Segments_SegmentId",
                table: "TipificationReschedules");

            migrationBuilder.DropForeignKey(
                name: "FK_TipificationReschedules_SubCampaigns_SubCampaignId",
                table: "TipificationReschedules");

            migrationBuilder.CreateTable(
                name: "Campaign",
                columns: table => new
                {
                    TempId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.UniqueConstraint("AK_Campaign_TempId", x => x.TempId);
                });

            migrationBuilder.CreateTable(
                name: "Segment",
                columns: table => new
                {
                    TempId = table.Column<int>(nullable: false),
                    TempId1 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.UniqueConstraint("AK_Segment_TempId", x => x.TempId);
                    table.UniqueConstraint("AK_Segment_TempId1", x => x.TempId1);
                });

            migrationBuilder.CreateTable(
                name: "SubCampaign",
                columns: table => new
                {
                    TempId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.UniqueConstraint("AK_SubCampaign_TempId", x => x.TempId);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_gestionReschedules_Segment_SegmentId",
                table: "gestionReschedules",
                column: "SegmentId",
                principalTable: "Segment",
                principalColumn: "TempId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TipificationReschedules_Campaign_CampaignId",
                table: "TipificationReschedules",
                column: "CampaignId",
                principalTable: "Campaign",
                principalColumn: "TempId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TipificationReschedules_Segment_SegmentId",
                table: "TipificationReschedules",
                column: "SegmentId",
                principalTable: "Segment",
                principalColumn: "TempId1",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TipificationReschedules_SubCampaign_SubCampaignId",
                table: "TipificationReschedules",
                column: "SubCampaignId",
                principalTable: "SubCampaign",
                principalColumn: "TempId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
