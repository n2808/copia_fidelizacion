﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class updateModelBackofficeManagements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ObservationValidator",
                table: "BackOfficeManagementsHistories");

            migrationBuilder.AddColumn<int>(
                name: "TypificationBackValidationId",
                table: "BackofficeManagements",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UserValidationId",
                table: "BackofficeManagements",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TypificationBackValidationId",
                table: "BackofficeManagements");

            migrationBuilder.DropColumn(
                name: "UserValidationId",
                table: "BackofficeManagements");

            migrationBuilder.AddColumn<string>(
                name: "ObservationValidator",
                table: "BackOfficeManagementsHistories",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
