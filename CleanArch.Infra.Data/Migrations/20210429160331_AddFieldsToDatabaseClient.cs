﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFieldsToDatabaseClient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Field41",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field42",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field43",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field44",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field45",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field46",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field47",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field48",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field49",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field50",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Offer7",
                table: "DatabaseClientBase",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Field41",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field42",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field43",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field44",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field45",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field46",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field47",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field48",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field49",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field50",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Offer7",
                table: "DatabaseClientBase");
        }
    }
}
