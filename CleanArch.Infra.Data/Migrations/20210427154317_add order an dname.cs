﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addorderandname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "bulkStructures",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "bulkStructures",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "bulkStructures");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "bulkStructures");
        }
    }
}
