﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "campaigns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Image1 = table.Column<string>(nullable: true),
                    Image2 = table.Column<string>(nullable: true),
                    Image3 = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    intId = table.Column<int>(nullable: true),
                    GuidId = table.Column<Guid>(nullable: true),
                    order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_campaigns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CanChanged = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "courses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Descrption = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_courses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bulkStructures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    ExcelField = table.Column<string>(nullable: false),
                    DatbaseField = table.Column<string>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bulkStructures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_bulkStructures_campaigns_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "DatabaseClientBase",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Account = table.Column<string>(nullable: true),
                    UniqueId = table.Column<string>(nullable: true),
                    AsignedId = table.Column<string>(nullable: true),
                    CustomerCode = table.Column<string>(nullable: true),
                    BaseName = table.Column<string>(nullable: true),
                    ParnerName = table.Column<string>(nullable: true),
                    AccountType = table.Column<string>(nullable: true),
                    AccountStatus = table.Column<string>(nullable: true),
                    Names = table.Column<string>(nullable: true),
                    LastNames = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    DocumentType = table.Column<string>(nullable: true),
                    Document = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    SocialClass = table.Column<string>(nullable: true),
                    DialerPhone1 = table.Column<string>(nullable: true),
                    DialerPhone2 = table.Column<string>(nullable: true),
                    DialerPhone3 = table.Column<string>(nullable: true),
                    DialerPhone4 = table.Column<string>(nullable: true),
                    DialerPhone5 = table.Column<string>(nullable: true),
                    DialerPhone6 = table.Column<string>(nullable: true),
                    DialerPhone7 = table.Column<string>(nullable: true),
                    DialerServiceCode = table.Column<string>(nullable: true),
                    LoadId = table.Column<int>(nullable: false),
                    LoadName = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    Offer1 = table.Column<string>(nullable: true),
                    Offer2 = table.Column<string>(nullable: true),
                    Offer3 = table.Column<string>(nullable: true),
                    Offer4 = table.Column<string>(nullable: true),
                    Offer5 = table.Column<string>(nullable: true),
                    Offer6 = table.Column<string>(nullable: true),
                    Field1 = table.Column<string>(nullable: true),
                    Field2 = table.Column<string>(nullable: true),
                    Field3 = table.Column<string>(nullable: true),
                    Field4 = table.Column<string>(nullable: true),
                    Field5 = table.Column<string>(nullable: true),
                    Field6 = table.Column<string>(nullable: true),
                    Field7 = table.Column<string>(nullable: true),
                    Field8 = table.Column<string>(nullable: true),
                    Field9 = table.Column<string>(nullable: true),
                    Field10 = table.Column<string>(nullable: true),
                    Field11 = table.Column<string>(nullable: true),
                    Field12 = table.Column<string>(nullable: true),
                    Field13 = table.Column<string>(nullable: true),
                    Field14 = table.Column<string>(nullable: true),
                    Field15 = table.Column<string>(nullable: true),
                    Field16 = table.Column<string>(nullable: true),
                    Field17 = table.Column<string>(nullable: true),
                    Field18 = table.Column<string>(nullable: true),
                    Field19 = table.Column<string>(nullable: true),
                    Field20 = table.Column<string>(nullable: true),
                    Field21 = table.Column<string>(nullable: true),
                    Field22 = table.Column<string>(nullable: true),
                    Field23 = table.Column<string>(nullable: true),
                    Field24 = table.Column<string>(nullable: true),
                    Field25 = table.Column<string>(nullable: true),
                    Field26 = table.Column<string>(nullable: true),
                    Field27 = table.Column<string>(nullable: true),
                    Field28 = table.Column<string>(nullable: true),
                    Field29 = table.Column<string>(nullable: true),
                    Field30 = table.Column<string>(nullable: true),
                    Field31 = table.Column<string>(nullable: true),
                    Field32 = table.Column<string>(nullable: true),
                    Field33 = table.Column<string>(nullable: true),
                    Field34 = table.Column<string>(nullable: true),
                    Field35 = table.Column<string>(nullable: true),
                    Field36 = table.Column<string>(nullable: true),
                    Field37 = table.Column<string>(nullable: true),
                    Field38 = table.Column<string>(nullable: true),
                    Field39 = table.Column<string>(nullable: true),
                    Field40 = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatabaseClientBase", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DatabaseClientBase_campaigns_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServicesDialer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    VDN = table.Column<string>(nullable: true),
                    DialrerName = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    DisplayReportName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CallTypeName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    CampaignId = table.Column<int>(nullable: false),
                    status = table.Column<bool>(nullable: false),
                    SubCampaignId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicesDialer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServicesDialer_campaigns_SubCampaignId",
                        column: x => x.SubCampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubCampaigns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Image1 = table.Column<string>(nullable: true),
                    Image2 = table.Column<string>(nullable: true),
                    Image3 = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    intId = table.Column<int>(nullable: true),
                    GuidId = table.Column<Guid>(nullable: true),
                    order = table.Column<int>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCampaigns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubCampaigns_campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Tipification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NameLinked = table.Column<string>(nullable: true),
                    CampaignId = table.Column<int>(nullable: false),
                    SegmentName = table.Column<string>(nullable: true),
                    Dialer1Code = table.Column<string>(nullable: true),
                    Dialer2Code = table.Column<string>(nullable: true),
                    Report1Code = table.Column<string>(nullable: true),
                    Report2Code = table.Column<string>(nullable: true),
                    Report3Code = table.Column<string>(nullable: true),
                    IsContacted = table.Column<string>(nullable: true),
                    IsEfective = table.Column<string>(nullable: true),
                    GestionNextStatus = table.Column<short>(nullable: false),
                    status = table.Column<bool>(nullable: false),
                    priority = table.Column<string>(nullable: true),
                    Group1 = table.Column<string>(nullable: true),
                    Group2 = table.Column<string>(nullable: true),
                    Group3 = table.Column<string>(nullable: true),
                    Group4 = table.Column<string>(nullable: true),
                    SubCampaignId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tipification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tipification_campaigns_SubCampaignId",
                        column: x => x.SubCampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Configurations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OrderId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CategoriesId = table.Column<Guid>(nullable: false),
                    GuidId = table.Column<Guid>(nullable: true),
                    IntId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Configurations_Categories_CategoriesId",
                        column: x => x.CategoriesId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "states",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_states", x => x.Id);
                    table.ForeignKey(
                        name: "FK_states_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Segments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ValidationScript = table.Column<string>(nullable: true),
                    LoadScript = table.Column<string>(nullable: true),
                    SubCampaignId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Segments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Segments_SubCampaigns_SubCampaignId",
                        column: x => x.SubCampaignId,
                        principalTable: "SubCampaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ServicesDialerTipification",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    ServiceDialerName = table.Column<string>(nullable: true),
                    ServiceDialerId = table.Column<int>(nullable: false),
                    TipicationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicesDialerTipification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServicesDialerTipification_ServicesDialer_ServiceDialerId",
                        column: x => x.ServiceDialerId,
                        principalTable: "ServicesDialer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ServicesDialerTipification_Tipification_TipicationId",
                        column: x => x.TipicationId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    StateId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cities_states_StateId",
                        column: x => x.StateId,
                        principalTable: "states",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "DatabasesClientLoads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    Reported = table.Column<bool>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    RowsUploaded = table.Column<int>(nullable: true),
                    ReportYear = table.Column<string>(nullable: true),
                    ReportMonth = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatabasesClientLoads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DatabasesClientLoads_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_bulkStructures_SegmentId",
                table: "bulkStructures",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Cities_StateId",
                table: "Cities",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Configurations_CategoriesId",
                table: "Configurations",
                column: "CategoriesId");

            migrationBuilder.CreateIndex(
                name: "IX_DatabaseClientBase_SegmentId",
                table: "DatabaseClientBase",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DatabasesClientLoads_SegmentId",
                table: "DatabasesClientLoads",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Segments_SubCampaignId",
                table: "Segments",
                column: "SubCampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_ServicesDialer_SubCampaignId",
                table: "ServicesDialer",
                column: "SubCampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_ServicesDialerTipification_ServiceDialerId",
                table: "ServicesDialerTipification",
                column: "ServiceDialerId");

            migrationBuilder.CreateIndex(
                name: "IX_ServicesDialerTipification_TipicationId",
                table: "ServicesDialerTipification",
                column: "TipicationId");

            migrationBuilder.CreateIndex(
                name: "IX_states_CountryId",
                table: "states",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_SubCampaigns_CampaignId",
                table: "SubCampaigns",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_Tipification_SubCampaignId",
                table: "Tipification",
                column: "SubCampaignId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bulkStructures");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Configurations");

            migrationBuilder.DropTable(
                name: "courses");

            migrationBuilder.DropTable(
                name: "DatabaseClientBase");

            migrationBuilder.DropTable(
                name: "DatabasesClientLoads");

            migrationBuilder.DropTable(
                name: "ServicesDialerTipification");

            migrationBuilder.DropTable(
                name: "states");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "Segments");

            migrationBuilder.DropTable(
                name: "ServicesDialer");

            migrationBuilder.DropTable(
                name: "Tipification");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "SubCampaigns");

            migrationBuilder.DropTable(
                name: "campaigns");
        }
    }
}
