﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addForeignKeyModelBackOfficeManagements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SecondRingLoadId",
                table: "BackOfficeManagementsHistories",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SecondRingLoadId",
                table: "BackofficeManagements",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BackOfficeManagementsHistories_SecondRingLoadId",
                table: "BackOfficeManagementsHistories",
                column: "SecondRingLoadId");

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_SecondRingLoadId",
                table: "BackofficeManagements",
                column: "SecondRingLoadId");

            migrationBuilder.AddForeignKey(
                name: "FK_BackofficeManagements_SecondRingLoads_SecondRingLoadId",
                table: "BackofficeManagements",
                column: "SecondRingLoadId",
                principalTable: "SecondRingLoads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BackOfficeManagementsHistories_SecondRingLoads_SecondRingLoadId",
                table: "BackOfficeManagementsHistories",
                column: "SecondRingLoadId",
                principalTable: "SecondRingLoads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackofficeManagements_SecondRingLoads_SecondRingLoadId",
                table: "BackofficeManagements");

            migrationBuilder.DropForeignKey(
                name: "FK_BackOfficeManagementsHistories_SecondRingLoads_SecondRingLoadId",
                table: "BackOfficeManagementsHistories");

            migrationBuilder.DropIndex(
                name: "IX_BackOfficeManagementsHistories_SecondRingLoadId",
                table: "BackOfficeManagementsHistories");

            migrationBuilder.DropIndex(
                name: "IX_BackofficeManagements_SecondRingLoadId",
                table: "BackofficeManagements");

            migrationBuilder.DropColumn(
                name: "SecondRingLoadId",
                table: "BackOfficeManagementsHistories");

            migrationBuilder.DropColumn(
                name: "SecondRingLoadId",
                table: "BackofficeManagements");
        }
    }
}
