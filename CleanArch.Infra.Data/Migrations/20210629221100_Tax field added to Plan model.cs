﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class TaxfieldaddedtoPlanmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "MessagePrice",
                table: "Plans",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit");

            migrationBuilder.AddColumn<string>(
                name: "Tax",
                table: "Plans",
                maxLength: 5,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tax",
                table: "Plans");

            migrationBuilder.AlterColumn<bool>(
                name: "MessagePrice",
                table: "Plans",
                type: "bit",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
