﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class ChangeRelationSegmentIdBUlkstructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_bulkStructures_campaigns_SegmentId",
                table: "bulkStructures");

            migrationBuilder.AddForeignKey(
                name: "FK_bulkStructures_Segments_SegmentId",
                table: "bulkStructures",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_bulkStructures_Segments_SegmentId",
                table: "bulkStructures");

            migrationBuilder.AddForeignKey(
                name: "FK_bulkStructures_campaigns_SegmentId",
                table: "bulkStructures",
                column: "SegmentId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
