﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFieldsToClientsDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Field51",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field52",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field53",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field54",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field55",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field56",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field57",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field58",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field59",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field60",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field61",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field62",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field63",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field64",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field65",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field66",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field67",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field68",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field69",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field70",
                table: "databaseClientReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field51",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field52",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field53",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field54",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field55",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field56",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field57",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field58",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field59",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field60",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field61",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field62",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field63",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field64",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field65",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field66",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field67",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field68",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field69",
                table: "DatabaseClientBase",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Field70",
                table: "DatabaseClientBase",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Field51",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field52",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field53",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field54",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field55",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field56",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field57",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field58",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field59",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field60",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field61",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field62",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field63",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field64",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field65",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field66",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field67",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field68",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field69",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field70",
                table: "databaseClientReports");

            migrationBuilder.DropColumn(
                name: "Field51",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field52",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field53",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field54",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field55",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field56",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field57",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field58",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field59",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field60",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field61",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field62",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field63",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field64",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field65",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field66",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field67",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field68",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field69",
                table: "DatabaseClientBase");

            migrationBuilder.DropColumn(
                name: "Field70",
                table: "DatabaseClientBase");
        }
    }
}
