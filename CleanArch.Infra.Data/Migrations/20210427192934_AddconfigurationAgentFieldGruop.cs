﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddconfigurationAgentFieldGruop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DatabaseClientBase_campaigns_SegmentId",
                table: "DatabaseClientBase");

            migrationBuilder.DropForeignKey(
                name: "FK_InformativeAgentFields_campaigns_SegmentId",
                table: "InformativeAgentFields");

            migrationBuilder.DropColumn(
                name: "InformationGroup",
                table: "InformativeAgentFields");

            migrationBuilder.AddColumn<Guid>(
                name: "InformationGroupId",
                table: "InformativeAgentFields",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_InformativeAgentFields_InformationGroupId",
                table: "InformativeAgentFields",
                column: "InformationGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_DatabaseClientBase_Segments_SegmentId",
                table: "DatabaseClientBase",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_InformativeAgentFields_Configurations_InformationGroupId",
                table: "InformativeAgentFields",
                column: "InformationGroupId",
                principalTable: "Configurations",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_InformativeAgentFields_Segments_SegmentId",
                table: "InformativeAgentFields",
                column: "SegmentId",
                principalTable: "Segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DatabaseClientBase_Segments_SegmentId",
                table: "DatabaseClientBase");

            migrationBuilder.DropForeignKey(
                name: "FK_InformativeAgentFields_Configurations_InformationGroupId",
                table: "InformativeAgentFields");

            migrationBuilder.DropForeignKey(
                name: "FK_InformativeAgentFields_Segments_SegmentId",
                table: "InformativeAgentFields");

            migrationBuilder.DropIndex(
                name: "IX_InformativeAgentFields_InformationGroupId",
                table: "InformativeAgentFields");

            migrationBuilder.DropColumn(
                name: "InformationGroupId",
                table: "InformativeAgentFields");

            migrationBuilder.AddColumn<Guid>(
                name: "InformationGroup",
                table: "InformativeAgentFields",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddForeignKey(
                name: "FK_DatabaseClientBase_campaigns_SegmentId",
                table: "DatabaseClientBase",
                column: "SegmentId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InformativeAgentFields_campaigns_SegmentId",
                table: "InformativeAgentFields",
                column: "SegmentId",
                principalTable: "campaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
