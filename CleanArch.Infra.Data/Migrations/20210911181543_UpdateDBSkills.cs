﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateDBSkills : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Activate",
                table: "Skills");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Skills",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Segment",
                table: "Skills",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Skills");

            migrationBuilder.DropColumn(
                name: "Segment",
                table: "Skills");

            migrationBuilder.AddColumn<bool>(
                name: "Activate",
                table: "Skills",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
