﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddfieldsCutinmodelDatabaseClientReportsandTemp_DatabaseClientDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Cut",
                table: "Temp_DatabaseClientDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Cut",
                table: "databaseClientReports",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cut",
                table: "Temp_DatabaseClientDetail");

            migrationBuilder.DropColumn(
                name: "Cut",
                table: "databaseClientReports");
        }
    }
}
