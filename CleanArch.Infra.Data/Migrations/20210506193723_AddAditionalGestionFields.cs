﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddAditionalGestionFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Scritp1",
                table: "Segments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Scritp2",
                table: "Segments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentIncome",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentProduct",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NextIncome",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NextProduct",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryObservation",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryObservation2",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentIncome",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentProduct",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NextIncome",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NextProduct",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryObservation",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "entryObservation2",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Scritp1",
                table: "Segments");

            migrationBuilder.DropColumn(
                name: "Scritp2",
                table: "Segments");

            migrationBuilder.DropColumn(
                name: "CurrentIncome",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "CurrentProduct",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "NextIncome",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "NextProduct",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "entryObservation",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "entryObservation2",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "CurrentIncome",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "CurrentProduct",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "NextIncome",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "NextProduct",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "entryObservation",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "entryObservation2",
                table: "ActiveGestions");
        }
    }
}
