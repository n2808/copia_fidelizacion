﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addSourceId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DialerServiceClientId",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DialerServiceId",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DialerServiceClientId",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DialerServiceId",
                table: "ActiveGestions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DialerServiceClientId",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "DialerServiceId",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "DialerServiceClientId",
                table: "ActiveGestions");

            migrationBuilder.DropColumn(
                name: "DialerServiceId",
                table: "ActiveGestions");
        }
    }
}
