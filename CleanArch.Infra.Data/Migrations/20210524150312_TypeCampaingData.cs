﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class TypeCampaingData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDigital",
                table: "SubCampaigns",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsInbound",
                table: "SubCampaigns",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOutbound",
                table: "SubCampaigns",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDigital",
                table: "Segments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsInbound",
                table: "Segments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOutbound",
                table: "Segments",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDigital",
                table: "SubCampaigns");

            migrationBuilder.DropColumn(
                name: "IsInbound",
                table: "SubCampaigns");

            migrationBuilder.DropColumn(
                name: "IsOutbound",
                table: "SubCampaigns");

            migrationBuilder.DropColumn(
                name: "IsDigital",
                table: "Segments");

            migrationBuilder.DropColumn(
                name: "IsInbound",
                table: "Segments");

            migrationBuilder.DropColumn(
                name: "IsOutbound",
                table: "Segments");
        }
    }
}
