﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFieldstModelScheduleAppointment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DateOne",
                table: "ScheduledAppointments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DateThree",
                table: "ScheduledAppointments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DateTwo",
                table: "ScheduledAppointments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOne",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "DateThree",
                table: "ScheduledAppointments");

            migrationBuilder.DropColumn(
                name: "DateTwo",
                table: "ScheduledAppointments");
        }
    }
}
