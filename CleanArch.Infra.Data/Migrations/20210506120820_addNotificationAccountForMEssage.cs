﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addNotificationAccountForMEssage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Message",
                table: "NotificationAccount");

            migrationBuilder.AddColumn<int>(
                name: "NotificationSMSAccountId",
                table: "SubCampaigns",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Message1",
                table: "NotificationAccount",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Message2",
                table: "NotificationAccount",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Method",
                table: "NotificationAccount",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubCampaigns_NotificationSMSAccountId",
                table: "SubCampaigns",
                column: "NotificationSMSAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubCampaigns_NotificationAccount_NotificationSMSAccountId",
                table: "SubCampaigns",
                column: "NotificationSMSAccountId",
                principalTable: "NotificationAccount",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubCampaigns_NotificationAccount_NotificationSMSAccountId",
                table: "SubCampaigns");

            migrationBuilder.DropIndex(
                name: "IX_SubCampaigns_NotificationSMSAccountId",
                table: "SubCampaigns");

            migrationBuilder.DropColumn(
                name: "NotificationSMSAccountId",
                table: "SubCampaigns");

            migrationBuilder.DropColumn(
                name: "Message1",
                table: "NotificationAccount");

            migrationBuilder.DropColumn(
                name: "Message2",
                table: "NotificationAccount");

            migrationBuilder.DropColumn(
                name: "Method",
                table: "NotificationAccount");

            migrationBuilder.AddColumn<string>(
                name: "Message",
                table: "NotificationAccount",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
