﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddSubCampaign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubCampaignId",
                table: "ServicesDialer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DialerServiceTypeCall",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DialerServiceTypeCall",
                table: "ActiveGestions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServicesDialer_SubCampaignId",
                table: "ServicesDialer",
                column: "SubCampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServicesDialer_SubCampaigns_SubCampaignId",
                table: "ServicesDialer",
                column: "SubCampaignId",
                principalTable: "SubCampaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServicesDialer_SubCampaigns_SubCampaignId",
                table: "ServicesDialer");

            migrationBuilder.DropIndex(
                name: "IX_ServicesDialer_SubCampaignId",
                table: "ServicesDialer");

            migrationBuilder.DropColumn(
                name: "SubCampaignId",
                table: "ServicesDialer");

            migrationBuilder.DropColumn(
                name: "DialerServiceTypeCall",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "DialerServiceTypeCall",
                table: "ActiveGestions");
        }
    }
}
