﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class UpdateDbBackOfficeMana : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackofficeManagements_Tipification_TypificationdBackId",
                table: "BackofficeManagements");

            migrationBuilder.DropIndex(
                name: "IX_BackofficeManagements_TypificationdBackId",
                table: "BackofficeManagements");

            migrationBuilder.DropColumn(
                name: "TypificationdBackId",
                table: "BackofficeManagements");

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_TypificationBackId",
                table: "BackofficeManagements",
                column: "TypificationBackId");

            migrationBuilder.AddForeignKey(
                name: "FK_BackofficeManagements_Tipification_TypificationBackId",
                table: "BackofficeManagements",
                column: "TypificationBackId",
                principalTable: "Tipification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackofficeManagements_Tipification_TypificationBackId",
                table: "BackofficeManagements");

            migrationBuilder.DropIndex(
                name: "IX_BackofficeManagements_TypificationBackId",
                table: "BackofficeManagements");

            migrationBuilder.AddColumn<int>(
                name: "TypificationdBackId",
                table: "BackofficeManagements",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeManagements_TypificationdBackId",
                table: "BackofficeManagements",
                column: "TypificationdBackId");

            migrationBuilder.AddForeignKey(
                name: "FK_BackofficeManagements_Tipification_TypificationdBackId",
                table: "BackofficeManagements",
                column: "TypificationdBackId",
                principalTable: "Tipification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
