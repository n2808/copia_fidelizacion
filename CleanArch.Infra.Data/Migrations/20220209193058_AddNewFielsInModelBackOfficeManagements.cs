﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddNewFielsInModelBackOfficeManagements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ObservationValidator",
                table: "BackOfficeManagementsHistories",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ObservationValidator",
                table: "BackofficeManagements",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ObservationValidator",
                table: "BackOfficeManagementsHistories");

            migrationBuilder.DropColumn(
                name: "ObservationValidator",
                table: "BackofficeManagements");
        }
    }
}
