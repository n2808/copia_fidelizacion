﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addGestionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActiveGestions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Skill = table.Column<string>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    MacchineName = table.Column<string>(nullable: true),
                    TeamLeader = table.Column<string>(nullable: true),
                    Manager = table.Column<string>(nullable: true),
                    tipificacionId = table.Column<int>(nullable: true),
                    Dialercode = table.Column<string>(nullable: true),
                    ClientName = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    Customerdocument = table.Column<string>(nullable: true),
                    CampaignName = table.Column<string>(nullable: true),
                    SubCampaignName = table.Column<string>(nullable: true),
                    SegmentName = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    SubcampaignId = table.Column<int>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActiveGestions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActiveGestions_campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActiveGestions_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActiveGestions_SubCampaigns_SubcampaignId",
                        column: x => x.SubcampaignId,
                        principalTable: "SubCampaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActiveGestions_Tipification_tipificacionId",
                        column: x => x.tipificacionId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Gestions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Skill = table.Column<string>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    MacchineName = table.Column<string>(nullable: true),
                    TeamLeader = table.Column<string>(nullable: true),
                    Manager = table.Column<string>(nullable: true),
                    tipificacionId = table.Column<int>(nullable: true),
                    Dialercode = table.Column<string>(nullable: true),
                    ClientName = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    Customerdocument = table.Column<string>(nullable: true),
                    CampaignName = table.Column<string>(nullable: true),
                    SubCampaignName = table.Column<string>(nullable: true),
                    SegmentName = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    SubcampaignId = table.Column<int>(nullable: false),
                    CampaignId = table.Column<int>(nullable: false),
                    status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gestions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gestions_campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Gestions_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Gestions_SubCampaigns_SubcampaignId",
                        column: x => x.SubcampaignId,
                        principalTable: "SubCampaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Gestions_Tipification_tipificacionId",
                        column: x => x.tipificacionId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActiveGestions_CampaignId",
                table: "ActiveGestions",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_ActiveGestions_SegmentId",
                table: "ActiveGestions",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ActiveGestions_SubcampaignId",
                table: "ActiveGestions",
                column: "SubcampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_ActiveGestions_tipificacionId",
                table: "ActiveGestions",
                column: "tipificacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Gestions_CampaignId",
                table: "Gestions",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_Gestions_SegmentId",
                table: "Gestions",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Gestions_SubcampaignId",
                table: "Gestions",
                column: "SubcampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_Gestions_tipificacionId",
                table: "Gestions",
                column: "tipificacionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActiveGestions");

            migrationBuilder.DropTable(
                name: "Gestions");
        }
    }
}
