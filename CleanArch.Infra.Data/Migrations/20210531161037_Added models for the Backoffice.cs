﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddedmodelsfortheBackoffice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BackofficeGestionReports",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    ChangeDate = table.Column<DateTime>(nullable: true),
                    BeforeTMCode = table.Column<string>(nullable: true),
                    BeforeValue = table.Column<string>(nullable: true),
                    BeforeName = table.Column<string>(nullable: true),
                    NewValue = table.Column<string>(nullable: true),
                    NewValueTax = table.Column<string>(nullable: true),
                    NewTMCode = table.Column<string>(nullable: true),
                    NewName = table.Column<string>(nullable: true),
                    Account = table.Column<string>(nullable: true),
                    Observation = table.Column<string>(nullable: true),
                    TypePlanId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackofficeGestionReports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BackofficeGestionReports_Configurations_TypePlanId",
                        column: x => x.TypePlanId,
                        principalTable: "Configurations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "BackofficeGestions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    ChangeDate = table.Column<DateTime>(nullable: true),
                    BeforeTMCode = table.Column<string>(nullable: true),
                    BeforeValue = table.Column<string>(nullable: true),
                    BeforeName = table.Column<string>(nullable: true),
                    NewValue = table.Column<string>(nullable: true),
                    NewValueTax = table.Column<string>(nullable: true),
                    NewTMCode = table.Column<string>(nullable: true),
                    NewName = table.Column<string>(nullable: true),
                    Account = table.Column<string>(nullable: true),
                    Observation = table.Column<string>(nullable: true),
                    TypePlanId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackofficeGestions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BackofficeGestions_Configurations_TypePlanId",
                        column: x => x.TypePlanId,
                        principalTable: "Configurations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeGestionReports_TypePlanId",
                table: "BackofficeGestionReports",
                column: "TypePlanId");

            migrationBuilder.CreateIndex(
                name: "IX_BackofficeGestions_TypePlanId",
                table: "BackofficeGestions",
                column: "TypePlanId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BackofficeGestionReports");

            migrationBuilder.DropTable(
                name: "BackofficeGestions");
        }
    }
}
