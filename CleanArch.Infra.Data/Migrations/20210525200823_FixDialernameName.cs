﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class FixDialernameName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DialrerName",
                table: "ServicesDialer");

            migrationBuilder.AddColumn<string>(
                name: "DialerName",
                table: "ServicesDialer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DialerName",
                table: "ServicesDialer");

            migrationBuilder.AddColumn<string>(
                name: "DialrerName",
                table: "ServicesDialer",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
