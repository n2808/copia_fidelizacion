﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class addClientReportId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DatabaseClientReportsId",
                table: "GestionReportsOffers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DatabaseClientReportsId",
                table: "GestionReports",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GestionReportsOffers_DatabaseClientReportsId",
                table: "GestionReportsOffers",
                column: "DatabaseClientReportsId");

            migrationBuilder.CreateIndex(
                name: "IX_GestionReports_DatabaseClientReportsId",
                table: "GestionReports",
                column: "DatabaseClientReportsId");

            migrationBuilder.AddForeignKey(
                name: "FK_GestionReports_databaseClientReports_DatabaseClientReportsId",
                table: "GestionReports",
                column: "DatabaseClientReportsId",
                principalTable: "databaseClientReports",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_GestionReportsOffers_databaseClientReports_DatabaseClientReportsId",
                table: "GestionReportsOffers",
                column: "DatabaseClientReportsId",
                principalTable: "databaseClientReports",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GestionReports_databaseClientReports_DatabaseClientReportsId",
                table: "GestionReports");

            migrationBuilder.DropForeignKey(
                name: "FK_GestionReportsOffers_databaseClientReports_DatabaseClientReportsId",
                table: "GestionReportsOffers");

            migrationBuilder.DropIndex(
                name: "IX_GestionReportsOffers_DatabaseClientReportsId",
                table: "GestionReportsOffers");

            migrationBuilder.DropIndex(
                name: "IX_GestionReports_DatabaseClientReportsId",
                table: "GestionReports");

            migrationBuilder.DropColumn(
                name: "DatabaseClientReportsId",
                table: "GestionReportsOffers");

            migrationBuilder.DropColumn(
                name: "DatabaseClientReportsId",
                table: "GestionReports");
        }
    }
}
