﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddRescheduleModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "GuidId2",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "GuidId3",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Inbound",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IntId1",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IntId2",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Outbound",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Positive",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SegmentId",
                table: "Configurations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubcampaignId",
                table: "Configurations",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "gestionReschedules",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    TipificationId = table.Column<int>(nullable: false),
                    TipificationCode = table.Column<string>(nullable: true),
                    SegmentId = table.Column<int>(nullable: false),
                    GestionId = table.Column<int>(nullable: false),
                    AgentId = table.Column<Guid>(nullable: true),
                    Count = table.Column<int>(nullable: false),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gestionReschedules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_gestionReschedules_Users_AgentId",
                        column: x => x.AgentId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gestionReschedules_GestionReports_GestionId",
                        column: x => x.GestionId,
                        principalTable: "GestionReports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gestionReschedules_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gestionReschedules_Tipification_TipificationId",
                        column: x => x.TipificationId,
                        principalTable: "Tipification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateTable(
            //    name: "Groups",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(nullable: false),
            //        CreatedAt = table.Column<DateTime>(nullable: true),
            //        UpdatedAt = table.Column<DateTime>(nullable: true),
            //        DeletedAt = table.Column<DateTime>(nullable: true),
            //        CreatedBy = table.Column<Guid>(nullable: true),
            //        UpdatedBy = table.Column<Guid>(nullable: true),
            //        CreatedByName = table.Column<string>(nullable: true),
            //        UpdatedByName = table.Column<Guid>(nullable: true),
            //        Name = table.Column<string>(maxLength: 255, nullable: false),
            //        DisplayName = table.Column<string>(maxLength: 255, nullable: false),
            //        UserUpdate = table.Column<string>(maxLength: 255, nullable: true),
            //        Status = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Groups", x => x.Id);
            //    });

            migrationBuilder.CreateTable(
                name: "TipificationReschedules",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    TipificationCode = table.Column<string>(nullable: true),
                    CampaignId = table.Column<int>(nullable: true),
                    SubCampaignId = table.Column<int>(nullable: true),
                    SegmentId = table.Column<int>(nullable: true),
                    Hascall = table.Column<bool>(nullable: false),
                    Vdn = table.Column<int>(nullable: false),
                    load = table.Column<int>(nullable: false),
                    CustomData1 = table.Column<string>(nullable: true),
                    CustomData2 = table.Column<string>(nullable: true),
                    CustomData3 = table.Column<string>(nullable: true),
                    CustomData4 = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipificationReschedules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TipificationReschedules_campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "campaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TipificationReschedules_Segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "Segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TipificationReschedules_SubCampaigns_SubCampaignId",
                        column: x => x.SubCampaignId,
                        principalTable: "SubCampaigns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "rescheduleHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<Guid>(nullable: true),
                    GestionRescheduleId = table.Column<Guid>(nullable: false),
                    TipificationId = table.Column<Guid>(nullable: false),
                    TipificationName = table.Column<string>(nullable: true),
                    Observation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rescheduleHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_rescheduleHistories_gestionReschedules_GestionRescheduleId",
                        column: x => x.GestionRescheduleId,
                        principalTable: "gestionReschedules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_rescheduleHistories_Configurations_TipificationId",
                        column: x => x.TipificationId,
                        principalTable: "Configurations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_gestionReschedules_AgentId",
                table: "gestionReschedules",
                column: "AgentId");

            migrationBuilder.CreateIndex(
                name: "IX_gestionReschedules_GestionId",
                table: "gestionReschedules",
                column: "GestionId");

            migrationBuilder.CreateIndex(
                name: "IX_gestionReschedules_SegmentId",
                table: "gestionReschedules",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_gestionReschedules_TipificationId",
                table: "gestionReschedules",
                column: "TipificationId");

            migrationBuilder.CreateIndex(
                name: "IX_rescheduleHistories_GestionRescheduleId",
                table: "rescheduleHistories",
                column: "GestionRescheduleId");

            migrationBuilder.CreateIndex(
                name: "IX_rescheduleHistories_TipificationId",
                table: "rescheduleHistories",
                column: "TipificationId");

            migrationBuilder.CreateIndex(
                name: "IX_TipificationReschedules_CampaignId",
                table: "TipificationReschedules",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_TipificationReschedules_SegmentId",
                table: "TipificationReschedules",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_TipificationReschedules_SubCampaignId",
                table: "TipificationReschedules",
                column: "SubCampaignId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "rescheduleHistories");

            migrationBuilder.DropTable(
                name: "TipificationReschedules");

            migrationBuilder.DropTable(
                name: "gestionReschedules");

            migrationBuilder.DropColumn(
                name: "GuidId2",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "GuidId3",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "Inbound",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "IntId1",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "IntId2",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "Outbound",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "Positive",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "SegmentId",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "SubcampaignId",
                table: "Configurations");
        }
    }
}
