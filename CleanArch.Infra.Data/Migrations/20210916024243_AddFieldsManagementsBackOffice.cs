﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddFieldsManagementsBackOffice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EndValidation",
                table: "ManagementsBackOffice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "InitValidation",
                table: "ManagementsBackOffice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Process",
                table: "ManagementsBackOffice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndValidation",
                table: "ManagementsBackOffice");

            migrationBuilder.DropColumn(
                name: "InitValidation",
                table: "ManagementsBackOffice");

            migrationBuilder.DropColumn(
                name: "Process",
                table: "ManagementsBackOffice");
        }
    }
}
