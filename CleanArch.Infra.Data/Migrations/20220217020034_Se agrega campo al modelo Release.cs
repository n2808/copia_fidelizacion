﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class SeagregacampoalmodeloRelease : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubCampaignId",
                table: "Release",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Release_SubCampaignId",
                table: "Release",
                column: "SubCampaignId");

            migrationBuilder.AddForeignKey(
                name: "FK_Release_SubCampaigns_SubCampaignId",
                table: "Release",
                column: "SubCampaignId",
                principalTable: "SubCampaigns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Release_SubCampaigns_SubCampaignId",
                table: "Release");

            migrationBuilder.DropIndex(
                name: "IX_Release_SubCampaignId",
                table: "Release");

            migrationBuilder.DropColumn(
                name: "SubCampaignId",
                table: "Release");
        }
    }
}
