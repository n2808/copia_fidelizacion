﻿using Application.Common.interfaces;
using CleanArch.Domain.Models;
using Core.Models.configuration;
using Core.Models.location;
using Core.Models.Operation;
using Domain.Models.Apps;
using Domain.Models.Backoffice;
using Domain.Models.ClaroUser;
using Domain.Models.FetchFaceEvidences;
using Domain.Models.Gestion;
using Domain.Models.Groups;
using Domain.Models.location;
using Domain.Models.ManagementsBackoffice;
using Domain.Models.Offer;
using Domain.Models.Operation;
using Domain.Models.Operators;
using Domain.Models.Plans;
using Domain.Models.Products;
using Domain.Models.Quotes;
using Domain.Models.Releases;
using Domain.Models.ReSchedule;
using Domain.Models.Rol;
using Domain.Models.Scheduling;
using Domain.Models.SecondRingLoads;
using Domain.Models.Skill;
using Domain.Models.Status;
using Domain.Models.Surveys;
using Domain.Models.SurveysResults;
using Domain.Models.Tipification;
using Domain.Models.Tipifications;
using Domain.Models.User;
using Domain.Notification;
using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace CleanArch.Infra.Data.Context
{
    public class U27ApplicationDBContext : DbContext, IU27ApplicationDBContext
    {
        public U27ApplicationDBContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Course> courses { get; set; }

        #region App

        public DbSet<AppNotification> AppNotification { get; set; }
        public DbSet<SegmentAppNotification> SegmentAppNotifications { get; set; }

        #endregion

        #region Offer

        public DbSet<GestionReportsOffers> GestionReportsOffers { get; set; }

        #endregion


        #region Notification
        public DbSet<NotificationLog> NotificationLog { get; set; }
        public DbSet<NotificationAccount> NotificationAccount { get; set; }


        #endregion

        #region Operation
        public DbSet<Campaign> campaigns { get; set; }
        public DbSet<SubCampaign> SubCampaigns { get; set; }
        public DbSet<Segment> Segments { get; set; }
        public DbSet<ServicesDialer> ServicesDialer { get; set; }
        #endregion


        #region Tipification

        public DbSet<ServicesDialerTipification> ServicesDialerTipification { get; set; }
        public DbSet<Tipification> Tipification { get; set; }

        internal Task SqlQueryAsync<T>(T v)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Location
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> states { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<NewCity> NewCities { get; set; }

        #endregion

        #region DatabaseClient
        public DbSet<GestionStructure> bulkStructures { get; set; }
        public DbSet<DatabasesClientLoad> DatabasesClientLoads { get; set; }
        public DbSet<DatabasesClientLoadOperation> DatabasesClientLoadOperation { get; set; }
        public DbSet<DatabaseClientBase> DatabaseClientBase { get; set; }
        public DbSet<DatabaseClientDetail> DatabaseClientDetail { get; set; }
        public DbSet<DatabaseClientReports> databaseClientReports{ get; set; }
        public DbSet<Temp_DatabaseClientDetail> Temp_DatabaseClientDetail { get; set; }
        #endregion

        #region Configuration
        public DbSet<Category> Categories { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        #endregion

        #region Gestion
        public DbSet<InformativeAgentFields> InformativeAgentFields { get; set; }

        public DbSet<ActiveGestion> ActiveGestions  { get; set; }
        public DbSet<GestionReports> GestionReports { get; set; }
        public DbSet<GestionStructure> GestionStructure { get; set; }
        //public DbSet<GestionReportsHistories> GestionReportsHistories { get; set; }

        #endregion


        #region Survey
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyQuestion> SurveyQuestions { get; set; }
        public DbSet<SurveyResponse> SurveyResponses { get; set; }
        public DbSet<SurveySegment> SurveySegments{ get; set; }

        #endregion


        #region SurveysResults
        public DbSet<SurveyResultQuestion> SurveyResultQuestions { get; set; }
        public DbSet<SurveyResultGestion> SurveyResultGestions { get; set; }
        public DbSet<SurveyResultAnswers> SurveyResultAnswers { get; set; }

        #endregion

        #region User
        public DbSet<User> Users { get; set; }
        public DbSet<UserRol> UserRols { get; set; }

        #endregion

        #region Rol
        public DbSet<Rol> Rols { get; set; }

        #endregion

        #region Scheduling
        public DbSet<ScheduledAppointment> ScheduledAppointments { get; set; }
        public DbSet<ScheduledAppointmentReport> ScheduledAppointmentReports { get; set; }

        #endregion

        #region Backoffice
        public DbSet<BackofficeGestion> BackofficeGestions { get; set; }
        public DbSet<BackofficeGestionReport> BackofficeGestionReports { get; set; }

        #endregion

        #region Plans
        public DbSet<Plan> Plans { get; set; }

        #endregion

        #region Product
        public DbSet<Product> products { get; set; }

        #endregion

        #region
        public DbSet<Group> Groups { get; set; }
        #endregion

        #region Operators
        public DbSet<Operator> Operators { get; set; }
        #endregion

        #region Quotes
        public DbSet<Quote> Quotes { get; set; }
        #endregion

        #region Status
        public DbSet<Status> Status { get; set; }
        #endregion

        #region
        public DbSet<EvidenceGestion> EvidenceGestions { get; set; }
        #endregion
        #region ReSchedule

        public DbSet<GestionReschedule> gestionReschedules { get; set; }
        public DbSet<RescheduleHistory> rescheduleHistories { get; set; }
        public DbSet<TipificationReschedule> TipificationReschedules { get; set; }

        #endregion


        #region StoreProcedure

        #endregion

        #region ClaroUsers
        public DbSet<ClaroUser> ClaroUsers { get; set; }

        #endregion

        #region Skills
        public DbSet<Skills> Skills { get; set; }
        #endregion

        #region BackofficeManagements
        public DbSet<BackOfficeManagements> BackofficeManagements { get; set; }
        public DbSet<BackOfficeManagementsHistories> BackOfficeManagementsHistories { get; set; }
        public DbSet<BackOfficeOperations> BackOfficeOperations { get; set; }
        #endregion

        public DbSet<SecondRingLoad> SecondRingLoads { get; set; }

        public DbSet<SubCategoryTipification> SubCategoryTipification { get; set; }

        public DbSet<Release> Release { get; set; }

        #region FetchFaceEvidences
        public DbSet<FetchFaceEvidence> FetchFaceEvidence { get; set; }
        #endregion


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Load all assemblies from configurations folder in infra.data
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        }




    }
}
