﻿using Domain.Models.Gestion;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class EvidenceGestionConfiguration : IEntityTypeConfiguration<EvidenceGestion>
    {
        public void Configure(EntityTypeBuilder<EvidenceGestion> builder)
        {
            builder.Property(x => x.GestionReportsId)
                   .IsRequired();

            builder.Property(x => x.FileName)
                  .IsRequired()
                  .HasMaxLength(255);

            builder.Property(x => x.FilePath)
                  .IsRequired()
                  .HasMaxLength(255);
        }
    }
}
