﻿using Core.Models.location;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.Property(x => x.Code)
                   .IsRequired()
                   .HasMaxLength(5);

            builder.Property(x => x.Name)
                  .IsRequired()
                  .HasMaxLength(25);
        }
    }
}
