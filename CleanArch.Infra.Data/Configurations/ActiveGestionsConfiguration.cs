﻿using Core.Models.Operation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class ActiveGestionsConfiguration : IEntityTypeConfiguration<ActiveGestion>
    {
        public void Configure(EntityTypeBuilder<ActiveGestion> builder)
        {
            builder.Property(x => x.Account)
                 .HasMaxLength(25);

            builder.Property(x => x.Identification)
                 .HasMaxLength(20);
        }
    }
}
