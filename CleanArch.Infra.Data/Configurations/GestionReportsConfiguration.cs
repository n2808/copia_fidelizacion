﻿using Core.Models.Operation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class GestionReportsConfiguration : IEntityTypeConfiguration<GestionReports>
    {
        public void Configure(EntityTypeBuilder<GestionReports> builder)
        {
            builder.Property(x => x.Account)
                  .HasMaxLength(25);

            builder.Property(x => x.Identification)
                  .HasMaxLength(20);
        }
    }


}
