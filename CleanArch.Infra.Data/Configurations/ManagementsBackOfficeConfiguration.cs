﻿using Domain.Models.ManagementsBackoffice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class ManagementsBackOfficeConfiguration : IEntityTypeConfiguration<BackOfficeManagements>
    {
        public void Configure(EntityTypeBuilder<BackOfficeManagements> builder)
        {
            builder.Property(x => x.ManagementId)
                   .IsRequired();

            builder.Property(x => x.TypeOperation)
                   .IsRequired();

            builder.Property(x => x.Operation)
                   .IsRequired();
        }
    }
}
