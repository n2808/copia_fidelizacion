﻿using Domain.Models.FetchFaceEvidences;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class FetchFaceEvidenceConfiguration : IEntityTypeConfiguration<FetchFaceEvidence>
    {
        public void Configure(EntityTypeBuilder<FetchFaceEvidence> builder)
        {
            builder.Property(x => x.Document)
                  .HasMaxLength(25);

            builder.Property(x => x.ActiveGestionId)
                   .IsRequired();
        }
    }
}
