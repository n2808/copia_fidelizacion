﻿using Domain.Models.Plans;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class PlanConfiguration : IEntityTypeConfiguration<Plan>
    {
        public void Configure(EntityTypeBuilder<Plan> builder)
        {
            builder.Property(x => x.Name)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Price)
                   .IsRequired()
                   .HasMaxLength(20);

            builder.Property(x => x.Tax)
                   .IsRequired()
                   .HasMaxLength(5);

            builder.Property(x => x.PriceWithTax)
                   .IsRequired()
                   .HasMaxLength(20);

            builder.Property(x => x.Currency)
                   .IsRequired()
                   .HasMaxLength(3);

            builder.Property(x => x.TMCode)
                   .HasMaxLength(30);

            builder.Property(x => x.MessagePrice) 
                   .HasMaxLength(255);

            builder.Property(x => x.UserUpdate)
                   .HasMaxLength(255);


            builder.Property(x => x.Status)
                   .IsRequired();
        }
    }
}
