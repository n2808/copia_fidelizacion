﻿using Domain.Models.Skill;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class SkillConfiguration : IEntityTypeConfiguration<Skills>
    {
        public void Configure(EntityTypeBuilder<Skills> builder)
        {
            builder.Property(x => x.Name)
                   .IsRequired()
                   .HasMaxLength(255);

            builder.Property(x => x.DisplayName)
                   .IsRequired()
                   .HasMaxLength(255);

            builder.Property(x => x.Code)
                   .HasMaxLength(25);

            //builder.Property(x => x.Activate)
            //       .IsRequired();
        }
    }
}
