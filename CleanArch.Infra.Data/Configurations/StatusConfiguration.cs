﻿using Domain.Models.Status;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class StatusConfiguration : IEntityTypeConfiguration<Status>
    {
        public void Configure(EntityTypeBuilder<Status> builder)
        {
            builder.Property(x => x.Name)
                   .IsRequired();
            builder.Property(x => x.DisplayName)
                   .IsRequired();
            builder.Property(x => x.Description)
                   .IsRequired();
        }
    }
}
