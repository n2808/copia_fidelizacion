﻿using Domain.Models.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.Property(x => x.Name)
                  .IsRequired()
                  .HasMaxLength(255);

            builder.Property(x => x.DisplayName)
                   .IsRequired()
                   .HasMaxLength(255);

            builder.Property(x => x.GeneralLegalText)
                   .IsRequired();            

            builder.Property(x => x.UserUpdate)
                   .HasMaxLength(255);

            builder.Property(x => x.Status)
                   .IsRequired();
        }
    }
}
