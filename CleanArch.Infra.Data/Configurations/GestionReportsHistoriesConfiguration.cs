﻿using Domain.Models.Gestion;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    class GestionReportsHistoriesConfiguration : IEntityTypeConfiguration<GestionReportsHistories>
    {
        public void Configure(EntityTypeBuilder<GestionReportsHistories> builder)
        {
            builder.Property(x => x.Phone)
                   .IsRequired()
                   .HasMaxLength(25);
        }
    }
}
