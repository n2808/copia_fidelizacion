﻿using Domain.Models.Backoffice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class BackOfficeOperationsConfiguration : IEntityTypeConfiguration<BackOfficeOperations>
    {
        public void Configure(EntityTypeBuilder<BackOfficeOperations> builder)
        {
            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(x => x.DisplayName)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(x => x.Operation)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(x => x.Process)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(x => x.Code)
                .IsRequired()
                .HasMaxLength(10);

            builder.Property(x => x.TypeOperation)
                .IsRequired()
                .HasMaxLength(150);
        }
    }
}
