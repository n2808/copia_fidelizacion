﻿using Domain.Models.Groups;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class GroupConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.Property(x => x.Name)
                   .IsRequired()
                   .HasMaxLength(255);

            builder.Property(x => x.DisplayName)
                   .IsRequired()
                   .HasMaxLength(255);

            builder.Property(x => x.UserUpdate)
                   .HasMaxLength(255);

            builder.Property(x => x.Status)
                   .IsRequired();
        }
    }
}
