﻿using Core.Models.location;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.Property(x => x.Code)
                   .HasMaxLength(5);

            builder.Property(x => x.Name)
                  .IsRequired()
                  .HasMaxLength(100);
        }
    }
}
