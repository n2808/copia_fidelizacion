﻿using Domain.Models.Tipification;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class SubCategoryTipificationConfiguration : IEntityTypeConfiguration<SubCategoryTipification>
    {
        public void Configure(EntityTypeBuilder<SubCategoryTipification> builder)
        {
            builder.Property(x => x.TypeCategory)
                  .HasMaxLength(30);
        }
    }
}
