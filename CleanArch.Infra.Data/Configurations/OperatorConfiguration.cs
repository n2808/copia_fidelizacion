﻿using Domain.Models.Operators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class OperatorConfiguration : IEntityTypeConfiguration<Operator>
    {
        public void Configure(EntityTypeBuilder<Operator> builder)
        {
            builder.Property(x => x.Name)
                  .IsRequired()
                  .HasMaxLength(255);

            builder.Property(x => x.DisplayName)
                   .IsRequired()
                   .HasMaxLength(255);          

            builder.Property(x => x.UserUpdate)
                   .HasMaxLength(255);

            builder.Property(x => x.Status)
                   .IsRequired();
        }
    }
}
