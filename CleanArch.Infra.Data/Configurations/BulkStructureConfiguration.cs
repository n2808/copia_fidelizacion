﻿using Core.Models.Operation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Configurations
{
    public class BulkStructureConfiguration : IEntityTypeConfiguration<BulkStructure>
    {
        public void Configure(EntityTypeBuilder<BulkStructure> builder)
        {
            builder.Property(b => b.DatbaseField).IsRequired();
            builder.Property(b => b.ExcelField).IsRequired();
        }
    }


}
