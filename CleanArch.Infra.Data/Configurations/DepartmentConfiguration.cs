﻿using Domain.Models.location;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class DepartmentConfiguration : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.Property(x => x.Code)
                   .IsRequired()
                   .HasMaxLength(5);

            builder.Property(x => x.Name)
                  .IsRequired()
                  .HasMaxLength(100);
        }
    }
}
