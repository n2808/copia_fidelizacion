﻿using Domain.Models.SecondRingLoads;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class SecondRingLoadConfiguration : IEntityTypeConfiguration<SecondRingLoad>
    {
        public void Configure(EntityTypeBuilder<SecondRingLoad> builder)
        {
            builder.Property(x => x.account)
                .HasMaxLength(20);

            builder.Property(x => x.phone)
                .HasMaxLength(20);

            builder.Property(x => x.assignedId)
                .HasMaxLength(20);
        }
    }
}
