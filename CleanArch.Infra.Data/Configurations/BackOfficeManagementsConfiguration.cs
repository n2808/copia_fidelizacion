﻿using Domain.Models.ManagementsBackoffice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class BackOfficeManagementsConfiguration : IEntityTypeConfiguration<BackOfficeManagements>
    {
        public void Configure(EntityTypeBuilder<BackOfficeManagements> builder)
        {
            builder.Property(x => x.TypificationName)
                   .HasMaxLength(150);

            builder.Property(x => x.StateEnd)
                .HasMaxLength(150);
        }
    }
}
