﻿using Domain.Models.ClaroUser;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Configurations
{
    public class ClaroUserConfiguration : IEntityTypeConfiguration<ClaroUser>
    {
        public void Configure(EntityTypeBuilder<ClaroUser> builder)
        {
            builder.Property(x => x.Code)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Charge)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Location)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Region)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Ring)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Supervisor)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.IdentificationSupervisor)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Coordinator)
                  .IsRequired()
                  .HasMaxLength(150);

            builder.Property(x => x.IdentificationCoordinator)
                   .IsRequired()
                   .HasMaxLength(150);
        }
    }
}
