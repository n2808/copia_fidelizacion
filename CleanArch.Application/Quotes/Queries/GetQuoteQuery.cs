﻿using Application.DTOs.Quotes;
using Application.Interfaces.Quotes;
using Domain.Models.Operators;
using Domain.Models.Plans;
using Domain.Models.Products;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Quotes.Queries
{
    public class GetQuoteQuery : IRequest<List<QuoteDto>>
    {
        public int Without { get; set; }
        public int? SubCampaignId { get; set; }
        public Guid? ProductId { get; set; }
        public Guid? OperatorId { get; set; }
        public Guid? PlanId { get; set; }
        public Domain.Models.Operation.SubCampaign SubCampaign { get; set; }
        public Product Product { get; set; }
        public Operator Operator { get; set; }
        public Plan Plan { get; set; }

    }

    public class GetQuoteQueryHandler : IRequestHandler<GetQuoteQuery, List<QuoteDto>>
    {
        private readonly IQuoteService _quoteService;
        public GetQuoteQueryHandler(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        public async Task<List<QuoteDto>> Handle(GetQuoteQuery request, CancellationToken cancellationToken)
        {
            List<QuoteDto> quoteDtos = _quoteService.GetQuotes(request);

            return quoteDtos;
        }

    }
}
