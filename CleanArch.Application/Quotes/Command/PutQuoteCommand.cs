﻿using Application.DTOs.Quotes;
using Application.Interfaces.Quotes;
using AutoMapper;
using Domain.Models.Quotes;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Quotes.Command
{
    public class PutQuoteCommand : IRequest<QuoteDto>
    {
        public Guid Id { get; set; }
        public string UserUpdate { get; set; }
        public bool? Status { get; set; }
        public int? SubCampaignId { get; set; }
        public Guid? ProductId { get; set; }
        public Guid? OperatorId { get; set; }
        public Guid? PlanId { get; set; }
    }

    public class PutQuoteCommandHandler : IRequestHandler<PutQuoteCommand, QuoteDto>
    {
        private readonly IQuoteService _quoteService;
        private readonly IMapper _mapper;
        public PutQuoteCommandHandler(IQuoteService quoteService, IMapper mapper)
        {
            _quoteService = quoteService;
            _mapper = mapper;
        }
        public async Task<QuoteDto> Handle(PutQuoteCommand request, CancellationToken cancellationToken)
        {
            Quote quote = _quoteService.PutQuote(request);

            return _mapper.Map<QuoteDto>(quote);

        }
    }
}
