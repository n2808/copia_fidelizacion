﻿using Application.Interfaces.Quotes;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Quotes.Command
{
    public class DeactivateQuoteCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class DeactivateQuoteCommandHandler : IRequestHandler<DeactivateQuoteCommand, bool>
    {
        private readonly IQuoteService _quoteService;
        public DeactivateQuoteCommandHandler(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }
        public async Task<bool> Handle(DeactivateQuoteCommand request, CancellationToken cancellationToken)
        {
            _quoteService.DeactivateQuote(request.Id);

            return true;
        }
    }
}
