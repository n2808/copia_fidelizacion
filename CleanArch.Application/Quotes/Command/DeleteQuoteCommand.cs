﻿using Application.Interfaces.Quotes;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Quotes.Command
{
    public class DeleteQuoteCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class DeleteQuoteCommandHandler : IRequestHandler<DeleteQuoteCommand, bool>
    {

        private readonly IQuoteService _quoteService;
        public DeleteQuoteCommandHandler(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        public async Task<bool> Handle(DeleteQuoteCommand request, CancellationToken cancellationToken)
        {
            _quoteService.DeleteQuote(request.Id);

            return true;
        }
    }
}
