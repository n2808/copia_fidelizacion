﻿using Application.Interfaces.Quotes;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Quotes.Command
{
    public class ActivateQuoteCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }
    public class ActivateQuoteCommandHandler : IRequestHandler<ActivateQuoteCommand, bool>
    {
        private readonly IQuoteService _quoteService;
        public ActivateQuoteCommandHandler(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }
        public async Task<bool> Handle(ActivateQuoteCommand request, CancellationToken cancellationToken)
        {
            _quoteService.ActivateQuote(request.Id);

            return true;
        }
    }
}
