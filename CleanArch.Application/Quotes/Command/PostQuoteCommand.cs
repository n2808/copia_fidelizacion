﻿using Application.DTOs.Quotes;
using Application.Interfaces.Quotes;
using AutoMapper;
using Domain.Models.Quotes;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Quotes.Command
{
    public class PostQuoteCommand : IRequest<QuoteDto>
    {
        public string UserUpdate { get; set; }
        public bool? Status { get; set; }
        public int? SubCampaignId { get; set; }
        public Guid? ProductId { get; set; }
        public Guid? OperatorId { get; set; }
        public Guid? PlanId { get; set; }
    }

    public class PostQuoteCommandHandler : IRequestHandler<PostQuoteCommand, QuoteDto>
    {
        private readonly IQuoteService _quoteService;
        private readonly IMapper _mapper;
        public PostQuoteCommandHandler(IQuoteService quoteService, IMapper mapper)
        {
            _quoteService = quoteService;
            _mapper = mapper;
        }
        public async Task<QuoteDto> Handle(PostQuoteCommand request, CancellationToken cancellationToken)
        {
            Quote quote = _quoteService.PostQuote(_mapper.Map<Quote>(request));

            return _mapper.Map<QuoteDto>(quote);
        }
    }
}
