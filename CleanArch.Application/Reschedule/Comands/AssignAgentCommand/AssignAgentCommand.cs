﻿using Application.DTOs.Reschedule;
using Application.Interfaces.Reschedule;
using AutoMapper;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Models.ReSchedule;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Reschedule.Comands.AssignAgentCommand
{
    public class AssignAgentCommand : IRequest<RescheduleGestionReturn>
    {
        public int? SegmentId { get; set; }
    }

    public class AssignAgentCommandHandle : IRequestHandler<AssignAgentCommand, RescheduleGestionReturn>
    {

        private readonly IGestionRescheduleService _gestionRescheduleService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public AssignAgentCommandHandle(
            IGestionRescheduleService gestionRescheduleService,
            IMapper mapper,
            ICurrentUserService currentUserService
        )
        {
            _gestionRescheduleService = gestionRescheduleService;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }
        public async Task<RescheduleGestionReturn> Handle(AssignAgentCommand request, CancellationToken cancellationToken)
        {

            Guid CurrentUser = (Guid)_currentUserService.GetUserInfo().Id;



            await _gestionRescheduleService.RequestAGestionRescheduleWithoutAgent(request.SegmentId);



            var gestionsReschedule = await _gestionRescheduleService.GestionRescheduleByAgent(CurrentUser);
            return new RescheduleGestionReturn
            {
                AvailableGestions = await _gestionRescheduleService.GetCountGestionRescheduleAvailable(true),
                GestionsAssigned = _mapper.Map<List<GestionRescheduleDto>>(gestionsReschedule)
            };
        }
    }



}
