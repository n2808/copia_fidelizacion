﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Reschedule.Comands.AssignAgentCommand
{
    public class AssignAgentCommandValidator : AbstractValidator<AssignAgentCommand>
    {
        public AssignAgentCommandValidator()
        {
            RuleFor(x => x.SegmentId)
                         .NotNull()
                         .NotEqual(0)
                         .WithMessage("El segmento no es valido");
        }  
    }
}
