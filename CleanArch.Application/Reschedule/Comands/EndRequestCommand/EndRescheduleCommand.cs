﻿using Application.Interfaces.Reschedule;
using AutoMapper;
using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Reschedule.Comands.EndRescheduleCommand
{
    public class EndRescheduleCommand : IRequest
    {
        public Guid? Id { get; set; }
        public Guid? Tipificacion { get; set; }
        public bool? status { get; set; }
    }

    public class EndRescheduleCommandHandle : IRequestHandler<EndRescheduleCommand>
    {

        private readonly IGestionRescheduleService _gestionRescheduleService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public EndRescheduleCommandHandle( 
            IGestionRescheduleService gestionRescheduleService,
            IMapper mapper,
            ICurrentUserService currentUserService
        )
        {
            _gestionRescheduleService = gestionRescheduleService;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }

        public async Task<Unit> Handle(EndRescheduleCommand request, CancellationToken cancellationToken)
        {

            await _gestionRescheduleService.UpdateGestionReschedule((Guid)request.Id,(Guid)_currentUserService.GetUserInfo().Id, (bool)request.status);

            return Unit.Value;
        }

       
    }



}
