﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Reschedule.Comands.EndRescheduleCommand
{
    public class EndRescheduleCommandValidator : AbstractValidator<EndRescheduleCommand>
    {
        public EndRescheduleCommandValidator()
        {
            RuleFor(x => x.Id)
                         .NotNull()
                         .NotEqual(Guid.Empty)
                         .WithMessage("El Id no es valido");

            RuleFor(x => x.status)
                         .NotNull()
                         .WithMessage("El status es obligatorio");

            RuleFor(x => x.status)
                         .NotNull()
                         .WithMessage("El status es obligatorio");


        }  
    }
}
