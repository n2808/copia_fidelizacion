﻿using Application.DTOs.Reschedule;
using Application.Interfaces.Reschedule;
using AutoMapper;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Models.ReSchedule;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Reschedule.Queries.GetAvailableRescheduleQuery
{
    public class GetGestionsByAgentQuery : IRequest<RescheduleGestionReturn>
    {
        public bool status { get; set; } = true;
    }

    public class GetGestionsByAgentHandle : IRequestHandler<GetGestionsByAgentQuery, RescheduleGestionReturn>
    {

        private readonly IGestionRescheduleService _gestionRescheduleService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public GetGestionsByAgentHandle(
            IGestionRescheduleService gestionRescheduleService,
            IMapper mapper,
            ICurrentUserService currentUserService
        )
        {
            _gestionRescheduleService = gestionRescheduleService;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }
        public async Task<RescheduleGestionReturn> Handle(GetGestionsByAgentQuery request, CancellationToken cancellationToken)
        {
            var gestionsReschedule = await _gestionRescheduleService.GestionRescheduleByAgent((Guid)_currentUserService.GetUserInfo().Id);
            var availablesGestions = await _gestionRescheduleService.GetCountGestionRescheduleAvailable(request.status);


            return new RescheduleGestionReturn
            {
                AvailableGestions = availablesGestions,
                GestionsAssigned = _mapper.Map<List<GestionRescheduleDto>>(gestionsReschedule)
            };
        }
    }




}
