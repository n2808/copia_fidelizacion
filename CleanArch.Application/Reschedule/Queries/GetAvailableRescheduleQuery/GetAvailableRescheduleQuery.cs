﻿using Application.DTOs.Reschedule;
using Application.Interfaces.Reschedule;
using AutoMapper;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Models.ReSchedule;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Reschedule.Queries.GetAvailableRescheduleQuery
{
    public class GetAvailableRescheduleQuery : IRequest<List<RescheduleGroupDto>>
    {
        public bool status { get; set; } = true;
    }

    public class GetAvailableRescheduleQueryHandle : IRequestHandler<GetAvailableRescheduleQuery, List<RescheduleGroupDto>>
    {

        private readonly IGestionRescheduleService _gestionRescheduleService;

        public GetAvailableRescheduleQueryHandle(
            IGestionRescheduleService gestionRescheduleService
            
        )
        {
            _gestionRescheduleService = gestionRescheduleService;
            
        }
        public async Task<List<RescheduleGroupDto>> Handle(GetAvailableRescheduleQuery request, CancellationToken cancellationToken)
        {
           return await _gestionRescheduleService.GetCountGestionRescheduleAvailable(request.status);

        }
    }




}
