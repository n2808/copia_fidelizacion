﻿using Application.DTOs.Reschedule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Reschedule
{
    public class RescheduleGestionReturn
    {
        public List<RescheduleGroupDto> AvailableGestions { get; set; }
        public List<GestionRescheduleDto> GestionsAssigned { get; set; }
    }
}
