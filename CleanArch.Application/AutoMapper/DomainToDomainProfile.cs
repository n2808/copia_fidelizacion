﻿using AutoMapper;
using Core.Models.Operation;
using Domain.Models.ManagementsBackoffice;
using Domain.Models.ReSchedule;

namespace CleanArch.Application.AutoMapper
{
    public class DomainToDomainProfile : Profile
    {
        public DomainToDomainProfile()
        {
            CreateMap<ActiveGestion,GestionReports>()
                .ForMember(x => x.DatabaseClientReportsId, opt => opt.MapFrom(j => j.DatabaseClientDetailId));
            CreateMap<DatabaseClientDetail, DatabaseClientReports>();

            CreateMap<ActiveGestion, GestionReschedule>()
                .ForMember(x => x.TipificationId, opt => opt.MapFrom(j => j.tipificacionId))
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.GestionId, opt => opt.MapFrom(j => j.Id));

            CreateMap<ActiveGestion, BackOfficeManagements>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => opt.Ignore())
                .ForMember(x => x.ManagementId, opt => opt.MapFrom(j => j.Id))
                .ForMember(x => x.TypeOperation, opt => opt.MapFrom(j => j.EntryData15))
                .ForMember(x => x.Operation, opt => opt.MapFrom(j => j.EntryData16))
                .ForMember(x => x.TypificationId, opt => opt.MapFrom(j => j.tipificacionId))
                .ForMember(x => x.Observation, opt => opt.MapFrom(j => j.EntryData8));

            CreateMap<BackOfficeManagements, BackOfficeManagementsHistories>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.BackOfficeManagementId, opt => opt.MapFrom(j => j.Id));
        }

    }
}
