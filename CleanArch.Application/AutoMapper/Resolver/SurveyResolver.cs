﻿using Domain.Core.Enums;
using System;

namespace Application.AutoMapper.Resolver
{
    public static  class SurveyResolver
    {
        public  static bool TypeCuestion(Guid TypeId)
        {

            if (TypeId == TypeQuestionEnum.Single)
                return false;
            return true;
        }
    }
}
