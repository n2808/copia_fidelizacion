﻿using Application.AutoMapper.Resolver;
using Application.DTOs;
using Application.DTOs.Backoffice;
using Application.DTOs.BackOfficeManagements;
using Application.DTOs.Campaign;
using Application.DTOs.Configurations;
using Application.DTOs.DataBaseClient;
using Application.DTOs.EvidenceGestion;
using Application.DTOs.FetchFaceEvidence;
using Application.DTOs.GestionReports;
using Application.DTOs.Group;
using Application.DTOs.location;
using Application.DTOs.ManagementsBackOffice;
using Application.DTOs.Operators;
using Application.DTOs.Plans;
using Application.DTOs.Products;
using Application.DTOs.Quotes;
using Application.DTOs.Release;
using Application.DTOs.Reschedule;
using Application.DTOs.Scheduling;
using Application.DTOs.SecondRingLoad;
using Application.DTOs.Segment;
using Application.DTOs.Skills;
using Application.DTOs.Status;
using Application.DTOs.User;
using Application.ServicseDialer.DTOs;
using AutoMapper;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Models;
using Core.Events.SMS;
using Core.Models.configuration;
using Core.Models.location;
using Core.Models.Operation;
using Domain.Models.Apps;
using Domain.Models.Backoffice;
using Domain.Models.ClaroUser;
using Domain.Models.FetchFaceEvidences;
using Domain.Models.Gestion;
using Domain.Models.Groups;
using Domain.Models.location;
using Domain.Models.ManagementsBackoffice;
using Domain.Models.Operation;
using Domain.Models.Operators;
using Domain.Models.Plans;
using Domain.Models.Products;
using Domain.Models.Quotes;
using Domain.Models.Releases;
using Domain.Models.ReSchedule;
using Domain.Models.Rol;
using Domain.Models.Scheduling;
using Domain.Models.SecondRingLoads;
using Domain.Models.Skill;
using Domain.Models.Status;
using Domain.Models.Surveys;
using Domain.Models.Tipification;
using Domain.Models.Tipifications;
using Domain.Models.User;
using Domain.Notification;

namespace CleanArch.Application.AutoMapper
{
    public class DomainToViewModelProfile : Profile
    {
        public DomainToViewModelProfile()
        {
            //User
            CreateMap<User, UserDto>();
            CreateMap<UserRol, UserRolDto>();
            CreateMap<Rol, RolDto>();

            //segmentAppnotification
            CreateMap<SegmentAppNotification, AppNotificationDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.AppNotification.Name))
                .ForMember(d => d.Name2, opt => opt.MapFrom(s => s.AppNotification.Name2))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.AppNotification.Description))
                .ForMember(d => d.feature1, opt => opt.MapFrom(s => s.AppNotification.feature1));



            CreateMap<Tipification, TipificationDto>();
            CreateMap<Course, CourseViewModel>();
            CreateMap<ActiveGestion, GestionDto>();
            CreateMap<AppNotification, AppNotificationDto>();
            CreateMap<NotificationAccount, NotificationAccountViewModel>();
            CreateMap<DatabaseClientDetail, ClientValidatedDataViewModel>()
                .ForMember(d => d.nameClient, opt => opt.MapFrom(s => s.Names))
                .ForMember(d => d.addressClient, opt => opt.MapFrom(s => s.Address))
                .ForMember(d => d.documentClient, opt => opt.MapFrom(s => s.Document))
                .ForMember(d => d.emailClient, opt => opt.MapFrom(s => s.Email));


            CreateMap<SurveySegment, SurveySegmentDto>()
                .ForMember(d => d.Survey, opt => opt.MapFrom(s => s.Survey));
            CreateMap<Survey, SurveyDto>()
                .ForMember(d => d.SurveyQuestions, opt => opt.MapFrom(s => s.SurveyQuestions));
            CreateMap<SurveyQuestion, SurveyQuestionDto>()
                .ForMember(d => d.SurveyResponses, opt => opt.MapFrom(s => s.SurveyResponses))
                .ForMember(d => d.IsMultiple,opt => opt.MapFrom( x => SurveyResolver.TypeCuestion(x.TypeId)));
            CreateMap<SurveyResponse, SurveyResponseDto>();

            //CampaignsData
            CreateMap<SubCampaign, SubCampaignDto>();
                //.ForMember(x => x.campaign, opt => opt.MapFrom(x => x.Campaign));
               
            CreateMap<Campaign, CampaignDto>();


            //ServiceDialer
            CreateMap<ServicesDialer, ServiceDialerDto>();
            CreateMap<ServicesDialer, CampaignDto>()
               .ForMember(d => d.SegmentName, opt => opt.MapFrom(s => s.Segment.Name))
               .ForMember(d => d.SubCampaignId, opt => opt.MapFrom(s => s.Segment.SubCampaignId))
               .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Campaign.Name))
               .ForMember(d => d.SubCampaignName, opt => opt.MapFrom(s => s.Segment.SubCampaign.Name))
               .ForMember(d => d.WelcomeScript, opt => opt.MapFrom(s => s.Segment.GreetingDialog))
               .ForMember(d => d.SegmentId, opt => opt.MapFrom(s => s.SegmentId))
               .ForMember(d => d.Image1, opt => opt.MapFrom(s => s.Segment.SubCampaign.Image1))
               .ForMember(d => d.Image2, opt => opt.MapFrom(s => s.Segment.SubCampaign.Image2))
               .ForMember(d => d.Image3, opt => opt.MapFrom(s => s.Segment.SubCampaign.Image3))
               .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Campaign.Id));


            // Location
            CreateMap<City, CityDto>().ReverseMap();
            CreateMap<State, StateDto>().ReverseMap();

            // Scheduling
            CreateMap<ScheduledAppointment, ScheduledAppointmentDto>();
                //.ForMember(x => x.AppointmentDateText, opt => opt.MapFrom(x => DatetimeFormatResolver.YearMonthDay(x.AppointmentDate)));

            // Backoffice
            CreateMap<BackofficeGestion, BackofficeGestionDto>()
                .ForMember(x => x.ChangeDateText, opt => opt.MapFrom(x => DatetimeFormatResolver.YearMonthDay(x.ChangeDate)));

            // Configuration
            CreateMap<Configuration, ConfigurationDto>();
            CreateMap<Category, CategoryDto>();

            // Plans
            CreateMap<Plan, PlanDto>();

            // Products
            CreateMap<Product, ProductDto>();

            //Group
            CreateMap<Group, GroupDto>();

            // Releases
            CreateMap<Release, ReleaseDto>();

            #region Reschedule
            CreateMap<GestionReschedule, GestionRescheduleDto>()
                .ForMember(x => x.Gestion, opt => opt.MapFrom(j => j.GestionReports));
            CreateMap<GestionReports, GestionDto>();

            #endregion

            // Operators
            CreateMap<Operator, OperatorDto>();

            // Operators
            CreateMap<Quote, QuoteDto>();

            //Status
            CreateMap<Status, StatusDto>();

            //EvidenceGestion
            CreateMap<EvidenceGestion, EvidenceGestionDto>();

            //GestionReportTipification
            CreateMap<GestionReports, GestionReportsDto>()
                .ForMember(d => d.CreatedAtFormat, opt => opt.MapFrom(o => DatetimeFormatResolver.YearMonthDayHoursMinutes(o.CreatedAt)))
                .ForMember(d => d.UpdatedAtFormat, opt => opt.MapFrom(o => DatetimeFormatResolver.YearMonthDayHoursMinutes(o.EndAt)));

            //ClaroUSer
            CreateMap<ClaroUser, ClaroUserDto>().ReverseMap();

            //Segment
            CreateMap<Segment, SegmentDto>();

            //Skill
            CreateMap<Skills, SkillDto>().ReverseMap();

            //ManagementsBackOffice
            CreateMap<BackOfficeManagements, BackOfficeManagementsDto>()
                .ForMember(d => d.InitValidationFormat, opt => opt.MapFrom(o => DatetimeFormatResolver.YearMonthDayHoursMinutes(o.InitValidation)))
                .ForMember(d => d.EndValidationFormat, opt => opt.MapFrom(o => DatetimeFormatResolver.YearMonthDayHoursMinutes(o.EndValidation)))
                .ForMember(d => d.CreatedAtFormat, opt => opt.MapFrom(o => DatetimeFormatResolver.YearMonthDayHoursMinutes(o.CreatedAt)))
                .ForMember(d => d.UpdateAtFormat, opt => opt.MapFrom(o => DatetimeFormatResolver.YearMonthDayHoursMinutes(o.UpdatedAt)));

            CreateMap<BackOfficeManagements, BackOfficeManagementHistoriesDto>()
                .ForMember(d => d.CreatedAtFormat, opt => opt.MapFrom(o => DateTimeHoursFormatResolve.YearMonthDayHoursMinutes(o.CreatedAt)))
                .ForMember(d => d.UpdateAtFormat, opt => opt.MapFrom(o => DateTimeHoursFormatResolve.YearMonthDayHoursMinutes(o.CreatedAt)));

            CreateMap<BackOfficeManagementsHistories, BackOfficeManagementHistoriesDto>()
                .ForMember(d => d.CreatedAtFormat, opt => opt.MapFrom(o => DateTimeHoursFormatResolve.YearMonthDayHoursMinutes(o.CreatedAt)))
                .ForMember(d => d.UpdateAtFormat, opt => opt.MapFrom(o => DateTimeHoursFormatResolve.YearMonthDayHoursMinutes(o.CreatedAt)));

            //BackOfficeOperations
            CreateMap<BackOfficeOperations, BackOfficeOperationDto>();

            //Department
            CreateMap<Department, DepartmentDto > ().ReverseMap();

            //NewCity
            CreateMap<NewCity,NewCityDto>().ReverseMap();

            CreateMap<Country, CountryDto>().ReverseMap();

            CreateMap<Department, CountryDepartmentDto>();

            CreateMap<DatabaseClientBase, DataBaseClientDto>();

            CreateMap<SecondRingLoad, SecondRingLoadDto>();

            //FetchFaceEvidence
            CreateMap<FetchFaceEvidence, FetchFaceEvidenceDto>().ReverseMap();

            CreateMap<SubCategoryTipificationDto, SubCategoryTipification>();


        }
    }
}