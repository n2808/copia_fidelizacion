﻿using Application.Backoffice.Commands.DeleteBackoffice;
using Application.Backoffice.Commands.PostBackoffice;
using Application.Backoffice.Commands.PutBackoffice;
using Application.Backoffice.Queries;
using Application.BackOfficeManagements.Command.Post;
using Application.BackOfficeManagements.Command.Put;
using Application.ClaroUser.Command.Post;
using Application.ClaroUser.Command.Put;
using Application.ClaroUser.Queries;
using Application.Configurations.Categories.Command;
using Application.DTOs;
using Application.DTOs.ActiveGestion;
using Application.DTOs.Backoffice;
using Application.DTOs.BackOfficeManagements;
using Application.DTOs.Configurations;
using Application.DTOs.DataBaseClient;
using Application.DTOs.EvidenceGestion;
using Application.DTOs.FetchFaceEvidence;
using Application.DTOs.GestionReports;
using Application.DTOs.Group;
using Application.DTOs.location;
using Application.DTOs.ManagementsBackOffice;
using Application.DTOs.Operators;
using Application.DTOs.Plans;
using Application.DTOs.Products;
using Application.DTOs.Quotes;
using Application.DTOs.Release;
using Application.DTOs.Scheduling;
using Application.DTOs.SecondRingLoad;
using Application.DTOs.Segment;
using Application.DTOs.Skills;
using Application.DTOs.Status;
using Application.DTOs.User;
using Application.EvidenceGestion.Queries;
using Application.FetchFaceEvidence.Commands;
using Application.Group.Command;
using Application.Location.Queries.Countries;
using Application.Location.Queries.Departments;
using Application.Location.Queries.NewCities;
using Application.Operators.Command;
using Application.Plans.Command;
using Application.Products.Command;
using Application.Quotes.Command;
using Application.Releases.Command.Post;
using Application.Releases.Command.Put;
using Application.Rol.Commands.DeleteRol;
using Application.Rol.Commands.PostRol;
using Application.Rol.Commands.PutRol;
using Application.Scheduling.Commands.PostScheduled;
using Application.Scheduling.Commands.PutScheduled;
using Application.Segments.Command;
using Application.Segments.Command.Post;
using Application.Segments.Command.Put;
using Application.Segments.Queries;
using Application.Skills.Command.Delete;
using Application.Skills.Command.Post;
using Application.Skills.Command.Put;
using Application.Skills.Queries;
using Application.Status.Command;
using Application.User.Commands.DeleteUser;
using Application.User.Commands.PostUser;
using Application.User.Commands.PutUser;
using Application.ViewModel;
using AutoMapper;
using Core.Models.configuration;
using Core.Models.location;
using Core.Models.Operation;
using Domain.Models.Backoffice;
using Domain.Models.ClaroUser;
using Domain.Models.FetchFaceEvidences;
using Domain.Models.Gestion;
using Domain.Models.Groups;
using Domain.Models.location;
using Domain.Models.ManagementsBackoffice;
using Domain.Models.Operation;
using Domain.Models.Operators;
using Domain.Models.Plans;
using Domain.Models.Products;
using Domain.Models.Quotes;
using Domain.Models.Releases;
using Domain.Models.Rol;
using Domain.Models.Scheduling;
using Domain.Models.SecondRingLoads;
using Domain.Models.Skill;
using Domain.Models.Status;
using Domain.Models.Tipification;
using Domain.Models.Tipifications;
using Domain.Models.User;

namespace CleanArch.Application.AutoMapper
{
    public class ViewModelToDomainProfile : Profile
    {
        public ViewModelToDomainProfile()
        {
            CreateMap<UserDto, User>().ReverseMap();
            CreateMap<BackofficeUserDto, User>().ReverseMap();
            CreateMap<PostUserCommand, User>();
            CreateMap<UserDto, User>().ReverseMap();
            CreateMap<RolDto, Rol>().ReverseMap();
            CreateMap<PostRolCommand, Rol>();
            CreateMap<PutRolCommand, Rol>();
            CreateMap<DeleteRolCommand, Rol>();
            CreateMap<PutStatusActivateUserById, User>();
            CreateMap<PutStatusDeactivateUserById, User>();
            CreateMap<PutUserCommand, User>();
            CreateMap<DeleteUserCommand, User>();
            CreateMap<TipificationDto, Tipification>();
            CreateMap<EndGestionViewModel, ActiveGestion>();
            CreateMap<PostScheduledAppointmentCommand, ScheduledAppointment>();
            CreateMap<ScheduledAppointmentDto, ScheduledAppointment>();
            CreateMap<PutScheduledAppointmentCommand, ScheduledAppointment>();
            CreateMap<PutBackofficeGestionCommand, BackofficeGestion>();
            CreateMap<PostBackofficeGestionCommand, BackofficeGestion>();
            CreateMap<PostBackofficeGestionCommand, BackofficeGestion>();
            CreateMap<BackofficeGestionDto, BackofficeGestion>();

            // Configuration
            CreateMap<ConfigurationDto, Configuration>();
            CreateMap<CategoryDto, Category>();
            CreateMap<PutCategoryCommand, Category>();
            CreateMap<PostCategoryCommand, Category>();

            // Plans           
            CreateMap<PlanDto, Plan>();
            CreateMap<PutPlanCommand, Plan>();
            CreateMap<PostPlanCommand, Plan>();

            // Products           
            CreateMap<ProductDto, Product>();
            CreateMap<PutProductCommand, Product>();
            CreateMap<PostProductCommand, Product>();

            // Group
            CreateMap<GroupDto, Group>();
            CreateMap<PostGroupCommand, Group>();
            CreateMap<PutGroupCommand, Group>();

            // Operators
            CreateMap<OperatorDto, Operator>();
            CreateMap<PostOperatorCommand, Operator>();
            CreateMap<PutOperatorCommand, Operator>();

            // Quotes
            CreateMap<QuoteDto, Quote>();
            CreateMap<PostQuoteCommand, Quote>();
            CreateMap<PutQuoteCommand, Quote>();

            //Rols
            CreateMap<PutStatusActivateRolById, Rol>();
            CreateMap<PutStatusDeactivateRolById, Rol>();

            //UserRol
            CreateMap<UserRolDto, UserRol>().ReverseMap();
            CreateMap<PutUserCommand, UserRol>();

            //ActiveGestion
            CreateMap<ActiveGestionDto, ActiveGestion>().ReverseMap();

            //status
            CreateMap<StatusDto, Status>();
            CreateMap<PostStatusCommand, Status>();
            CreateMap<PutStatusCommand, Status>();

            //EvidenceGestion
            CreateMap<EvidenceGestionDto, EvidenceGestion>();
            CreateMap<EvidenceGestionCommand, EvidenceGestion>();

            //GestionReports
            CreateMap<GestionReportsDto, GestionReports>().ReverseMap();

            //ClaroUSer
            CreateMap<ClaroUserDto, ClaroUser>().ReverseMap();
            CreateMap<ClaroUserDto, ClaroUser>();
            CreateMap<GetCodeClaroUserQuery, ClaroUser>();
            CreateMap<PostClaroUserCommand, ClaroUser>();
            CreateMap<PutClaroUserCommand, ClaroUser>();
            CreateMap<PutClaroUserCommand, ClaroUserDto>();

            //Segment
            CreateMap<SegmentDto, Segment>();
            CreateMap<PostSegmentCommand, Segment>();
            CreateMap<PutSegmentCommand, Segment>();
            CreateMap<DeleteGroupCommand, Segment>();
            CreateMap<PutStatusActivateSegmentByIdCommand, Segment>();
            CreateMap<PutStatusDeActivateSegmentByIdCommand, Segment>();
            CreateMap<GetSegmentByIdQuery, Segment>();
            CreateMap<GetSegmentByIdCampaingQuery, Segment>();

            //Skill
            CreateMap<SkillDto, Skills>().ReverseMap();
            CreateMap<PostSkillCommand, Skills>();
            CreateMap<PutSkillsCommand, Skills>();
            CreateMap<DeleteSkillCommand, Skills>();
            CreateMap<PutSkillsActivateCommand, Skills>();
            CreateMap<PutSkillsDeActivateCommand, Skills>();
            CreateMap<GetSkillQueryById, Skills>();

            //ManagementBackOffice
            CreateMap<BackOfficeManagementsDto, BackOfficeManagements>();
            CreateMap<PostBackOfficeManagementsCommand, BackOfficeManagements>();
            CreateMap<PutBackOfficeManagementsCommand, BackOfficeManagements>();
            CreateMap<PutEndBackOfficeManagementCommand, BackOfficeManagements>();
            CreateMap<PutEndValidatorManagementCommand, BackOfficeManagements>();
            CreateMap<CreateBackOfficeManegementViewModel, BackOfficeManagements>();

            CreateMap<BackOfficeManagementHistoriesDto, BackOfficeManagements>();
            CreateMap<BackOfficeManagementHistoriesDto, BackOfficeManagementsHistories>();

            //BackOfficeOperations
            CreateMap<BackOfficeOperationDto, BackOfficeOperations>();
            CreateMap<PostBackOfficeOperationCommand, BackOfficeOperations>();
            CreateMap<PutBackOfficeOperationCommand, BackOfficeOperations>();
            CreateMap<DeleteBackOfficeOperationCommand, BackOfficeOperations>();
            CreateMap<PutBackOfficeOperationActivateCommand, BackOfficeOperations>();
            CreateMap<PutBackOfficeOperationsDeActivateCommand, BackOfficeOperations>();
            CreateMap<GetBackOfficeOperationQueryById, BackOfficeOperations>();

            //Departments
            CreateMap<DepartmentDto, Department>().ReverseMap();
            CreateMap<GetDepartmentsByIdQuery, Department>();

            //City
            CreateMap<CityDto, City>().ReverseMap();
            CreateMap<GetNewCitiesByIdQuery, NewCity>();

            CreateMap<CountryDto, Country>().ReverseMap();
            CreateMap<GetCountriesByIdQuery, Country>();

            //DataBaseClient
            CreateMap<DataBaseClientDto, DatabaseClientBase>().ReverseMap();

            CreateMap<SecondRingLoadDto, SecondRingLoad>().ReverseMap();


            //FetchFaceEvidence
            CreateMap<FetchFaceEvidence, FetchFaceEvidenceDto>();
            CreateMap<PostFetchFaceEvidenceCommand, FetchFaceEvidence>().ReverseMap();

            CreateMap<SubCategoryTipification, SubCategoryTipificationDto>();

            // Releases
            CreateMap<ReleaseDto, Release>();
            CreateMap<PostReleaseCommand, Release>();
            CreateMap<PutReleasesCommand, Release>();
            CreateMap<PutReleasesDeActivateCommand, Release>();
            CreateMap<PutReleasesActivateCommand, Release>();


        }
    }
}

