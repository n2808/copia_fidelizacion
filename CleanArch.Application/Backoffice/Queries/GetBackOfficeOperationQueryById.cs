﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Queries
{
    public class GetBackOfficeOperationQueryById : IRequest<BackOfficeOperationDto>
    {
        public int Id { get; set; }
    }

    public class GetBackOfficeOperationQueryByIdHandler : IRequestHandler<GetBackOfficeOperationQueryById, BackOfficeOperationDto>
    {
        private readonly IBackOfficeOperationsService _backOfficeOperationsService;
        private readonly IMapper _mapper;

        public GetBackOfficeOperationQueryByIdHandler(IBackOfficeOperationsService backOfficeOperationsService, IMapper mapper)
        {
            _backOfficeOperationsService = backOfficeOperationsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeOperationDto> Handle(GetBackOfficeOperationQueryById request, CancellationToken cancellationToken)
        {
            var backOffice = await _backOfficeOperationsService.GetBackOfficeOperationById(request.Id);
            return _mapper.Map<BackOfficeOperationDto>(backOffice);
        }
    }
}
