﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Queries
{
    public class GetBackOfficeOperationsQuery : IRequest<List<BackOfficeOperationDto>>
    {
        public int without { get; set; }
    }

    public class GetBackOfficeOperationsQueryHandler : IRequestHandler<GetBackOfficeOperationsQuery, List<BackOfficeOperationDto>>
    {
        private readonly IBackOfficeOperationsService _backOfficeOperationsService;

        public GetBackOfficeOperationsQueryHandler(IBackOfficeOperationsService backOfficeOperationsService)
        {
            _backOfficeOperationsService = backOfficeOperationsService;
        }
        public async Task<List<BackOfficeOperationDto>> Handle(GetBackOfficeOperationsQuery request, CancellationToken cancellationToken)
        {
            return await _backOfficeOperationsService.GetBackOfficeOperations(request.without);
        }
    }
}
