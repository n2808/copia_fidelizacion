﻿using Application.Interfaces.Backoffice;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.DeleteBackoffice
{
    public class DeleteBackofficeGestionCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class GetBackofficeGestionByIdQueryHandler : IRequestHandler<DeleteBackofficeGestionCommand, bool>
    {

        private readonly IBackofficeGestionService _backofficeGestionService;

        public GetBackofficeGestionByIdQueryHandler(IBackofficeGestionService backofficeGestionService)
        {
            _backofficeGestionService = backofficeGestionService;

        }

        public async Task<bool> Handle(DeleteBackofficeGestionCommand request, CancellationToken cancellationToken)
        {
            _backofficeGestionService.DeleteBackofficeGestion(request.Id);

            return true;
        }
    }
}
