﻿using Application.Interfaces.Backoffice;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.DeleteBackoffice
{
    public class DeleteBackOfficeOperationCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }

    public class DeleteBackOfficeOperationCommandHandler : IRequestHandler<DeleteBackOfficeOperationCommand, bool>
    {
        private readonly IBackOfficeOperationsService _backOfficeOperationsService;

        public DeleteBackOfficeOperationCommandHandler(IBackOfficeOperationsService backOfficeOperationsService)
        {
            _backOfficeOperationsService = backOfficeOperationsService;
        }

        public async Task<bool> Handle(DeleteBackOfficeOperationCommand request, CancellationToken cancellationToken)
        {
            await _backOfficeOperationsService.DeleteBackOfficeOperation(request.Id);
            return true;
        }
    }
}
