﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using Domain.Models.Backoffice;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.PutBackoffice
{
    public class PutBackofficeGestionCommand : IRequest<BackofficeGestionDto>
    {
        public Guid Id { get; set; }
        public DateTime? ChangeDate { get; set; }
        public string BeforeTMCode { get; set; }
        public string BeforeValue { get; set; }
        public string BeforeName { get; set; }
        public string NewValue { get; set; }
        public string NewValueTax { get; set; }
        public string NewTMCode { get; set; }
        public string NewName { get; set; }
        public string Account { get; set; }
        public string Observation { get; set; }
        public Guid? TypePlanId { get; set; }
    }

    public class PutBackofficeGestionCommandHandler : IRequestHandler<PutBackofficeGestionCommand, BackofficeGestionDto>
    {
        private readonly IBackofficeGestionService _backofficeGestionService;
        private readonly IMapper _mapper;

        public PutBackofficeGestionCommandHandler(IBackofficeGestionService backofficeGestionService, IMapper mapper)
        {
            _backofficeGestionService = backofficeGestionService;
            _mapper = mapper;
        }

        public async Task<BackofficeGestionDto> Handle(PutBackofficeGestionCommand request, CancellationToken cancellationToken)
        {
            BackofficeGestion backofficeGestion = _backofficeGestionService.PutBackofficeGestion(request);

            return _mapper.Map<BackofficeGestionDto>(backofficeGestion);
        }
    }
}
