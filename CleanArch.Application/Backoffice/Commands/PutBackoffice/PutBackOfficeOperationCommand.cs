﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.PutBackoffice
{
    public class PutBackOfficeOperationCommand : IRequest<BackOfficeOperationDto>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string TypeOperation { get; set; }
        public string Operation { get; set; }
        public string Process { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
    }

    public class PutBackOfficeOperationCommandHandler : IRequestHandler<PutBackOfficeOperationCommand, BackOfficeOperationDto>
    {
        private readonly IBackOfficeOperationsService _backOfficeOperations;
        private readonly IMapper _mapper;
        public PutBackOfficeOperationCommandHandler(IBackOfficeOperationsService backOfficeOperations, IMapper mapper)
        {
            _backOfficeOperations = backOfficeOperations;
            _mapper = mapper;
        }
        public async Task<BackOfficeOperationDto> Handle(PutBackOfficeOperationCommand request, CancellationToken cancellationToken)
        {
            var backOffice = await _backOfficeOperations.PutBackOfficeOperation(request);
            return _mapper.Map<BackOfficeOperationDto>(backOffice);
        }
    }
}
