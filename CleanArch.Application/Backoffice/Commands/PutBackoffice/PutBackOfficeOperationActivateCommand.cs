﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.PutBackoffice
{
    public class PutBackOfficeOperationActivateCommand : IRequest<BackOfficeOperationDto>
    {
        public int Id { get; set; }
    }

    public class PutBackOfficeOperationActivateCommandHandler : IRequestHandler<PutBackOfficeOperationActivateCommand, BackOfficeOperationDto>
    {
        private readonly IBackOfficeOperationsService _backOfficeOperationsService;
        private readonly IMapper _mapper;

        public PutBackOfficeOperationActivateCommandHandler(IBackOfficeOperationsService backOfficeOperationsService, IMapper mapper)
        {
            _backOfficeOperationsService = backOfficeOperationsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeOperationDto> Handle(PutBackOfficeOperationActivateCommand request, CancellationToken cancellationToken)
        {
            var backOffice = await _backOfficeOperationsService.PutActivateBackOfficeOperation(request);
            return _mapper.Map<BackOfficeOperationDto>(backOffice);
        }
    }
}
