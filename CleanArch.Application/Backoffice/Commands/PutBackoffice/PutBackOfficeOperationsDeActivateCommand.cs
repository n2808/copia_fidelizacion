﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.PutBackoffice
{
    public class PutBackOfficeOperationsDeActivateCommand : IRequest<BackOfficeOperationDto>
    {
        public int Id { get; set; }
    }

    public class PutBackOfficeOperationsDeActivateCommandHandler : IRequestHandler<PutBackOfficeOperationsDeActivateCommand, BackOfficeOperationDto>
    {
        private readonly IBackOfficeOperationsService _backOfficeOperationsService;
        private readonly IMapper _mapper;

        public PutBackOfficeOperationsDeActivateCommandHandler(IBackOfficeOperationsService backOfficeOperationsService, IMapper mapper)
        {
            _backOfficeOperationsService = backOfficeOperationsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeOperationDto> Handle(PutBackOfficeOperationsDeActivateCommand request, CancellationToken cancellationToken)
        {
            var backOffice = await _backOfficeOperationsService.PutDeActivateBackOfficeOperation(request);
            return _mapper.Map<BackOfficeOperationDto>(backOffice);
        }
    }
}
