﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using Domain.Models.Backoffice;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.PostBackoffice
{
    public class PostBackOfficeOperationCommand : IRequest<BackOfficeOperationDto>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string TypeOperation { get; set; }
        public string Operation { get; set; }
        public string Process { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
    }

    public class PostBackOfficeOperationCommandHandler : IRequestHandler<PostBackOfficeOperationCommand, BackOfficeOperationDto>
    {
        private readonly IBackOfficeOperationsService _backOfficeOperationsService;
        private readonly IMapper _mapper;

        public PostBackOfficeOperationCommandHandler(IBackOfficeOperationsService backOfficeOperationsService, IMapper mapper)
        {
            _backOfficeOperationsService = backOfficeOperationsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeOperationDto> Handle(PostBackOfficeOperationCommand request, CancellationToken cancellationToken)
        {
            var backOffice = await _backOfficeOperationsService.PostBackOfficeOperation(_mapper.Map<BackOfficeOperations>(request));
            return _mapper.Map<BackOfficeOperationDto>(backOffice);
        }
    }
}
