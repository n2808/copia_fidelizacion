﻿using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using Domain.Models.Backoffice;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Backoffice.Commands.PostBackoffice
{
    public class PostBackofficeGestionCommand : IRequest<BackofficeGestionDto>
    {
        public DateTime? ChangeDate { get; set; }
        public string BeforeTMCode { get; set; }
        public string BeforeValue { get; set; }
        public string BeforeName { get; set; }
        public string NewValue { get; set; }
        public string NewValueTax { get; set; }
        public string NewTMCode { get; set; }
        public string NewName { get; set; }
        public string Account { get; set; }
        public string Observation { get; set; }
        public Guid? TypePlanId { get; set; }

    }

    public class PostBackofficeGestionCommandHandler : IRequestHandler<PostBackofficeGestionCommand, BackofficeGestionDto>
    {
        private readonly IBackofficeGestionService _backofficeGestionService;
        private readonly IMapper _mapper;
        public PostBackofficeGestionCommandHandler(IBackofficeGestionService backofficeGestionService, IMapper mapper)
        {
            _backofficeGestionService = backofficeGestionService;
            _mapper = mapper;
        }
        public async Task<BackofficeGestionDto> Handle(PostBackofficeGestionCommand request, CancellationToken cancellationToken)
        {
            BackofficeGestion backofficeGestion = _backofficeGestionService
                                                  .PostBackofficeGestion(_mapper.Map<BackofficeGestion>(request));

            return _mapper.Map<BackofficeGestionDto>(backofficeGestion);
        }
    }
}
