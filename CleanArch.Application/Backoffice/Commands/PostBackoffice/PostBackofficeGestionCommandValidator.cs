﻿using Application.Interfaces.Backoffice;
using FluentValidation;

namespace Application.Backoffice.Commands.PostBackoffice
{
    public class PostBackofficeGestionCommandValidator : AbstractValidator<PostBackofficeGestionCommand>
    {
        private readonly IBackofficeGestionService _backofficeGestionService;

        public PostBackofficeGestionCommandValidator(IBackofficeGestionService backofficeGestionService)
        {
            _backofficeGestionService = backofficeGestionService;

            //RuleFor(x => x.ChangeDate)
            // .NotEmpty().WithMessage("La fecha de Agendamiento no puede estar vacía")
            // .GreaterThanOrEqualTo(DateTime.Now).WithMessage("La fecha no puede ser menor a la actual");

            //RuleFor(x => x.BeforeTMCode)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.BeforeValue)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.BeforeTMCode)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.BeforeName)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.NewValue)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.NewValueTax)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.NewTMCode)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.NewName)
            //   .NotEmpty()
            //   .WithMessage("La gestión no existe");

            //RuleFor(x => x.Account)
            // .NotEmpty()
            // .WithMessage("La gestión no existe");

            //RuleFor(x => x.TypePlanId)
            // .NotEmpty()
            // .WithMessage("La gestión no existe");

        }
    }
}
