﻿using Application.DTOs.User;
using Application.Interfaces.User;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User.Commands.PutUser
{
    public class PutUserCommand : IRequest<UserDto>
    {
        public Guid Id { get; set; }
        public string Document { get; set; }
        public string Names { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public string PassWord { get; set; }

        public string login { get; set; }


        public Guid? CoordinadorId { get; set; }

        public Guid? GerenteId { get; set; }

        public int SubCampaignId { get; set; }

        public List<Guid> Roles { get; set; }
    }

    public class PutUserCommandHandler : IRequestHandler<PutUserCommand, UserDto>
    {
        private readonly IUserService _user;
        private readonly IMapper _mapper;

        public PutUserCommandHandler(IUserService user, IMapper mapper)
        {
            _user = user;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(PutUserCommand request, CancellationToken cancellationToken)
        {
            var data = await _user.GetAllByLogin(request.login);

            if (!data.status)
            {
                throw new Exception(data.message + " " + string.Join(", ", data.campaings.ToArray()));
            }

            var login = _user.GetByLoginPut(request.login, request.SubCampaignId, request.Id);

            if (!login)
            {
                throw new Exception("El login ya existe en la subcampaña seleccionada");
            }

            var user = _user.PutUser(request);
            return _mapper.Map<UserDto>(user);
        }
    }
}
