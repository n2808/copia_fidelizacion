﻿using Application.DTOs.User;
using Application.Interfaces.User;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User.Commands.PutUser
{
    public class PutStatusDeactivateUserById : IRequest<UserDto>
    {
        public Guid Id { get; set; }
    }

    public class PutStatusDeactivateUserByIdCommandHandler : IRequestHandler<PutStatusDeactivateUserById, UserDto>
    {
        private readonly IUserService _userService;
        private readonly IMapper _autoMapper;

        public PutStatusDeactivateUserByIdCommandHandler(IUserService user, IMapper autoMapper)
        {
            _userService = user;
            _autoMapper = autoMapper;
        }

        public async Task<UserDto> Handle(PutStatusDeactivateUserById request, CancellationToken cancellationToken)
        {
            var user = _userService.PutStatusDeactivateUserById(request);

            return _autoMapper.Map<UserDto>(user);
        }
    }

}
