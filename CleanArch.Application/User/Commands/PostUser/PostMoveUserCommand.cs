﻿using Application.Interfaces.User;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User.Commands.PutUser
{
    public class PostMoveUserCommand : IRequest<bool>
    {
        public int SubCampaing { get; set; }
        public List<Guid> usersIds { get; set; }
    }

    public class PostMoveUserCommandHandler : IRequestHandler<PostMoveUserCommand, bool>
    {
        private readonly IUserService _userService;

        public PostMoveUserCommandHandler(IUserService userService)
        {
            _userService = userService;
        }
        public async Task<bool> Handle(PostMoveUserCommand request, CancellationToken cancellationToken)
        {
            _userService.PostSubcampaingUsers(request);
            return true;
        }
    }
}
