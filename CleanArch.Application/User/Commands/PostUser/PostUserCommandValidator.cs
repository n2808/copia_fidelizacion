﻿using Application.Interfaces.User;
using FluentValidation;

namespace Application.User.Commands.PostUser
{
    public class PostUserCommandValidator : AbstractValidator<PostUserCommand>
    {
        private readonly IUserService _userService;
        public PostUserCommandValidator(
            IUserService userService
            )
        {
            _userService = userService;

            RuleFor(x => x.Document)
               .NotEmpty()
               .WithMessage("El documento no puede estar vacía");

            RuleFor(x => x.Names)
               .NotEmpty()
               .WithMessage("El nombre no puede estar vacía");

            RuleFor(x => x.LastName)
                   .NotEmpty()
                   .WithMessage("El apellido no puede estar vacía");

            /*RuleFor(x => new { x.login, x.SubCampaignId })
                .NotEmpty()
                .Must(m => _userService.GetByLogin(m.login, m.SubCampaignId))
                .WithMessage("El login ya existe en la subcampaña seleccionada");*/


            RuleFor(x => x.login)
               .NotEmpty()
               .WithMessage("El login no puede estar vacía");

            RuleFor(x => x.PassWord)
                .NotEmpty()
                .WithMessage("El password no puede estar vacía");
        }
    }
}
