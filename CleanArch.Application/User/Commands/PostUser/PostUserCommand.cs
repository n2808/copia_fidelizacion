﻿using Application.Core.Exceptions;
using Application.DTOs.User;
using Application.Interfaces.User;
using AutoMapper;
using Domain.Models.User;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User.Commands.PostUser
{
    public class PostUserCommand : IRequest<UserDto>
    {
        public string Document { get; set; }
        public string Names { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public string PassWord { get; set; }
        public string login { get; set; }
        public Guid? CoordinadorId { get; set; }
        public Guid? GerenteId { get; set; }
        public int SubCampaignId { get; set; }
        public List<Guid> Roles { get; set; }
    }

    public class PostUserCommandHandler : IRequestHandler<PostUserCommand, UserDto>
    {
        private readonly IUserService _user;
        private readonly IMapper _autoMapper;

        public PostUserCommandHandler(IUserService user, IMapper autoMapper)
        {
            _user = user;
            _autoMapper = autoMapper;
        }


        public async Task<UserDto> Handle(PostUserCommand request, CancellationToken cancellationToken)
        {

            var login = _user.GetByLogin(request.login, request.SubCampaignId);

            if (!login)
            {
                throw new Exception("El login ya existe en la subcampaña seleccionada");
            }

           var data = await _user.GetAllByLogin(request.login);

            if(!data.status)
            {
                throw new Exception(data.message + " " + string.Join(", ", data.campaings.ToArray()));
            }

            var ListUserRol = new List<UserRol>() { };

            foreach (var rol in request.Roles)
            {
                ListUserRol.Add(new UserRol()
                {
                    RolId = rol
                });
            }

            var userObj = _autoMapper.Map<Domain.Models.User.User>(request);
            userObj.UserRol = ListUserRol;
            var user = _user
                    .PostUser(userObj);

            return _autoMapper.Map<UserDto>(user);
        }
    }
}
