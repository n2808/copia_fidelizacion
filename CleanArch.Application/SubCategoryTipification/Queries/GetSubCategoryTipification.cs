﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.SubCategoryTipification.Queries
{
    public class GetSubCategoryTipification : IRequest<List<SubCategoryTipificationDto>>
    {
        public int Code { get; set; }
    }

    public class GetSubCategoryTipificationHandler : IRequestHandler<GetSubCategoryTipification, List<SubCategoryTipificationDto>>
    {

        private readonly ISubCategoryTipificationService _subCategoryTipificationService;
        private readonly IMapper _mapper;

        public GetSubCategoryTipificationHandler(ISubCategoryTipificationService subCategoryTipificationService, IMapper mapper)
        {

            _subCategoryTipificationService = subCategoryTipificationService;
            _mapper = mapper;
        }
        public async Task<List<SubCategoryTipificationDto>> Handle(GetSubCategoryTipification request, CancellationToken cancellationToken)
        {
            return await _subCategoryTipificationService.GetSubCategory(request.Code);
        }
    }
}
