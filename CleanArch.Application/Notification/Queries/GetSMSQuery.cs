﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Notification.Queries
{
    public class GetSMSQuery : IRequest<bool>
    {
        public string Id { get; set; }
    }
}
