﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using CleanArch.Application.Interfaces;
using CleanArch.Application.Interfaces.Notifications;
using Application.Core.Exceptions;

namespace Application.Notification.Commands
{
    public class SendSMSCommand : IRequest
    {
        public int GestionId { get; set; }
        public string phone { get; set; }
        public int AppId { get; set; }
    }

    public class SendSMSCommandHandler : IRequestHandler<SendSMSCommand>
    {
        private readonly ISMSService _sMSService;
        private readonly INotificationLogService _notificationLogService;
        private readonly IActiveGestionService _activeGestionService;
        private readonly ISegmentAppNotificationService _segmentAppNotificationService;

        public SendSMSCommandHandler(
            INotificationLogService notificationLogService,
            ISegmentAppNotificationService segmentAppNotificationService,
            IActiveGestionService activeGestionService,
            ISMSService sMSService
        )
        {
            _sMSService = sMSService;
            _notificationLogService = notificationLogService;
            _segmentAppNotificationService = segmentAppNotificationService;
            _activeGestionService = activeGestionService;
        }

        public async Task<Unit> Handle(SendSMSCommand request, CancellationToken cancellationToken)
        {

            var gestion = await _activeGestionService.GetGestionById(request.GestionId);

            var app = _segmentAppNotificationService.GetById(request.AppId);
            //var app = _segmentAppNotificationService.GetById(41);

            if(app.NotificationAccountId == null)
                throw new BadRequestException("Cuenta no configurada");

            _notificationLogService.Post(await _sMSService.Send(request.phone,app.Message,(int)app.NotificationAccountId), gestion);

            return Unit.Value;
        }

    }
}
