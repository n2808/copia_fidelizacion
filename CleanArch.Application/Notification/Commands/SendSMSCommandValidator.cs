﻿using CleanArch.Application.Interfaces;
using Domain.Interfaces;
using FluentValidation;

namespace Application.Notification.Commands
{
    public class SendSMSCommandValidator : AbstractValidator<SendSMSCommand>
    {
        private readonly IAuthRepository _authRepository;
        private readonly IGestionService _gestionService;
        private readonly IActiveGestionService _activeGestionService;
        private readonly ISegmentAppNotificationService _segmentAppNotificationService;
        public SendSMSCommandValidator(
            IAuthRepository authRepository, 
            IGestionService gestionService, 
            IActiveGestionService activeGestionService,
            ISegmentAppNotificationService segmentAppNotificationService
        )
        {
            _authRepository = authRepository;
            _gestionService = gestionService;
             _activeGestionService = activeGestionService;
             _segmentAppNotificationService = segmentAppNotificationService;

            RuleFor(c => c.AppId).NotNull().Must(_segmentAppNotificationService.CheckById);
            RuleFor(c => c.GestionId).NotNull().Must(_activeGestionService.CheckAtiveGestionById).WithMessage("La gestion no esta disponible");
            RuleFor(c => c.phone).NotNull().Length(10).WithMessage("el telefono no es valido");          


        }

        

    }
}
