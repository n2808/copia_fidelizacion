﻿using CleanArch.Application.Interfaces;
using CleanArch.Application.Interfaces.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Notification.Commands
{
    public class RpaSendSMSCommand : IRequest
    {
        public string phone { get; set; }
        public string message { get; set; }
    }

    public class RpaSendSMSCommandHandler : IRequestHandler<RpaSendSMSCommand>
    {
        private readonly ISMSService _sMSService;
        private readonly INotificationLogService _notificationLogService;
        public RpaSendSMSCommandHandler(ISMSService sMSService, INotificationLogService notificationLogService)
        {
            _sMSService = sMSService;
            _notificationLogService = notificationLogService;
        }
        public async Task<Unit> Handle(RpaSendSMSCommand request, CancellationToken cancellationToken)
        {
            //await _sMSService.SendRPA(request.phone, request.message);
            return Unit.Value;
        }
    }
}
