﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ClaroUser.Queries
{
    public class GetCodeClaroUserQuery : IRequest<ClaroUserDto>
    {
        public string Code { get; set; }
    }

    public class GetCodeClaroUserQueryHandler : IRequestHandler<GetCodeClaroUserQuery, ClaroUserDto>
    {

        private readonly IClaroUserService _claroUserService;
        private readonly IMapper _mapper;

        public GetCodeClaroUserQueryHandler(IClaroUserService claroUserService, IMapper mapper)
        {
            _claroUserService = claroUserService;
            _mapper = mapper;

        }
        public async Task<ClaroUserDto> Handle(GetCodeClaroUserQuery request, CancellationToken cancellationToken)
        {
            var userClaro = await _claroUserService.GetUserByCode(request);
            return _mapper.Map<ClaroUserDto>(userClaro);
        }
    }
}
