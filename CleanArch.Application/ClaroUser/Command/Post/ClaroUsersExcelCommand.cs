﻿using Application.Common.Response;
using Application.DTOs;
using Application.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ClaroUser.Command.Post
{
    public class ClaroUsersExcelCommand : IRequest<ApiResponse<IList<ClaroUserDto>>>
    {
        public IFormFile File { get; set; }
    }

    public class ClaroUsersExcelCommandHandler : IRequestHandler<ClaroUsersExcelCommand, ApiResponse<IList<ClaroUserDto>>>
    {
        private readonly IClaroUserService _claroUserService;
        public ClaroUsersExcelCommandHandler(IClaroUserService claroUserService)
        {
            _claroUserService = claroUserService;
        }
        public async Task<ApiResponse<IList<ClaroUserDto>>> Handle(ClaroUsersExcelCommand request, CancellationToken cancellationToken)
        {
            return await _claroUserService.SaveFile(request.File);
        }
    }
}
