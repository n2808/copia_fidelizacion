﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ClaroUser.Command.Post
{
    public class PostClaroUserCommand : IRequest<ClaroUserDto>
    {
        public string Code { get; set; }
        public string Charge { get; set; }
        public string Location { get; set; }
        public string Region { get; set; }
        public string Ring { get; set; }
        public string Supervisor { get; set; }
        public string IdentificationSupervisor { get; set; }
        public string Coordinator { get; set; }
        public string IdentificationCoordinator { get; set; }
    }

    public class PostClaroUserCommandHandler : IRequestHandler<PostClaroUserCommand, ClaroUserDto>
    {
        private readonly IClaroUserService _claroUserService;
        private readonly IMapper _mapper;
        public PostClaroUserCommandHandler(IClaroUserService claroUserService, IMapper mapper)
        {
            _claroUserService = claroUserService;
            _mapper = mapper;
        }
        public async Task<ClaroUserDto> Handle(PostClaroUserCommand request, CancellationToken cancellationToken)
        {
            var claroUser = await _claroUserService.PostClaroUser(_mapper.Map<Domain.Models.ClaroUser.ClaroUser>(request));
            return _mapper.Map<ClaroUserDto>(claroUser);
        }
    }
}
