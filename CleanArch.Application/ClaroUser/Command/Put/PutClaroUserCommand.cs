﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ClaroUser.Command.Put
{
    public class PutClaroUserCommand : IRequest<ClaroUserDto>
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Charge { get; set; }
        public string Location { get; set; }
        public string Region { get; set; }
        public string Ring { get; set; }
        public string Supervisor { get; set; }
        public string IdentificationSupervisor { get; set; }
        public string Coordinator { get; set; }
        public string IdentificationCoordinator { get; set; }
    }

    public class PutClaroUserCommandHandler : IRequestHandler<PutClaroUserCommand, ClaroUserDto>
    {
        private readonly IClaroUserService _claroUserService;
        private readonly IMapper _mapper;

        public PutClaroUserCommandHandler(IClaroUserService claroUserService, IMapper mapper)
        {
            _claroUserService = claroUserService;
            _mapper = mapper;
        }
        public async Task<ClaroUserDto> Handle(PutClaroUserCommand request, CancellationToken cancellationToken)
        {
            var claroUser = await _claroUserService.PutClaroUser(request);
            return _mapper.Map<ClaroUserDto>(claroUser);
        }
    }
}
