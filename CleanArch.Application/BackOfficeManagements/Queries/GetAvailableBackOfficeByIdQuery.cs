﻿using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Queries
{
    public class GetAvailableBackOfficeByIdQuery : IRequest<BackOfficeManagementsDto>
    {
        public Guid Id { get; set; }
    }

    public class GetAvailableBackOfficeByIdQueryHandler : IRequestHandler<GetAvailableBackOfficeByIdQuery, BackOfficeManagementsDto>
    {
        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        private readonly IMapper _mapper;

        public GetAvailableBackOfficeByIdQueryHandler(IBackOfficeManagementsService backOfficeManagementsService, IMapper mapper)
        {
            _backOfficeManagementsService = backOfficeManagementsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeManagementsDto> Handle(GetAvailableBackOfficeByIdQuery request, CancellationToken cancellationToken)
        {
            var backoffice = await _backOfficeManagementsService.GetBackOfficeById(request.Id);
            return _mapper.Map<BackOfficeManagementsDto>(backoffice);
        }
    }
}
