﻿using Application.DTOs.BackOfficeManagements;
using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Queries
{
    public class GetBackOfficeAllQuery : IRequest<List<BackOfficeManagementsDto>>
    {
    }

    public class GetBackOfficeAllQueryHandler : IRequestHandler<GetBackOfficeAllQuery, List<BackOfficeManagementsDto>>
    {
        private readonly IBackOfficeManagementsService _backOfficeManagementsService;

        public GetBackOfficeAllQueryHandler(IBackOfficeManagementsService backOfficeManagementsService)
        {
            _backOfficeManagementsService = backOfficeManagementsService;
        }
        public async Task<List<BackOfficeManagementsDto>> Handle(GetBackOfficeAllQuery request, CancellationToken cancellationToken)
        {
            var data = await _backOfficeManagementsService.GetBackOfficeManagementAll();
            return data;
        }
    }
}
