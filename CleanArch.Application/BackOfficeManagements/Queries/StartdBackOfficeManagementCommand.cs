﻿using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Queries
{
    public class StartdBackOfficeManagementCommand : IRequest<BackOfficeManagementsDto>
    {
        public Guid Id { get; set; }
    }

    public class StartdBackOfficeManagementQueryHandler : IRequestHandler<StartdBackOfficeManagementCommand, BackOfficeManagementsDto>
    {
        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        private readonly IMapper _mapper;
        public StartdBackOfficeManagementQueryHandler(IBackOfficeManagementsService backOfficeManagementsService, IMapper mapper)
        {
            _backOfficeManagementsService = backOfficeManagementsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeManagementsDto> Handle(StartdBackOfficeManagementCommand request, CancellationToken cancellationToken)
        {
            var managements = await _backOfficeManagementsService.StartManagements(request.Id);
            return _mapper.Map<BackOfficeManagementsDto>(managements);
        }
    }
}
