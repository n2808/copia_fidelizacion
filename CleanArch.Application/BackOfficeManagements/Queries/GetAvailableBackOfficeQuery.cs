﻿using Application.DTOs.BackOfficeManagements;
using Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Queries
{
    public class GetAvailableBackOfficeQuery : IRequest<List<BackOfficeManagementsGroupDto>>
    {
        public bool Activate { get; set; } = true;
        public string Process { get; set; }


    }
    public class GetAvailableBackOfficeQueryHandler : IRequestHandler<GetAvailableBackOfficeQuery, List<BackOfficeManagementsGroupDto>>
    {

        private readonly IBackOfficeManagementsService _backOfficeManagementsService;

        public GetAvailableBackOfficeQueryHandler(
            IBackOfficeManagementsService backOfficeManagementsService

        )
        {
            _backOfficeManagementsService = backOfficeManagementsService;

        }
        public async Task<List<BackOfficeManagementsGroupDto>> Handle(GetAvailableBackOfficeQuery request, CancellationToken cancellationToken)
        {
            return await _backOfficeManagementsService.GetCountGestionBackOfficeAvailable(request.Process, request.Activate);

        }
    }
}
