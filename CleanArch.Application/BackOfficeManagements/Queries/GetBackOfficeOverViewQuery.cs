﻿using Application.DTOs.BackOfficeManagements;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Queries
{
    public class GetBackOfficeOverViewQuery : IRequest<List<BackOfficeGroupByStatusNameDto>>
    {
    }

    public class GetBackOfficeOverViewQueryHandler : IRequestHandler<GetBackOfficeOverViewQuery, List<BackOfficeGroupByStatusNameDto>>
    {
        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        public GetBackOfficeOverViewQueryHandler(IBackOfficeManagementsService backOfficeManagementsService)
        {
            _backOfficeManagementsService = backOfficeManagementsService;
        }
        public async Task<List<BackOfficeGroupByStatusNameDto>> Handle(GetBackOfficeOverViewQuery request, CancellationToken cancellationToken)
        {
            return await _backOfficeManagementsService.GetGestionBackOfficeManagements();
        }
    }
}
