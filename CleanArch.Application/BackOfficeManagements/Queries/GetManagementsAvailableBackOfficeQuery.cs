﻿using Application.DTOs.BackOfficeManagements;
using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using AutoMapper;
using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Queries
{
    public class GetManagementsAvailableBackOfficeQuery : IRequest<BackOfficeManagementsReturn>
    {
        public bool Active { get; set; } = true;
        public string Process { get; set; }

    }
    public class GetManagementsAvailableBackOfficeQueryHandler : IRequestHandler<GetManagementsAvailableBackOfficeQuery, BackOfficeManagementsReturn>
    {

        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public GetManagementsAvailableBackOfficeQueryHandler(
            IBackOfficeManagementsService backOfficeManagementsService,
            IMapper mapper,
            ICurrentUserService currentUserService
        )
        {
            _backOfficeManagementsService = backOfficeManagementsService;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }
        public async Task<BackOfficeManagementsReturn> Handle(GetManagementsAvailableBackOfficeQuery request, CancellationToken cancellationToken)
        {
            var backOfficeManagements = await _backOfficeManagementsService.BackOfficeManagementByAgent((Guid)_currentUserService.GetUserInfo().Id, request.Process);
            var availablesBackOfficeManagements = await _backOfficeManagementsService.GetCountGestionBackOfficeAvailable(request.Process, request.Active);

            return new BackOfficeManagementsReturn
            {
                AvailableManagements = availablesBackOfficeManagements,
                ManagementAssigned = _mapper.Map<List<BackOfficeManagementsDto>>(backOfficeManagements)
            };
        }
    }
}
