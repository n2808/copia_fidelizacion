﻿using Application.DTOs.BackOfficeManagements;
using Application.DTOs.ManagementsBackOffice;
using System.Collections.Generic;

namespace Application.BackOfficeManagements
{
    public class BackOfficeManagementsReturn
    {
        public List<BackOfficeManagementsGroupDto> AvailableManagements { get; set; }
        public List<BackOfficeManagementsDto> ManagementAssigned { get; set; }
    }
}
