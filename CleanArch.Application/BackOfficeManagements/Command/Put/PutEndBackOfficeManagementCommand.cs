﻿using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Command.Put
{
    public class PutEndBackOfficeManagementCommand : IRequest<BackOfficeManagementsDto>
    {
        public Guid Id { get; set; }
        public int? TypificationBackId { get; set; }
        public string TypificationNameBack { get; set; }
        public string ObservationBack { get; set; }
        public Guid? StatusId { get; set; }
        public string StateEnd { get; set; }
    }

    public class PutEndBackOfficeManagementCommandHandler : IRequestHandler<PutEndBackOfficeManagementCommand, BackOfficeManagementsDto>
    {
        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        private readonly IMapper _mapper;
        public PutEndBackOfficeManagementCommandHandler(IBackOfficeManagementsService backOfficeManagementsService, IMapper mapper)
        {
            _backOfficeManagementsService = backOfficeManagementsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeManagementsDto> Handle(PutEndBackOfficeManagementCommand request, CancellationToken cancellationToken)
        {
            var endManagement = await _backOfficeManagementsService.PutEndManagements(request);
            return _mapper.Map<BackOfficeManagementsDto>(endManagement);
        }
    }
}

