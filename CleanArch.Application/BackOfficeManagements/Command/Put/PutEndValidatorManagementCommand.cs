﻿using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Command.Put
{
    public class PutEndValidatorManagementCommand : IRequest<BackOfficeManagementsDto>
    {
        public Guid Id { get; set; }
        public string ObservationValidator { get; set; }
        public Guid? StatusId { get; set; }
        public bool validateSale {get;set; }
    }

    public class PutEndValidatorManagementCommandHandler : IRequestHandler<PutEndValidatorManagementCommand, BackOfficeManagementsDto>
    {
        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        private readonly IMapper _mapper;
        public PutEndValidatorManagementCommandHandler(IBackOfficeManagementsService backOfficeManagementsService, IMapper mapper)
        {
            _backOfficeManagementsService = backOfficeManagementsService;
            _mapper = mapper;
        }
        public async Task<BackOfficeManagementsDto> Handle(PutEndValidatorManagementCommand request, CancellationToken cancellationToken)
        {
            var endManagement = await _backOfficeManagementsService.PutEndManagementsValidator(request);
            return _mapper.Map<BackOfficeManagementsDto>(endManagement);
        }
    }
}
