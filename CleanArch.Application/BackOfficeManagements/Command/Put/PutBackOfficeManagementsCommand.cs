﻿using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Command.Put
{
    public class PutBackOfficeManagementsCommand : IRequest<BackOfficeManagementsDto>
    {
         public Guid Id { get; set; }
        public int? ManagementId { get; set; }
        public int? TypificationId { get; set; }
        public string TypificationName { get; set; }
        public string TypeOperation { get; set; }
        public string Operation { get; set; }
        public string Observation { get; set; }
        public Guid UserId { get; set; }
        public int? TypificationBackId { get; set; }
        public string TypificationNameBack { get; set; }
        public string ObservationBack { get; set; }
        public int SegmentId { get; set; }
        public DateTime InitValidation { get; set; }
        public DateTime EndValidation { get; set; }
        public string Process { get; set; }
        public int? GestionId { get; set; }
        public int? Count { get; set; }
        public bool Active { get; set; }
        public Guid? StatusId { get; set; }
        public string StatusName { get; set; }
        public int? SecondRingLoadId { get; set; }
    }

    public class PutManagementsBackOfficeCommandHandler : IRequestHandler<PutBackOfficeManagementsCommand, BackOfficeManagementsDto>
    {
        private readonly IBackOfficeManagementsService _managementsBackOfficeService;
        private readonly IMapper _mapper;

        public PutManagementsBackOfficeCommandHandler(IBackOfficeManagementsService managementsBackOfficeService, IMapper mapper)
        {
            _managementsBackOfficeService = managementsBackOfficeService;
            _mapper = mapper;
        }
        public async Task<BackOfficeManagementsDto> Handle(PutBackOfficeManagementsCommand request, CancellationToken cancellationToken)
        {
            var managements = await _managementsBackOfficeService.PutManagements(request);
            return _mapper.Map<BackOfficeManagementsDto>(managements);
        }
    }
}
