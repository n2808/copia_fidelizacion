﻿using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using AutoMapper;
using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BackOfficeManagements.Command.Post
{
    public class AssignAgentManagementCommand : IRequest<BackOfficeManagementsReturn>
    {
        public bool active { get; set; }
        public string process { get; set; }
        public string typeOperation { get; set; }
        public string operation { get; set; }
        public int segmentId { get; set; }

    }

    public class AssignAgentManagementCommandHandler : IRequestHandler<AssignAgentManagementCommand, BackOfficeManagementsReturn>
    {

        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public AssignAgentManagementCommandHandler(
            IBackOfficeManagementsService backOfficeManagementsService,
            IMapper mapper,
            ICurrentUserService currentUserService
        )
        {
            _backOfficeManagementsService = backOfficeManagementsService;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }
        public async Task<BackOfficeManagementsReturn> Handle(AssignAgentManagementCommand request, CancellationToken cancellationToken)
        {
            Guid CurrentUser = (Guid)_currentUserService.GetUserInfo().Id;

            await _backOfficeManagementsService.RequestBackOfficeManagementWithoutAgent(request);
            var backofficeManagement = await _backOfficeManagementsService.BackOfficeManagementByAgent(CurrentUser, request.process);


            return new BackOfficeManagementsReturn
            {
                AvailableManagements = await _backOfficeManagementsService.GetCountGestionBackOfficeAvailable(request.process, true),
                ManagementAssigned = _mapper.Map<List<BackOfficeManagementsDto>>(backofficeManagement)
            };
        }
    }
}
