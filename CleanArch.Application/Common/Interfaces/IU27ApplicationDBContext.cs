﻿using CleanArch.Domain.Models;
using Core.Models.configuration;
using Core.Models.location;
using Core.Models.Operation;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.interfaces
{
    public interface IU27ApplicationDBContext
    {
        DbSet<GestionStructure> bulkStructures { get; set; }
        DbSet<Campaign> campaigns { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<City> Cities { get; set; }
        DbSet<Configuration> Configurations { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Course> courses { get; set; }
        DbSet<DatabaseClientBase> DatabaseClientBase { get; set; }
        DbSet<DatabaseClientDetail> DatabaseClientDetail { get; set; }
        DbSet<DatabasesClientLoad> DatabasesClientLoads { get; set; }
        DbSet<InformativeAgentFields> InformativeAgentFields { get; set; }
        DbSet<Segment> Segments { get; set; }
        DbSet<ServicesDialerTipification> ServicesDialerTipification { get; set; }
        DbSet<State> states { get; set; }
        DbSet<Tipification> Tipification { get; set; }
        DbSet<GestionReports> GestionReports { get; set; }


        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}