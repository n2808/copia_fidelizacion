﻿using Application.Core.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Exceptions.Handle
{
    public class ValidationExceptionHandlerMiddleware
    {
        private readonly RequestDelegate next;

        public ValidationExceptionHandlerMiddleware(RequestDelegate next) => this.next = next;

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await this.next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            string result = "";
            switch (exception)
            {
            case NotFoundException validationException:
                //custom handling of my exception
                context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            result = JsonConvert.SerializeObject(new { error = exception.Message });//your serialized json object with error data
            break;
            default:
                context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            result = JsonConvert.SerializeObject(new { error = exception.Message });//your serialized json object with error data
             break;

        }

        return context.Response.WriteAsync(result);
    }
}
}
