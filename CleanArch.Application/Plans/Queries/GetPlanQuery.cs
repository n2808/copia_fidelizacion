﻿using Application.DTOs.Plans;
using Application.Interfaces.Plans;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Plans.Queries
{
    public class GetPlanQuery : IRequest<List<PlanDto>>
    {
        public int Without { get; set; }
    }

    public class GetPlanQueryHandler : IRequestHandler<GetPlanQuery, List<PlanDto>>
    {
        private readonly IPlanService _planService;
        public GetPlanQueryHandler(IPlanService planService)
        {
            _planService = planService;
        }

        public async Task<List<PlanDto>> Handle(GetPlanQuery request, CancellationToken cancellationToken)
        {
            List<PlanDto> planDtos =  await _planService.GetPlans(request.Without);

            return planDtos;
        }        

    }
}
