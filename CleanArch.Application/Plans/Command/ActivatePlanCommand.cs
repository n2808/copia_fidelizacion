﻿using Application.Interfaces.Plans;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Plans.Command
{
    public class ActivatePlanCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class ActivatePlanCommandHandler : IRequestHandler<ActivatePlanCommand, bool>
    {
        private readonly IPlanService _planService;
        public ActivatePlanCommandHandler(IPlanService planService)
        {
            _planService = planService;
        }
        public async Task<bool> Handle(ActivatePlanCommand request, CancellationToken cancellationToken)
        {
            await _planService.ActivatePlan(request.Id);

            return true;
        }
    }
}
