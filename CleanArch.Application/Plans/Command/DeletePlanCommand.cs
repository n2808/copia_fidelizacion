﻿using Application.Interfaces.Plans;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Plans.Command
{
    public class DeletePlanCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class DeletePlanCommandHandler : IRequestHandler<DeletePlanCommand, bool>
    {

        private readonly IPlanService _planService;

        public DeletePlanCommandHandler(IPlanService planService)
        {
            _planService = planService;

        }

        public async Task<bool> Handle(DeletePlanCommand request, CancellationToken cancellationToken)
        {
            await _planService.DeletePlan(request.Id);

            return true;
        }
    }
}
