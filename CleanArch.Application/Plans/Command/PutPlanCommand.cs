﻿using Application.DTOs.Plans;
using Application.Interfaces.Plans;
using AutoMapper;
using Domain.Models.Plans;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Plans.Command
{
    public class PutPlanCommand : IRequest<PlanDto>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public double PriceWithTax { get; set; }
        public string Currency { get; set; }
        public string Tax { get; set; }
        public string MinutesIncluded { get; set; }
        public string MinutesIncludedLDI { get; set; }
        public string SMSIncluded { get; set; }
        public string SMSIncludedLDI { get; set; }
        public string APPSIncluided { get; set; }
        public string TMCode { get; set; }
        public string MessagePrice { get; set; }
        public string Description { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; }
    }

    public class PutPlanCommandHandler : IRequestHandler<PutPlanCommand, PlanDto>
    {
        private readonly IPlanService _planService;
        private readonly IMapper _mapper;
        public PutPlanCommandHandler(IPlanService planService, IMapper mapper)
        {
            _planService = planService;
            _mapper = mapper;
        }
        public async Task<PlanDto> Handle(PutPlanCommand request, CancellationToken cancellationToken)
        {
            Plan plan = await _planService.PutPlan(request);

            return _mapper.Map<PlanDto>(plan);

        }
    }
}
