﻿using Application.Interfaces.Plans;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Plans.Command
{
    public class DeactivatePlanCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }
    public class DeactivatePlanCommandHandler : IRequestHandler<DeactivatePlanCommand, bool>
    {
        private readonly IPlanService _planService;
        public DeactivatePlanCommandHandler(IPlanService planService)
        {
            _planService = planService;
        }
        public async Task<bool> Handle(DeactivatePlanCommand request, CancellationToken cancellationToken)
        {
            await _planService.DeactivatePlan(request.Id);

            return true;
        }
    }
}
