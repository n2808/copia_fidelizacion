﻿using FluentValidation;

namespace Application.Plans.Command
{
    public class PostPlanCommandValidator : AbstractValidator<PostPlanCommand>
    {
        public PostPlanCommandValidator()
        {
            RuleFor(x => x.Name)
                          .NotEmpty()
                          .WithMessage("El nombre no puede estar vacío")
                          .MaximumLength(255)
                          .WithMessage("La longitud de caracteres no debe ser mayor a 3");

            RuleFor(x => x.Tax)
                         .NotEmpty()
                         .WithMessage("El impuesto no puede estar vacío")
                          .MaximumLength(5)
                          .WithMessage("La longitud de caracteres no debe ser mayor a 5");

            RuleFor(x => x.Price)
                           .NotEmpty()
                           .WithMessage("El precio no puede estar vacío");


            RuleFor(x => x.PriceWithTax)
                           .NotEmpty()
                           .WithMessage("El precio con impuesto no puede estar vacío");

            RuleFor(x => x.Currency)
                          .NotEmpty()
                          .WithMessage("La moneda no puede estar vacía")
                          .MaximumLength(3)
                          .WithMessage("La longitud de caracteres no debe ser mayor a 3");

            RuleFor(x => x.MessagePrice)
                          .NotEmpty()
                          .WithMessage("El mensaje del precio no puede estar vacío");

        }
    }
}
