﻿using Application.ServicseDialer.DTOs;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ServicesDialer.Queries
{
    public class GetServiceDialerQuery : IRequest<List<ServiceDialerDto>>
    {
        public  int SubCampaignId { get; set; } 
    }

    public class GetServiceDialerQueryHandler : IRequestHandler<GetServiceDialerQuery, List<ServiceDialerDto>>
    {
        private readonly IServiceDialerService _serviceDialerService;

        public GetServiceDialerQueryHandler(
          IServiceDialerService serviceDialerService
        )
        {
            _serviceDialerService = serviceDialerService;
        }

        public async Task<List<ServiceDialerDto>> Handle(GetServiceDialerQuery request, CancellationToken cancellationToken)
        {
            
           return _serviceDialerService
                    .GetServiceDialerBySubCampaignId(request.SubCampaignId);
        
        }

    }



}
