﻿using Application.DTOs.Campaign;
using CleanArch.Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.SubCampaign.Queries
{
    public class GetSubCampaignsQuery : IRequest<List<SubCampaignDto>>
    {
    }

    public class GetSubCampaignsQueryHandler : IRequestHandler<GetSubCampaignsQuery, List<SubCampaignDto>>
    {
        private readonly ISubCampaignService _subcampaignService;

        public GetSubCampaignsQueryHandler(
          ISubCampaignService subCampaignService
          
            )
        {
            _subcampaignService = subCampaignService;
        }

        public async Task<List<SubCampaignDto>> Handle(GetSubCampaignsQuery request, CancellationToken cancellationToken)
        {

            return await _subcampaignService.GetSubcampaigns();
        }
    }
}
