﻿using Application.Core.Exceptions;
using Application.DTOs.Plans;
using Application.Interfaces.Plans;
using Application.Interfaces.User;
using Application.Plans.Command;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces;
using Domain.Models.Plans;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.Plans
{
    public class PlanService : IPlanService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentUserService _currentUserService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public PlanService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentUserService currentUserService, IUserService userService)
        {
            _unitOfWork = unitOfWork;
            _currentUserService = currentUserService;
            _userService = userService;
            _mapper = mapper;
        }

        public async Task<List<PlanDto>> GetPlans(int without)
        {
            var source = _unitOfWork.PlanRepository.Get();

            return without == 1 ? await source.ProjectTo<PlanDto>(_mapper.ConfigurationProvider).ToListAsync()
                                : await source.Where(x => x.Status).ProjectTo<PlanDto>(_mapper.ConfigurationProvider).ToListAsync();
         
        }
        public async Task<Plan> PostPlan(Plan plan)
        {
            plan.UserUpdate = GetNameCurrentUser();
            return await _unitOfWork.PlanRepository.Add(plan);
        }

        public async Task<Plan> PutPlan(PutPlanCommand putPlanCommand)
        {
            Plan plan = await _unitOfWork.PlanRepository.GetById(putPlanCommand.Id) ?? throw new NotFoundException("El plan que está intentando editar no existe.");           

            plan = _mapper.Map<PutPlanCommand, Plan>(putPlanCommand, plan);
            plan.UserUpdate = GetNameCurrentUser();
            return await _unitOfWork.PlanRepository.Put(plan);
        }

        public async Task<bool> DeletePlan(Guid id)
        {
            Plan entity = await _unitOfWork.PlanRepository.GetById(id) ?? throw new NotFoundException("No se encontró la entidad a eliminar.");
            return await _unitOfWork.PlanRepository.Delete(entity);
        }

        public async Task<bool> ActivatePlan(Guid id)
        {
            Plan plan = await _unitOfWork.PlanRepository.GetById(id) ?? throw new NotFoundException("Plan", "Id");  
            plan.UserUpdate = GetNameCurrentUser();
            plan.Status = true;
            await _unitOfWork.PlanRepository.Put(plan);
            return true;
        }

        public async Task<bool> DeactivatePlan(Guid id)
        {
            Plan plan = await _unitOfWork.PlanRepository.GetById(id) ?? throw new NotFoundException("Plan", "Id");
            plan.UserUpdate = GetNameCurrentUser();
            plan.Status = false;
            await _unitOfWork.PlanRepository.Put(plan);
            return true;
        }

        public string GetNameCurrentUser()
        {
            Guid idCurrentUser = (Guid)_currentUserService.GetUserInfo().Id;
            string name = _userService.GetById(idCurrentUser).Names;
            string lastName = _userService.GetById(idCurrentUser).LastName == null ? "" : _userService.GetById(idCurrentUser).LastName;
            string fullName = name + " " + lastName;
            return fullName;
        }

    }
}
