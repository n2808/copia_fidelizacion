﻿using Application.BackOfficeManagements.Command.Post;
using Application.BackOfficeManagements.Command.Put;
using Application.BackOfficeManagements.Queries;
using Application.Core.Exceptions;
using Application.DTOs.BackOfficeManagements;
using Application.DTOs.EvidenceGestion;
using Application.DTOs.ManagementsBackOffice;
using Application.Interfaces;
using Application.Interfaces.Status;
using Application.ViewModel;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArchitecture.Application.Common.Interfaces;
using Core.Models.Operation;
using Domain.Interfaces;
using Domain.Interfaces.EvidenceGestion;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services
{
    public class BackOfficeManagementsService : IBackOfficeManagementsService
    {
        private readonly IBackOfficeManagementsRepository _managementsBackOfficeRepository;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        private readonly ITipificationService _tipificationService;
        private readonly IStatusService _statusService;
        private readonly IEvidenceGestionRepository _evidenceGestionRepository;

        public string _authFullName;



        public BackOfficeManagementsService(IBackOfficeManagementsRepository managementsBackOfficeRepository, IMapper mapper
            , ICurrentUserService currentUserService, ITipificationService tipificationService
            , IStatusService statusService, IEvidenceGestionRepository evidenceGestionRepository)
        {
            _managementsBackOfficeRepository = managementsBackOfficeRepository;
            _mapper = mapper;
            _currentUserService = currentUserService;
            _tipificationService = tipificationService;
            _statusService = statusService;
            _evidenceGestionRepository = evidenceGestionRepository;


            _authFullName = _currentUserService.GetUserInfo().Name + ' ' + _currentUserService.GetUserInfo().LastName;
        }

        public async Task<string> GetNameTypification(int id)
        {
            if (id != 0)
            {
                var tipification = await _tipificationService.GetTipificationById(id);
                var name = tipification.Name;
                return name;
            }
            return "";
        }

        public async Task<string> GetNameStatus(Guid id)
        {
            var status = await _statusService.GetStatusById(id);
            if (status != null)
            {
                return status.Name;
            }
            return "SIN ESTADO";
        }

        public async Task CreateManagement(Domain.Models.ManagementsBackoffice.BackOfficeManagements managements, ActiveGestion gestion)
        {
            if (managements.TypificationId != 0)
            {
                var nameTypification = await GetNameTypification((int)managements.TypificationId);
                managements.TypificationName = nameTypification;
            }

            managements.StatusName = "Pendiente";
            managements.CreatedBy = gestion.CreatedBy;
            managements.CreatedByName = gestion.CreatedByName;
            var management = await _managementsBackOfficeRepository.PostManagementBackOffice(managements);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);
        }

        public async Task PostManagement(ActiveGestion gestion)
        {
            var managements = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements>(gestion);

            // OPERACIONES LEALTAD
            if (gestion.SubcampaignId == 10 && Convert.ToBoolean(gestion.EntryData7) == true)
            {
                managements.Process = "operation";
                await CreateManagement(managements, gestion);
            }

            // OPERACIONES SEGUNDO ANILLO
            if (gestion.SubcampaignId == 9 && gestion.IsRetained == true)
            {
                managements.Process = "retained";
                //managements.Operation = "Retenido";
                managements.TypeOperation = gestion.EntryData36;
                await CreateManagement(managements, gestion);
            }

            // VENTAS GUATEMALA
            if (gestion.SubcampaignId == 17 && Convert.ToBoolean(gestion.IsSale) == true)
            {
                managements.Process = "validation";
                managements.Operation = "Venta";
                managements.TypeOperation = gestion.SegmentName;
                await CreateManagement(managements, gestion);
            }

            // 611 PORTABILIDAD
            if (gestion.SubcampaignId == 12 && Convert.ToBoolean(gestion.EntryData15) == true)
            {
                managements.Process = "operation";
                managements.Operation = "Portabilidad";
                managements.TypeOperation = gestion.SegmentName;
                await CreateManagement(managements, gestion);
            }

        }

        public async Task<bool> PostManagementSecondRing(CreateBackOfficeManegementViewModel data)
        {
            var secondMap = _mapper.Map<CreateBackOfficeManegementViewModel, Domain.Models.ManagementsBackoffice.BackOfficeManagements>(data);
            var management = await _managementsBackOfficeRepository.PostManagementBackOffice(secondMap);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);
            if (management != null)
            {
                return true;
            }
            return false;
        }


        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PostManagements(Domain.Models.ManagementsBackoffice.BackOfficeManagements managementsBackOffice)
        {
            if (managementsBackOffice.TypificationId != 0)
            {
                var nameTypification = await GetNameTypification((int)managementsBackOffice.TypificationId);
                managementsBackOffice.TypificationName = nameTypification;
            }
            managementsBackOffice.CreatedBy = _currentUserService.GetUserInfo().Id;
            managementsBackOffice.CreatedByName = _authFullName;
            managementsBackOffice.Process = "operation";
            managementsBackOffice.InitValidation = DateTime.Now;
            var management = await _managementsBackOfficeRepository.PostManagementBackOffice(managementsBackOffice);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);
            return management;
        }

        public Domain.Models.ManagementsBackoffice.BackOfficeManagements GetManagementById(Guid id)
        {
            return _managementsBackOfficeRepository.Get()
                .Include(x => x.Management)
                .Include(x => x.Management.UserCreated)
                .Include(x => x.Management.tipificacion)
                .Where(x => x.Id == id)
                .FirstOrDefault() ?? throw new NotFoundException("La gestión que está intentando buscar no existe.");
        }

        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PutManagements(PutBackOfficeManagementsCommand request)
        {
            var IdManagements = request.Id;
            Domain.Models.ManagementsBackoffice.BackOfficeManagements managements = GetManagementById(IdManagements);
            if(request.Operation == null && request.SegmentId == 0)
            {
                managements.ManagementId = request.ManagementId;
                await EditManagement(managements);
            } else
            {
                managements = _mapper.Map<PutBackOfficeManagementsCommand, Domain.Models.ManagementsBackoffice.BackOfficeManagements>(request, managements);
                managements.UpdatedBy = _currentUserService.GetUserInfo().Id;
                managements.UpdatedByName = _authFullName;
                await EditManagement(managements);
            }

            return managements;
        }
         
        public async Task<bool> EditManagement (Domain.Models.ManagementsBackoffice.BackOfficeManagements backOfficeManagements)
        {
            var management = await _managementsBackOfficeRepository.PutManagementBackOffice(backOfficeManagements);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);

            return true;
        }

        public async Task<List<Domain.Models.ManagementsBackoffice.BackOfficeManagements>> BackOfficeManagementByAgent(Guid AgentId, string process)
        {
            return await _managementsBackOfficeRepository
                       .Get()
                       .Include(x => x.Management)
                       .Include(x => x.Management.UserCreated)
                       .Include(x => x.Management.tipificacion)
                       .Include(x => x.SecondRingLoad)
                       .Where(c => c.Active == true &&
                                   c.UserId == AgentId &&
                                   c.Process == process
                              )
                       .ToListAsync();
        }
        public async Task<List<BackOfficeManagementsGroupDto>> GetCountGestionBackOfficeAvailable(string process, bool statusId = true)
        {
            var UserSubcampaignId = _currentUserService.GetUserInfo().SubCampaignId;

            return await _managementsBackOfficeRepository
                        .Get()
                        .Include(x => x.Segment)
                        .ThenInclude(x => x.SubCampaign)
                        .Where(c =>
                            c.TypificationBackId == null &&
                            c.UserId == null &&
                            c.Segment.SubCampaignId == UserSubcampaignId &&
                            c.Process == process
                         )
                        .GroupBy(x => new
                        {
                            SegmentId = x.SegmentId,
                            SegmentName = x.Segment.Name,
                            SubcampaignId = x.Segment.SubCampaignId,
                            SubcampaignName = x.Segment.SubCampaign.Name,
                            TypeOperation = x.TypeOperation,
                            Process = x.Process,
                            Operation = x.Operation
                        })
                        .Select(g => new BackOfficeManagementsGroupDto
                        {
                            SegmentId = g.Key.SegmentId,
                            SegmentName = g.Key.SegmentName,
                            SubcampaignId = g.Key.SubcampaignId,
                            SubcampaignName = g.Key.SubcampaignName,
                            TypeOperation = g.Key.TypeOperation,
                            Process = g.Key.Process,
                            Operation = g.Key.Operation,
                            count = g.Count()
                        })
                        .ToListAsync();
        }

        public async Task<List<BackOfficeGroupByStatusNameDto>> GetGestionBackOfficeManagements()
        {
            var UserSubcampaignId = _currentUserService.GetUserInfo().SubCampaignId;

            return await _managementsBackOfficeRepository
                        .Get()
                        .Include(x => x.Segment)
                        .ThenInclude(x => x.SubCampaign)
                        .Where(c =>
                            c.Segment.SubCampaignId == UserSubcampaignId
                         )
                        .GroupBy(x => new
                        {
                            StatusName = x.StatusName,
                        })
                        .Select(g => new BackOfficeGroupByStatusNameDto
                        {
                            Name = g.Key.StatusName,
                            Count = g.Count()
                        })
                        .ToListAsync();
        }

        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> RequestBackOfficeManagementWithoutAgent(AssignAgentManagementCommand request)
        {
            if (request.segmentId != null)
            {
                var backofficeManagementsFilter = _managementsBackOfficeRepository.Get().Where(x => x.UserId == null).OrderBy(x => x.CreatedAt);
                backofficeManagementsFilter = backofficeManagementsFilter
                    .Where(x => x.SegmentId == request.segmentId && x.Process == request.process && x.TypeOperation == request.typeOperation && x.Operation == request.operation)
                    .OrderBy(x => x.CreatedAt);
                var backofficeManagements = backofficeManagementsFilter.FirstOrDefault();
                if (backofficeManagements != null)
                {
                    backofficeManagements.UserId = (Guid)_currentUserService.GetUserInfo().Id;
                    backofficeManagements.Active = true;
                    backofficeManagements.UpdatedAt = DateTime.Now;
                    backofficeManagements.UpdatedBy = (Guid)_currentUserService.GetUserInfo().Id;
                    backofficeManagements.UpdatedByName = _authFullName;
                    backofficeManagements.StatusName = "Asignado";
                    var management = await _managementsBackOfficeRepository.PutManagementBackOffice(backofficeManagements);
                    var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
                    await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);
                    return management;
                }
            }

            throw new NotFoundException($"no hay gestiones disponibles para el segmento {request.segmentId}");
        }

     

        public async Task<List<BackOfficeManagementsDto>> GetBackOfficeManagementAll()
        {
            var data = await _managementsBackOfficeRepository
                        .Get()
                        .Include(x => x.Histories)
                        .ProjectTo<BackOfficeManagementsDto>(_mapper.ConfigurationProvider)
                        .ToListAsync();

            return data;
        }

        public async Task AddBackOfficeManagement(ActiveGestion gestion)
        {
            var managements = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements>(gestion);
            if (managements.TypificationId != 0)
            {
                var nameTypification = await GetNameTypification((int)managements.TypificationId);
                managements.TypificationName = nameTypification;
            }
            managements.CreatedBy = gestion.CreatedBy;
            managements.CreatedByName = gestion.CreatedByName;
            managements.Process = "operation";
            managements.InitValidation = DateTime.Now;
            await _managementsBackOfficeRepository.PostManagementBackOffice(managements);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(managements);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);
        }

        public async Task UpdateBackOfficeManagement(Guid ManagementId, Guid AgentId, bool status)
        {
            Domain.Models.ManagementsBackoffice.BackOfficeManagements managements = GetManagementById(ManagementId);
            managements.Active = !status;
            managements.UpdatedBy = AgentId;
            managements.UpdatedAt = DateTime.Now;
            managements.UpdatedByName = _authFullName;
            managements.Count++;
            var management = await _managementsBackOfficeRepository.PutManagementBackOffice(managements);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);

        }

        public List<BackOfficeManagementsDto> GetProccess(string process, string segmentId)
        {
            throw new NotImplementedException();
        }

        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PutEndManagementsValidator(PutEndValidatorManagementCommand request)
        {
            Domain.Models.ManagementsBackoffice.BackOfficeManagements management = GetManagementById(request.Id);
            

            if (request.validateSale)
            {
                management.Process = "sale";
                management.Active = true;
                management.UserValidationId = management.UserId;
                management.UserId = null;
                management.TypificationBackValidationId = management.TypificationBackId;
                management.TypificationBackId = null;
            } else
            {
                management.Active = false;
            }

            //management.ObservationValidator = request.ObservationValidator;
            management.StatusId = request.StatusId;

            await _managementsBackOfficeRepository.PutManagementBackOffice(management);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);

            return management;
        }

        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PutEndManagements(PutEndBackOfficeManagementCommand request)
        {
            Domain.Models.ManagementsBackoffice.BackOfficeManagements management = GetManagementById(request.Id);
            management = _mapper.Map<PutEndBackOfficeManagementCommand, Domain.Models.ManagementsBackoffice.BackOfficeManagements>(request, management);

            if (management == null) throw new NotImplementedException();


            if (request.TypificationBackId != null)
            {
                var nameTypification = await GetNameTypification((int)request.TypificationBackId);
                management.TypificationNameBack = nameTypification;
            }


            if (request.StatusId != null)
            {
                var nameStatus = await GetNameStatus((Guid)request.StatusId);
                management.StatusName = nameStatus;
                management.StatusId = request.StatusId;
            }

            management.UpdatedBy = _currentUserService.GetUserInfo().Id;
            management.UpdatedByName = _authFullName;
            management.EndValidation = DateTime.Now;
            management.UpdatedAt = DateTime.Now;
            management.Active = false;
            await _managementsBackOfficeRepository.PutManagementBackOffice(management);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);

            //Eliminar la imagen de evidencia del managements
            var evidences = _evidenceGestionRepository.GetEvidenceGestion().Where(x => x.GestionReportsId == management.ManagementId)
                                                               .ProjectTo<EvidenceGestionDto>(_mapper.ConfigurationProvider)
                                                               .ToList();

            string targetPath = @"Resources/PublicFiles/Evidences";
            foreach (var item in evidences)
            {
                if (File.Exists(targetPath + "/" + item.FileName))
                {
                    System.IO.File.Delete(targetPath + "/" + item.FileName);
                }
            }

            return management;
        }

        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> StartManagements(Guid id)
        {
            Domain.Models.ManagementsBackoffice.BackOfficeManagements managements = GetManagementById(id);
            managements.UpdatedBy = _currentUserService.GetUserInfo().Id;
            managements.UpdatedByName = _authFullName;
            managements.UpdatedAt = DateTime.Now;
            managements.InitValidation = DateTime.Now;
            managements.Active = true;
            managements.StatusName = "En Validacion";
            var management = await _managementsBackOfficeRepository.PutManagementBackOffice(managements);
            var managementHistory = _mapper.Map<Domain.Models.ManagementsBackoffice.BackOfficeManagements, Domain.Models.ManagementsBackoffice.BackOfficeManagementsHistories>(management);
            await _managementsBackOfficeRepository.PostManagementBackOfficeHistory(managementHistory);
            return management;
        }

        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> GetBackOfficeById(Guid id)
        {
            return GetManagementById(id);
        }

        public async Task<bool> UpdateManagementSecondRing(ManualBaseViewModel manualBase, int gestionId, int tipificationId)
        {
            var status = await _statusService.GetStatusById((Guid)manualBase.finalStatusId);
            var tipification = await _tipificationService.GetTipificationById(tipificationId);
            var bo = await GetById((Guid)manualBase.Id);

            bo.TypificationId = tipification.Id;
            bo.TypificationName = tipification.Name;
            bo.StatusId = status.Id;
            bo.StatusName = status.Name;
            bo.ManagementId = gestionId;
            bo.Active = false;
            bo.EndValidation = DateTime.Now;
            await _managementsBackOfficeRepository.PutManagementBackOffice(bo);

            return true;
        }

        public async Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> GetById(Guid id)
        {
            return await _managementsBackOfficeRepository.Get()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync() ?? throw new NotFoundException("La gestión que está intentando buscar no existe.");
        }

       
    }
}
