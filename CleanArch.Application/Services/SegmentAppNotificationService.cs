﻿
using Application.Courses.Commands;
using Application.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Core.Bus;
using CleanArch.Domain.Interfaces;
using Domain.Models.Apps;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Application.Services
{
    public class SegmentAppNotificationService : ISegmentAppNotificationService
    {
        private readonly ISegmentAppNotificationRepository _segmentAppNotificationRepository;
        private readonly IMapper _autoMapper;

        public SegmentAppNotificationService(
            ISegmentAppNotificationRepository segmentAppNotificationRepository, 
            IMapper autoMapper
            )
        {
            _segmentAppNotificationRepository = segmentAppNotificationRepository;
            _autoMapper = autoMapper;
        }

        public List<AppNotificationDto> GetAppsBySegmentId(int Id)
        {
            return   _segmentAppNotificationRepository
                      .GetApps()
                      .Include(x => x.AppNotification)
                      .Where(x => x.SegmentId == Id)
                      .ProjectTo<AppNotificationDto>(_autoMapper.ConfigurationProvider)
                      .ToList();
        }


        public bool CheckById(int Id)
        {
            return _segmentAppNotificationRepository
                    .GetApps()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }

        public SegmentAppNotification GetById(int Id)
        {
            return _segmentAppNotificationRepository
                    .GetApps()
                    .Include(x => x.AppNotification)
                    .Where(c => c.Id == Id)
                    .FirstOrDefault();
        }


    }
}
