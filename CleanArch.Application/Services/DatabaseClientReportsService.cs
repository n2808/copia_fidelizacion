﻿using AutoMapper;
using CleanArch.Application.Interfaces;
using Core.Models.Operation;
using Domain.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class DatabaseClientReportsService : IDatabaseClientReportsService
    {
        private readonly IDatabaseClientReportsRepository _databaseClientReportsRepository;
        private readonly IMapper _autoMapper;


        public DatabaseClientReportsService(
            IDatabaseClientReportsRepository databaseClientReportsRepository
        )
        {
            _databaseClientReportsRepository = databaseClientReportsRepository;
        }


        public bool CheckDatabaseClientReportById(int Id)
        {
            return _databaseClientReportsRepository
                    .GetClient()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }


        public async Task<DatabaseClientReports> PostAndUpdate(DatabaseClientReports Client)
        {

            return CheckDatabaseClientReportById(Client.Id) ?
                 await  _databaseClientReportsRepository.PutClient(Client) :
                 await _databaseClientReportsRepository.PostClient(Client);

        }




    }
}
