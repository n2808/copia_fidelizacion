﻿using Application.Core.Exceptions;
using Application.DTOs.Products;
using Application.Interfaces.Products;
using Application.Interfaces.User;
using Application.Products.Command;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Products;
using Domain.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public ProductService(IProductRepository productRepository, IMapper mapper, ICurrentUserService currentUserService, IUserService userService)
        {
            _productRepository = productRepository;
            _currentUserService = currentUserService;
            _userService = userService;
            _mapper = mapper;
        }

        public List<ProductDto> GetProducts(int without)
        {
            var source = _productRepository.Get();

            return without == 1 ? source.ProjectTo<ProductDto>(_mapper.ConfigurationProvider).ToList()
                                : source.Where(x => x.Status).ProjectTo<ProductDto>(_mapper.ConfigurationProvider).ToList();
          
        }
        public Product PostProduct(Product product)
        {
            product.UserUpdate = GetNameCurrentUser();
            return _productRepository.Post(product);
        }

        public string GetNameCurrentUser()
        {
            Guid idCurrentUser = (Guid)_currentUserService.GetUserInfo().Id;
            string name = _userService.GetById(idCurrentUser).Names;
            string lastName = _userService.GetById(idCurrentUser).LastName == null ? "" : _userService.GetById(idCurrentUser).LastName;
            string fullName = name + " " + lastName;
            return fullName;
        }

        public Product PutProduct(PutProductCommand putProductCommand)
        {
            Product product = GetProductById(putProductCommand.Id);

            if (product == null)
            {
                throw new NotFoundException("Product", "Id");
            }

            product = _mapper.Map<PutProductCommand, Product>(putProductCommand, product);
            product.UserUpdate = GetNameCurrentUser();
            _productRepository.Put(product);

            return product;
        }

        public bool ActivateProduct(Guid id)
        {
            Product product = GetProductById(id);

            if (product == null)
            {
                throw new NotFoundException("ProducDto", "Id");
            }

            product.UserUpdate = GetNameCurrentUser();

            return _productRepository.Activate(product);
        }

        public bool DeactivateProduct(Guid id)
        {
            Product product = GetProductById(id);

            if (product == null)
            {
                throw new NotFoundException("ProducDto", "Id");
            }
                       
            product.UserUpdate = GetNameCurrentUser();

            return _productRepository.Deactivate(product);
        }              
        public bool DeleteProduct(Guid id)
        {
            Product product = GetProductById(id);

            if (product == null)
            {
                throw new NotFoundException("ProducDto", "Id");
            }                       

            return _productRepository.Delete(product);
        }

        public Product GetProductById(Guid id)
        {
            return _productRepository.GetById(id);
        }
    }
}
