﻿
using Application.Core.Exceptions;
using Application.DTOs;
using Application.DTOs.User;
using Application.Gestions.Queries.GetInboundInitialGestion;
using Application.Gestions.Queries.GetInitialGestion;
using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Interfaces;
using Core.Models.Operation;
using Core.Values;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class GestionService : IGestionService
    {
        private readonly IMapper _autoMapper;
        private readonly IInformativeAgentFieldsService _informativeAgentFieldsService;
        private readonly IDatabaseClientDetailService _databaseClientDetailService;
        private readonly IGestionRepository _gestionRepository;
        private readonly IActiveGestionRepository _activeGestionRepository;


        private DatabaseClientDetail cd = null;

        public GestionService(
            IInformativeAgentFieldsService informativeAgentFieldsService,
            IDatabaseClientDetailService databaseClientDetailService,
            IGestionRepository gestionRepository,
            IActiveGestionRepository activeGestionRepository,
            IMapper autoMapper
         )
        {
            _autoMapper = autoMapper;
            _informativeAgentFieldsService = informativeAgentFieldsService;
            _databaseClientDetailService = databaseClientDetailService;
            _gestionRepository = gestionRepository;
            _activeGestionRepository = activeGestionRepository;

        }


        private List<ClientDataViewModel> GeClientDataAndGestionViewModel(int segmentId, DatabaseClientDetail cd)
        {
            var q = _informativeAgentFieldsService.GetInformativeFieldsBySegmentId(segmentId);
            var Clientinformation = new List<ClientDataViewModel>() { };
            foreach (var Fields in q)
            {
                string Field = GetPropertyValueHelper.Get(cd, Fields.DBFieldName),
                        Tittle = Fields.DisplayName,
                        Group = Fields.InformationGroup?.Name;
                int order = Fields.Order;

                Clientinformation.Add(new ClientDataViewModel()
                {
                    Name = Tittle,
                    Value = Field,
                    Group = Group,
                    Order = order

                });

            }

            return Clientinformation;
        }


        public async Task<ClientDataAndGestionViewModel> GetClientsDataFields(CampaignDto cpDto, GetInitialGestionQuery request , UserDto userDto)
        {
            cd = request.ClientId != null ? 
                    await _databaseClientDetailService.GetById((int)request.ClientId) :  
                request.Phone != null && cd == null ? 
                    await _databaseClientDetailService.GetByPhone(request.Phone) : cd;
            
            if (cd == null)
                cd = await _databaseClientDetailService.GetById(5) ?? throw new NotFoundException("Cliente", request.ClientId.ToString());


            RequestGestionParam rp = new RequestGestionParam()
            {
                ClientId = cd.Id,
                DialerServiceCode = request.DialerServiceCode,
                Phone = request.Phone,
                SourceId = request.SourceId,
            };
            return new ClientDataAndGestionViewModel()
            {
                GestionDto =  await PostGestion(cd, cpDto, rp, userDto),
                clientDataViewModels = GeClientDataAndGestionViewModel(cpDto.SegmentId, cd),
                clientValidatedDataViewModel = _autoMapper.Map<ClientValidatedDataViewModel>(cd),
            };
        }

        public async Task<ClientDataAndGestionViewModel> GetClientsDataFields(CampaignDto cpDto, GetInboundInitialGestionQuery request , UserDto userDto)
        {
            cd = await _databaseClientDetailService.GetById(3) ?? throw new NotFoundException("Cliente", 3);


            RequestGestionParam rp = new RequestGestionParam()
            {
                ClientId = cd.Id,
                DialerServiceCode = request.DialerServiceCode,
                Phone = request.Phone,
                SkillAvaya = request.SkillAvaya,
                SourceId = ""
            };

            return new ClientDataAndGestionViewModel()
            {
                GestionDto =  await PostGestion(cd, cpDto, rp, userDto),
                clientDataViewModels = GeClientDataAndGestionViewModel(cpDto.SegmentId, cd),
                clientValidatedDataViewModel = _autoMapper.Map<ClientValidatedDataViewModel>(cd),
            };
        }



        private async Task<GestionDto> PostGestion(DatabaseClientDetail cd , CampaignDto cpDto, RequestGestionParam request , UserDto userDto)
        {


            ActiveGestion activeGestion = new ActiveGestion(){
                Customerdocument = cd.Document,
                CustomerName = cd.Names,
                SegmentId = cpDto.SegmentId,
                SegmentName = cpDto.SegmentName,
                SubcampaignId = cpDto.SubCampaignId,
                SubCampaignName = cpDto.SubCampaignName,
                CampaignId = cpDto.Id,
                CampaignName = cpDto.Name,
                Phone = request.Phone, 
                Skill = request.DialerServiceCode,
                status = 1,
                CreatedByName = userDto.Names,
                CreatedBy = userDto.Id,
                CreatedAt = DateTime.Now,
                Dialercode =request.DialerServiceCode,
                DatabaseClientDetailId = cd.Id,
                DialerServiceId = request.SourceId,
                TeamLeader = "",
                ClientName = cd.Names,
                SkillAvaya = request.SkillAvaya,
            };

            var gs = await _activeGestionRepository.PostGestion(activeGestion);
            var gDto = _autoMapper.Map<GestionDto>(gs);

            return gDto;
        }


    

    }

    public class RequestGestionParam {
        public string DialerServiceCode { get; set; }
        public string Login { get; set; }
        public int? ClientId { get; set; }
        public string SourceId { get; set; }
        public string Phone { get; set; }
        public string SkillAvaya { get; set; }

    }


}
