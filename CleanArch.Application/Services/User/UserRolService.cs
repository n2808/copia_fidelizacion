﻿using Application.Core.Exceptions;
using Application.DTOs.User;
using Application.Interfaces.User;
using Application.User.Commands.PostUser;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.User;
using Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Services.User
{
    public class UserRolService : IUserRolService
    {
        private readonly IUserRolRepository _userRolRepository;
        private readonly IMapper _mapper;

        public UserRolService(
            IUserRolRepository userRolRepository,
            IMapper mapper
            )
        {
            _userRolRepository = userRolRepository;
            _mapper = mapper;
        }

        public bool Delete(Guid id)
        {
            UserRolDto userRolDto = _userRolRepository.Get()
                                      .Where(x => x.UserId == id)
                                      .ProjectTo<UserRolDto>(_mapper.ConfigurationProvider)
                                      .FirstOrDefault();

            UserRol userRol = _mapper.Map<UserRol>(userRolDto);

            return _userRolRepository.Delete(userRol);
        }

        public bool Delete(UserRol entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<UserRol> Get()
        {
            return _userRolRepository.Get();
            
        }

        public UserRol Post(UserRol userRol)
        {
            return _userRolRepository.Post(userRol);
        }

        public List<UserRol> PostRange(List<UserRol> userRol)
        {
            throw new NotImplementedException();
        }

        public List<UserRol> PostUserRol(PostUserCommand data, UserRol userRol)
        {
          
            throw new BadRequestException("Error al ingresar el user rol");
        }

        public UserRol Put(UserRol userRol)
        {            

            if (userRol == null)
            {
                throw new NotFoundException("UserRol", "Id");
            }

            return _userRolRepository.Put(userRol);
        }
    }
}
