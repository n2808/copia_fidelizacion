﻿using Application.Core.Exceptions;
using Application.DTOs.User;
using Application.Interfaces.User;
using Application.User.Commands.PostUser;
using Application.User.Commands.PutUser;
using Application.ViewModel;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.User;
using Domain.Models.User;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRolRepository _userRolRepository;
        private readonly IMapper _autoMapper;
        private readonly ICurrentUserService _currentUser;

        private readonly ISubCampaignService _subCampaignService;
        private int? subcampaignId = null; 
        private Guid? CreatedBy = null; 
        private Guid? UpdatedBy = null; 
        public UserService(
            IUserRepository userRepository,
            IUserRolRepository userRolRepository,
            ICurrentUserService currentUser,
            IMapper autoMapper,
             ISubCampaignService subCampaignService


            )
        {
            _userRepository = userRepository;
            _userRolRepository = userRolRepository;
            _currentUser = currentUser;
            _autoMapper = autoMapper;
            _subCampaignService = subCampaignService;
            subcampaignId = _currentUser.GetUserInfo().SubCampaignId;
            CreatedBy = _currentUser.GetUserInfo().Id;
            UpdatedBy = _currentUser.GetUserInfo().Id;

        }
        public List<UserDto> GetUser(int without)
        {
            var source = _userRepository.Get();           

            return without == 1 ? source.Where(x => x.SubCampaignId == subcampaignId).ProjectTo<UserDto>(_autoMapper.ConfigurationProvider).ToList()
                                : source.Where(x => x.SubCampaignId == subcampaignId && x.Status).ProjectTo<UserDto>(_autoMapper.ConfigurationProvider).ToList(); 
            
        }

        public UserDto GetUserByDocumentAndSubcampaign(string document, int subCampaignId)
        {
            UserDto users = new UserDto();

            users = _userRepository
                      .Get()
                      .Where(c => c.Document == document.Trim() && c.SubCampaignId == subCampaignId)
                      .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                      .FirstOrDefault();
            return users;

        }
        public Domain.Models.User.User GetById(Guid Id)
        {
            return _userRepository
                    .Get()
                    .Where(c => c.Id == Id && c.SubCampaignId == subcampaignId)
                    .FirstOrDefault();
        }

        public bool CheckById(Guid? Id)
        {
            return _userRepository
                    .Get()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }

        public bool CheckByDocumentAndSubcampaign(string document, int? subcampaign)
        {
            return _userRepository
                    .Get()
                    .Where(c => c.Document == document && c.SubCampaignId == subcampaign)
                    .Count() == 0 ? false : true;
        }

        public Domain.Models.User.User PostUser(Domain.Models.User.User user)
        {
            user.CreatedBy = CreatedBy;
            return _userRepository.Post(user);
          
        }

        public Domain.Models.User.User PutStatusActivateUserById(PutStatusActivateUserById request)
        {
            Domain.Models.User.User user = GetById(request.Id);

            user = _autoMapper.Map<PutStatusActivateUserById, Domain.Models.User.User>(request, user);
            user.UpdatedBy = CreatedBy;
            user.UpdatedAt = DateTime.Now;
            _userRepository.PutStatusActivateUserById(user);

            return user;
        }

        public Domain.Models.User.User PutStatusDeactivateUserById(PutStatusDeactivateUserById request)
        {
            Domain.Models.User.User user = GetById(request.Id);

            user = _autoMapper.Map<PutStatusDeactivateUserById, Domain.Models.User.User>(request, user);
            user.UpdatedBy = CreatedBy;
            user.UpdatedAt = DateTime.Now;
            _userRepository.PutStatusDeactivateUserById(user);

            return user;
        }

        public Domain.Models.User.User PutUser(PutUserCommand request)
        {
            Domain.Models.User.User user = GetById(request.Id);

            DeleteUserRol(user.Id);

            var ListUserRol = new List<UserRol>() { };

            foreach (var rol in request.Roles)
            {
                ListUserRol.Add(new UserRol()
                {
                    RolId = rol,
                    UserId = request.Id
                });
            }

            if (ListUserRol.Count > 0)
            {
                PostUserRol(ListUserRol);
            }

            user = _autoMapper.Map<PutUserCommand, Domain.Models.User.User>(request, user);
            user.UpdatedBy = CreatedBy;
            user.UpdatedAt = DateTime.Now;
            _userRepository.Put(user);
            return user;
        }

        public Domain.Models.User.User PutUserExcel(UserDto userDto)
        {
            var user = _autoMapper.Map<Domain.Models.User.User>(userDto);
            _userRepository.Put(user);

            return user;
        }

        public List<UserRol> PostUserRol(List<UserRol> listUserRol)
        {
            var y = _userRolRepository
                        .PostRange(listUserRol);
            return y;
        }

        public bool DeleteUser(Guid id)
        {
            Domain.Models.User.User user = _userRepository.Get()
                                      .Where(x => x.Id == id)
                                      .FirstOrDefault() ?? throw new NotFoundException("El usuario que intenta eliminar no se encuentra");          
            return _userRepository.Delete(user);
        }

        public bool DeleteUserRol(Guid id)
        {
            var userRols = _userRolRepository.Get()
                                      .Where(x => x.UserId == id)
                                      .ToList();


            return _userRolRepository.DeleteRange(userRols);
        }

        public bool GetByLogin(String login, int subCampaignId)
        {
            return _userRepository
                    .Get()
                    .Where(c => c.login == login && c.SubCampaignId == subCampaignId)
                    .Count() == 0 ? true : false; 
        }

        public async Task<Domain.Models.User.User> GetUserByLogin(String login, int subCampaignId)
        {
            return await _userRepository
                    .Get()
                    .Where(c => c.login == login && c.SubCampaignId == subcampaignId)
                    .FirstOrDefaultAsync();
        }

        public async Task<CreateUserViewModel> GetAllByLogin(String login)
        {
            var data = _userRepository
                    .Get()
                    .Where(c => c.login == login && c.Status == true)
                    .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider).ToList();

            if (data.Count() > 1)
            {
                List<string> campaings = new List<string>();
                for (int i = 0; i < data.Count; i++)
                {
                    //Lammar función que busque el nombre de las subcampaings
                    string dato = await GetNameSubCampaing(data[i].SubCampaignId);
                    campaings.Add(dato);
                }
                return new CreateUserViewModel()
                {
                    campaings = campaings,
                    message = "El login ingresado se encuentra activo en otras campañas",
                    code = "401",
                    status = false
                };
            }

            return new CreateUserViewModel()
            {
                status = true
            };
        }

        public async Task<string> GetNameSubCampaing(int? id)
        {
            var campaing = await _subCampaignService.GetNameSubcampaigns(id);
            return campaing;
        }

        public bool GetByLoginPut(String login, int subCampaignId, Guid id)
        {

            var usu = _userRepository
                    .Get()
                    .Where(c => c.Id == id && c.login == login && c.SubCampaignId == subCampaignId)
                    .Count() == 0 ? false : true;

            if (!usu)
            {
                var validate = GetByLogin(login, subCampaignId);
                return validate;
            }

            return usu;
        }

        public Domain.Models.User.User InsertByLoginSubcampain(String login, int subCampaignId)
        {
            var validateUser = GetByLogin(login, subCampaignId);


            var user = new Domain.Models.User.User();

            if (validateUser)
            {
                user.login = login;
                user.PassWord = login;
                user.SubCampaignId = subCampaignId;
                user.Names = "Digital";
                user.CreatedBy = CreatedBy;
                var userData = _userRepository.Post(user);

                return userData;
            }

            return user;
        }

        public bool PostSubcampaingUsers(PostMoveUserCommand request)
        {
            var Ids = request.usersIds;
            var subCampaing = request.SubCampaing;
            var userUpdate = GetNameCurrentUser();
            Guid idCurrentUser = (Guid)_currentUser.GetUserInfo().Id;

            for (int i = 0; i < Ids.Count; i++)
            {
                var id = Ids[i];
                var user = _userRepository.Get().Where(x => x.Id == id).FirstOrDefault();
                _userRepository.PostSubCampaingUser(user, subCampaing, userUpdate, idCurrentUser);
            }
            return true;
        }

        private string GetNameCurrentUser()
        {
            Guid idCurrentUser = (Guid)_currentUser.GetUserInfo().Id;
            string name = GetById(idCurrentUser).Names;
            string lastName = GetById(idCurrentUser).LastName == null ? "" : GetById(idCurrentUser).LastName;
            string fullName = name + " " + lastName;
            return fullName;
        }


    }
}
