﻿
using Application.Core.Exceptions;
using Application.Courses.Commands;
using Application.DTOs;
using Application.Interfaces.Surveys;
using Application.Surveys.Queries;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using Domain.Interfaces.Surveys;
using Domain.Models.SurveysResults;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Application.Services.Surveys
{
    public class SurveyResultQuestionService : ISurveyResultQuestionService
    {
        private readonly ISurveyResultQuestionRepository  _surveyResultQuestionRepository;
        private readonly IMapper _autoMapper;

        public SurveyResultQuestionService(
            ISurveyResultQuestionRepository surveyResultQuestionRepository,
        IMapper autoMapper
        )
        {
            _surveyResultQuestionRepository = surveyResultQuestionRepository;
            _autoMapper = autoMapper;
        }

         

        public List<SurveyResultQuestion> PostSurveyQuestion(PostSurveyResponseCommand data, SurveyResultGestion Survey)
        {

            var ListSurveyRQ = new List<SurveyResultQuestion>() { };
            

            foreach (var x in data.survey)
            {

                ListSurveyRQ.Add( new SurveyResultQuestion()
                 {
                     Name = x.name,
                     SurveyResultGestionId = Survey.Id,
                     OrderId = 1,
                     SurveyResultAnswers = AddAnswerToQuestion(x.multiAnswer,x.simpleAnswer)

                });
            }

            if (ListSurveyRQ.Count > 0)
            {
                var y = _surveyResultQuestionRepository
                        .PostRange(ListSurveyRQ);
                return y;

            }
            throw new BadRequestException("Error al ingresar las preguntas");
        }


        private List<SurveyResultAnswers> AddAnswerToQuestion(List<string> MultiAnswer , string SingleAnswer)
        {

            var ListSurveyRA = new List<SurveyResultAnswers>() { };

            if (!string.IsNullOrEmpty(SingleAnswer))
            {
                ListSurveyRA.Add(SurveyAndswer(SingleAnswer));
                return ListSurveyRA;
            }
            
            foreach (var x in MultiAnswer)
                ListSurveyRA.Add(SurveyAndswer(x));

            return ListSurveyRA;
        }


        private SurveyResultAnswers SurveyAndswer(string name)
        {
            return new SurveyResultAnswers()
            {
                Name = name,
                OrderId = 1,
                Description = ""
            };
        }

        


    }
}
