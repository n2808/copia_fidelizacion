﻿
using Application.Courses.Commands;
using Application.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Core.Bus;
using CleanArch.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Application.Services.Surveys
{
    public class SurveySegmentService : ISurveySegmentService
    {
        private readonly ISurveySegmentRepository _surveySegmentRepository;
        private readonly IMapper _autoMapper;

        public SurveySegmentService(
            ISurveySegmentRepository surveySegmentRepository, 
            IMapper autoMapper
        )
        {
            _surveySegmentRepository = surveySegmentRepository;
            _autoMapper = autoMapper;
        }

        public List<SurveySegmentDto> GetSurveyBySegmentId(int Id)
        {


            return _surveySegmentRepository
                    .Get()
                    .Include(c => c.Segment)
                    .Include(c => c.Survey)
                    .Where(c => c.status == true && c.SegmentId == Id )
                    .ProjectTo<SurveySegmentDto>(_autoMapper.ConfigurationProvider)
                    .ToList();
        }

      
    }
}
