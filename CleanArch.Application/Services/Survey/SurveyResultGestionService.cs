﻿
using Application.Core.Exceptions;
using Application.Courses.Commands;
using Application.DTOs;
using Application.Interfaces.Surveys;
using Application.Surveys.Queries;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using Domain.Interfaces.Surveys;
using Domain.Models.SurveysResults;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Services.Surveys
{
    public class SurveyResultGestionService : ISurveyResultGestionService
    {
        private readonly ISurveyResultGestionRepository  _surveyResultGestionRepository;
        private readonly IActiveGestionService  _gestionService;
        private readonly ISurveyService  _surveyService;
        //private readonly ISurvey  _gestionService;
        private readonly IMapper _autoMapper;

        public SurveyResultGestionService(
            ISurveyResultGestionRepository surveyResultGestionRepository,
            ISurveyService surveyService,
            IActiveGestionService gestionService,
        IMapper autoMapper
        )
        {
            _surveyResultGestionRepository = surveyResultGestionRepository;
            _gestionService = gestionService;
            _surveyService = surveyService;
            _autoMapper = autoMapper;
        }

         

        public async Task<SurveyResultGestion> PostSurveyResult(PostSurveyResponseCommand data)
        {

            var gestion = await _gestionService.GetGestionById(data.GestionId) ?? throw new NotFoundException("gestion", data.GestionId);
            var survey = _surveyService.GetById(data.surveyId) ?? throw new NotFoundException("survey", data.surveyId);
                

            var surveyResultGestion = new SurveyResultGestion()
            {
                GestionId = data.GestionId,
                SegmentId = gestion.SegmentId,
                SurveyId = data.surveyId,
                SurveyName = survey.Name
            };

            _surveyResultGestionRepository.Post(surveyResultGestion);
            
            return surveyResultGestion;
        }

      
    }
}
