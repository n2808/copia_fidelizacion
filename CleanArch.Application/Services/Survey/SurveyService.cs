﻿
using Application.Interfaces.Surveys;
using AutoMapper;
using Domain.Interfaces.Surveys;
using Domain.Models.Surveys;
using System.Linq;

namespace CleanArch.Application.Services.Surveys
{
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly IMapper _autoMapper;

        public SurveyService(
            ISurveyRepository surveyRepository,
        IMapper autoMapper
        )
        {
            _surveyRepository = surveyRepository;
            _autoMapper = autoMapper;
        }

        public Survey GetById(int Id)
        {
            return  _surveyRepository
                    .Get()
                    .Where(c => c.Id == Id)
                    .FirstOrDefault();
        }

        public bool CheckById(int Id)
        {
            return _surveyRepository
                    .Get()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }



    }
}
