﻿
using Application.Courses.Commands;
using Application.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Core.Bus;
using CleanArch.Domain.Interfaces;
using Core.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Application.Services
{
    public class InformativeAgentFieldsService : IInformativeAgentFieldsService
    {
        private readonly IInformativeAgentFieldsRepository _InformativeAgentFieldsRepository;
        private readonly IMapper _autoMapper;

        public InformativeAgentFieldsService(IServiceDialerRepository serviceDialerRepository, IMapper autoMapper , IInformativeAgentFieldsRepository informativeAgentFieldsRepository)
        {
            _InformativeAgentFieldsRepository = informativeAgentFieldsRepository;
            _autoMapper = autoMapper;
        }

        public List<InformativeAgentFields> GetInformativeFieldsBySegmentId(int Id)
        {

            return _InformativeAgentFieldsRepository.GetBySegmentId(Id).Include(x => x.InformationGroup).ToList();
        }

      
    }
}
