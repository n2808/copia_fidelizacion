﻿using Application.DTOs.ActiveGestion;
using Application.Gestions.Commands;
using Application.Interfaces.Reschedule;
using Application.ViewModel;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Domain.Interfaces;
using Core.Models.Operation;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class ActiveGestionService : IActiveGestionService
    {
        private readonly IActiveGestionRepository _activeGestionRepository;
        private readonly IGestionReportRepository _gestionReportRepository;
        private readonly IDatabaseClientReportsService _databaseClientReportsService;
        private readonly IDatabaseClientReportsRepository _databaseClientReportsRepository;
        private readonly IDatabaseClientDetailService _databaseClientDetailService;
        private readonly IGestionReportsOffersService _gestionReportsOffersService;
        private readonly IRescheduleService _rescheduleService;
        private readonly IGestionReportService _gestionReportService;


        private readonly IMapper _autoMapper;

        public ActiveGestionService(
            IActiveGestionRepository activeGestionRepository,
            IGestionReportRepository gestionReportRepository,
            IDatabaseClientReportsService databaseClientReportsService,
            IGestionReportsOffersService gestionReportsOffersService,
            IDatabaseClientReportsRepository databaseClientReportsRepository,
            IDatabaseClientDetailService databaseClientDetailService,
            IGestionReportService gestionReportService,
            IMapper autoMapper

            )
        {
            _activeGestionRepository = activeGestionRepository;
            _gestionReportRepository = gestionReportRepository;
            _databaseClientReportsService = databaseClientReportsService;
            _databaseClientDetailService = databaseClientDetailService;
            _gestionReportsOffersService = gestionReportsOffersService;
            _databaseClientReportsRepository = databaseClientReportsRepository;
            _gestionReportService = gestionReportService;

            _autoMapper = autoMapper;
        }

        public bool CheckAtiveGestionById(int Id)
        {
            return _activeGestionRepository
                    .GetActiveGestion()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }

        public async Task<ActiveGestion> GetGestionById(int Id)
        {
            return await _activeGestionRepository
                    .GetActiveGestion()
                    .Where(c => c.Id == Id)
                    .FirstOrDefaultAsync();
        }

        public bool AllowUpdate(int Id, Guid UserId)
        {
            return _activeGestionRepository
                    .GetActiveGestion()
                    .Where(c => c.Id == Id && c.CreatedBy == UserId)
                    .Count() == 0 ? false : true;
        }


        public async Task<ActiveGestion> updateGestion(EndGestionCommand request)
        {

            var activeGestion = await GetGestionById(request.GestionId);

            activeGestion.tipificacionId = request.TipificationId;
            activeGestion.EndAt = DateTime.Now;

            _autoMapper.Map<EndGestionViewModel, ActiveGestion>(request.Gestion, activeGestion);

            var activeGestionResult = await _activeGestionRepository.PutGestion(activeGestion);

            var clientDetail = await _databaseClientDetailService.GetById((int)activeGestion.DatabaseClientDetailId);

            var clientReports = _autoMapper.Map<DatabaseClientReports>(clientDetail);


            clientReports.SubcampaignId = activeGestionResult.SubcampaignId;
            clientReports.CampaignId = activeGestionResult.CampaignId;




            // save Reports
            await _databaseClientReportsService.PostAndUpdate(clientReports);

            var gestionReport = await _gestionReportService.GetGestionById(activeGestionResult.Id, CancellationToken.None);

            _ = gestionReport == null ?
                await _gestionReportRepository.PostGestion(_autoMapper.Map<GestionReports>(activeGestionResult)) :
                await _gestionReportService.UpdateGestion(_autoMapper.Map<ActiveGestion, GestionReports>(activeGestionResult, gestionReport), CancellationToken.None);


            return activeGestion;

        }

        public async Task<List<ActiveGestionDto>> GetGestionActiveById(int Id)
        {
            return await _activeGestionRepository.GetById()
                    .Where(x => x.Id == Id)
                    .ProjectTo<ActiveGestionDto>(_autoMapper.ConfigurationProvider)
                    .ToListAsync();
        }
    }
}
