﻿using Application.Core.Exceptions;
using Application.DTOs.Status;
using Application.Interfaces.Status;
using Application.Interfaces.User;
using Application.Status.Command;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Status;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Status
{
    public class StatusService : IStatusService
    {
        private readonly IStatusRepository _statusRepository;
        private readonly IMapper _mapper;


        public StatusService(IStatusRepository statusRepository, IMapper mapper)
        {
            _statusRepository = statusRepository;
            _mapper = mapper;
        }

        public async Task<List<StatusDto>> GetStatus(bool isSale)
        {
            return await _statusRepository.Get().Where(x => x.IsSale == isSale).ProjectTo<StatusDto>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<List<StatusDto>> GetStatusSubCampaing(int subCampaing)
        {
            return await _statusRepository.Get()
                            .Where(x => x.SubcampaingId == subCampaing)
                            .ProjectTo<StatusDto>(_mapper.ConfigurationProvider)
                            .ToListAsync();
        }

        public async Task<Domain.Models.Status.Status> GetStatusById(Guid id)
        {
            return await _statusRepository.Get()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<Domain.Models.Status.Status> Poststatus(Domain.Models.Status.Status status)
        {
            return await _statusRepository.Post(status);
        }

        public async Task<Domain.Models.Status.Status> PutStatus(PutStatusCommand putStatusCommand)
        {
            Domain.Models.Status.Status status = await GetStatusById(putStatusCommand.Id);

            if(status == null)
            {
                throw new NotFoundException("Status", "Id");
            }

            status = _mapper.Map<PutStatusCommand, Domain.Models.Status.Status>(putStatusCommand, status);
            await _statusRepository.Put(status);

            return status;
        }

        public async Task<bool> DeleteStatus(Guid id)
        {
            
            StatusDto statusDto = _statusRepository.Get()
                                                .Where(x => x.Id == id)
                                                .ProjectTo<StatusDto>(_mapper.ConfigurationProvider)
                                                .FirstOrDefault();

            if (statusDto == null)
            {
                throw new NotFoundException("statusDto", "Id");
            }

            Domain.Models.Status.Status status = _mapper.Map<Domain.Models.Status.Status>(statusDto);
            return await _statusRepository.Delete(status);
        }

       
    }
}
