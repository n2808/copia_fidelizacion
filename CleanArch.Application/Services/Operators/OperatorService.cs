﻿using Application.Core.Exceptions;
using Application.DTOs.Operators;
using Application.Interfaces.Operators;
using Application.Interfaces.User;
using Application.Operators.Command;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Operators;
using Domain.Models.Operators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.Operators
{
    public class OperatorService : IOperatorService
    {
        private readonly IOperatorRepository _operatorRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public OperatorService(IOperatorRepository operatorRepository, IMapper mapper, ICurrentUserService currentUserService, IUserService userService)
        {
            _operatorRepository = operatorRepository;
            _currentUserService = currentUserService;
            _userService = userService;
            _mapper = mapper;
        }

        public List<OperatorDto> GetOperators(int without)
        {
            var source = _operatorRepository.Get();

            return without == 1 ? source.ProjectTo<OperatorDto>(_mapper.ConfigurationProvider).ToList()
                                : source.Where(x => x.Status).ProjectTo<OperatorDto>(_mapper.ConfigurationProvider).ToList();
          
        }

        public Operator PostOperator(Operator _operator)
        {
            _operator.UserUpdate = GetNameCurrentUser();
            return _operatorRepository.Post(_operator);
        }

        public string GetNameCurrentUser()
        {
            Guid idCurrentUser = (Guid)_currentUserService.GetUserInfo().Id;
            string name = _userService.GetById(idCurrentUser).Names;
            string lastName = _userService.GetById(idCurrentUser).LastName == null ? "" : _userService.GetById(idCurrentUser).LastName;
            string fullName = name + " " + lastName;
            return fullName;
        }

        public Operator PutOperator(PutOperatorCommand putOperatorCommand)
        {
            Operator _operator = GetOperatorById(putOperatorCommand.Id);

            if (_operator == null)
            {
                throw new NotFoundException("Operator", "Id");
            }

            _operator = _mapper.Map<PutOperatorCommand, Operator>(putOperatorCommand, _operator);
            _operator.UserUpdate = GetNameCurrentUser();
            _operatorRepository.Put(_operator);

            return _operator;
        }

        public bool ActivateOperator(Guid id)
        {
            Operator _operator = GetOperatorById(id);

            if (_operator == null)
            {
                throw new NotFoundException("Operator", "Id");
            }

            _operator.UserUpdate = GetNameCurrentUser();

            return _operatorRepository.Activate(_operator);
        }

        public bool DeactivateOperator(Guid id)
        {
            Operator _operator = GetOperatorById(id);

            if (_operator == null)
            {
                throw new NotFoundException("Operator", "Id");
            }

            _operator.UserUpdate = GetNameCurrentUser();

            return _operatorRepository.Deactivate(_operator);
        }

        public bool DeleteOperator(Guid id)
        {
            Operator _operator = GetOperatorById(id);

            if (_operator == null)
            {
                throw new NotFoundException("Operator", "Id");
            }

            return _operatorRepository.Delete(_operator);
        }

        public Operator GetOperatorById(Guid id)
        {
            return _operatorRepository.GetById(id);
        }
             
    }

    

   
}
