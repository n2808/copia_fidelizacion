﻿using Application.Core.Exceptions;
using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SubCategoryTipificationService : ISubCategoryTipificationService
    {
        private readonly IMapper _mapper;
        private readonly ISubCategoryTipification _subCategoryTipification;

        public SubCategoryTipificationService(ISubCategoryTipification subCategoryTipification, IMapper mapper)
        {
            _subCategoryTipification = subCategoryTipification;
            _mapper = mapper;
        }
        public async Task<List<SubCategoryTipificationDto>> GetSubCategory(int code)
        {
            return await _subCategoryTipification.GetByType()
                                       .Where(x => x.Code == code)
                                       .ProjectTo<SubCategoryTipificationDto>(_mapper.ConfigurationProvider)
                                       .ToListAsync() ?? throw new NotFoundException("La sub-categoria de la tipificación que está intentando buscar no existe.");
        }
    }
}
