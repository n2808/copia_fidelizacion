﻿using Application.Core.Exceptions;
using Application.DTOs.Skills;
using Application.Interfaces.Skill;
using Application.Skills.Command.Put;
using Application.Skills.Queries;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Skill;
using Domain.Models.Skill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Skill
{
    public class SkillService : ISkillService
    {
        private readonly ISkillRepository _skillRepository;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;

        public SkillService(ISkillRepository skillRepository, IMapper mapper, ICurrentUserService currentUserService)
        {
            _currentUserService = currentUserService;
            _skillRepository = skillRepository;
            _mapper = mapper;
        }

        public async Task<List<SkillDto>> GetSkills(int without)
        {
            var IdSubCampaing = _currentUserService.GetUserInfo().SubCampaignId;
            return without == 1 ? _skillRepository.Get().Where(x => x.SubCampaignId == IdSubCampaing).ProjectTo<SkillDto>(_mapper.ConfigurationProvider).ToList()
                              : _skillRepository.Get().Where(x => x.SubCampaignId == IdSubCampaing && x.Active).ProjectTo<SkillDto>(_mapper.ConfigurationProvider).ToList();
        }

        public async Task<Domain.Models.Skill.Skills> PostSkills(Domain.Models.Skill.Skills skills)
        {
            skills.CreatedBy = _currentUserService.GetUserInfo().Id;
            skills.CreatedByName = _currentUserService.GetUserInfo().Name;
            return await _skillRepository.Post(skills);
        }

        public async Task<Domain.Models.Skill.Skills> PutActivateSkills(PutSkillsActivateCommand request)
        {
            Domain.Models.Skill.Skills skill = await GetSkillById(request.Id);
            skill = _mapper.Map<PutSkillsActivateCommand, Domain.Models.Skill.Skills>(request, skill);
            skill.Active = true;
            skill.UpdatedBy = _currentUserService.GetUserInfo().Id;
            skill.UpdatedByName = _currentUserService.GetUserInfo().Name;
            _skillRepository.Activate(skill);
            return skill;

        }

        public async Task<Domain.Models.Skill.Skills> PutDeActivateSkills(PutSkillsDeActivateCommand request)
        {
            Domain.Models.Skill.Skills skill = await GetSkillById(request.Id);
            skill = _mapper.Map<PutSkillsDeActivateCommand, Domain.Models.Skill.Skills>(request, skill);
            skill.Active = false;
            skill.UpdatedBy = _currentUserService.GetUserInfo().Id;
            skill.UpdatedByName = _currentUserService.GetUserInfo().Name;
            _skillRepository.Deactivate(skill);
            return skill;
        }

        public async Task<Domain.Models.Skill.Skills> PutSkills(PutSkillsCommand skills)
        {
            Domain.Models.Skill.Skills skill = await GetSkillById(skills.Id);
            skill = _mapper.Map<PutSkillsCommand, Domain.Models.Skill.Skills>(skills, skill);
            skill.UpdatedBy = _currentUserService.GetUserInfo().Id;
            skill.UpdatedByName = _currentUserService.GetUserInfo().Name;
            return await _skillRepository.Put(skill);
        }
        public async Task<bool> DeleteSkills(int id)
        {
            Domain.Models.Skill.Skills skill = await GetSkillById(id);
            return await _skillRepository.Delete(skill);
        }

        public async Task<Domain.Models.Skill.Skills> GetSkillById(int id)
        {
            return _skillRepository.Get().Where(x => x.Id == id).FirstOrDefault() ?? throw new NotFoundException("El skill que está intentando buscar no existe.");
        }
    }
}
