﻿using Application.Core.Exceptions;
using Application.DTOs.Quotes;
using Application.Interfaces.Quotes;
using Application.Interfaces.User;
using Application.Quotes.Command;
using Application.Quotes.Queries;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Quotes;
using Domain.Models.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.Quotes
{
    public class QuoteService : IQuoteService
    {

        private readonly IQuoteRepository _quoteRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public QuoteService(IQuoteRepository quoteRepository, IMapper mapper, ICurrentUserService currentUserService, IUserService userService)
        {
            _quoteRepository = quoteRepository;
            _mapper = mapper;
            _currentUserService = currentUserService;
            _userService = userService;
        }
        public List<QuoteDto> GetQuotes(GetQuoteQuery getQuoteQuery)
        {

            IQueryable<Quote> quote = _quoteRepository.Get();

            quote = getQuoteQuery.Without == 1 ? quote : quote.Where(x => x.Status);
            quote = getQuoteQuery.SubCampaignId == null  ? quote : quote.Where(x => x.SubCampaignId == getQuoteQuery.SubCampaignId);
            quote = getQuoteQuery.ProductId == null  ? quote : quote.Where(x => x.ProductId == getQuoteQuery.ProductId);
            quote = getQuoteQuery.OperatorId == null  ? quote : quote.Where(x => x.OperatorId == getQuoteQuery.OperatorId);
            quote = getQuoteQuery.PlanId == null  ? quote : quote.Where(x => x.PlanId == getQuoteQuery.PlanId);

            return  quote
                    .ProjectTo<QuoteDto>(_mapper.ConfigurationProvider)
                    .ToList(); 
        }

        public Quote PostQuote(Quote quote)
        {
            quote.UserUpdate = GetNameCurrentUser();
            return _quoteRepository.Post(quote);
        }

        public string GetNameCurrentUser()
        {
            Guid idCurrentUser = (Guid)_currentUserService.GetUserInfo().Id;

            string name = _userService.GetById(idCurrentUser).Names;
            string lastName = _userService.GetById(idCurrentUser).LastName == null ? "" : _userService.GetById(idCurrentUser).LastName;
            string fullName = name + " " + lastName;
            return fullName;

        }

        public Quote PutQuote(PutQuoteCommand putQuoteCommand)
        {
            Quote quote = GetQuotetById(putQuoteCommand.Id);

            if (quote == null)
            {
                throw new NotFoundException("Quote", "Id");
            }

            quote = _mapper.Map<PutQuoteCommand, Quote>(putQuoteCommand, quote);
            quote.UserUpdate = GetNameCurrentUser();
            _quoteRepository.Put(quote);

            return quote;
        }

        public bool ActivateQuote(Guid id)
        {
            Quote quote = GetQuotetById(id);

            if (quote == null)
            {
                throw new NotFoundException("ProducDto", "Id");
            }

            quote.UserUpdate = GetNameCurrentUser();

            return _quoteRepository.Activate(quote);
        }

        public bool DeactivateQuote(Guid id)
        {
            Quote quote = GetQuotetById(id);

            if (quote == null)
            {
                throw new NotFoundException("Quote", "Id");
            }

            quote.UserUpdate = GetNameCurrentUser();

            return _quoteRepository.Deactivate(quote);
        }
        public bool DeleteQuote(Guid id)
        {
            Quote quote = GetQuotetById(id);

            if (quote == null)
            {
                throw new NotFoundException("Quote", "Id");
            }

            return _quoteRepository.Delete(quote);
        }

        public Quote GetQuotetById(Guid id)
        {
            return _quoteRepository.GetById(id);

        }
    }
}
