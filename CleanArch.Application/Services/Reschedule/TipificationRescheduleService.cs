﻿using Application.Core.Exceptions;
using Application.DTOs.Reschedule;
using Application.Interfaces.Reschedule;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.ReSchedule;
using Domain.Models.ReSchedule;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Reschedule
{
    public class TipificationRescheduleService : ITipificationRescheduleService
    {
        private readonly ITipificationRescheduleRepository _tipificationRescheduleRepository;

        public TipificationRescheduleService(

            ITipificationRescheduleRepository tipificationRescheduleRepository

        )
        {
            _tipificationRescheduleRepository = tipificationRescheduleRepository;

        }



        public async Task<TipificationReschedule> GetTipificationBySegmentIdAndCode(int segmentId, string code)
        {
            return await _tipificationRescheduleRepository
                        .Get()
                        .Where(c => c.status == true && c.SegmentId == segmentId && c.TipificationCode == code)
                        .FirstOrDefaultAsync();
        }





    }
}
