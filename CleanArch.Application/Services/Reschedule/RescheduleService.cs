﻿using Application.Interfaces.Reschedule;
using CleanArch.Application.Interfaces;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Reschedule
{
    public class RescheduleService : IRescheduleService
    {
        private ITipificationRescheduleService _tipificationRescheduleService;
        private IGestionRescheduleService _gestionRescheduleService;
        private ITipificationService _tipificationService;

        public RescheduleService(
            ITipificationRescheduleService tipificationRescheduleService,
            ITipificationService tipificationService,
            IGestionRescheduleService gestionRescheduleService
        )
        {
            _tipificationRescheduleService = tipificationRescheduleService;
            _tipificationService = tipificationService;
            _gestionRescheduleService = gestionRescheduleService;
        }

        public async Task Reschedule(ActiveGestion gestionReports)
        {

            var code = await _tipificationService.GetTipificationById((int)gestionReports.tipificacionId);
            if (code == null) return;
            

            var tipificationReschedule = await
                    _tipificationRescheduleService
                    .GetTipificationBySegmentIdAndCode(gestionReports.SegmentId, code.Dialer1Code);

            if (tipificationReschedule != null)  await this.SaveReschedule(gestionReports);

        }


        private async Task SaveReschedule(ActiveGestion gestionReports) {
            await  _gestionRescheduleService.AddGestionReschedule(gestionReports);
        }



    }
}
