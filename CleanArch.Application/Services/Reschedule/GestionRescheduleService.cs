﻿using Application.Core.Exceptions;
using Application.DTOs.Reschedule;
using Application.Interfaces.Reschedule;
using AutoMapper;
using CleanArchitecture.Application.Common.Interfaces;
using Core.Models.Operation;
using Domain.Interfaces.ReSchedule;
using Domain.Models.ReSchedule;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.Reschedule
{
    public class GestionRescheduleService : IGestionRescheduleService
    {
        private readonly IGestionRescheduleRepository _gestionRescheduleRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public GestionRescheduleService(

            IGestionRescheduleRepository gestionRescheduleRepository,
            ICurrentUserService currentUserService,
            IMapper mapper

        )
        {
            _gestionRescheduleRepository = gestionRescheduleRepository;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }





        public async Task<List<RescheduleGroupDto>> GetCountGestionRescheduleAvailable(bool statusId = true)
        {
            return await _gestionRescheduleRepository
                        .Get()
                        .Include(x => x.segment)
                        .ThenInclude(x => x.SubCampaign)
                        .Where(c =>
                            c.status == statusId &&
                            c.AgentId == null
                         )
                        .GroupBy(x => new
                        {
                            SegmentId = x.SegmentId,
                            SegmentName = x.segment.Name,
                            SubcampaignId = x.segment.SubCampaignId,
                            SubcampaignName = x.segment.SubCampaign.Name,
                        })
                        .Select(g => new RescheduleGroupDto
                        {
                            SegmentId = g.Key.SegmentId,
                            SegmentName = g.Key.SegmentName,
                            SubcampaignId = g.Key.SubcampaignId,
                            SubcampaignName = g.Key.SubcampaignName,
                            count = g.Count()
                        })
                        .ToListAsync();


        }


        public async Task<GestionReschedule> RequestAGestionRescheduleWithoutAgent(int? segmentId)
        {


            var gestionRescheduleFilter = _gestionRescheduleRepository.Get().Where(x => x.AgentId == null).OrderBy(x => x.CreatedAt);
            if (segmentId != null) gestionRescheduleFilter = gestionRescheduleFilter.Where(x => x.SegmentId == segmentId).OrderBy(x => x.CreatedAt);
            var gestionReschedule = gestionRescheduleFilter.FirstOrDefault();
            if (gestionReschedule != null)
            {
                gestionReschedule.AgentId = _currentUserService.GetUserInfo().Id;
                return await _gestionRescheduleRepository.Put(gestionReschedule);

            }


            throw new NotFoundException($"no hay gestiones disponibles para el segmento {segmentId}");
        }

        public async Task<List<GestionReschedule>> GestionRescheduleByAgent(Guid AgentId)
        {
            return await _gestionRescheduleRepository
                        .Get()
                        .Include(x => x.GestionReports)
                        .Where(c => c.status == true &&
                                    c.AgentId == AgentId
                               )
                        .ToListAsync();
        }


        public async Task UpdateGestionReschedule(Guid RescheduleId, Guid AgentId, bool status)
        {
            var gestionReschedule = await _gestionRescheduleRepository
                                    .Get()
                                    .Where(x => x.Id == RescheduleId)
                                    .FirstOrDefaultAsync();

            if (gestionReschedule == null) throw new NotFoundException("Gestion", RescheduleId);
           
            gestionReschedule.status = !status;
            gestionReschedule.UpdatedBy = AgentId;
            gestionReschedule.UpdatedAt = DateTime.Now;
            gestionReschedule.Count++;

            await _gestionRescheduleRepository.Put(gestionReschedule);
        }



        public async Task AddGestionReschedule(ActiveGestion gestion)
        {

            var gestionReschedule = _mapper.Map<GestionReschedule>(gestion);
            gestionReschedule.CreatedAt = DateTime.Now;
            await _gestionRescheduleRepository.Post(gestionReschedule);
        }
    }
}
