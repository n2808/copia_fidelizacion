﻿using Application.ClaroUser.Command.Put;
using Application.ClaroUser.Queries;
using Application.Common.Constants;
using Application.Common.Response;
using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ClaroUserService : IClaroUserService
    {
        private readonly IClaroUserRepository _claroUserRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IHostingEnvironment _env;
        private readonly IMapper _mapper;

        public ClaroUserService(IClaroUserRepository claroUserRepository, IHostingEnvironment env, IMapper mapper, ICurrentUserService currentUserService)
        {
            _claroUserRepository = claroUserRepository;
            _currentUserService = currentUserService;
            _mapper = mapper;
            _env = env;
        }

        public async Task<Domain.Models.ClaroUser.ClaroUser> GetUserByCode(string code)
        {
            return  _claroUserRepository.GetUsersCodeAdviser().Where(x => x.Code.Contains(code))
                   .FirstOrDefault() ?? null;
        }

        public async Task<Domain.Models.ClaroUser.ClaroUser> GetUserByCode(GetCodeClaroUserQuery request)
        {
            return await GetUserByCode(request.Code);
        }

        public async Task<Domain.Models.ClaroUser.ClaroUser> PostClaroUser(Domain.Models.ClaroUser.ClaroUser claroUser)
        {
            claroUser.CreatedByName = _currentUserService.GetUserInfo().Name;
            claroUser.CreatedBy = _currentUserService.GetUserInfo().Id;
            return await _claroUserRepository.Post(claroUser);
        }

        public async Task<bool> PostClaroUserExcel(Domain.Models.ClaroUser.ClaroUser claroUser)
        {
            claroUser.CreatedByName = _currentUserService.GetUserInfo().Name;
            claroUser.CreatedBy = _currentUserService.GetUserInfo().Id;
            await _claroUserRepository.Post(claroUser);
            return true;
        }

        public async Task<Domain.Models.ClaroUser.ClaroUser> PutClaroUser(PutClaroUserCommand claroUser)
        {
            Domain.Models.ClaroUser.ClaroUser userClaro = await GetUserByCode(claroUser.Code);
            userClaro = _mapper.Map<PutClaroUserCommand, Domain.Models.ClaroUser.ClaroUser>(claroUser, userClaro);
            userClaro.UpdatedBy = _currentUserService.GetUserInfo().Id;
            //userClaro.UpdatedByName = _currentUserService.GetUserInfo().Name;
            userClaro.UpdatedAt = DateTime.Today;
            return await _claroUserRepository.Put(userClaro);
        }

        public async Task<Domain.Models.ClaroUser.ClaroUser> PutClaroUser(ClaroUserDto claroUser)
        {
            Domain.Models.ClaroUser.ClaroUser userClaro = await GetUserByCode(claroUser.Code);
            claroUser.Id = userClaro.Id;
            userClaro = _mapper.Map<ClaroUserDto, Domain.Models.ClaroUser.ClaroUser>(claroUser, userClaro);
            userClaro.UpdatedBy = _currentUserService.GetUserInfo().Id;
            //userClaro.UpdatedByName = _currentUserService.GetUserInfo().Name;
            userClaro.UpdatedAt = new DateTime();
            return await _claroUserRepository.Put(userClaro);
        }
        public async Task<bool> PutClaroUserExcel(ClaroUserDto claroUser, Domain.Models.ClaroUser.ClaroUser existCode)
        {
            claroUser.Id = existCode.Id;
            var userClaro = _mapper.Map<ClaroUserDto, Domain.Models.ClaroUser.ClaroUser>(claroUser, existCode);
            userClaro.UpdatedBy = _currentUserService.GetUserInfo().Id;
            //userClaro.UpdatedByName = _currentUserService.GetUserInfo().Name;
            userClaro.UpdatedAt = new DateTime();
            await _claroUserRepository.Put(userClaro);
            return true;
        }

        public async Task<ApiResponse<IList<ClaroUserDto>>> SaveFile(IFormFile file)
        {
            var response = new ApiResponse<IList<ClaroUserDto>>();
            string fullPath = string.Empty;

            try
            {
                if (file == null || file.Length == 0)
                {
                    throw new Exception("Debe seleccionar un servicio / el archivo no tiene ningun item.");
                }

                SetFullPath(ref fullPath, file);
                IList<ClaroUserDto> userListDto = ReadFile(fullPath);

                foreach (var item in userListDto)
                {
                    //Validar si el codigo ya existe
                    var existCode = await GetUserByCode(item.Code);
                    if (existCode != null)
                    {
                        //Se actualiza el registro encontrado
                        await PutClaroUserExcel(item, existCode);
                    }
                    else
                    {
                        //Se crea el nurevo registro
                        var userClaro = _mapper.Map<Domain.Models.ClaroUser.ClaroUser>(item);
                        await PostClaroUserExcel(userClaro);
                    }
                }
                response.Message = "Operación Exitosa";
                response.Result = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }

            return response;
        }

        private (string, bool) ValidateExcel(ClaroUserDto item)
        {
            IList<string> errors = new List<string>();

            errors.Add(string.IsNullOrEmpty(item.Code) ? "El codigo es requerido" : null);
            errors.Add(string.IsNullOrEmpty(item.Charge) ? "El cargo es requerido" : null);
            errors.Add(string.IsNullOrEmpty(item.Location) ? "La ubicación del CALL CENTER es requerido" : null);
            errors.Add(string.IsNullOrEmpty(item.Region) ? "La región es requerida" : null);
            errors.Add(string.IsNullOrEmpty(item.Ring) ? "El anillo es requerido" : null);
            errors.Add(string.IsNullOrEmpty(item.Supervisor) ? "El nombre del supervisor es requerido" : null);
            errors.Add(string.IsNullOrEmpty(item.IdentificationSupervisor) ? "La identificación del supervisor es requerida" : null);
            errors.Add(string.IsNullOrEmpty(item.Coordinator) ? "El nombre del coordinador es requerido" : null);
            errors.Add(string.IsNullOrEmpty(item.IdentificationCoordinator) ? "La identificación del coordinador es requerida" : null);

            string error = string.Join(", ", errors.GroupBy(x => x).Select(x => x.First()).Where(x => x != null).ToArray());
            return (error, !string.IsNullOrEmpty(error));
        }

        private IList<ClaroUserDto> ReadFile(string temporalUrl)
        {
            // Se inicializa la lista de usuarios a validar.
            IList<ClaroUserDto> claroUserList = new List<ClaroUserDto>();

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            // Se crea al acceso al archivo.
            using (var stream = File.Open(temporalUrl, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet();
                    // Se toma la informacion del archivo de excel.
                    DataTable table = result.Tables[0];
                    // Contador que recorre las posiciones de la lista.
                    int i = 0;
                    // Se recorren los registros.
                    foreach (DataRow row in table.Rows)
                    {
                        var item = row.Table.Rows.Count;
                        // Se captura los datos del excel en cada posición.
                        string Code = row[0].ToString().ToUpper().Trim();
                        string Charge = row[1].ToString().ToUpper().Trim();
                        string Location = row[2].ToString().ToUpper().Trim();
                        string Region = row[3].ToString().ToUpper().Trim();
                        string Ring = row[4].ToString().ToUpper().Trim();
                        string Supervisor = row[5].ToString().ToUpper().Trim();
                        string IdentificationSupervisor = row[6].ToString().ToUpper().Trim();
                        string Coordinator = row[7].ToString().ToUpper().Trim();
                        string IdentificationCoordinator = row[8].ToString().ToUpper().Trim();

                        // Se valida solo la primera fila del archivo de excel que es el encabezado.
                        if (row.Table.Rows.Count == 0)
                        {
                            string[] currentFormat = new string[]
                            {
                                     Code,Charge
                            };

                            currentFormat = Array.ConvertAll(currentFormat, a => a.ToUpper());

                            if (!currentFormat.SequenceEqual(Array.ConvertAll(Constant.FORMAT, a => a.ToUpper())))
                            {
                                throw new Exception($"Formato incorrecto, el formato correcto es {string.Join(",", Constant.FORMAT)}");
                            }

                        }

                        // La posición cero es el encabezado y no se agrega al objeto.
                        if (i > 0)
                        {

                            ClaroUserDto user = new ClaroUserDto()
                            {
                                Code = Code,
                                Charge = Charge,
                                Location = Location,
                                Region = Region,
                                Ring = Ring,
                                Supervisor = Supervisor,
                                IdentificationSupervisor = IdentificationSupervisor,
                                Coordinator = Coordinator,
                                IdentificationCoordinator = IdentificationCoordinator
                            };

                            // Se agrega el objeto a la lista.
                            claroUserList.Add(user);

                            // Se valida el tamaño de la lista.
                            if (claroUserList.Count() > 150)// TODO: Definir variable.
                            {
                                throw new Exception($"El tamaño valido de registros para el archivo excel debe ser menor o igual a { 150 }");
                            }
                        }

                        // Se aumenta el contador en 1.
                        i++;
                    }
                }
            }

            // Listado de usuarios sin repetir.
            IList<ClaroUserDto> listUserWithoutRepeat = claroUserList.GroupBy(x => x.Code).Select(x => x.First()).ToList();
            return claroUserList;
        }

        private void SetFullPath(ref string fullPath, IFormFile file)
        {
            // Se define el nombre de la carpeta donde se guarda el archivo para validar.
            string nameFolder = "CargueMasivoClaroUSer";

            // Se hace referencia a la carpeta wwwrooot que esta en la raiz del proyecto.
            string webRootPath = _env.WebRootPath;

            // Se crea la ruta.
            string newPath = Path.Combine(webRootPath, nameFolder);

            // Se valida si la ruta existe o si no se crea.
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            if (file.Length > 0)
            {
                // Se obtiene la extensión del archivo.
                string extensionFile = Path.GetExtension(file.FileName).ToLower();

                // Se valida la extensión (.xlsx | .xls)
                if (!extensionFile.ToLower().Contains(".xlsx") || !extensionFile.ToLower().Contains(".xls"))
                {
                    throw new Exception("Formato incorrecto");
                }

                DateTime date = DateTime.Today;

                // Se define la ruta de guardado del archivo.
                fullPath = Path.Combine(newPath, date.ToString("MM-dd-yy") + file.FileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
        }
    }
}
