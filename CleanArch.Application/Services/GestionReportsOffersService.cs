﻿using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Interfaces;
using Core.Models.Operation;
using Domain.Models.Offer;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class GestionReportsOffersService : IGestionReportsOffersService
    {
        private readonly IGestionReportsOffersRepository _gestionReportsOffersRepository;
        private readonly IMapper _autoMapper;

        public GestionReportsOffersService(
            IGestionReportsOffersRepository gestionReportsOffersRepository, 
            IMapper autoMapper
            
            )
        {
            _gestionReportsOffersRepository = gestionReportsOffersRepository;
            _autoMapper = autoMapper;
        }

        public async Task<List<GestionReportsOffers>> GetOffersByGestionReportsId(int? gestionReportsId)
        {

            List<GestionReportsOffers> listOffers = new List<GestionReportsOffers>() { };

            listOffers = await _gestionReportsOffersRepository.GetOffers().Where(x => x.GestionReportsId == gestionReportsId).ToListAsync();

            return listOffers;
        }

        public async Task<OffersViewModel> AddOffers(OffersViewModel offers,ActiveGestion activeGestion)
        {

            List<GestionReportsOffers> listOffers = new List<GestionReportsOffers>() { };

            if (offers == null || offers.value == null)
                return offers;


            foreach (var x in offers.value)
            {
                listOffers.Add(new GestionReportsOffers() { 
                    Name = x,
                    GestionReportsId = (int)activeGestion.Id,
                    DatabaseClientReportsId = (int)activeGestion.DatabaseClientDetailId,
                });
            }

            if(listOffers.Count > 0)
            {
                await _gestionReportsOffersRepository.PostOffers(listOffers);
                
            }

            return offers;
        }

        public async Task DeleteOffers(List<GestionReportsOffers> offers)
        {
            await _gestionReportsOffersRepository.DeleteOffers(offers);
        }
    }
}
