﻿using Application.Interfaces;
using AutoMapper;
using CleanArch.Domain.Interfaces;
using Core.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Services
{
    public class GestionStructureService : IGestionStructureService
    {
        

        private readonly IGestionStructureRepository _gestionStructureRepository;
        private readonly IMapper _autoMapper;

        public GestionStructureService(IMapper autoMapper, IGestionStructureRepository gestionStructureRepository)
        {
            _gestionStructureRepository = gestionStructureRepository;
            _autoMapper = autoMapper;
        }

        public List<GestionStructure> GetGestionStructureBySegmentId(int Id)
        {
            return _gestionStructureRepository.GetBySegmentId(Id).ToList();
        }
    }
}
