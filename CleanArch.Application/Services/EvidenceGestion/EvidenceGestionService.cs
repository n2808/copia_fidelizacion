﻿using Application.DTOs.EvidenceGestion;
using Application.EvidenceGestion.Queries;
using Application.Interfaces.EvidenceGestion;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.EvidenceGestion;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.EvidenceGestion
{
    public class EvidenceGestionService : IEvidenceGestionService
    {
        private readonly IMapper _autoMapper;
        private readonly IEvidenceGestionRepository _evidenceGestionRepository;

        public EvidenceGestionService(IMapper autoMapper, IEvidenceGestionRepository evidenceGestionRepository)
        {
            _autoMapper = autoMapper;
            _evidenceGestionRepository = evidenceGestionRepository;
        }

        public List<EvidenceGestionDto> GetByIdEvidencesGestion(int Id)
        {
            var evidences = _evidenceGestionRepository.GetEvidenceGestion()
                                                        .Where(c => c.GestionReportsId == Id)
                                                        .ProjectTo<EvidenceGestionDto>(_autoMapper.ConfigurationProvider)
                                                        .ToList();

            return evidences;

        }

        public async Task<List<EvidenceGestionDto>>EvidenceGestion(EvidenceGestionCommand evidenceGestions)
        {
            var evidenceGestion =  _autoMapper.Map<Domain.Models.Gestion.EvidenceGestion>(evidenceGestions);

            var path200 = @"\\172.16.5.200\Repositorio\PrismaEvidences";
            string targetPath = @"Resources/PublicFiles/Evidences";
            var op = Directory.Exists(targetPath);
            if(!op)
            {
                Directory.CreateDirectory(targetPath);
            }

            DirectoryInfo di200 = new DirectoryInfo(@path200);

            foreach (var fi in di200.GetFiles("*_" + evidenceGestion.GestionReportsId + "_*"))
            {
                evidenceGestion.Id = new Guid();
                evidenceGestion.FilePath = path200;
                var archivo = fi.Name;
                evidenceGestion.FileName = archivo.ToString();

                var findEvidences = await GetByIdEvidenceFile(evidenceGestion.GestionReportsId, archivo.ToString());

                if (findEvidences.Count == 0)
                {
                    await _evidenceGestionRepository.PostEvidenceGestion(evidenceGestion);
                }
            }

            var evidences = await GetByIdEvidence(evidenceGestion.GestionReportsId);

            foreach (var item in evidences)
            {
                File.Copy(path200 + "/" + item.FileName, targetPath + "/" + item.FileName, true);
            }

            return evidences;
        }

        public async Task<List<EvidenceGestionDto>> GetByIdEvidence(int GestionReportsId)
        {
            return await _evidenceGestionRepository.Get()
                                        .Where(x => x.GestionReportsId == GestionReportsId)
                                        .ProjectTo<EvidenceGestionDto>(_autoMapper.ConfigurationProvider)
                                        .ToListAsync();
        }

        public async Task<List<EvidenceGestionDto>> GetByIdEvidenceFile(int GestionReportsId, string archivo)
        {
            return await _evidenceGestionRepository.Get()
                                        .Where(c => c.GestionReportsId == GestionReportsId && c.FileName == archivo)
                                        .ProjectTo<EvidenceGestionDto>(_autoMapper.ConfigurationProvider)
                                        .ToListAsync();
        }
    }
}
