﻿using AutoMapper;
using CleanArch.Application.Interfaces.Notifications;
using CleanArch.Domain.Interfaces.Notifications;
using Core.Events.SMS;
using Core.Models.Operation;
using Domain.Notification;

namespace CleanArch.Application.Services.Notifications
{
    public class NotificationLogService : INotificationLogService
    {
        private readonly INotificationLogRepository _notificationLogRepository;
        private readonly IMapper _autoMapper;

        public NotificationLogService(INotificationLogRepository notificationLogRepository, IMapper autoMapper)
        {
            _notificationLogRepository = notificationLogRepository;
            _autoMapper = autoMapper;
        }

        public bool  Post(NotificationAccountViewModel data,ActiveGestion gestion)
        {
            var x = new NotificationLog()
            {
                Name = data.Name,
                phone =data.phone,
                Sendby = gestion.CreatedByName,
                Message = data.Message1,
                flash =data.Name,
                UserId = gestion.CreatedBy,
                customdata1 = data.Description,
                customdata2 = data.Message2,
                AccountName = data.Account,
                status = true,
                CampaignId = gestion.CampaignId,
                SubCampaignId = gestion.SubcampaignId,
                SegmentId = gestion.SegmentId,
            };

            _notificationLogRepository.Post(x);
            return true;


        }

   
    }
}
