﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.Interfaces.Notifications;
using CleanArch.Domain.Interfaces.Notifications;
using Core.Events.SMS;
using System.Linq;

namespace CleanArch.Application.Services.Notifications
{
    public class NotificationAccountService : INotificationAccountService
    {
        private readonly INotificationAccountRepository _notificationAccountRepository;
        private readonly IMapper _autoMapper;

        public NotificationAccountService(INotificationAccountRepository notificationAccountRepository, IMapper autoMapper)
        {
            _notificationAccountRepository = notificationAccountRepository;
            _autoMapper = autoMapper;
        }

        public NotificationAccountViewModel GetAccountById(int Id)
        {
            return _notificationAccountRepository.Get()
                    .Where(c => c.status == true && c.Id == Id)
                    .ProjectTo<NotificationAccountViewModel>(_autoMapper.ConfigurationProvider)
                    .FirstOrDefault();
        }

   
    }
}
