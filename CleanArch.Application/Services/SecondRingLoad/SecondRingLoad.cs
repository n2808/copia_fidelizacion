﻿using Application.Common.Constants;
using Application.Common.Response;
using Application.Core.Exceptions;
using Application.DTOs.SecondRingLoad;
using Application.Interfaces;
using Application.Interfaces.SecondRingLoad;
using Application.Interfaces.User;
using Application.ViewModel;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.SecondRingLoad;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services.SecondRingLoad
{
    public class SecondRingLoad : ISecondRingLoad
    {
        private readonly IBackOfficeManagementsService _backOfficeManagementsService;
        private readonly ISecondRingLoadRepository _secondRingLoadRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IUserService _userService;
        private readonly IHostingEnvironment _env;
        private readonly IMapper _mapper;


        public SecondRingLoad(IBackOfficeManagementsService backOfficeManagementsService, ISecondRingLoadRepository secondRingLoadRepository, IHostingEnvironment env, IUserService userService, IMapper mapper, ICurrentUserService currentUserService)
        {
            _backOfficeManagementsService = backOfficeManagementsService;
            _secondRingLoadRepository = secondRingLoadRepository;
            _currentUserService = currentUserService;
            _userService = userService;
            _mapper = mapper;
            _env = env;
        }

        public async Task<string> GetIdUser(string login)
        {
            int subCampaign = 9;

            var user = await _userService.GetUserByLogin(login, subCampaign);
            if(user == null)
            {
                return "";
            }
            return user.Id.ToString();
        }

        public async Task<Domain.Models.SecondRingLoads.SecondRingLoad> PostSecondRingLoad(Domain.Models.SecondRingLoads.SecondRingLoad assigned)
        {
           
            assigned.CreatedByName = _currentUserService.GetUserInfo().Name;
            assigned.CreatedBy = _currentUserService.GetUserInfo().Id;
            return await _secondRingLoadRepository.Post(assigned);
        }
        public async Task<ApiResponse<IList<SecondRingLoadDto>>> SaveFile(IFormFile file)
        {
            var response = new ApiResponse<IList<SecondRingLoadDto>>();
            string fullPath = string.Empty;

            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(300));

            try
            {
                if (file == null || file.Length == 0)
                {
                    throw new Exception("Debe seleccionar un servicio / el archivo no contiene ningun registro.");
                }

                SetFullPath(ref fullPath, file);
                IList<SecondRingLoadDto> secondRingLoads = ReadFile(fullPath);
                List<SecondRingLoadDto> login = new List<SecondRingLoadDto>();

                foreach (var item in secondRingLoads)
                {
                    // Obtener el id del usuario asignado
                    var id = await GetIdUser(item.assignedLogin);
                    if(id != null && id != "")
                    {
                        if(item.type == "MOVIL" && item.phone == "")
                        {
                            login.Add(item);
                        }
                        if ((item.type == "HOGAR") && item.account == "" || item.phone == "")
                        {
                            login.Add(item);
                        }

                        item.assignedId = Guid.Parse(id);
                        item.status = false;
                        var assigned = _mapper.Map<Domain.Models.SecondRingLoads.SecondRingLoad>(item);
                        await PostSecondRingLoad(assigned);
                    } else
                    {
                        login.Add(item);
                    }
                }

                if(login.Count > 1)
                {
                    response.Url = GenerateExcelException(login);
                }

                response.Message = "se han registrado con exito la carga";
                response.Result = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }

            return response;
        }

        private IList<SecondRingLoadDto> ReadFile(string fullPath)
        {
            IList<SecondRingLoadDto> secondRingLoads = new List<SecondRingLoadDto>();

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            // Se crea al acceso al archivo.
            using (var stream = File.Open(fullPath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet();
                    // Se toma la informacion del archivo de excel.
                    DataTable table = result.Tables[0];
                    // Contador que recorre las posiciones de la lista.
                    int i = 0;
                    Guid loadId = Guid.NewGuid();
                    var date = DateTime.Now.ToString("yyyy_MM_dd_HH_mm");
                    var loadName = "SECOND_RING_" + date;

                    // Se recorren los registros.
                    foreach (DataRow row in table.Rows)
                    {
                        var item = row.Table.Rows.Count;
                        // Se captura los datos del excel en cada posición.
                        string account = row[0].ToString().ToUpper().Trim();
                        string phone = row[1].ToString().ToUpper().Trim();
                        string type = row[2].ToString().ToUpper().Trim();
                        string assignedLogin = row[3].ToString().ToUpper().Trim();

                        // Se valida solo la primera fila del archivo de excel que es el encabezado.
                        if (row.Table.Rows.Count == 0)
                        {
                            string[] currentFormat = new string[]
                            {
                                     type
                            };

                            currentFormat = Array.ConvertAll(currentFormat, a => a.ToUpper());

                            if (!currentFormat.SequenceEqual(Array.ConvertAll(Constant.FORMAT, a => a.ToUpper())))
                            {
                                throw new Exception($"Formato incorrecto, el formato correcto es {string.Join(",", Constant.FORMAT)}");
                            }

                        }

                        // La posición cero es el encabezado y no se agrega al objeto.
                        if (i > 0)
                        {
                            SecondRingLoadDto secondRingLoad = new SecondRingLoadDto()
                            {
                                account = account,
                                phone = phone,
                                type = type,
                                assignedLogin = assignedLogin,
                                loadId = loadId,
                                loadName = loadName
                            };

                            // Se agrega el objeto a la lista.
                            secondRingLoads.Add(secondRingLoad);

                            // Se valida el tamaño de la lista.
                            if (secondRingLoads.Count() > 100000)// TODO: Definir variable.
                            {
                                throw new Exception($"El tamaño valido de registros para el archivo excel debe ser menor o igual a { 150 }");
                            }
                        }

                        // Se aumenta el contador en 1.
                        i++;
                    }
                }
            }

            return secondRingLoads;
        }

        private void SetFullPath(ref string fullPath, IFormFile file)
        {
            // Se define el nombre de la carpeta donde se guarda el archivo para validar.
            string nameFolder = "SecondRingLoad";

            // Se hace referencia a la carpeta wwwrooot que esta en la raiz del proyecto.
            string webRootPath = _env.WebRootPath;

            // Se crea la ruta.
            string newPath = Path.Combine(webRootPath, nameFolder);

            // Se valida si la ruta existe o si no se crea.
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            if (file.Length > 0)
            {
                // Se obtiene la extensión del archivo.
                string extensionFile = Path.GetExtension(file.FileName).ToLower();

                // Se valida la extensión (.xlsx | .xls)
                if (!extensionFile.ToLower().Contains(".xlsx") || !extensionFile.ToLower().Contains(".xls"))
                {
                    throw new Exception("Formato incorrecto");
                }

                var date = DateTime.Now.ToString("yyyy_MM_dd_HH_mm");

                // Se define la ruta de guardado del archivo.
                fullPath = Path.Combine(newPath, date + file.FileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
        }
        
        private string GenerateExcelException(List<SecondRingLoadDto> login)
        {
            var date = DateTime.Now.ToString("yyyy_MM_dd_HH_mm");
            var name = "SecondRingLoad_" + date + ".xlsx";

            string targetPath = @"Resources/PublicFiles/SecondRingLoad/" + name;
            FileInfo newFile = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), targetPath));

            string FileName = string.Empty;

            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), targetPath));
            }

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage(newFile);

            try
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Load");
                ws.Cells["A1"].Value = "Cuenta";
                ws.Cells["B1"].Value = "Telefono";
                ws.Cells["C1"].Value = "Tipo";
                ws.Cells["D1"].Value = "Asignación";

                int loop = 1;
                foreach (var item in login)
                {
                    loop++;
                    ws.Cells["A" + loop].Value = item.account == null ? "" : item.account;
                    ws.Cells["B" + loop].Value = item.phone == null ? "" : item.phone;
                    ws.Cells["C" + loop].Value = item.type == null ? "" : item.type;
                    ws.Cells["D" + loop].Value = item.assignedLogin == null ? "" : item.assignedLogin;
                }

                pck.Save();
                pck.Dispose();

                string path = Path.Combine(targetPath);

                return path;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<GetSecondRingViewModel>> GetSeconds(int without)
        {
            var list =  await _secondRingLoadRepository.Get()
                .ProjectTo<SecondRingLoadDto>(_mapper.ConfigurationProvider).ToListAsync();

            var total = from d in list
                        group d by new { d.CreatedByName, d.loadId, d.loadName, d.status } into data
                        select new GetSecondRingViewModel()
                        {
                            CreatedName = data.Key.CreatedByName,
                            LoadId = data.Key.loadId,
                            LoadName = data.Key.loadName,
                            Status = data.Key.status,
                            Count = data.Count()
                        };

            return total.ToList();
        }

        public async Task<bool> GetSecondsAssign(Guid id)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(300));

            var secondAssign = await _secondRingLoadRepository.Get()
                                .Where(x => x.loadId == id && x.status == false)
                                .ToListAsync() ?? throw new NotFoundException("La carga que esta intentando buscar no exitse.");

            if (secondAssign != null)
            {
                foreach (var item in secondAssign)
                {
                    var data = new CreateBackOfficeManegementViewModel()
                    {
                        TypeOperation = item.type,
                        Operation = "Base Manual",
                        UserId = item.assignedId,
                        SegmentId = 16,
                        StatusName = "Asignado",
                        Process = "manual-base",
                        SecondRingLoadId = item.Id,
                        Active = true,
                        ManagementId = 21762
                    };
                    var response = await createBackOfficeManagement(data);
                    if (response)
                    {
                        await updateSecondRing(item);
                    }
                }

                return true;
            }

            return false;
        }

        public async Task<bool> updateSecondRing(Domain.Models.SecondRingLoads.SecondRingLoad data)
        {
            data.status = true;
            return await _secondRingLoadRepository.Put(data);
        }

        public async Task<bool> createBackOfficeManagement(CreateBackOfficeManegementViewModel data)
        {
            return await _backOfficeManagementsService.PostManagementSecondRing(data);
        }
    }
}
