﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.location;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.Location
{
    public class StateService : IStateService
    {
        private readonly IStateRepository _stateRepository;
        private readonly IMapper _autoMapper;

        public StateService(IStateRepository stateRepository, IMapper autoMapper)
        {
            _stateRepository = stateRepository;
            _autoMapper = autoMapper;
        }

        public List<StateDto> GetStatesByCountry(Guid id)
        {
            return _stateRepository.Get()
                                   .Where(x => x.Status && x.CountryId == id)
                                   .ProjectTo<StateDto>(_autoMapper.ConfigurationProvider)
                                   .ToList();
        }
    }
}
