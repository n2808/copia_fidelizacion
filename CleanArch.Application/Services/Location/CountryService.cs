﻿using Application.Core.Exceptions;
using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Models.location;
using Domain.Interfaces.location;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.Location
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IMapper _mapper;

        public CountryService(ICountryRepository countryRepository, IMapper mapper, IDepartmentRepository departmentRepository, ICityRepository cityRepository)
        {
            _departmentRepository = departmentRepository;
            _countryRepository = countryRepository;
            _cityRepository = cityRepository;
            _mapper = mapper;
        }
        public async Task<List<CountryDto>> GetCountries()
        {
            return await _countryRepository.Get().Where(x => x.Status).ProjectTo<CountryDto>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<List<CountryDepartmentDto>> GetCountriesByCode(string code)
        {
            var stateDto = new List<CountryDepartmentDto>();
            var country = await _countryRepository.Get().Where(x => x.Code == code).FirstOrDefaultAsync() ?? throw new NotFoundException("El país no esta configurado.");
            var departments = await _departmentRepository.Get().Where(x => x.CountryId == country.Id).ToListAsync() ?? throw new NotFoundException("No se encuentran departamentos configurados a este país.");
            var cities = await _cityRepository.Get().ToListAsync();

            foreach (var item in departments)
            {
                stateDto.Add(new CountryDepartmentDto()
                {
                    Name = item.Name,
                    Code = item.Code,
                    Cities = cities.Where(y => y.DepartmentId == item.Id).Select(x => new City { Id = x.Id, Name = x.Name, DepartmentId = x.DepartmentId }).ToList()
                });
            }
            return stateDto;
        }

        public async Task<Country> GetCountriesById(Guid countryId)
        {
            return await _countryRepository.Get().Where(x => x.Id == countryId).FirstOrDefaultAsync() ?? throw new NotFoundException("El país que está intentando buscar no existe.");
        }

        public async Task<List<CountryDepartmentDto>> GetCountriesByName(string name)
        {
            var stateDto = new List<CountryDepartmentDto>();
            var country = await _countryRepository.Get().Where(x => x.Name == name).FirstOrDefaultAsync();
            var departments = await _departmentRepository.Get().Where(x => x.CountryId == country.Id).ToListAsync();
            var cities = await _cityRepository.Get().ToListAsync();

            foreach (var item in departments)
            {
                stateDto.Add(new CountryDepartmentDto()
                {
                    Name = item.Name,
                    Code = item.Code,
                    Cities = cities.Where(y => y.DepartmentId == item.Id).Select(x => new City { Id = x.Id, Name = x.Name, DepartmentId = x.DepartmentId }).ToList()
                });
            }
            return stateDto;
        }
    }
}
