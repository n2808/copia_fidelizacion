﻿using Application.Core.Exceptions;
using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.location;
using Domain.Models.location;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Location
{
    public class NewCityService : INewCityService
    {
        private readonly INewCityRepository _newCityRepository;
        private readonly IMapper _mapper;

        public NewCityService(INewCityRepository newCityRepository, IMapper mapper)
        {
            _newCityRepository = newCityRepository;
            _mapper = mapper;
        }
        public async Task<List<NewCityDto>> GetNewCity(int without)
        {
            return without == 1 ? await _newCityRepository.Get().ProjectTo<NewCityDto>(_mapper.ConfigurationProvider).ToListAsync()
                              : await _newCityRepository.Get().Where(x => x.Active).ProjectTo<NewCityDto>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<NewCity> GetNewCityById(int id)
        {
            return await _newCityRepository.Get().Where(x => x.Id == id).FirstOrDefaultAsync() ?? throw new NotFoundException("La ciudad que está intentando buscar no existe.");
        }
    }
}
