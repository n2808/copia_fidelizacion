﻿using Application.Core.Exceptions;
using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.location;
using Domain.Models.location;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Location
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IMapper _mapper;

        public DepartmentService(IDepartmentRepository departmentRepository, ICityRepository cityRepository, IMapper mapper)
        {
            _departmentRepository = departmentRepository;
            _cityRepository = cityRepository;
            _mapper = mapper;
        }
        public async Task<List<DepartmentDto>> GetDepartment(int without)
        {
            return  without == 1 ? await _departmentRepository.Get().ProjectTo<DepartmentDto>(_mapper.ConfigurationProvider).ToListAsync()
                              : await _departmentRepository.Get().Where(x => x.Active).ProjectTo<DepartmentDto>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<Department> GetDepartmentById(int id)
        {
            return await _departmentRepository.Get().Where(x => x.Id == id).FirstOrDefaultAsync() ?? throw new NotFoundException("El departamento que está intentando buscar no existe.");
        }

    }
}
