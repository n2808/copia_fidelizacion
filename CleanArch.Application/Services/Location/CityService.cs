﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.location;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.Location
{
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;
        private readonly IMapper _autoMapper;

        public CityService(ICityRepository cityRepository, IMapper autoMapper)
        {
            _cityRepository = cityRepository;
            _autoMapper = autoMapper;
        }

        public List<CityDto> GetCities()
        {
            return _cityRepository.Get()
                               .Where(x => x.Active == true)
                               .Include(x => x.Department)
                               .ProjectTo<CityDto>(_autoMapper.ConfigurationProvider)
                               .ToList();
        }

        public List<CityDto> GetCitiesByState(int departmentId)
        {
            return _cityRepository.Get()
                                  .Where(x => x.Active == true && x.DepartmentId == departmentId)
                                  .Include(x => x.Department)
                                  .ProjectTo<CityDto>(_autoMapper.ConfigurationProvider)
                                  .ToList();
        }
    }
}
