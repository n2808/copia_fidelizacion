﻿using Application.DTOs.User;
using Application.Interfaces.Rol;
using Application.Rol.Commands.PutRol;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.User;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.Rol
{
    public class RolService : IRolService
    {
        private readonly IRolRepository _rolRepository;
        private readonly IMapper _autoMapper;

        public RolService(
            IRolRepository rolRepository,
            IMapper autoMapper
        )
        {
            _rolRepository = rolRepository;
            _autoMapper = autoMapper;
        }

        public bool CheckById(Guid? Id)
        {
            return _rolRepository
                    .Get()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }

        public Domain.Models.Rol.Rol GetById(Guid Id)
        {
            return _rolRepository
                     .Get()
                     .Where(c => c.Id == Id)
                     .FirstOrDefault();
        }

        public List<RolDto> Get()
        {
            return _rolRepository
                    .Get()
                    .ProjectTo<RolDto>(_autoMapper.ConfigurationProvider)
                    .ToList();
        }

        public Domain.Models.Rol.Rol PostRol(Domain.Models.Rol.Rol rol)
        {
            return _rolRepository.Post(rol);
        }

        public Domain.Models.Rol.Rol PutRol(PutRolCommand request)
        {
            Domain.Models.Rol.Rol rol = GetById(request.Id);

            rol = _autoMapper.Map<PutRolCommand, Domain.Models.Rol.Rol>(request, rol);

            _rolRepository.Put(rol);

            return rol;
        }

        public bool DeleteRol(Guid id)
        {
            RolDto rolDto = _rolRepository.Get()
                                    .Where(x => x.Id == id)
                                    .ProjectTo<RolDto>(_autoMapper.ConfigurationProvider)
                                    .FirstOrDefault();

            Domain.Models.Rol.Rol rol = _autoMapper.Map<Domain.Models.Rol.Rol>(rolDto);

            return _rolRepository.Delete(rol);
        }

        public Domain.Models.Rol.Rol PutStatusActivateRolById(PutStatusActivateRolById request)
        {
            Domain.Models.Rol.Rol rol = GetById(request.Id);

            rol = _autoMapper.Map<PutStatusActivateRolById, Domain.Models.Rol.Rol>(request, rol);

            _rolRepository.PutStatusActivateRolById(rol);

            return rol;
        }

        public Domain.Models.Rol.Rol PutStatusDeactivateRolById(PutStatusDeactivateRolById request)
        {
            Domain.Models.Rol.Rol rol = GetById(request.Id);

            rol = _autoMapper.Map<PutStatusDeactivateRolById, Domain.Models.Rol.Rol>(request, rol);

            _rolRepository.PutStatusDeactivateRolById(rol);

            return rol;
        }
    }
}
