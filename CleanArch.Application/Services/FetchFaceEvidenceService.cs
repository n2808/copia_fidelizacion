﻿using Application.DTOs.FetchFaceEvidence;
using Application.FetchFaceEvidence.Queries;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services 
{
    public class FetchFaceEvidenceService : IFetchFaceEvidenceService
    {
        private readonly IMapper _mapper;
        private readonly IFetchFaceEvidenceRepository _fetchFaceEvidenceRepository;
        private readonly ICurrentUserService _currentUserService;
        public FetchFaceEvidenceService(IMapper mapper, IFetchFaceEvidenceRepository fetchFaceEvidenceRepository, ICurrentUserService currentUserService)
        {
            _mapper = mapper;
            _fetchFaceEvidenceRepository = fetchFaceEvidenceRepository;
            _currentUserService = currentUserService;
        }

        public async Task<FetchFaceEvidenceDto> FetchFaceEvidence(FetchFaceEvidenceCommand fetchFaceEvidenceCommand)
        {
            var fetchFaceEvidence = await _fetchFaceEvidenceRepository.GetById()
                                        .Where(x => x.Id == fetchFaceEvidenceCommand.FetchFaceId)
                                        .ProjectTo<FetchFaceEvidenceDto>(_mapper.ConfigurationProvider)
                                        .FirstOrDefaultAsync();
            if (fetchFaceEvidence.URL != null)
            {
                string targetPath = @"Resources/PublicFiles/Evidences";
                string[] fileName = fetchFaceEvidence.URL.Split("\\");
                File.Copy(fetchFaceEvidence.URL, targetPath + "/" + fileName[fileName.Count() - 1], true);
                var resourcesPath = targetPath + "/" + fileName[fileName.Count() - 1];
                fetchFaceEvidence.UrlPath = resourcesPath;
            }
            return fetchFaceEvidence;
        }

        public Task<Domain.Models.FetchFaceEvidences.FetchFaceEvidence> PostfetchFaceEvidence(Domain.Models.FetchFaceEvidences.FetchFaceEvidence fetchFaceEvidence)
        {
            fetchFaceEvidence.CreatedByName = _currentUserService.GetUserInfo().Name;
            fetchFaceEvidence.CreatedBy = _currentUserService.GetUserInfo().Id;
            return _fetchFaceEvidenceRepository.PostFetchFaceEvidence(fetchFaceEvidence);
        }
    }
}
