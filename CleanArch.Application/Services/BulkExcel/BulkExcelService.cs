﻿using Application.Common.Constants;
using Application.Common.Response;
using Application.DTOs.User;
using Application.Interfaces.BulkExcel;
using Application.Interfaces.User;
using AutoMapper;
using Domain.Models.User;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;


namespace Application.Services.BulkExcel
{
    public class BulkExcelService : IBulkExcelService
    {
        private readonly IHostingEnvironment _env;
        private readonly IUserService _userService;
        private readonly IUserRolService _userRolService;
        private readonly IMapper _mapper;

        public BulkExcelService(IHostingEnvironment env, IUserService userService, IUserRolService userRolService, IMapper mapper)
        {
            _env = env;
            _userService = userService;
            _userRolService = userRolService;
            _mapper = mapper;
        }

        /// <summary>
        /// Método que se encarga del almacenamiento de los usuarios obtenidos del excel.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="subCampaignId"></param>
        /// <returns></returns>
        public ApiResponse<IList<UserDto>> SaveFile(IFormFile file, int subCampaignId)
        {
            var response = new ApiResponse<IList<UserDto>>();
            var validateExcelUser = new List<UserDto>();
            string fullPath = string.Empty;

            try
            {

                if (file == null || file.Length == 0)
                {
                    throw new Exception("Debe seleccionar un archivo");
                }


                SetFullPath(ref fullPath, file);  

                IList<UserDto> userListDto = ReadFile(fullPath);

                foreach (var userDto in userListDto)
                {
                    // Validación usuarios.
                    var validate = ValidateExcel(userDto);
                    if (validate.Item2)
                    {
                        userDto.Result = validate.Item1;
                        validateExcelUser.Add(userDto);
                    }
                    else
                    {
                        userDto.SubCampaignId = subCampaignId;

                        // Validamos el usuario en base de datos para poderlo ingresar.
                        // (Devuelve true si ya existe).
                        bool checkByDocumentAndSubcampaign = _userService.CheckByDocumentAndSubcampaign(userDto.Document, userDto.SubCampaignId);
                        // (Devuelve false si ya existe).
                        bool existsLonginAndSubcampaign = _userService.GetByLogin(userDto.login, (int)userDto.SubCampaignId);

                        // Validamos si el número de documento existe para la subcampaña que ingresa.
                        if (checkByDocumentAndSubcampaign)
                        {
                            // Validamos si el login existe para la subcampaña que ingresa (Devuelve false si ya existe).
                            if (!existsLonginAndSubcampaign)
                            {
                                userDto.Result = "El usuario ya existe";
                                validateExcelUser.Add(userDto);
                            }
                            else
                            {
                                // Actualizamos el usuario.
                                if (EditUser(userDto))
                                {
                                    userDto.Result = "El usuario se editó correctamente";
                                    validateExcelUser.Add(userDto);
                                }
                                else
                                {
                                    userDto.Result = "El usuario no fué editado";
                                    validateExcelUser.Add(userDto);
                                }

                            }
                        }
                        else if (existsLonginAndSubcampaign)
                        {
                            if (CreateUser(userDto))
                            {
                                userDto.Result = "El usuario se creó correctamente";
                                validateExcelUser.Add(userDto);
                            }
                            else
                            {
                                userDto.Result = "El usuario no se insertó";
                                validateExcelUser.Add(userDto);
                            }
                        }
                        else
                        {
                            userDto.Result = "El login no puede estar duplicado para la misma Subcampaña";
                            validateExcelUser.Add(userDto);
                        }
                    }

                }

                // Generar excel de descarga.
                response.Url = GenerateDownloadExcel(validateExcelUser);

                //response.Data = validateExcelUser;
                response.Message = "Operación Exitosa";
                response.Result = true;
            }
            catch (Exception ex)
            {

                response.Message = ex.Message;
            }

            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
            return response;
        }

        private string GenerateDownloadExcel(List<UserDto> validateExcelUser)
        {
            

            FileInfo newFile = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), "Resources/Users/ExcelDownload/ActualizacionUsuarios.xlsx"));
          

            string FileName = string.Empty;

            if (newFile.Exists)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), "Resources/Users/ExcelDownload/ActualizacionUsuarios.xlsx"));
               
            }

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage(newFile);




            try
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");

                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#B7DEE8");
                ws.Cells["A1:H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A1:H1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                ws.Cells["A1"].Value = "Nombres";
                ws.Cells["B1"].Value = "Apellidos";
                ws.Cells["C1"].Value = "Documento";
                ws.Cells["D1"].Value = "Correo";
                ws.Cells["E1"].Value = "Teléfono";
                ws.Cells["F1"].Value = "Login";
                ws.Cells["G1"].Value = "Contraseña";
                ws.Cells["H1"].Value = "Resultado";
               

                int loop = 1;
                foreach (var item in validateExcelUser)
                {
                    loop++;
                    ws.Cells["A" + loop].Value = item.Names == null ? "" : item.Names;
                    ws.Cells["B" + loop].Value = item.LastName == null ? "" : item.LastName;
                    ws.Cells["C" + loop].Value = item.Document == null ? "" : item.Document;
                    ws.Cells["D" + loop].Value = item.Email == null ? "" : item.Email;
                    ws.Cells["E" + loop].Value = item.Phone1 == null ? "" : item.Phone1;
                    ws.Cells["F" + loop].Value = item.login == null ? "" : item.login;
                    ws.Cells["G" + loop].Value = item.PassWord == null ? "" : item.PassWord;
                    ws.Cells["H" + loop].Value = item.Result == null ? "" : item.Result;                    

                }

                ws.Cells["A:AZ"].AutoFitColumns();
                pck.Save();
                pck.Dispose();

                string path = Path.Combine(Directory.GetCurrentDirectory(), "Resources/Users/ExcelDownload/ActualizacionUsuarios.xlsx");

                return path;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        /// <summary>
        /// Método que se encarga de editar los usuarios del excel según la validación.
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        private bool EditUser(UserDto userDto)
        {

            UserDto userUpdate = _userService.GetUserByDocumentAndSubcampaign(userDto.Document, (int)userDto.SubCampaignId);

            //Actualizamos el usuario.
            userUpdate.login = userDto.login;
            var updateUser = _userService.PutUserExcel(userUpdate);

            // Si el usuario es actualizado, le modificamos el rol.
            if (updateUser != null)
            {
                // Buscamos todos los registros que le pertenezcan a ese usuario.
                List<UserRol> userRols = _userRolService.Get().Where(x => x.UserId == updateUser.Id).ToList();

                if (userRols.Count > 0)
                {
                    foreach (var userRol in userRols)
                    {
                        userRol.RolId = new Guid(userDto.RolId.ToString());
                        _userRolService.Put(userRol);
                    }
                }
                else
                {
                    // Si no tiene ningún rol asignado, le asignamos el rol.
                    var newUserRol = new Domain.Models.User.UserRol()
                    {
                        RolId = new Guid(userDto.RolId.ToString()),
                        UserId = userUpdate.Id
                    };
                    _userRolService.Post(newUserRol);
                }

                return true;
            }
            else
            {
                return false;
            }


        }

        /// <summary>
        /// Método que se encarga de crear los usuarios del excel según la validación.
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        private bool CreateUser(UserDto userDto)
        {
            try
            {
                Domain.Models.User.User user = new Domain.Models.User.User()
                {
                    Names = userDto.Names,
                    LastName = userDto.LastName,
                    Document = userDto.Document,
                    Email = userDto.Email,
                    Phone1 = userDto.Phone1,
                    login = userDto.login,
                    PassWord = userDto.PassWord,
                    SubCampaignId = userDto.SubCampaignId
                };

                // Enviamos el usuario para crearlo.
                var createdUser = _userService.PostUser(user);

                // Si el usuario es creado, le asignamos un rol.
                if (createdUser != null)
                {
                    //Insertar role
                    Domain.Models.User.UserRol userRol = new Domain.Models.User.UserRol()
                    {
                        RolId = new Guid(userDto.RolId),
                        UserId = createdUser.Id
                    };

                    _userRolService.Post(userRol);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }



        }

        /// <summary>
        /// Método que se encarga de validar cada una de las celdas del excel y devolver errores si los hay.
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public (string, bool) ValidateExcel(UserDto userDto)
        {
            IList<string> errors = new List<string>();

            errors.Add(string.IsNullOrEmpty(userDto.Names) ? "El nombre es requerido" : null);
            errors.Add(string.IsNullOrEmpty(userDto.Document) ? "El documento es requerido" : null);
            errors.Add(string.IsNullOrEmpty(userDto.login) ? "El login es requerido" : null);
            errors.Add(string.IsNullOrEmpty(userDto.PassWord) ? "La contraseña es requerida" : null);
            errors.Add(string.IsNullOrEmpty(userDto.RolId) ? "La contraseña es requerida" : null);

            string error = string.Join(", ", errors.GroupBy(x => x).Select(x => x.First()).Where(x => x != null).ToArray());
            return (error, !string.IsNullOrEmpty(error));
        }

        /// <summary>
        /// Método que se encarga de establecer la ruta para guardar el archivo seleccionado.
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="file"></param>
        public void SetFullPath(ref string fullPath, IFormFile file)
        {

            // Se define el nombre de la carpeta donde se guarda el archivo para validar.
            string nameFolder = "CargueMasivoUsuarios";

            // Se hace referencia a la carpeta wwwrooot que esta en la raiz del proyecto.
            string webRootPath = _env.WebRootPath;

            // Se crea la ruta.
            string newPath = Path.Combine(webRootPath, nameFolder);

            // Se valida si la ruta existe o si no se crea.
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            if (file.Length > 0)
            {
                // Se obtiene la extensión del archivo.
                string extensionFile = Path.GetExtension(file.FileName).ToLower();

                // Se valida la extensión (.xlsx | .xls)
                if (!extensionFile.ToLower().Contains(".xlsx") || !extensionFile.ToLower().Contains(".xls"))
                {
                    throw new Exception("Formato incorrecto");
                }

                // Se define la ruta de guardado del archivo.
                fullPath = Path.Combine(newPath, file.FileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }

        }

        /// <summary>
        /// Método privado que lee un archivo de excel en una ruta especifica y devuelve una lista de usuarios.
        /// </summary>
        /// <param name="rutaArchivoTemporal"></param>
        /// <returns>Una lista de candidatos básicos</returns>
        private IList<UserDto> ReadFile(string temporalUrl)
        {

            // Se inicializa la lista de equipos a validar.
            IList<UserDto> userList = new List<UserDto>();

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            // Se crea al acceso al archivo.
            using (var stream = File.Open(temporalUrl, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet();


                    // Se toma la informacion del archivo de excel.
                    DataTable table = result.Tables[0];

                    // Contador que recorre las posiciones de la lista.
                    int i = 0;
                    // Se recorren los registros.
                    foreach (DataRow row in table.Rows)
                    {
                        var item = row.Table.Rows.Count;
                        // Se captura los datos del excel en cada posición.
                        string name = row[0].ToString().ToUpper().Trim();
                        string lastname = row[1].ToString().ToUpper().Trim();
                        string document = row[2].ToString().ToUpper().Trim();
                        string email = row[3].ToString().ToUpper().Trim();
                        string phone = row[4].ToString().ToUpper().Trim();
                        string login = row[5].ToString().ToUpper().Trim();
                        string password = row[6].ToString().ToUpper().Trim();
                        string rolId = row[8].ToString().ToUpper().Trim();


                        // Se valida solo la primera fila del archivo de excel que es el encabezado.
                        if (row.Table.Rows.Count == 0)
                        {

                            string[] currentFormat = new string[]
                            {
                                     name,lastname
                            };

                            currentFormat = Array.ConvertAll(currentFormat, a => a.ToUpper());

                            if (!currentFormat.SequenceEqual(Array.ConvertAll(Constant.FORMAT, a => a.ToUpper())))
                            {
                                throw new Exception($"Formato incorrecto, el formato correcto es {string.Join(",", Constant.FORMAT)}");
                            }

                        }

                        // La posición cero es el encabezado y no se agrega al objeto.
                        if (i > 0)
                        {

                            UserDto user = new UserDto()
                            {
                                Names = name,
                                LastName = lastname,
                                Document = document,
                                Email = email,
                                Phone1 = phone,
                                login = login,
                                PassWord = password,
                                RolId = rolId
                            };

                            // Se agrega el objeto a la lista.
                            userList.Add(user);

                            // Se valida el tamaño de la lista.
                            if (userList.Count() > 150)// TODO: Definir variable.
                            {
                                throw new Exception($"El tamaño valido de registros para el archivo excel debe ser menor o igual a { 150 }");
                            }
                        }

                        // Se aumenta el contador en 1.
                        i++;

                    }

                }
            }

            // Listado de usuarios sin repetir.
            IList<UserDto> listUserWithoutRepeat = userList.GroupBy(x => x.login).Select(x => x.First()).ToList();
            return userList;

        }
    }
}