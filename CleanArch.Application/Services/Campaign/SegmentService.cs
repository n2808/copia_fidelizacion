﻿
using Application.Core.Exceptions;
using Application.Courses.Commands;
using Application.DTOs;
using Application.DTOs.Segment;
using Application.Segments.Command;
using Application.Segments.Command.Put;
using Application.Segments.Queries;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Core.Bus;
using CleanArch.Domain.Interfaces;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Campaigns;
using Domain.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class SegmentService : ISegmentService
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly ISegmentRepository _segmentRepository;
        private readonly IMapper _mapper;

        public SegmentService(ICurrentUserService currentUserService, ISegmentRepository segmentRepository, IMapper mapper)
        {
            _currentUserService = currentUserService;
            _segmentRepository = segmentRepository;
            _mapper = mapper;
        }

        public bool checkById(int Id)
        {
            return _segmentRepository
                    .Get()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }

        public async Task<List<SegmentDto>> GetSegment(int without)
        {
            return without == 1 ? _segmentRepository.Get().ProjectTo<SegmentDto>(_mapper.ConfigurationProvider).ToList()
                               : _segmentRepository.Get().Where(x => x.Status).ProjectTo<SegmentDto>(_mapper.ConfigurationProvider).ToList();
        }

        public async Task<Segment> GetSegmentById(GetSegmentByIdQuery request)
        {
            Segment segment = await GetByIdSegment(request.Id);
            segment = _mapper.Map<GetSegmentByIdQuery, Segment>(request, segment);
            return await _segmentRepository.GetSegmentById(segment);
        }

        public async Task<Segment> GetByIdSegment(int Id)
        {
            return _segmentRepository.Get().Where(c => c.Id == Id).FirstOrDefault() ?? throw new NotFoundException("El segmento que está intentando buscar no existe.");
        }

        public async Task<List<SegmentDto>> GetSegmentByIdCampaing(int id)
        {
            return _segmentRepository.Get().Where(x => x.SubCampaignId == id).ProjectTo<SegmentDto>(_mapper.ConfigurationProvider).ToList();
        }

        public async Task<Segment> PostSegment(Segment segment)
        {
            segment.CreatedByName = _currentUserService.GetUserInfo().Name;
            segment.CreatedBy = _currentUserService.GetUserInfo().Id;
            return await _segmentRepository.Post(segment);
        }

        public async Task<Segment> PutSegment(PutSegmentCommand request)
        {
            Segment segment = await GetByIdSegment(request.Id);
            segment = _mapper.Map<PutSegmentCommand, Segment>(request, segment);
            //segment.UpdatedByName = _currentUserService.GetUserInfo().Name;
            segment.UpdatedBy = _currentUserService.GetUserInfo().Id;
            return await _segmentRepository.Put(segment);
        }

        public async Task<Segment> PutStatusActivateSegmentById(PutStatusActivateSegmentByIdCommand request)
        {
            Segment segment = await GetByIdSegment(request.Id);
            segment = _mapper.Map<PutStatusActivateSegmentByIdCommand, Segment>(request, segment);
            //segment.UpdatedByName = _currentUserService.GetUserInfo().Name;
            segment.UpdatedBy = _currentUserService.GetUserInfo().Id;
            return await _segmentRepository.PutStatusActivateSegmentById(segment);
        }

        public async Task<Segment> PutStatusDeActivateSegmentById(PutStatusDeActivateSegmentByIdCommand request)
        {
            Segment segment = await GetByIdSegment(request.Id);
            segment = _mapper.Map<PutStatusDeActivateSegmentByIdCommand, Segment>(request, segment);
            //segment.UpdatedByName = _currentUserService.GetUserInfo().Name;
            segment.UpdatedBy = _currentUserService.GetUserInfo().Id;
            return await _segmentRepository.PutStatusDeActivateSegmentById(segment);
        }

        public async Task<bool> DeleteSegment(int id)
        {
            Segment segment = await GetByIdSegment(id);
            return await _segmentRepository.Delete(segment);
        }

        
    }
}
