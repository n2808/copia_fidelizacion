﻿using Application.DTOs.Campaign;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using Domain.Interfaces.Campaigns;
using Domain.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class SubCampaignService : ISubCampaignService
    {
        private readonly ISubCampaignRepository _subCampaignRepository;
        private readonly IMapper _autoMapper;

        public SubCampaignService(
            ISubCampaignRepository subCampaignRepository, 
            IMapper autoMapper
         )
        {
            _subCampaignRepository = subCampaignRepository;
            _autoMapper = autoMapper;
        }


        public bool checkById(int Id)
        {
            return _subCampaignRepository
                    .Get()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }

        public async Task<string> GetNameSubcampaigns(int? Id)
        {
            return await _subCampaignRepository.GetById(Id);
        }

        public async Task<List<SubCampaignDto>>  GetSubcampaigns()
        {
            return await _subCampaignRepository
                    .Get()
                    .Include(x => x.Campaign)
                    .Where(x => x.Status == true)
                    .ProjectTo<SubCampaignDto>(_autoMapper.ConfigurationProvider)
                    .ToListAsync();
        }

    }
}
