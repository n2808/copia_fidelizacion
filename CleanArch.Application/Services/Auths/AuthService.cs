﻿
using Application.Auth.Commands;
using Application.Common.Auth;
using Application.Common.Exceptions;
using Application.Core.Exceptions;
using Application.DTOs.User;
using Application.ViewModel.Auth;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.Interfaces.Auths;
using CleanArch.Domain.Interfaces;
using Domain.Interfaces;
using Domain.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArch.Application.Services.Auths
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository _authRepository;
        private readonly IMapper _autoMapper;
        private readonly ISubCampaignService _subCampaignService;
        private readonly IServiceDialerRepository _serviceDialerRepository;

        public AuthService(
            IMapper autoMapper,
            IAuthRepository authRepository,
            ISubCampaignService subCampaignService,
            IServiceDialerRepository serviceDialerRepository
        )
        {
            _authRepository = authRepository;
            _autoMapper = autoMapper;
            _subCampaignService = subCampaignService;
            _serviceDialerRepository = serviceDialerRepository;
        }

        public UserDto GetUserByLogin(string login)
        {
            return _authRepository
                    .GetUsers()
                    .Where(c => c.login == login && c.Status == true)
                    .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                    .FirstOrDefault();
        }

        public UserDto GetUserById(Guid? Id)
        {
            return _authRepository
                    .GetUsers()
                    .Where(c => c.Id == Id)
                    .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                    .FirstOrDefault();
        }

        public AuthViewModel GetAuth(PostLoginCommand auth)
        {
            var userDto = _authRepository
                    .GetUsers()
                    .Where(x =>
                        x.login == auth.Login &&
                        x.PassWord == auth.Password &&
                        x.SubCampaignId == auth.SubCampaignId
                        )
                    .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                    .FirstOrDefault();

            if (userDto == null)
                throw new UnAuthorizeException();

            if(userDto.Active == false)
            {
                throw new UnAuthorizeActiveException();
            }
            return new AuthViewModel()
            {
                user = userDto,
                token = GetAuthToken(userDto)
            };
        }

        public async Task<AuthInBoundViewModel> GetAuthInbound(PostLoginInboundCommand auth)
        {
            var userDto = _authRepository
                    .GetUsers()
                    .Where(x =>
                        x.login == auth.Login &&
                        //x.PassWord == auth.Password &&
                        x.Status == true
                        )
                    .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                    .ToList();

            if (userDto.Count == 0 || userDto[0].PassWord != auth.Password) //Validate login and password
            {
                return new AuthInBoundViewModel()
                {
                    message = "Usuario/Contraseña Incorrecto",
                    code = "401",
                    status = false
                };
            }

            if (userDto.Count() > 1) // Validate user existent in others subcampaing and active
            {
                List<string> campaings = new List<string>();
                for (int i = 0; i < userDto.Count; i++)
                {
                    //Lammar función que busque el nombre de las subcampaings
                    string dato = await GetNameSubCampaing(userDto[i].SubCampaignId);
                    campaings.Add(dato);
                }
                return new AuthInBoundViewModel()
                {
                    campaings = campaings,
                    message = "El login está activo en varias campañas",
                    code = "401",
                    status = false
                };

            }

            return new AuthInBoundViewModel()
            {
                user = userDto[0],
                token = GetAuthToken(userDto[0]),
                status = true
            };
        }

        public async Task<string> GetNameSubCampaing(int? id)
        {
            var campaing = await _subCampaignService.GetNameSubcampaigns(id);
            return campaing;
        }

        public UserDto GetDataUSerCampaing(Guid? Id)
        {
            return _authRepository
                    .GetUsers()
                    .Where(c => c.Id == Id)
                    .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                    .FirstOrDefault();
        }

        public string GetAuthToken(UserDto user)
        {
            AuthToken auth = new AuthToken() { };
            return auth.GenerateToken(user);
        }

        public async Task<AuthOutBoundViewModel> GetAuthOutBound(PostLoginOutboundCommand auth)
        {
            var userDto = await _authRepository
                    .GetUsers()
                    .Where(x =>
                        x.login == auth.Login &&
                        x.Status == true &&
                        x.SubCampaignId != null
                        )
                    .ProjectTo<UserDto>(_autoMapper.ConfigurationProvider)
                    .ToListAsync();

            var servicesDialer = await _serviceDialerRepository.Get().Where(x => x.VDN == auth.ServiceDialer && x.status == true).FirstOrDefaultAsync();
            int? subCampaignServiceDialer = servicesDialer.SubCampaignId;

            if (userDto.Count == 0) //Validate login and password
            {
                return new AuthOutBoundViewModel()
                {
                    message = "Usuario Incorrecto",
                    code = "401",
                    status = false
                };
            }

            if (userDto.Count() > 1) // Validate user existent in others subcampaing and active
            {
                List<string> campaings = new List<string>();
                for (int i = 0; i < userDto.Count; i++)
                {
                    //Lammar función que busque el nombre de las subcampaings
                    string dato = await GetNameSubCampaing(userDto[i].SubCampaignId);
                    campaings.Add(dato);
                }
                return new AuthOutBoundViewModel()
                {
                    campaings = campaings,
                    message = "El login está activo en varias campañas",
                    code = "401",
                    status = false
                };
            }

            int? subCampaignUser = 0;
            for (int i = 0; i < userDto.Count; i++)
            {
                subCampaignUser = userDto[i].SubCampaignId;
            }

            if(subCampaignUser != subCampaignServiceDialer)
            {
                return new AuthOutBoundViewModel()
                {
                    message = "El Usuario ingresado no pertenece a esta campaña.",
                    code = "401",
                    status = false
                };
            }

            return new AuthOutBoundViewModel()
            {
                user = userDto[0],
                token = GetAuthToken(userDto[0]),
                status = true
            };
        }
    }
}
