﻿
using Application.Courses.Commands;
using Application.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Core.Bus;
using CleanArch.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Application.Services
{
    public class AppNotificationService : IAppNotificationService
    {
        private readonly IAppNotificationRepository _appNotificationRepository;
        private readonly IMapper _autoMapper;

        public AppNotificationService(IAppNotificationRepository appNotificationRepository, IMapper autoMapper)
        {
            _appNotificationRepository = appNotificationRepository;
            _autoMapper = autoMapper;
        }

        public List<AppNotificationDto> GetSMSNotification()
        {
            return _appNotificationRepository.GetApps()
                    .Where(c => c.status == true)
                    .ProjectTo<AppNotificationDto>(_autoMapper.ConfigurationProvider)
                    .ToList();
        }




        public bool CheckAppNotificationById(int Id)
        {
            return _appNotificationRepository
                    .GetApps()
                    .Where(c => c.Id == Id)
                    .Count() == 0 ? false : true;
        }


        public AppNotificationDto GetById(int Id)
        {
            return _appNotificationRepository
                    .GetApps()
                    .Where(c => c.Id == Id)
                    .ProjectTo<AppNotificationDto>(_autoMapper.ConfigurationProvider)
                    .FirstOrDefault();

        }

    }
}
