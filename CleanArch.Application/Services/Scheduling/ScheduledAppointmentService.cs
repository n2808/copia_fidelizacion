﻿using Application.Core.Exceptions;
using Application.DTOs.Scheduling;
using Application.Interfaces.Scheduling;
using Application.Scheduling.Commands.PutScheduled;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.Scheduling;
using Domain.Models.Scheduling;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Services.Scheduling
{
    public class ScheduledAppointmentService : IScheduledAppointmentService
    {
        private readonly IScheduledAppointmentRepository _scheduledAppointment;
        private readonly IMapper _autoMapper;

        public ScheduledAppointmentService(IScheduledAppointmentRepository scheduledAppointment, IMapper autoMapper)
        {
            _scheduledAppointment = scheduledAppointment;
            _autoMapper = autoMapper;
        }
        public List<ScheduledAppointmentDto> GetScheduledAppointment()
        {
            return _scheduledAppointment.Get()
                                        .ProjectTo<ScheduledAppointmentDto>(_autoMapper.ConfigurationProvider)
                                        .ToList();
        }

        public ScheduledAppointment PostScheduledAppointment(ScheduledAppointment scheduledAppointment)
        {
            scheduledAppointment.Active = false;
            return _scheduledAppointment.Post(scheduledAppointment);
        }

        public ScheduledAppointment PutScheduledAppointment(PutScheduledAppointmentCommand request)
        {

            ScheduledAppointment scheduledAppointment = GetScheduledAppointmentById(request.Id);
            
            scheduledAppointment =  _autoMapper.Map<PutScheduledAppointmentCommand, ScheduledAppointment>(request, scheduledAppointment);         

            _scheduledAppointment.Put(scheduledAppointment);

            return scheduledAppointment;

        }

        public bool DeleteScheduledAppointment(Guid id)
        {
            ScheduledAppointmentDto scheduledAppointmentDto = _scheduledAppointment.Get()
                                                               .Where(x => x.Id == id)
                                                               .ProjectTo<ScheduledAppointmentDto>(_autoMapper.ConfigurationProvider)
                                                               .FirstOrDefault();

            if (scheduledAppointmentDto == null)
            {                
                throw new NotFoundException("ScheduledAppointment", "Id");
            }

            ScheduledAppointment scheduledAppointment = _autoMapper.Map<ScheduledAppointment>(scheduledAppointmentDto);

            return _scheduledAppointment.Delete(scheduledAppointment);

        }
            
        public ScheduledAppointment GetScheduledAppointmentById(Guid id)
        {
            return _scheduledAppointment
                    .Get()
                    .Where(x => x.Id == id)
                    .FirstOrDefault();
        }


    }
}
