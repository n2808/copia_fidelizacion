﻿
using Application.Courses.Commands;
using Application.DTOs;
using Application.ServicseDialer.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using CleanArch.Domain.Core.Bus;
using CleanArch.Domain.Interfaces;
using Domain.Models.Operation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CleanArch.Application.Services
{
    public class ServiceDialerService : IServiceDialerService
    {
        private readonly IServiceDialerRepository _serviceDialerRepository;
        private readonly IMapper _autoMapper;

        public ServiceDialerService(IServiceDialerRepository serviceDialerRepository, IMapper autoMapper)
        {
            _serviceDialerRepository = serviceDialerRepository;
            _autoMapper = autoMapper;
        }

        public CampaignDto GetCampaignByServiceCode(string Code)
        {
            return _serviceDialerRepository
                    .Get()
                    .Where(sd => sd.VDN == Code)
                    .Include(sd => sd.Segment)
                    .Include(sd => sd.Segment.SubCampaign)
                    .Include(sd => sd.Campaign)
                    .ProjectTo<CampaignDto>(_autoMapper.ConfigurationProvider)
                    .FirstOrDefault();
        }

        public List<ServiceDialerDto> GetServiceDialerBySubCampaignId(int Id)
        {

            return _serviceDialerRepository
                    .Get()
                    .Where(x => x.SubCampaignId == Id)
                    .Include(sd => sd.Segment)
                    .Include(sd => sd.Segment.SubCampaign)
                    .Include(sd => sd.Campaign)
                    .ProjectTo<ServiceDialerDto>(_autoMapper.ConfigurationProvider)
                    .ToList();
        }

      
    }
}
