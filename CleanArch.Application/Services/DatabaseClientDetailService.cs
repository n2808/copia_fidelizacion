﻿using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArch.Domain.Interfaces;
using Core.Models.Operation;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class DatabaseClientDetailService : IDatabaseClientDetailService
    {
        private readonly IDatabaseClientDetailRepository _IDatabaseClientDetailRepository;
        private readonly IMapper _autoMapper;

        public DatabaseClientDetailService(IServiceDialerRepository serviceDialerRepository, IMapper autoMapper , IDatabaseClientDetailRepository databaseClientDetailRepository)
        {
            _IDatabaseClientDetailRepository = databaseClientDetailRepository;
            _autoMapper = autoMapper;
        }

        public async Task<DatabaseClientDetail> GetById(int Id)
        {

            return  await _IDatabaseClientDetailRepository
                     .GetClient(Id)
                     .FirstOrDefaultAsync();
        }

        public async Task<DatabaseClientDetail> GetByPhone(string phone)
        {

            return await _IDatabaseClientDetailRepository.GetClient()
                    .Where(x => x.DialerPhone1 == phone
                        || x.DialerPhone2 == phone
                        || x.DialerPhone3 == phone
                        || x.DialerPhone4 == phone
                        || x.DialerPhone5 == phone
                        || x.DialerPhone6 == phone
                        || x.DialerPhone7 == phone
                    )
                    .FirstOrDefaultAsync();
        }





     
    }
}
