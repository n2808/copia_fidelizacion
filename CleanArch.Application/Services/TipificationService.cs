﻿using Application.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArch.Domain.Interfaces;
using Domain.Models.Tipifications;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class TipificationService : ITipificationService
    {
        private readonly ITipificationRepository _tipificationRepository;
        private readonly IMapper _autoMapper;

        public TipificationService(ITipificationRepository tipificationRepository, IMapper autoMapper)
        {
            _tipificationRepository = tipificationRepository;
            _autoMapper = autoMapper;
        }

        public List<TipificationDto> GetTipificationsBySubcampaign(int Id)
        {
            return _tipificationRepository.GetTipifications()
                    .Where(c => c.status == true && c.SubCampaignId == Id)
                    .ProjectTo<TipificationDto>(_autoMapper.ConfigurationProvider)
                    .ToList();
        }

        public List<TipificationDto> GetTipificationsByDialerService(string name)
        {
            return _tipificationRepository
                    .GetTipifications()
                    .Where(c => c.status == true && c.servicesDialerTipifications.Any(j => j.ServiceDialerName == name))
                    .ProjectTo<TipificationDto>(_autoMapper.ConfigurationProvider)
                    .ToList();
        }

        public async Task<Tipification> GetTipificationById(int Id)
        {
            return  await _tipificationRepository
                    .GetTipifications()
                    .Where(c => c.status == true && c.Id == Id)
                    .FirstOrDefaultAsync();

        }
        public List<TipificationDto> GetTipificationsByName(string name, int? id)
        {
            return _tipificationRepository.GetTipificationsByName()
                .Where(x => x.Name.Contains(name) && x.SubCampaignId == id)
                .ProjectTo<TipificationDto>(_autoMapper.ConfigurationProvider)
                .ToList();
        }
    }
}
