﻿using Application.Core.Exceptions;
using Application.DTOs.Group;
using Application.Group.Command;
using Application.Interfaces.Group;
using Application.Interfaces.User;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Group;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Services.Group
{
    public class GroupService : IGroupService
    {
        private readonly IGroupRepository _groupRepository;
        private readonly ICurrentUserService _currentUserService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public GroupService(IGroupRepository groupRepository, IMapper mapper, ICurrentUserService currentUserService, IUserService userService)
        {
            _groupRepository = groupRepository;
            _currentUserService = currentUserService;
            _userService = userService;
            _mapper = mapper;
        }
        public List<GroupDto> GetGroups(int without)
        {
            var source = _groupRepository.Get();

            return without == 1 ? source.ProjectTo<GroupDto>(_mapper.ConfigurationProvider).ToList()
                                : source.Where(x => x.Status).ProjectTo<GroupDto>(_mapper.ConfigurationProvider).ToList();
         
        }

        private Domain.Models.Groups.Group GetGroupById(Guid id)
        {
            return _groupRepository
                  .Get()
                  .Where(x => x.Id == id)
                  .FirstOrDefault();
        }

        public bool ActivateGroup(Guid id)
        {
            GroupDto groupDto = _groupRepository.Get()
                                             .Where(x => x.Id == id)
                                             .ProjectTo<GroupDto>(_mapper.ConfigurationProvider)
                                             .FirstOrDefault();
            if (groupDto == null)
            {
                throw new NotFoundException("GroupDto", "Id");
            }

            Domain.Models.Groups.Group group = _mapper.Map<Domain.Models.Groups.Group>(groupDto);
            group.UserUpdate = GetNameCurrentUser();

            return _groupRepository.Activate(group);
        }

        public bool DeactivateGroup(Guid id)
        {
            GroupDto groupDto = _groupRepository.Get()
                                             .Where(x => x.Id == id)
                                             .ProjectTo<GroupDto>(_mapper.ConfigurationProvider)
                                             .FirstOrDefault();
            if (groupDto == null)
            {
                throw new NotFoundException("GroupDto", "Id");
            }

            Domain.Models.Groups.Group group = _mapper.Map<Domain.Models.Groups.Group>(groupDto);
            group.UserUpdate = GetNameCurrentUser();

            return _groupRepository.Deactivate(group);
        }

        public bool DeleteGroup(Guid id)
        {
            GroupDto groupDto = _groupRepository.Get()
                                                .Where(x => x.Id == id)
                                                .ProjectTo<GroupDto>(_mapper.ConfigurationProvider)
                                                .FirstOrDefault();

            if(groupDto == null)
            {
                throw new NotFoundException("groupDto", "Id");
            }

            Domain.Models.Groups.Group group = _mapper.Map<Domain.Models.Groups.Group>(groupDto);

            return _groupRepository.Delete(group);
        }

        public Domain.Models.Groups.Group PostGroup(Domain.Models.Groups.Group group)
        {
            group.UserUpdate = GetNameCurrentUser();
            return _groupRepository.Post(group);
        }

        private string GetNameCurrentUser()
        {
            Guid idCurrentUser = (Guid)_currentUserService.GetUserInfo().Id;
            string name = _userService.GetById(idCurrentUser).Names;
            string lastName = _userService.GetById(idCurrentUser).LastName == null ? "" : _userService.GetById(idCurrentUser).LastName;
            string fullName = name + " " + lastName;
            return fullName;
        }

        public Domain.Models.Groups.Group PutGroup(PutGroupCommand putGroupCommand)
        {
            Domain.Models.Groups.Group group = GetGroupById(putGroupCommand.Id);

            if (group == null)
            {
                throw new NotFoundException("Grupo", "Id");
            }

            group = _mapper.Map<PutGroupCommand, Domain.Models.Groups.Group>(putGroupCommand, group);
            group.UserUpdate = GetNameCurrentUser();
            _groupRepository.Put(group);

            return group;
        }
    }
}
