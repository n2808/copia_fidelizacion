﻿using Application.Core.Exceptions;
using Application.DTOs.GestionReports;
using Application.Interfaces;
using Application.Interfaces.User;
using Application.ViewModel;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArch.Application.Interfaces;
using CleanArchitecture.Application.Common.Interfaces;
using Core.Models.Operation;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace CleanArch.Application.Services
{
    public class GestionReportService : IGestionReportService
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _autoMapper;
        private readonly IGestionStructureService _gestionStructureService;
        private readonly IGestionReportRepository _gestionReportRepository;
        private readonly IUserService _userService;
        private readonly ITipificationService _tipificationService;

        public GestionReportService(
            IGestionStructureService gestionStructureService,
            IGestionReportRepository gestionReportRepository,
            IMapper autoMapper,
            ICurrentUserService currentUserService,
            IUserService userService,
            ITipificationService tipificationService
         )
        {
            _autoMapper = autoMapper;
            _gestionStructureService = gestionStructureService;
            _gestionReportRepository = gestionReportRepository;
            _currentUserService = currentUserService;           
            _userService = userService;
            _tipificationService = tipificationService;
        }

        public bool AllowEndGestion(int Id)
        {
            return _gestionReportRepository
                    .GetGestionReports()
                    .Where(c => c.Id == Id)
                    .Count() != 0 ? false : true;
        }

        public List<GestionReportsDto> GetGestionReportsSegmentId(int segmentId, WhereViewModel whereViewModel)
        {
            var gestionStructure = _gestionStructureService.GetGestionStructureBySegmentId(segmentId)
                                             .Where(c => c.FrontendField == whereViewModel.field).FirstOrDefault();

            if (gestionStructure == null)
            {
                throw new NotFoundException("GestionStructure", "La columna " + whereViewModel.field + " no se encuentra mapeada");
            }

            var gestionReports = _gestionReportRepository.GetGestionReportsSegmentId(segmentId, whereViewModel.field, whereViewModel.value);

            if (gestionReports.ToList().Count() == 0)
            {
                throw new NotFoundException("Gestion", "No se encontraron gestiones retenidas");
            }

            return _autoMapper.Map<List<GestionReportsDto>>(gestionReports);
        }

        public async Task<GestionReports> GetGestionById(int Id, CancellationToken cancellationToken)
        {
            return await _gestionReportRepository
                    .GetGestionReports()
                    .Where(c => c.Id == Id)
                    .SingleOrDefaultAsync(cancellationToken);
        }


        public List<GestionReportsDto> GetGestionTipification(string tipificationName)
        {
            //Obtener el id de la subcampaña
            Guid idCurrentUser = (Guid)_currentUserService.GetUserInfo().Id;
            var id = _userService.GetById(idCurrentUser).SubCampaignId;
            var tipification = _tipificationService.GetTipificationsByName(tipificationName, id).Select(x => x.Id).ToList();

            var query = _gestionReportRepository.GetGestionReports()
                        .Where(x => tipification.Contains(x.tipificacionId ?? 0))
                        .ToList();

            return _autoMapper.Map<List<GestionReports>, List<GestionReportsDto>>(query);
        }

        public async Task<GestionReports> UpdateGestion(GestionReports gestion, CancellationToken cancellationToken)
        {
            return await _gestionReportRepository
                        .Put(gestion);
        }

        public async Task<List<GestionReportsDto>> GetGestionReportsPhone(string phone)
        {
          return await _gestionReportRepository.Get()
                .Where(x => x.Phone == phone)
                .OrderByDescending(x => x.CreatedAt)
                .ProjectTo<GestionReportsDto>(_autoMapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<GestionReportsDto>> GetGestionReportsAccount(string account)
        {
            return await _gestionReportRepository.Get()
               .Where(x => x.Account == account)
               .OrderByDescending(x => x.CreatedAt)
               .ProjectTo<GestionReportsDto>(_autoMapper.ConfigurationProvider)
               .ToListAsync();
        }

        public async Task<List<GestionReportsDto>> GetGestionReportsIdentification(string identification)
        {
            return await _gestionReportRepository.Get()
               .Where(x => x.Identification == identification)
               .OrderByDescending(x => x.CreatedAt)
               .ProjectTo<GestionReportsDto>(_autoMapper.ConfigurationProvider)
               .ToListAsync();
        }
    }
}

