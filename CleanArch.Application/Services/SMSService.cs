﻿using Application.Core.Exceptions;
using CleanArch.Application.Interfaces;
using CleanArch.Application.Interfaces.Notifications;
using Core.Enums;
using Core.Events;
using Core.Events.SMS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class SMSService: ISMSService
    {
        private readonly INotificationAccountService _notificationAccountService;

        private readonly INotificationService _notificationService;



        public SMSService( 
            INotificationAccountService notificationAccountService,

            INotificationService notificationService

         )
        {
            _notificationAccountService = notificationAccountService;

            _notificationService = notificationService;

        }

        
        public async Task<NotificationAccountViewModel> Send(string phone, string message, int AccountId)
        {

            var account = _notificationAccountService.GetAccountById(AccountId) ?? throw new BadRequestException("Cuenta no configurada");
            try
            {

                account.phone = phone;
                account.Message1 = message;


                var notifications = new List<INotification>();

                switch (account.TypeNotification)
                {
                    case ((int)NotificationEnum.Cocom):
                        notifications.Add(new SMSNotificationCocom(account));
                    break;
                    case ((int)NotificationEnum.masiveApp):
                        notifications.Add(new SMSNotificationMaSivApp(account));
                    break;
                }


                await _notificationService.Send(notifications);
                return account;
            }
            catch (Exception Ex)
            {

                throw new BadRequestException(Ex.Message);
            }


        }








    }
}
