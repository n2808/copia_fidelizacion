﻿using Application.Backoffice.Commands.PutBackoffice;
using Application.Core.Exceptions;
using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using Application.Interfaces.User;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CleanArchitecture.Application.Common.Interfaces;
using Domain.Interfaces.Backoffice;
using Domain.Models.Backoffice;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Backoffice
{
    public class BackOfficeOperationsService : IBackOfficeOperationsService
    {
        private readonly IBackOfficeOperationsRepository _backOfficeOperationsRepository;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        private readonly IUserService _userService;

        public BackOfficeOperationsService(IBackOfficeOperationsRepository backOfficeOperationsRepository, IMapper mapper, ICurrentUserService currentUserService, IUserService userService)
        {
            _backOfficeOperationsRepository = backOfficeOperationsRepository;
            _mapper = mapper;
            _currentUserService = currentUserService;
            _userService = userService;
        }

        public async Task<BackOfficeOperations> GetBackOfficeOperationById(int id)
        {
            return await _backOfficeOperationsRepository.Get().Where(x => x.Id == id).FirstOrDefaultAsync() ?? throw new NotFoundException("La operación del back-office que está intentando buscar no existe.");
        }

        public async Task<List<BackOfficeOperationDto>> GetBackOfficeOperations(int without)
        {
            return without == 1 ? await _backOfficeOperationsRepository.Get().ProjectTo<BackOfficeOperationDto>(_mapper.ConfigurationProvider).ToListAsync()
                                : await _backOfficeOperationsRepository.Get().Where(x => x.Active).ProjectTo<BackOfficeOperationDto>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<BackOfficeOperations> PostBackOfficeOperation(BackOfficeOperations backOfficeOperations)
        {
            backOfficeOperations.CreatedByName = _currentUserService.GetUserInfo().Name;
            backOfficeOperations.CreatedBy = _currentUserService.GetUserInfo().Id;
            return await _backOfficeOperationsRepository.Post(backOfficeOperations);
        }

        public async Task<BackOfficeOperations> PutActivateBackOfficeOperation(PutBackOfficeOperationActivateCommand backOfficeOperations)
        {
            BackOfficeOperations operationBackOffice = await GetBackOfficeOperationById(backOfficeOperations.Id);
            operationBackOffice = _mapper.Map<PutBackOfficeOperationActivateCommand, BackOfficeOperations>(backOfficeOperations, operationBackOffice);
            operationBackOffice.UpdatedBy = _currentUserService.GetUserInfo().Id;
            //operationBackOffice.UpdatedByName = _currentUserService.GetUserInfo().Name;
            operationBackOffice.UpdatedAt = new DateTime();
            operationBackOffice.Active = true;
            return await _backOfficeOperationsRepository.Activate(operationBackOffice);
        }

        public async Task<BackOfficeOperations> PutBackOfficeOperation(PutBackOfficeOperationCommand backOfficeOperations)
        {
            BackOfficeOperations operationBackOffice = await GetBackOfficeOperationById(backOfficeOperations.Id);
            operationBackOffice = _mapper.Map<PutBackOfficeOperationCommand, BackOfficeOperations>(backOfficeOperations, operationBackOffice);
            operationBackOffice.UpdatedBy = _currentUserService.GetUserInfo().Id;
            //operationBackOffice.UpdatedByName = _currentUserService.GetUserInfo().Name;
            operationBackOffice.UpdatedAt = new DateTime();
            return await _backOfficeOperationsRepository.Put(operationBackOffice);
        }

        public async Task<BackOfficeOperations> PutDeActivateBackOfficeOperation(PutBackOfficeOperationsDeActivateCommand backOfficeOperations)
        {
            BackOfficeOperations operationBackOffice = await GetBackOfficeOperationById(backOfficeOperations.Id);
            operationBackOffice = _mapper.Map<PutBackOfficeOperationsDeActivateCommand, BackOfficeOperations>(backOfficeOperations, operationBackOffice);
            operationBackOffice.UpdatedBy = _currentUserService.GetUserInfo().Id;
            //operationBackOffice.UpdatedByName = _currentUserService.GetUserInfo().Name;
            operationBackOffice.UpdatedAt = new DateTime();
            operationBackOffice.Active = false;
            return await _backOfficeOperationsRepository.Deactivate(operationBackOffice);
        }
        public async Task<bool> DeleteBackOfficeOperation(int id)
        {
            BackOfficeOperations operationBackOffice = await GetBackOfficeOperationById(id);
            return await _backOfficeOperationsRepository.Delete(operationBackOffice);
        }

    }
}
