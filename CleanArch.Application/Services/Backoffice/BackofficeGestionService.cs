﻿using Application.Backoffice.Commands.PutBackoffice;
using Application.Core.Exceptions;
using Application.DTOs.Backoffice;
using Application.Interfaces.Backoffice;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Interfaces.Backoffice;
using Domain.Models.Backoffice;
using System;
using System.Linq;

namespace Application.Services.Backoffice
{
    public class BackofficeGestionService : IBackofficeGestionService
    {

        private readonly IBackofficeGestionRepository _backofficeGestionRepository;
        private readonly IMapper _mapper;

        public BackofficeGestionService(IBackofficeGestionRepository backofficeGestionRepository, IMapper mapper)
        {
            _backofficeGestionRepository = backofficeGestionRepository;
            _mapper = mapper;
        }

        public BackofficeGestion PostBackofficeGestion(BackofficeGestion backofficeGestion)
        {
            return _backofficeGestionRepository.Post(backofficeGestion);
        }

        public BackofficeGestion PutBackofficeGestion(PutBackofficeGestionCommand backofficeGestionCommand)
        {
            BackofficeGestion backofficeGestion = GetBackofficeGestionById(backofficeGestionCommand.Id);

            backofficeGestion = _mapper.Map<PutBackofficeGestionCommand, BackofficeGestion>(backofficeGestionCommand, backofficeGestion);

            _backofficeGestionRepository.Put(backofficeGestion);

            return backofficeGestion;
        }

        public bool DeleteBackofficeGestion(Guid id)
        {
            BackofficeGestionDto backofficeGestionDto = _backofficeGestionRepository.Get()
                                                               .Where(x => x.Id == id)
                                                               .ProjectTo<BackofficeGestionDto>(_mapper.ConfigurationProvider)
                                                               .FirstOrDefault();
            if (backofficeGestionDto == null)
            {
                throw new NotFoundException("BackofficeGestionDto", "Id");
            }

            BackofficeGestion backofficeGestion = _mapper.Map<BackofficeGestion>(backofficeGestionDto);

            return _backofficeGestionRepository.Delete(backofficeGestion);

        }

        public BackofficeGestion GetBackofficeGestionById(Guid id)
        {
            return _backofficeGestionRepository
                    .Get()
                    .Where(x => x.Id == id)
                    .FirstOrDefault();
        }


    }
}
