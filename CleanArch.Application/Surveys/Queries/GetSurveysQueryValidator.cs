﻿using Application.Surveys.Queries;
using Domain.Interfaces;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Courses.Commands
{
    public class GetSurveysQueryValidator : AbstractValidator<GetSurveysQuery>
    {
        private readonly IAuthRepository _authRepository;
        public GetSurveysQueryValidator(IAuthRepository authRepository, IGestionRepository gestionRepository)
        {
            _authRepository = authRepository;

            RuleFor(c => c.SegmentId).NotNull();


        }
    }
}
