﻿using Application.DTOs;
using CleanArch.Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Surveys.Queries
{
    public class GetSurveysQuery : IRequest<List<SurveySegmentDto>>
    {
        public int SegmentId { get; set; }

    }

    public class GetSurveysQueryHandler : IRequestHandler<GetSurveysQuery, List<SurveySegmentDto>>
    {
        private readonly ISurveySegmentService _surveySegmentService;

        public GetSurveysQueryHandler(
            ISurveySegmentService surveySegmentService
        )
        {
            _surveySegmentService = surveySegmentService;            
            
        }



        public async Task<List<SurveySegmentDto>> Handle(GetSurveysQuery request, CancellationToken cancellationToken)
        {
            var x = _surveySegmentService.GetSurveyBySegmentId(request.SegmentId);

            return x;
        }


    }
}
