﻿using Application.Interfaces.Surveys;
using Application.Surveys.Queries;
using CleanArch.Application.Interfaces;
using FluentValidation;

namespace Application.Courses.Commands
{
    public class PostSurveyResponseCommandValidator : AbstractValidator<PostSurveyResponseCommand>
    {
        private readonly IActiveGestionService _activeGestionService;
        private readonly ISurveyService  _surveyService;
        public PostSurveyResponseCommandValidator(
            IActiveGestionService activeGestionService,
            ISurveyService surveyService
        )
        {
            _activeGestionService = activeGestionService;
            _surveyService  = surveyService;

            RuleFor(c => c.surveyId)
                .NotNull()
                .Must(_surveyService.CheckById)
                .WithMessage("La encuesta no existe");

            RuleFor(c => c.GestionId)
                .NotNull()
                .Must(_activeGestionService.CheckAtiveGestionById)
                .WithMessage("La Gestion No existe");


        }
    }
}
