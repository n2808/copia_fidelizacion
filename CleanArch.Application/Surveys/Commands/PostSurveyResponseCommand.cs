﻿using Application.Interfaces.Surveys;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Surveys.Queries
{

    public class SurveyResponseViewModel
    {
        public string name { get; set; }
        public List<string> multiAnswer { get; set; }
        public string simpleAnswer { get; set; }
    }


    public class PostSurveyResponseCommand : IRequest
    {
        public int GestionId { get; set; }
        public List<SurveyResponseViewModel> survey { get; set; }
        public int surveyId { get; set; }

    }

    public class PostSurveyResponseCommandHandler : IRequestHandler<PostSurveyResponseCommand>
    {
        private readonly ISurveyResultGestionService _surveyResultGestionService;
        private readonly ISurveyResultQuestionService _surveyResultQuestionService;

        public PostSurveyResponseCommandHandler(
            ISurveyResultGestionService surveyResultGestionService,
            ISurveyResultQuestionService surveyResultQuestionService
        )
        {
            _surveyResultGestionService = surveyResultGestionService;
            _surveyResultQuestionService = surveyResultQuestionService;
        }



        public async Task<Unit> Handle(PostSurveyResponseCommand request, CancellationToken cancellationToken)
        {

            _surveyResultQuestionService.PostSurveyQuestion(request, await _surveyResultGestionService.PostSurveyResult(request));

            return Unit.Value;
        }


    }
}
