﻿using Application.DTOs.User;
using Application.Interfaces.Rol;
using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Rol.Commands.PostRol
{
    public class PostRolCommand : IRequest<RolDto>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }

        public bool Status { get; set; }
    }

    public class PostRolCommandHandler : IRequestHandler<PostRolCommand, RolDto>
    {
        private readonly IRolService _rol;
        private readonly IMapper _mapper;

        public PostRolCommandHandler(IRolService rol, IMapper mapper)
        {
            _rol = rol;
            _mapper = mapper;
        }

        public async Task<RolDto> Handle(PostRolCommand request, CancellationToken cancellationToken)
        {
            var rol = _rol
                        .PostRol(_mapper.Map<Domain.Models.Rol.Rol>(request));

            return _mapper.Map<RolDto>(rol);
        }
    }

}
