﻿using Application.DTOs.User;
using Application.Interfaces.Rol;
using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Rol.Commands.PutRol
{
    public class PutRolCommand : IRequest<RolDto>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public bool Status { get; set; }
    }

    public class PutRolCommandHandler : IRequestHandler<PutRolCommand, RolDto>
    {
        private readonly IRolService _rolService;
        private readonly IMapper _mapper;

        public PutRolCommandHandler(IRolService rol, IMapper mapper)
        {
            _rolService = rol;
            _mapper = mapper;
        }
        public async Task<RolDto> Handle(PutRolCommand request, CancellationToken cancellationToken)
        {
            var rol = _rolService.PutRol(request);

            return _mapper.Map<RolDto>(rol);
        }
    }
}
