﻿using Application.DTOs.User;
using Application.Interfaces.Rol;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Rol.Commands.PutRol
{
    public class PutStatusDeactivateRolById : IRequest<RolDto>
    {
        public Guid Id { get; set; }
    }

    public class PutStatusDeactivateRolByIdCommandHandler : IRequestHandler<PutStatusDeactivateRolById, RolDto>
    {
        private readonly IRolService _rolService;
        private readonly IMapper _mapper;

        public PutStatusDeactivateRolByIdCommandHandler(IRolService rolService, IMapper mapper)
        {
            _rolService = rolService;
            _mapper = mapper;
        }

        public async Task<RolDto> Handle(PutStatusDeactivateRolById request, CancellationToken cancellationToken)
        {
            var rol = _rolService.PutStatusDeactivateRolById(request);

            return _mapper.Map<RolDto>(rol);
        }
    }
}
