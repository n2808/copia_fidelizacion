﻿using Application.DTOs.User;
using Application.Interfaces.Rol;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Rol.Commands.PutRol
{
    public class PutStatusActivateRolById : IRequest<RolDto>
    {
        public Guid Id { get; set; }
    }

    public class PutStatusActivateRolByIdCommandHandler : IRequestHandler<PutStatusActivateRolById, RolDto>
    {
        private readonly IRolService _rolService;
        private readonly IMapper _mapper;

        public PutStatusActivateRolByIdCommandHandler(IRolService rolService, IMapper mapper)
        {
            _rolService = rolService;
            _mapper = mapper;
        }

        public async Task<RolDto> Handle(PutStatusActivateRolById request, CancellationToken cancellationToken)
        {
            var rol = _rolService.PutStatusActivateRolById(request);

            return _mapper.Map<RolDto>(rol);
        }
    }
}
