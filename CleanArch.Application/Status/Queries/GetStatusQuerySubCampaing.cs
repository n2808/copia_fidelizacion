﻿using Application.DTOs.Status;
using Application.Interfaces.Status;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Status.Queries
{
    public class GetStatusQuerySubCampaing : IRequest<List<StatusDto>>
    {
        public int subCampaign { get; set; }
    }

    public class GetStatusQuerySubCampaingHandler : IRequestHandler<GetStatusQuerySubCampaing, List<StatusDto>>
    {
        private readonly IStatusService _statusService;

        public GetStatusQuerySubCampaingHandler(IStatusService statusService)
        {
            _statusService = statusService;
        }

        public async Task<List<StatusDto>> Handle(GetStatusQuerySubCampaing request, CancellationToken cancellationToken)
        {
            return await _statusService.GetStatusSubCampaing(request.subCampaign);
        }
    }
}
