﻿using Application.DTOs.Status;
using Application.Interfaces.Status;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Status.Queries
{
    public class GetStatusQuery : IRequest<List<StatusDto>>
    {
        public bool isSale { get; set; }
    }

    public class GetStatusQueryHandler : IRequestHandler<GetStatusQuery, List<StatusDto>>
    {
        private readonly IStatusService _statusService;

        public GetStatusQueryHandler(IStatusService statusService)
        {
            _statusService = statusService;
        }

        public async Task<List<StatusDto>> Handle(GetStatusQuery request, CancellationToken cancellationToken)
        {
            return await _statusService.GetStatus(request.isSale);
        }
    }

}
