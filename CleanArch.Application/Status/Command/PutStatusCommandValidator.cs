﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Status.Command
{
    public class PutStatusCommandValidator : AbstractValidator<PutStatusCommand>
    {
        public PutStatusCommandValidator()
        {
            RuleFor(x => x.Name)
                         .NotEmpty()
                         .WithMessage("El nombre no puede estar vacío");

            RuleFor(x => x.DisplayName)
                                .NotEmpty()
                                .WithMessage("El campo display name no puede estar vacío");

            RuleFor(x => x.Description)
                                .NotEmpty()
                                .WithMessage("El campo descripción no puede estar vacío");
        }
    }
}
