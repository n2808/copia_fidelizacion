﻿using Application.DTOs.Status;
using Application.Interfaces.Status;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Status.Command
{
    public class PostStatusCommand : IRequest<StatusDto>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }

    public class PoststatusCommandHandler : IRequestHandler<PostStatusCommand, StatusDto>
    {
        private readonly IStatusService _statusService;
        private readonly IMapper _mapper;

        public PoststatusCommandHandler(IStatusService statusService, IMapper mapper)
        {
            _statusService = statusService;
            _mapper = mapper;
        }

        public async Task<StatusDto> Handle(PostStatusCommand request, CancellationToken cancellationToken)
        {
            var status = await _statusService.Poststatus(_mapper.Map<Domain.Models.Status.Status>(request));
            return _mapper.Map<StatusDto>(status);
        }
    }
}
