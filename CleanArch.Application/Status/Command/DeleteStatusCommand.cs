﻿using Application.Interfaces.Status;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Status.Command
{
    public class DeleteStatusCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class DeleteStatusCommandHandler : IRequestHandler<DeleteStatusCommand, bool>
    {
        private readonly IStatusService _statusService;

        public DeleteStatusCommandHandler(IStatusService statusService)
        {
            _statusService = statusService;
        }

        public async Task<bool> Handle(DeleteStatusCommand request, CancellationToken cancellationToken)
        {
            await _statusService.DeleteStatus(request.Id);
            return true;
        }
    }
}
