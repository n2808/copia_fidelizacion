﻿using Application.DTOs.Status;
using Application.Interfaces.Status;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Status.Command
{
    public class PutStatusCommand : IRequest<StatusDto>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool IsSale { get; set; }
    }

    public class PutStatusCommandHandler : IRequestHandler<PutStatusCommand, StatusDto>
    {
        private readonly IStatusService _statusService;
        private readonly IMapper _mapper;

        public PutStatusCommandHandler(IStatusService statusService, IMapper mapper)
        {
            _statusService = statusService;
            _mapper = mapper;
        }

        public async Task<StatusDto> Handle(PutStatusCommand request, CancellationToken cancellationToken)
        {
            var status = await _statusService.PutStatus(request);
            return _mapper.Map<StatusDto>(status);
        }
    }
}
