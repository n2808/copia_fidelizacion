﻿using Application.DTOs.Skills;
using Application.Interfaces.Skill;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Skills.Command.Put
{
    public class PutSkillsActivateCommand : IRequest<SkillDto>
    {
        public int Id { get; set; }
    }
    public class PutSkillsActivateCommandHandler : IRequestHandler<PutSkillsActivateCommand, SkillDto>
    {
        private readonly ISkillService _skillService;
        private readonly IMapper _mapper;

        public PutSkillsActivateCommandHandler(ISkillService skillService, IMapper mapper)
        {
            _skillService = skillService;
            _mapper = mapper;
        }
        public async Task<SkillDto> Handle(PutSkillsActivateCommand request, CancellationToken cancellationToken)
        {
            var skill = await _skillService.PutActivateSkills(request);
            return _mapper.Map<SkillDto>(skill);
        }
    }
}
