﻿using Application.DTOs.Campaign;
using Application.DTOs.Skills;
using Application.Interfaces.Skill;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Skills.Command.Put
{
    public class PutSkillsDeActivateCommand : IRequest<SkillDto>
    {
        public int Id { get; set; }
    }
    public class PutSkillsDeActivateCommandHandler : IRequestHandler<PutSkillsDeActivateCommand, SkillDto>
    {
        private readonly ISkillService _skillService;
        private readonly IMapper _mapper;

        public PutSkillsDeActivateCommandHandler(ISkillService skillService, IMapper mapper)
        {
            _skillService = skillService;
            _mapper = mapper;
        }
        public async Task<SkillDto> Handle(PutSkillsDeActivateCommand request, CancellationToken cancellationToken)
        {
            var skill = await _skillService.PutDeActivateSkills(request);
            return _mapper.Map<SkillDto>(skill);
        }
    }
}
