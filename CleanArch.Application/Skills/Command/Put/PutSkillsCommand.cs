﻿using Application.DTOs.Campaign;
using Application.DTOs.Skills;
using Application.Interfaces.Skill;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Skills.Command.Put
{
    public class PutSkillsCommand : IRequest<SkillDto>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public string Segment { get; set; }
        public bool Active { get; set; }
        public int? SubCampaignId { get; set; }
    }

    public class PutSkillsCommandHandler : IRequestHandler<PutSkillsCommand, SkillDto>
    {
        private readonly ISkillService _skillService;
        private readonly IMapper _mapper;

        public PutSkillsCommandHandler(ISkillService skillService, IMapper mapper)
        {
            _skillService = skillService;
            _mapper = mapper;
        }
        public async Task<SkillDto> Handle(PutSkillsCommand request, CancellationToken cancellationToken)
        {
            var skill = await _skillService.PutSkills(request);
            return _mapper.Map<SkillDto>(skill);
        }
    }
}
