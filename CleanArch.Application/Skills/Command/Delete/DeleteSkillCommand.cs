﻿using Application.Interfaces.Skill;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Skills.Command.Delete
{
    public class DeleteSkillCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }

    public class DeleteSkillCommandHandler : IRequestHandler<DeleteSkillCommand, bool>
    {
        private readonly ISkillService _skillService;
        public DeleteSkillCommandHandler(ISkillService skillService)
        {
            _skillService = skillService;
        }
        public async Task<bool> Handle(DeleteSkillCommand request, CancellationToken cancellationToken)
        {
            await _skillService.DeleteSkills(request.Id);
            return true;
        }
    }
}
