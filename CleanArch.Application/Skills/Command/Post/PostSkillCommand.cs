﻿using Application.DTOs.Skills;
using Application.Interfaces.Skill;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Skills.Command.Post
{
    public class PostSkillCommand : IRequest<SkillDto>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public string Segment { get; set; }
        public bool Active { get; set; }
        public int? SubCampaignId { get; set; }
    }

    public class PostSkillCommandHandler : IRequestHandler<PostSkillCommand, SkillDto>
    {
        private readonly ISkillService _skillService;
        private readonly IMapper _mapper;
        public PostSkillCommandHandler(ISkillService skillService, IMapper mapper)
        {
            _skillService = skillService;
            _mapper = mapper;
        }
        public async Task<SkillDto> Handle(PostSkillCommand request, CancellationToken cancellationToken)
        {
            var skill = await _skillService.PostSkills(_mapper.Map<Domain.Models.Skill.Skills>(request));
            return _mapper.Map<SkillDto>(skill);
        }
    }
}
