﻿using Application.DTOs.Skills;
using Application.Interfaces.Skill;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Skills.Queries
{
    public class GetSkillQueryById : IRequest<SkillDto>
    {
        public int Id { get; set; }
    }

    public class GetSkillQueryByIdCommandHandler : IRequestHandler<GetSkillQueryById, SkillDto>
    {
        private readonly ISkillService _skillService;
        private readonly IMapper _mapper;
        public GetSkillQueryByIdCommandHandler(ISkillService skillService, IMapper mapper)
        {
            _skillService = skillService;
            _mapper = mapper;
        }

        public async Task<SkillDto> Handle(GetSkillQueryById request, CancellationToken cancellationToken)
        {
            var skill = await _skillService.GetSkillById(request.Id);
            return _mapper.Map<SkillDto>(skill);
        }
    }


}
