﻿using Application.DTOs.Skills;
using Application.Interfaces.Skill;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Skills.Queries
{
    public class GetSkillsQuery : IRequest<List<SkillDto>>
    {
        public int without { get; set; }
    }

    public class GetSkillsQueryHandler : IRequestHandler<GetSkillsQuery, List<SkillDto>>
    {
        private readonly ISkillService _skillService;

        public GetSkillsQueryHandler(ISkillService skillService)
        {
            _skillService = skillService;
        }
        public async Task<List<SkillDto>> Handle(GetSkillsQuery request, CancellationToken cancellationToken)
        {
            return await _skillService.GetSkills(request.without);
        }
    }
}
