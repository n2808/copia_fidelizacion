﻿using Application.DTOs.FetchFaceEvidence;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FetchFaceEvidence.Queries
{
    public class FetchFaceEvidenceCommand : IRequest<FetchFaceEvidenceDto>
    {
        public int FetchFaceId { get; set; }
    }

    public class FetchFaceEvidenceCommandHandler : IRequestHandler<FetchFaceEvidenceCommand, FetchFaceEvidenceDto>
    {
        private readonly IFetchFaceEvidenceService _fetchFaceEvidenceService;
        private readonly IMapper _mapper;

        public FetchFaceEvidenceCommandHandler(IFetchFaceEvidenceService fetchFaceEvidenceService, IMapper mapper)
        {
            _fetchFaceEvidenceService = fetchFaceEvidenceService;
            _mapper = mapper;
        }
        public async Task<FetchFaceEvidenceDto> Handle(FetchFaceEvidenceCommand request, CancellationToken cancellationToken)
        {
            return await _fetchFaceEvidenceService.FetchFaceEvidence(request);
        }
    }
}
