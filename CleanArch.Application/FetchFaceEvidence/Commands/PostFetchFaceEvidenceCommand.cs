﻿using Application.DTOs.FetchFaceEvidence;
using Application.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FetchFaceEvidence.Commands
{
    public class PostFetchFaceEvidenceCommand : IRequest<FetchFaceEvidenceDto>
    {
        public string Document { get; set; }
        public bool Status { get; set; }
        public int ActiveGestionId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class PostFetchFaceEvidenceCommandHandler : IRequestHandler<PostFetchFaceEvidenceCommand, FetchFaceEvidenceDto>
    {
        private readonly IFetchFaceEvidenceService _fetchFaceEvidenceService;
        private readonly IMapper _mapper;
        public PostFetchFaceEvidenceCommandHandler(IFetchFaceEvidenceService fetchFaceEvidenceService, IMapper mapper)
        {
            _fetchFaceEvidenceService = fetchFaceEvidenceService;
            _mapper = mapper;
        }
        public async Task<FetchFaceEvidenceDto> Handle(PostFetchFaceEvidenceCommand request, CancellationToken cancellationToken)
        {
            var fetchFaceEvidence = await _fetchFaceEvidenceService.PostfetchFaceEvidence(_mapper.Map<Domain.Models.FetchFaceEvidences.FetchFaceEvidence>(request));
            return _mapper.Map<FetchFaceEvidenceDto>(fetchFaceEvidence);
        }
    }
}
