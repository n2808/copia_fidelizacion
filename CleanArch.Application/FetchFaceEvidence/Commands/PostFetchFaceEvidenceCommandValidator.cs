﻿using CleanArch.Application.Interfaces;
using FluentValidation;

namespace Application.FetchFaceEvidence.Commands
{
    public class PostFetchFaceEvidenceCommandValidator : AbstractValidator<PostFetchFaceEvidenceCommand>
    {

        private readonly IActiveGestionService _activeGestionService;
        public PostFetchFaceEvidenceCommandValidator(
            IActiveGestionService activeGestionService
            )
        {
            _activeGestionService = activeGestionService;
            RuleFor(x => x.Document)
                     .NotEmpty()
                     .WithMessage("El Document no puede estar vacío");

            RuleFor(x => x.ActiveGestionId)
                           .NotNull()
                           .Must(_activeGestionService.CheckAtiveGestionById).WithMessage("La gestion no existe");
        }
    }
}
