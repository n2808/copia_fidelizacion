﻿using Application.Common.Response;
using Application.DTOs.User;
using Application.Interfaces.BulkExcel;
using MediatR;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.BulkExcel.command
{
    public class BulkExcelCommand : IRequest<ApiResponse<IList<UserDto>>>
    {
        public IFormFile File  { get; set; }
        public int SubCampaignId { get; set; }
    }
    public class BulkExcelCommandHandler : IRequestHandler<BulkExcelCommand, ApiResponse<IList<UserDto>>>
    {
        private readonly IBulkExcelService _bulkExcelService;
       
        public BulkExcelCommandHandler(IBulkExcelService bulkExcelService)
        {
            _bulkExcelService = bulkExcelService;            
        }
        public async Task<ApiResponse<IList<UserDto>>> Handle(BulkExcelCommand request, CancellationToken cancellationToken)
        {
            return _bulkExcelService.SaveFile(request.File, request.SubCampaignId);
            
        }
    }
}
