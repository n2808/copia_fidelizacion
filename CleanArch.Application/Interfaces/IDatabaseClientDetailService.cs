﻿using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface IDatabaseClientDetailService
    {

        Task<DatabaseClientDetail> GetById(int Id);
        Task<DatabaseClientDetail> GetByPhone(string phone);
    }
}
