﻿using Application.Common.Response;
using Application.DTOs.SecondRingLoad;
using Application.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces.SecondRingLoad
{
    public interface ISecondRingLoad
    {
        Task<ApiResponse<IList<SecondRingLoadDto>>> SaveFile(IFormFile file);
        Task<List<GetSecondRingViewModel>> GetSeconds(int without);
        Task<bool> GetSecondsAssign(Guid id);
        //Task<bool> PutSecondsAssign(Guid id);
    }
}
