﻿using Application.DTOs.Operators;
using Application.Operators.Command;
using Domain.Models.Operators;
using System;
using System.Collections.Generic;

namespace Application.Interfaces.Operators
{
    public interface IOperatorService
    {
        List<OperatorDto> GetOperators(int without);
        Operator GetOperatorById(Guid id);
        Operator PostOperator(Operator product);
        Operator PutOperator(PutOperatorCommand putProductCommand);
        bool DeleteOperator(Guid id);
        bool ActivateOperator(Guid id);
        bool DeactivateOperator(Guid id);
    }
}
