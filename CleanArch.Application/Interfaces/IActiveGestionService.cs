﻿using Application.DTOs.ActiveGestion;
using Application.Gestions.Commands;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface IActiveGestionService
    {

        bool CheckAtiveGestionById(int Id);
        Task<ActiveGestion> updateGestion(EndGestionCommand request);

        Task<ActiveGestion> GetGestionById(int Id);



        Task<List<ActiveGestionDto>> GetGestionActiveById(int Id);
    }
}
