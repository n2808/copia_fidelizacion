﻿using Application.Surveys.Queries;
using Domain.Models.SurveysResults;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Surveys
{
    public interface ISurveyResultGestionService
    {
        Task<SurveyResultGestion> PostSurveyResult(PostSurveyResponseCommand data);
    }
}
