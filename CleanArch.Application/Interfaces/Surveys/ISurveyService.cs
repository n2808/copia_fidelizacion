﻿using Application.Surveys.Queries;
using Domain.Models.Surveys;
using Domain.Models.SurveysResults;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces.Surveys
{
    public interface ISurveyService
    {
        Survey GetById(int Id);
        bool CheckById(int Id);
    }
}
