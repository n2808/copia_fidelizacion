﻿using Application.Surveys.Queries;
using Domain.Models.SurveysResults;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces.Surveys
{
    public interface ISurveyResultQuestionService
    {
        List<SurveyResultQuestion> PostSurveyQuestion(PostSurveyResponseCommand data , SurveyResultGestion Survey);
    }
}
