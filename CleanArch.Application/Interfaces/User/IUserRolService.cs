﻿using Domain.Models.User;
using System.Collections.Generic;
using System.Linq;

namespace Application.Interfaces.User
{
    public interface IUserRolService
    {
        IQueryable<UserRol> Get();
        UserRol Post(UserRol userRol);
        UserRol Put(UserRol userRol);
        List<UserRol> PostRange(List<UserRol> userRol);
        bool Delete(UserRol entity);
    }
}
