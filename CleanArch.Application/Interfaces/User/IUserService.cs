﻿using Application.DTOs.User;
using Application.User.Commands.PutUser;
using Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces.User
{
    public interface IUserService
    {
        List<UserDto> GetUser(int without);
        UserDto GetUserByDocumentAndSubcampaign(string document, int subCampaignId);
        Domain.Models.User.User PutUserExcel(UserDto userDto);
        Domain.Models.User.User GetById(Guid Id);
        Domain.Models.User.User PostUser(Domain.Models.User.User user);

        Domain.Models.User.User PutStatusActivateUserById(PutStatusActivateUserById request);

        Domain.Models.User.User PutStatusDeactivateUserById(PutStatusDeactivateUserById request);
        Domain.Models.User.User PutUser(PutUserCommand request);
        bool DeleteUser(Guid id);
        bool CheckById(Guid? Id);
        bool CheckByDocumentAndSubcampaign(string document, int? subcampaign);

        bool GetByLogin(String login, int subCampaignId);
        Task<CreateUserViewModel> GetAllByLogin(String login);

        bool GetByLoginPut(String login, int subCampaignId, Guid id);

        bool PostSubcampaingUsers(PostMoveUserCommand request);
        Task<Domain.Models.User.User> GetUserByLogin(String login, int subCampaignId);
    }
}
