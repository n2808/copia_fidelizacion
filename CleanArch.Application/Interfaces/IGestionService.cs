﻿using Application.DTOs;
using Application.DTOs.User;
using Application.Gestions.Queries.GetInboundInitialGestion;
using Application.Gestions.Queries.GetInitialGestion;
using CleanArch.Application.ViewModel;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface IGestionService
    {
        //ClientDataAndGestionViewModel GetClientsDataFields(CampaignDto cpDto, GetInitialGestionQuery request, UserDto userDto);
        Task<ClientDataAndGestionViewModel> GetClientsDataFields(CampaignDto cpDto, GetInitialGestionQuery request, UserDto userDto);
        Task<ClientDataAndGestionViewModel> GetClientsDataFields(CampaignDto cpDto, GetInboundInitialGestionQuery request, UserDto userDto);


    }
}
