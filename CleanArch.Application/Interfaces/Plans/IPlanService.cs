﻿using Application.DTOs.Plans;
using Application.Plans.Command;
using Domain.Models.Plans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces.Plans
{
    public interface IPlanService
    {
        Task<List<PlanDto>> GetPlans(int without);
        Task<Plan> PostPlan(Plan plan);
        Task<Plan> PutPlan(PutPlanCommand putPlanCommand);
        Task<bool> DeletePlan(Guid id);
        Task<bool> ActivatePlan(Guid id);
        Task<bool> DeactivatePlan(Guid id);
    }
}