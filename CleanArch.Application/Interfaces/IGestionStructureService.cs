﻿using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IGestionStructureService
    {
        List<GestionStructure> GetGestionStructureBySegmentId(int Id);
    }
}
