﻿using Application.Common.Response;
using Application.DTOs.User;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Application.Interfaces.BulkExcel
{
    public interface IBulkExcelService
    {
        ApiResponse<IList<UserDto>> SaveFile(IFormFile file, int subCampaignId);
    }
}
