﻿using Application.ClaroUser.Command.Put;
using Application.ClaroUser.Queries;
using Application.Common.Response;
using Application.DTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IClaroUserService
    {
        Task<Domain.Models.ClaroUser.ClaroUser> GetUserByCode(GetCodeClaroUserQuery request);
        Task<ApiResponse<IList<ClaroUserDto>>> SaveFile(IFormFile file);
        Task<Domain.Models.ClaroUser.ClaroUser> PostClaroUser(Domain.Models.ClaroUser.ClaroUser claroUser);
        Task<Domain.Models.ClaroUser.ClaroUser> PutClaroUser(PutClaroUserCommand claroUser);
    }
}
