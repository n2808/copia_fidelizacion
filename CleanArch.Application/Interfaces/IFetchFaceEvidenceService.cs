﻿using Application.DTOs.FetchFaceEvidence;
using Application.FetchFaceEvidence.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFetchFaceEvidenceService
    {
        Task<Domain.Models.FetchFaceEvidences.FetchFaceEvidence> PostfetchFaceEvidence(Domain.Models.FetchFaceEvidences.FetchFaceEvidence fetchFaceEvidence);
        Task<FetchFaceEvidenceDto> FetchFaceEvidence(FetchFaceEvidenceCommand fetchFaceEvidenceCommand);
    }
}
