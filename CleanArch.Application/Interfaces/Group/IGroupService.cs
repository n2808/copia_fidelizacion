﻿using Application.DTOs.Group;
using Application.Group.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces.Group
{
    public interface IGroupService
    {
        List<GroupDto> GetGroups(int without);
        Domain.Models.Groups.Group PostGroup(Domain.Models.Groups.Group group);
        Domain.Models.Groups.Group PutGroup(PutGroupCommand putGruopCommand);
        bool DeleteGroup(Guid id);
        bool ActivateGroup(Guid id);
        bool DeactivateGroup(Guid id);
    }
}
