﻿using Application.DTOs.Reschedule;
using Core.Models.Operation;
using Domain.Models.ReSchedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Reschedule
{
    public interface IRescheduleService
    {
        Task Reschedule(ActiveGestion gestionReports);
    }
}
