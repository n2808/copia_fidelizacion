﻿using Application.DTOs.Reschedule;
using Application.Reschedule.Comands.EndRescheduleCommand;
using Core.Models.Operation;
using Domain.Models.ReSchedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Reschedule
{
    public interface IGestionRescheduleService
    {
        
        /// <summary>
        /// Get a count of reschedules, group by segment id
        /// </summary>
        /// <param name="statusId"></param>
        /// <returns></returns>

        Task<List<RescheduleGroupDto>> GetCountGestionRescheduleAvailable(bool statusId = true);


        /// <summary>
        /// Assign a gestionReschedule to an Agent for the work
        /// </summary>
        /// <param name="segmentId"></param>
        /// <returns></returns>
        Task<GestionReschedule> RequestAGestionRescheduleWithoutAgent(int? segmentId);

        /// <summary>
        /// Get a list of GestionReschedule that have a Agent
        /// </summary>
        /// <param name="AgentId"></param>
        /// <returns>List<GestionReschedule></returns>
        Task<List<GestionReschedule>> GestionRescheduleByAgent(Guid AgentId);


        /// <summary>
        /// Add a new gestionReschedule
        /// </summary>
        /// <param name="gestion"></param>
        /// <returns></returns>
        Task AddGestionReschedule(ActiveGestion gestion);


        /// <summary>
        /// typify a GestionReschedule
        /// </summary>
        /// <param name="AgentId"></param>
        /// <returns>List<GestionReschedule></returns>
        Task UpdateGestionReschedule(Guid RescheduleId, Guid AgentId, bool status);
    }
}
