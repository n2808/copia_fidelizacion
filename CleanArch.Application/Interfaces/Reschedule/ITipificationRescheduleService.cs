﻿using Application.DTOs.Reschedule;
using Domain.Models.ReSchedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Reschedule
{
    public interface ITipificationRescheduleService
    {
        Task<TipificationReschedule> GetTipificationBySegmentIdAndCode(int segmentId, string code);
    }
}
