﻿using Application.DTOs.EvidenceGestion;
using Application.EvidenceGestion.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces.EvidenceGestion
{
    public interface IEvidenceGestionService
    {
        List<EvidenceGestionDto> GetByIdEvidencesGestion(int Id);
        Task<List<EvidenceGestionDto>> EvidenceGestion(EvidenceGestionCommand evidenceGestion);
    }
}
