﻿using Application.DTOs;
using CleanArch.Application.ViewModel;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.Interfaces
{
    public interface IInformativeAgentFieldsService
    {

        List<InformativeAgentFields> GetInformativeFieldsBySegmentId(int Id);
    }
}
