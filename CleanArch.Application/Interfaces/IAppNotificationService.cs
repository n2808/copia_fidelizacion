﻿using Application.DTOs;
using CleanArch.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.Interfaces
{
    public interface IAppNotificationService
    {

        List<AppNotificationDto> GetSMSNotification();
        bool CheckAppNotificationById(int Id);

        AppNotificationDto GetById(int Id);
    }
}
