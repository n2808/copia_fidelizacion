﻿using Application.Configurations.Categories.Command;
using Application.DTOs.Configurations;
using Core.Models.configuration;
using System;
using System.Collections.Generic;

namespace Application.Interfaces.Configurations
{
    public interface ICategoryService
    {
        List<CategoryDto> GetCategories(int without);
        Category PostCategory(Category category);
        Category PutCategory(PutCategoryCommand category);
        Category ActivateCategory(Guid id);
        Category DeactivateCategory(Guid id);
        bool DeleteCategory(Guid id);
      
    }
}
