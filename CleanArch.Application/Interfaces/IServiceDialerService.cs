﻿using Application.DTOs;
using Application.ServicseDialer.DTOs;
using CleanArch.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.Interfaces
{
    public interface IServiceDialerService
    {

        CampaignDto GetCampaignByServiceCode(string Code);
        List<ServiceDialerDto> GetServiceDialerBySubCampaignId(int Id);
    }
}
