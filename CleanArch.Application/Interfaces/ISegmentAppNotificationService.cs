﻿using Application.DTOs;
using CleanArch.Application.ViewModel;
using Domain.Models.Apps;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.Interfaces
{
    public interface ISegmentAppNotificationService
    {

        List<AppNotificationDto> GetAppsBySegmentId(int Id);

        bool CheckById(int Id);
        SegmentAppNotification GetById(int Id);

    }
}
