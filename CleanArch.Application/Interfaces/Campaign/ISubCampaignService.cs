﻿using Application.DTOs.Campaign;
using Domain.Models.Operation;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface ISubCampaignService
    {

        bool checkById(int Id);
        Task<List<SubCampaignDto>> GetSubcampaigns();
        Task<string> GetNameSubcampaigns(int? Id);

    }
}
