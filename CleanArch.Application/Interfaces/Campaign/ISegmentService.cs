﻿using Application.DTOs;
using Application.DTOs.Segment;
using Application.Segments.Command;
using Application.Segments.Command.Put;
using Application.Segments.Queries;
using CleanArch.Application.ViewModel;
using Domain.Models.Operation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface ISegmentService
    {
        bool checkById(int Id);
        Task<List<SegmentDto>> GetSegment(int without);
        Task<Segment> PostSegment(Segment group);
        Task<Segment> PutSegment(PutSegmentCommand group);
        Task<Segment> PutStatusActivateSegmentById(PutStatusActivateSegmentByIdCommand request);
        Task<Segment> PutStatusDeActivateSegmentById(PutStatusDeActivateSegmentByIdCommand request);
        Task<bool> DeleteSegment(int id);
        Task<Segment> GetSegmentById(GetSegmentByIdQuery request);
        Task<List<SegmentDto>> GetSegmentByIdCampaing(int id);
    }
}
