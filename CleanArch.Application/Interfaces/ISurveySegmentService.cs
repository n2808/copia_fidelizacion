﻿using Application.DTOs;
using CleanArch.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.Interfaces
{
    public interface ISurveySegmentService
    {

        List<SurveySegmentDto> GetSurveyBySegmentId(int Id);
    }
}
