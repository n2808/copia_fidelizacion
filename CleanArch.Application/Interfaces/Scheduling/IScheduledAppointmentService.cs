﻿using Application.DTOs.Scheduling;
using Application.Scheduling.Commands.PutScheduled;
using Domain.Models.Scheduling;
using System;
using System.Collections.Generic;

namespace Application.Interfaces.Scheduling
{
    public interface IScheduledAppointmentService
    {
        List<ScheduledAppointmentDto> GetScheduledAppointment();        
        ScheduledAppointment PostScheduledAppointment(ScheduledAppointment scheduledAppointment);
        ScheduledAppointment PutScheduledAppointment(PutScheduledAppointmentCommand request);
        bool DeleteScheduledAppointment(Guid id);


    }
}
