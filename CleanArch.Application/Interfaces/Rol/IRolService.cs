﻿using Application.DTOs.User;
using Application.Rol.Commands.PutRol;
using System;
using System.Collections.Generic;

namespace Application.Interfaces.Rol
{
    public interface IRolService
    {
        List<RolDto> Get();
        Domain.Models.Rol.Rol GetById(Guid Id);
        Domain.Models.Rol.Rol PostRol(Domain.Models.Rol.Rol rol);
        Domain.Models.Rol.Rol PutRol(PutRolCommand request);
        bool DeleteRol(Guid id);
        bool CheckById(Guid? Id);
        Domain.Models.Rol.Rol PutStatusActivateRolById(PutStatusActivateRolById request);

        Domain.Models.Rol.Rol PutStatusDeactivateRolById(PutStatusDeactivateRolById request);
    }
}
