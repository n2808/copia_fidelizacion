﻿using Application.DTOs.Quotes;
using Application.Quotes.Command;
using Application.Quotes.Queries;
using Domain.Models.Quotes;
using System;
using System.Collections.Generic;

namespace Application.Interfaces.Quotes
{
    public interface IQuoteService
    {
        List<QuoteDto> GetQuotes(GetQuoteQuery getQuoteQuery);
        Quote GetQuotetById(Guid id);
        Quote PostQuote(Quote quote);
        Quote PutQuote(PutQuoteCommand putQuoteCommand);
        bool DeleteQuote(Guid id);
        bool ActivateQuote(Guid id);
        bool DeactivateQuote(Guid id);

    }
}
