﻿using Application.DTOs.Products;
using Application.Products.Command;
using Domain.Models.Products;
using System;
using System.Collections.Generic;

namespace Application.Interfaces.Products
{
    public interface IProductService
    {
        List<ProductDto> GetProducts(int without);
        Product GetProductById(Guid id);
        Product PostProduct(Product product);
        Product PutProduct(PutProductCommand putProductCommand);
        bool DeleteProduct(Guid id);
        bool ActivateProduct(Guid id);
        bool DeactivateProduct(Guid id);
    }
}
