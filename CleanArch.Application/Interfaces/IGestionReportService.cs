﻿using Application.DTOs.GestionReports;
using Application.ViewModel;
using Core.Models.Operation;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface IGestionReportService
    {
        bool AllowEndGestion(int Id);

        List<GestionReportsDto> GetGestionReportsSegmentId(int Id, WhereViewModel whereViewModel);

        Task<GestionReports> GetGestionById(int Id, CancellationToken cancellationToken);

        Task<GestionReports> UpdateGestion(GestionReports gestion, CancellationToken cancellationToken);
        
        List<GestionReportsDto> GetGestionTipification(string tipificationName);

        Task<List<GestionReportsDto>> GetGestionReportsPhone(string phone);
        Task<List<GestionReportsDto>> GetGestionReportsAccount(string account);
        Task<List<GestionReportsDto>> GetGestionReportsIdentification(string identification);

    }
}
