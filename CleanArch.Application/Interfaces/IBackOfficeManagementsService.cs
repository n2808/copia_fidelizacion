﻿using Application.BackOfficeManagements.Command.Post;
using Application.BackOfficeManagements.Command.Put;
using Application.DTOs.BackOfficeManagements;
using Application.DTOs.ManagementsBackOffice;
using Application.ViewModel;
using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IBackOfficeManagementsService
    {
        Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PostManagements(Domain.Models.ManagementsBackoffice.BackOfficeManagements managementsBackOffice);
        Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PutManagements(PutBackOfficeManagementsCommand putManagementsBackOffice);
        Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> StartManagements(Guid id);
        Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PutEndManagements(PutEndBackOfficeManagementCommand request);
        Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> PutEndManagementsValidator(PutEndValidatorManagementCommand request);
        Task PostManagement(ActiveGestion activeGestion);
        Task<List<BackOfficeManagementsGroupDto>> GetCountGestionBackOfficeAvailable(string process, bool statusId = true);
        Task<List<BackOfficeGroupByStatusNameDto>> GetGestionBackOfficeManagements();
        Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> RequestBackOfficeManagementWithoutAgent(AssignAgentManagementCommand request);
        Task<List<Domain.Models.ManagementsBackoffice.BackOfficeManagements>> BackOfficeManagementByAgent(Guid AgentId, string process);
        Task AddBackOfficeManagement(ActiveGestion gestion);
        Task UpdateBackOfficeManagement(Guid ManagementId, Guid AgentId, bool status);
        Task<Domain.Models.ManagementsBackoffice.BackOfficeManagements> GetBackOfficeById(Guid id);
        Task<List<BackOfficeManagementsDto>> GetBackOfficeManagementAll();
        Task<bool> PostManagementSecondRing(CreateBackOfficeManegementViewModel data);
        Task<bool> UpdateManagementSecondRing(ManualBaseViewModel manualBase, int gestionId, int tipificationId);
    }
}
