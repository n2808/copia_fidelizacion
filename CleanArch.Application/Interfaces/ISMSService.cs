﻿using Application.DTOs;
using CleanArch.Application.ViewModel;
using Core.Events.SMS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface ISMSService
    {
        Task<NotificationAccountViewModel> Send(string phone, string message, int AccountId);

    }
}
