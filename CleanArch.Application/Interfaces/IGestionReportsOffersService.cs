﻿using CleanArch.Application.ViewModel;
using Core.Models.Operation;
using Domain.Models.Offer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface IGestionReportsOffersService
    {
        Task<List<GestionReportsOffers>> GetOffersByGestionReportsId(int? gestionReportsId);
        Task<OffersViewModel> AddOffers(OffersViewModel offers, ActiveGestion activeGestion);
        Task DeleteOffers(List<GestionReportsOffers> offers);
    }
}
