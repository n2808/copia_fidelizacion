﻿using Application.DTOs.Status;
using Application.Status.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Status
{
    public interface IStatusService
    {
        Task<List<StatusDto>> GetStatus(bool without);
        Task<List<StatusDto>> GetStatusSubCampaing(int subCampaing);
        Task<Domain.Models.Status.Status> GetStatusById(Guid id);
        Task<Domain.Models.Status.Status> Poststatus(Domain.Models.Status.Status status);
        Task<Domain.Models.Status.Status> PutStatus(PutStatusCommand putStatusCommand);
        Task<bool> DeleteStatus(Guid id);
    }
}
