﻿using Core.Models.Operation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface IDatabaseClientReportsService
    {

        Task<DatabaseClientReports> PostAndUpdate(DatabaseClientReports Client);
    }
}
