﻿using Application.DTOs.location;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces.Location
{
    public interface IStateService
    {
        List<StateDto> GetStatesByCountry(Guid id);
    }
}
