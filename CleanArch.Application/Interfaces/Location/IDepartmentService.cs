﻿using Application.DTOs.location;
using Domain.Models.location;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Location
{
    public interface IDepartmentService
    {
        Task<List<DepartmentDto>> GetDepartment(int without);
        Task<Department> GetDepartmentById(int id);
    }
}
