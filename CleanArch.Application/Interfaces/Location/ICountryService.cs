﻿using Application.DTOs.location;
using Core.Models.location;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces.Location
{
    public interface ICountryService
    {
        Task<List<CountryDto>> GetCountries();
        Task<Country> GetCountriesById(Guid countryId);
        Task<List<CountryDepartmentDto>> GetCountriesByCode(string code);
        Task<List<CountryDepartmentDto>> GetCountriesByName(string name);
    }
}
