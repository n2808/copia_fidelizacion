﻿using Application.DTOs.location;
using Domain.Models.location;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Location
{
    public interface INewCityService
    {
        Task<List<NewCityDto>> GetNewCity(int without);
        Task<NewCity> GetNewCityById(int id);
    }
}
