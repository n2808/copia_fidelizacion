﻿using Application.DTOs.location;
using System;
using System.Collections.Generic;

namespace Application.Interfaces.Location
{
    public interface ICityService
    {
        List<CityDto> GetCitiesByState(int departmentId);
        List<CityDto> GetCities();
    }
}
