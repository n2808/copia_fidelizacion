﻿using Application.DTOs;
using CleanArch.Application.ViewModel;
using Core.Events.SMS;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.Interfaces.Notifications
{
    public interface INotificationAccountService
    {

        NotificationAccountViewModel GetAccountById(int Id);

    }
}
