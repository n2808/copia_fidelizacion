﻿using CleanArch.Application.Services.Notifications;
using Core.Events.SMS;
using Core.Models.Operation;

namespace CleanArch.Application.Interfaces.Notifications
{
    public interface INotificationLogService
    {

        bool Post(NotificationAccountViewModel data, ActiveGestion gestion);
    }
}
