﻿using Application.DTOs;
using CleanArch.Application.ViewModel;
using Domain.Models.Tipifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces
{
    public interface ITipificationService
    {

        List<TipificationDto> GetTipificationsBySubcampaign(int Id);
        List<TipificationDto> GetTipificationsByDialerService(string name);
        Task<Tipification> GetTipificationById(int Id);
        List<TipificationDto> GetTipificationsByName(string name, int? id);
    }
}
