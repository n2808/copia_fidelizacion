﻿using Application.Auth.Commands;
using Application.DTOs;
using Application.DTOs.User;
using Application.ViewModel.Auth;
using CleanArch.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interfaces.Auths
{
    public interface IAuthService
    {
        UserDto GetUserByLogin(string login);
        AuthViewModel GetAuth(PostLoginCommand auth);
        Task<AuthOutBoundViewModel> GetAuthOutBound(PostLoginOutboundCommand auth);
        Task<AuthInBoundViewModel> GetAuthInbound(PostLoginInboundCommand auth);
        UserDto GetUserById(Guid? Id);
    }
}
