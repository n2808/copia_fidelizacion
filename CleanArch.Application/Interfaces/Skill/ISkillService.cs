﻿using Application.DTOs.Skills;
using Application.Skills.Command.Put;
using Application.Skills.Queries;
using Domain.Models.Skill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Skill
{
    public interface ISkillService
    {
        Task<List<SkillDto>>  GetSkills(int without);
        Task<Domain.Models.Skill.Skills> GetSkillById(int id);
        Task<Domain.Models.Skill.Skills> PostSkills(Domain.Models.Skill.Skills skills);
        Task<Domain.Models.Skill.Skills> PutSkills(PutSkillsCommand skills);
        Task<bool> DeleteSkills(int id);
        Task<Domain.Models.Skill.Skills> PutActivateSkills(PutSkillsActivateCommand skills);
        Task<Domain.Models.Skill.Skills> PutDeActivateSkills(PutSkillsDeActivateCommand skills);
      
    }
}
