﻿using Application.Backoffice.Commands.PutBackoffice;
using Domain.Models.Backoffice;
using System;

namespace Application.Interfaces.Backoffice
{
    public interface IBackofficeGestionService
    {       
        BackofficeGestion PostBackofficeGestion(BackofficeGestion backofficeGestion);
        BackofficeGestion PutBackofficeGestion(PutBackofficeGestionCommand putBackofficeGestionCommand);
        bool DeleteBackofficeGestion(Guid id);
    }
}
