﻿using Application.Backoffice.Commands.PutBackoffice;
using Application.DTOs.Backoffice;
using Domain.Models.Backoffice;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Backoffice
{
    public interface IBackOfficeOperationsService
    {
        Task<List<BackOfficeOperationDto>> GetBackOfficeOperations(int without);
        Task<BackOfficeOperations> GetBackOfficeOperationById(int id);
        Task<BackOfficeOperations> PostBackOfficeOperation(BackOfficeOperations backOfficeOperations);
        Task<BackOfficeOperations> PutBackOfficeOperation(PutBackOfficeOperationCommand backOfficeOperations);
        Task<bool> DeleteBackOfficeOperation(int id);
        Task<BackOfficeOperations> PutActivateBackOfficeOperation(PutBackOfficeOperationActivateCommand backOfficeOperations);
        Task<BackOfficeOperations> PutDeActivateBackOfficeOperation(PutBackOfficeOperationsDeActivateCommand backOfficeOperations);
    }
}
