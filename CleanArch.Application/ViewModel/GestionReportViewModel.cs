﻿using Application.DTOs.GestionReports;
using System.Collections.Generic;

namespace Application.ViewModel
{
    public class GestionReportViewModel
    {
        public List<GestionReportDataViewModel> GestionDataInformation { get; set; }
        public List<GestionReportsDto> gestionReport { get; set; }
    }
}
