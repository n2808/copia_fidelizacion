﻿using Application.DTOs;
using Application.DTOs.User;
using CleanArch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.ViewModel
{
    public class GestionViewModel 
    {
        public List<ClientDataViewModel> ClientInformation { get; set;}
        public CampaignDto Campaign { get; set;}
        public UserDto User { get; set;}
        public List<TipificationDto> Tipification { get; set;}
        public GestionDto gestion { get; set;}
        public List<AppNotificationDto> SMSNotification { get; set;}
        public ClientValidatedDataViewModel ValidationClientfields { get; set;}
        
    }

 
}
