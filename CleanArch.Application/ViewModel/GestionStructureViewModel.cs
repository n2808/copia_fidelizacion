﻿using Application.DTOs.GestionReports;
using System.Collections.Generic;

namespace Application.ViewModel
{
    public class GestionReportDataViewModel
        {
            public string Name { get; set; }
            public int? Order { get; set; } = 1;
            public string Value { get; set; }
        }

        public class GestionReportDataAndGestionReportViewModel
        {
            public GestionReportsDto gestionReportsDto { get; set; }
            public List<GestionReportDataViewModel> gestionReportDataViewModels { get; set; }

            //public ClientValidatedDataViewModel clientValidatedDataViewModel { get; set; }

        }
    
}
