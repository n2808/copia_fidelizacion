﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ViewModel
{
    public class CreateBackOfficeManegementViewModel
    {
        public string TypeOperation { get; set; }
        public string Operation { get; set; }
        public Guid UserId { get; set; }
        public int SegmentId { get; set; }
        public string StatusName {get;set;}
        public string Process { get; set; }
        public int SecondRingLoadId { get; set; }
        public bool Active { get; set; }
        public int ManagementId { get; set; }
    }
}
