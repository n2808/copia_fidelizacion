﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ViewModel
{
    public class GetSecondRingViewModel
    {
        public string CreatedName { get; set; }
        public Guid LoadId { get; set; }
        public string LoadName { get; set; }
        public bool Status { get; set; }
        public int Count { get; set; }
    }
}
