﻿using Application.DTOs;
using CleanArch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Application.ViewModel
{
    public class ClientDataViewModel
    {
        public string Group { get; set;}
        public string Name { get; set;}
        public int? Order { get; set; } = 1;
        public string Value { get; set; }
    }

    public class ClientDataAndGestionViewModel
    {
        public GestionDto GestionDto { get; set; }

        public List<ClientDataViewModel> clientDataViewModels { get; set;  }

        public ClientValidatedDataViewModel clientValidatedDataViewModel { get; set;  }

    }

    public class ClientValidatedDataViewModel
    {
        public string nameClient { get; set; }
        public string documentClient { get; set; }
        public string emailClient { get; set; }
        public string addressClient { get; set; }
        public string birthDateClient { get; set; }
        public string personCounted { get; set; }
        public string nameThird { get; set; }
        public string documentThird { get; set; }
    }

}
