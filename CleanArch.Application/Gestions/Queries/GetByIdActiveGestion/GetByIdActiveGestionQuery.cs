﻿using Application.DTOs.ActiveGestion;
using CleanArch.Application.Interfaces;
using Core.Models.Operation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Queries.GetByIdActiveGestion
{
    public class GetByIdActiveGestionQuery : IRequest<List<ActiveGestionDto>> 
    {
        public int Id { get; set; }
    }

    public class GetByIdActiveGestionQueryHandler : IRequestHandler<GetByIdActiveGestionQuery, List<ActiveGestionDto>>
    {
        private readonly IActiveGestionService _activeGestionService;

        public GetByIdActiveGestionQueryHandler(IActiveGestionService activeGestionService)
        {
            _activeGestionService = activeGestionService;
        }

        public async Task<List<ActiveGestionDto>> Handle(GetByIdActiveGestionQuery request, CancellationToken cancellationToken)
        {
            return  await _activeGestionService.GetGestionActiveById(request.Id);
        }
    }
}
