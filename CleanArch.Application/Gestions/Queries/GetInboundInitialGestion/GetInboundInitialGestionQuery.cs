﻿
using CleanArch.Application.ViewModel;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArchitecture.Application.Common.Interfaces;
using System;
using CleanArch.Application.Interfaces.Auths;

namespace Application.Gestions.Queries.GetInboundInitialGestion
{
    public class GetInboundInitialGestionQuery :  IRequest<GestionViewModel>
    {
        public string DialerServiceCode { get; set; }
        public string Phone { get; set; }
        public string SkillAvaya { get; set; }
    }

    public class GetInboundInitialGestionQueryHandler : IRequestHandler<GetInboundInitialGestionQuery, GestionViewModel>
    {
        private IGestionService _gestionService;
        private IServiceDialerService _IserviceDialer;
        private ITipificationService _tipificationService;
        private IAuthService _authService;
        private ICurrentUserService _currentUserService;
        private readonly IMapper _autoMapper;
        private readonly ISegmentAppNotificationService _segmentAppNotificationService;

        public GetInboundInitialGestionQueryHandler(
            IMapper autoMapper,
            IServiceDialerService serviceDialerService,
            IGestionService gestionService,
            ITipificationService tipificationService,
            IAuthService authService,
            ICurrentUserService currentUserService,
            ISegmentAppNotificationService segmentAppNotificationService

        )
        {
            _autoMapper = autoMapper;
            _IserviceDialer = serviceDialerService;
            _gestionService = gestionService;
            _tipificationService = tipificationService;
            _currentUserService = currentUserService;
            _authService = authService;
            _segmentAppNotificationService = segmentAppNotificationService;
        }

        public async Task<GestionViewModel> Handle(GetInboundInitialGestionQuery request, CancellationToken cancellationToken)
        {
            var  cpDto   = _IserviceDialer
                            .GetCampaignByServiceCode(request.DialerServiceCode);
            var userDto = _authService
                            .GetUserById(_currentUserService.GetUserInfo()?.Id);
            var d = await _gestionService
                            .GetClientsDataFields(cpDto, request, userDto);



            var vm = new GestionViewModel
            {
                Campaign = cpDto,
                User = userDto,
                Tipification = _tipificationService.GetTipificationsByDialerService(request.DialerServiceCode),
                gestion = d.GestionDto,
                //ValidationClientfields = d.clientValidatedDataViewModel,
                SMSNotification = _segmentAppNotificationService.GetAppsBySegmentId(cpDto.SegmentId)
            };
            return vm;
        }

 
    }
}
