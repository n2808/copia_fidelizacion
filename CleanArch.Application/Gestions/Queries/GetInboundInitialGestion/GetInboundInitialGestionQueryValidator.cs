﻿using Application.Gestions.Queries.GetInboundInitialGestion;
using Domain.Interfaces;
using FluentValidation;

namespace Application.Courses.Commands
{
    public class GetInboundInitialGestionQueryValidator : AbstractValidator<GetInboundInitialGestionQuery>
    {
        private readonly IAuthRepository _authRepository;
        private readonly IGestionRepository _gestionRepository;
        public GetInboundInitialGestionQueryValidator(IAuthRepository authRepository, IGestionRepository gestionRepository)
        {
            _authRepository = authRepository;
            _gestionRepository = gestionRepository;
            RuleFor(x => x.Phone).NotNull();
            RuleFor(c => c.DialerServiceCode)
                .NotNull()
                .WithMessage("El VDN es obligatorio")
                .Must(_gestionRepository.ValidateDialerServiceCode)
                .WithMessage("No se ha encontrado el VDN");
           


        }
    }
}
