﻿using Application.DTOs.GestionReports;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Queries.GetGestionReportTification
{
    public class GetGestionReportTificationQuery : IRequest<List<GestionReportsDto>>
    {
        public string tipificationName { get; set; }
    }

    public class GetGestionReportTificationQueryHandler : IRequestHandler<GetGestionReportTificationQuery, List<GestionReportsDto>>
    {
        private readonly IGestionReportService _gestionReportService;

        public GetGestionReportTificationQueryHandler(IGestionReportService gestionReportService)
        {
            _gestionReportService = gestionReportService;
        }
        public async Task<List<GestionReportsDto>> Handle(GetGestionReportTificationQuery request, CancellationToken cancellationToken)
        {
            return _gestionReportService.GetGestionTipification(request.tipificationName);
        }
    }
}
