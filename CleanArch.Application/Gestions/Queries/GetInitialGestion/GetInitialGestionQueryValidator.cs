﻿using Application.Gestions.Queries.GetInitialGestion;
using Domain.Interfaces;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Courses.Commands
{
    public class GetInitialGestionQueryValidator : AbstractValidator<GetInitialGestionQuery>
    {
        private readonly IAuthRepository _authRepository;
        private readonly IGestionRepository _gestionRepository;
        public GetInitialGestionQueryValidator(IAuthRepository authRepository, IGestionRepository gestionRepository)
        {
            _authRepository = authRepository;
            _gestionRepository = gestionRepository;

            RuleFor(c => c.Login).NotNull().WithMessage("El login es obligatorio");
            RuleFor(c => c.Login).Must(_authRepository.ValidateByLogin).WithMessage("El login no existe");
            RuleFor(c => c.DialerServiceCode)
                .NotNull()
                .WithMessage("El VDN es obligatorio")
                .Must(_gestionRepository.ValidateDialerServiceCode)
                .WithMessage("No se ha encontrado el VDN");
           


        }
    }
}
