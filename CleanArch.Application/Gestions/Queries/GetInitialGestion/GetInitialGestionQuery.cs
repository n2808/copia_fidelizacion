﻿using Application.Common.interfaces;
using CleanArch.Application.ViewModel;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArch.Application.Interfaces.Auths;

namespace Application.Gestions.Queries.GetInitialGestion
{
    public class GetInitialGestionQuery :  IRequest<GestionViewModel>
    {
        public string DialerServiceCode { get; set; }
        public string Login { get; set; }
        public int? ClientId { get; set; }
        public string SourceId { get; set; }
        public string Phone { get; set; }
    }

    public class GetInitialGestionQueryHandler : IRequestHandler<GetInitialGestionQuery, GestionViewModel>
    {
        private  IU27ApplicationDBContext _context;
        private IGestionService _gestionService;
        private IServiceDialerService _IserviceDialer;
        private ITipificationService _tipificationService;
        private IAuthService _authService;
        private readonly IMapper _autoMapper;
        private readonly ISegmentAppNotificationService _segmentAppNotificationService;

        public GetInitialGestionQueryHandler(
            IU27ApplicationDBContext context,
            IMapper autoMapper,
            IServiceDialerService serviceDialerService,
            IGestionService gestionService,
            ITipificationService tipificationService,
            IAuthService authService,
            ISegmentAppNotificationService segmentAppNotificationService

        )
        {
            _context = context;
            _autoMapper = autoMapper;
            _IserviceDialer = serviceDialerService;
            _gestionService = gestionService;
            _tipificationService = tipificationService;
            _authService = authService;
            _segmentAppNotificationService = segmentAppNotificationService;
        }

        public async Task<GestionViewModel> Handle(GetInitialGestionQuery request, CancellationToken cancellationToken)
        {

            
            var  cpDto   = _IserviceDialer
                            .GetCampaignByServiceCode(request.DialerServiceCode);

            var userDto = _authService
                            .GetUserByLogin(request.Login);

            var d =  await _gestionService
                        .GetClientsDataFields(cpDto, request, userDto);



            var vm = new GestionViewModel
            {
                ClientInformation = d.clientDataViewModels,
                Campaign = cpDto,
                User = userDto,
                Tipification = _tipificationService.GetTipificationsByDialerService(request.DialerServiceCode),
                gestion = d.GestionDto,
                ValidationClientfields = d.clientValidatedDataViewModel,
                SMSNotification = _segmentAppNotificationService.GetAppsBySegmentId(cpDto.SegmentId)
            };
            return vm;
        }

  
    }
}
