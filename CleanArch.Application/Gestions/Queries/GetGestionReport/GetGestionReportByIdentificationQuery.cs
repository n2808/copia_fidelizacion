﻿using Application.DTOs.GestionReports;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Queries.GetGestionReport
{
    public class GetGestionReportByIdentificationQuery : IRequest<List<GestionReportsDto>>
    {
        public string Identification { get; set; }
    }

    public class GetGestionReportByIdentificationQueryHandler : IRequestHandler<GetGestionReportByIdentificationQuery, List<GestionReportsDto>>
    {
        private readonly IGestionReportService _gestionReportService;
        private readonly IMapper _mapper;
        public GetGestionReportByIdentificationQueryHandler(IGestionReportService gestionReportService, IMapper mapper)
        {
            _gestionReportService = gestionReportService;
            _mapper = mapper;
        }
        public async Task<List<GestionReportsDto>> Handle(GetGestionReportByIdentificationQuery request, CancellationToken cancellationToken)
        {
            return await _gestionReportService.GetGestionReportsIdentification(request.Identification);
        }
    }
}
