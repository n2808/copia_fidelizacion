﻿using Application.DTOs.GestionReports;
using Application.ViewModel;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Queries.GetGestionReport
{
    public class GetGestionReportQuery : IRequest<List<GestionReportsDto>>
    {
        public string where { get; set; }
        public int SegmentId { get; set; }
    }

    public class GetGestionReportQueryHandler : IRequestHandler<GetGestionReportQuery, List<GestionReportsDto>>
    {
        private readonly IGestionReportService _gestionReportService;

        private readonly IMapper _mapper;

        public GetGestionReportQueryHandler(IGestionReportService gestionReportService, IMapper mapper)
        {
            _gestionReportService = gestionReportService;
            _mapper = mapper;
        }

        public async Task<List<GestionReportsDto>> Handle(GetGestionReportQuery request, CancellationToken cancellationToken)
        {
            WhereViewModel whereViewModel = JsonSerializer.Deserialize<WhereViewModel>(request.where);

            var gestionReports = _gestionReportService.GetGestionReportsSegmentId(request.SegmentId, whereViewModel);

            return gestionReports;
        }
    }

}
