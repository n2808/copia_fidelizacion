﻿using Application.DTOs.GestionReports;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Queries.GetGestionReport
{
    public class GetGestionReportByPhoneQuery : IRequest<List<GestionReportsDto>>
    {
        public string Phone { get; set; }
    }

    public class GetGestionReportByPhoneQueryHandler : IRequestHandler<GetGestionReportByPhoneQuery, List<GestionReportsDto>>
    {
        private readonly IGestionReportService _gestionReportService;
        private readonly IMapper _mapper;
        public GetGestionReportByPhoneQueryHandler(IGestionReportService gestionReportService, IMapper mapper)
        {
            _gestionReportService = gestionReportService;
            _mapper = mapper;
        }
        public async Task<List<GestionReportsDto>> Handle(GetGestionReportByPhoneQuery request, CancellationToken cancellationToken)
        {
           return await _gestionReportService.GetGestionReportsPhone(request.Phone);
        }
    }
}
