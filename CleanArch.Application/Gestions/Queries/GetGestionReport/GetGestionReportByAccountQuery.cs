﻿using Application.DTOs.GestionReports;
using AutoMapper;
using CleanArch.Application.Interfaces;
using Core.Models.Operation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Queries.GetGestionReport
{
    public class GetGestionReportByAccountQuery : IRequest<List<GestionReportsDto>>
    {
        public string Account { get; set; }
    }

    public class GetGestionReportByAccountQueryHandler : IRequestHandler<GetGestionReportByAccountQuery, List<GestionReportsDto>>
    {
        private readonly IGestionReportService _gestionReportService;
        private readonly IMapper _mapper;

        public GetGestionReportByAccountQueryHandler(IGestionReportService gestionReportService, IMapper mapper)
        {
            _gestionReportService = gestionReportService;
            _mapper = mapper;
        }
        public async Task<List<GestionReportsDto>> Handle(GetGestionReportByAccountQuery request, CancellationToken cancellationToken)
        {
           return await _gestionReportService.GetGestionReportsAccount(request.Account);
        }
    }
}
