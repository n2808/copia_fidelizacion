﻿
using CleanArch.Application.ViewModel;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArchitecture.Application.Common.Interfaces;
using System;
using CleanArch.Application.Interfaces.Auths;
using Application.DTOs;
using Application.Core.Exceptions;

namespace Application.Gestions.Queries.GetRecoveryGestionQuery
{
    public class GetRecoveryGestionQuery : IRequest<GestionViewModel>
    {
        public int Id { get; set; }
    }

    public class GetRecoveryGestionQueryHandler : IRequestHandler<GetRecoveryGestionQuery, GestionViewModel>
    {
        private IGestionService _gestionService;
        private IServiceDialerService _IserviceDialer;
        private ITipificationService _tipificationService;
        private IAuthService _authService;
        private ICurrentUserService _currentUserService;
        private IGestionReportService _gestionReportService;

        private readonly IMapper _autoMapper;
        private readonly ISegmentAppNotificationService _segmentAppNotificationService;

        public GetRecoveryGestionQueryHandler(
            IMapper autoMapper,
            IServiceDialerService serviceDialerService,
            IGestionService gestionService,
            ITipificationService tipificationService,
            IAuthService authService,
            ICurrentUserService currentUserService,
            ISegmentAppNotificationService segmentAppNotificationService,
            IGestionReportService gestionReportService

        )
        {
            _autoMapper = autoMapper;
            _IserviceDialer = serviceDialerService;
            _gestionService = gestionService;
            _tipificationService = tipificationService;
            _currentUserService = currentUserService;
            _authService = authService;
            _segmentAppNotificationService = segmentAppNotificationService;
            _gestionReportService = gestionReportService;
        }

        public async Task<GestionViewModel> Handle(GetRecoveryGestionQuery request, CancellationToken cancellationToken)
        {

            var gestion = await _gestionReportService.GetGestionById(request.Id, cancellationToken) ?? throw new NotFoundException("Gestion", request.Id);


            return new GestionViewModel
            {
                Campaign = _IserviceDialer.GetCampaignByServiceCode(gestion.Dialercode),
                User = _authService.GetUserById(_currentUserService.GetUserInfo()?.Id),
                Tipification = _tipificationService.GetTipificationsByDialerService(gestion.Dialercode),
                gestion = _autoMapper.Map<GestionDto>(gestion),
                SMSNotification = _segmentAppNotificationService.GetAppsBySegmentId(gestion.SegmentId)
            };
          
        }


    }
}
