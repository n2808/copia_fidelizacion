﻿using Application.Gestions.Commands;
using Application.Gestions.Queries.GetInitialGestion;
using CleanArch.Application.Interfaces;
using Domain.Interfaces;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Courses.Commands
{
    public class EndGestionCommandValidator : AbstractValidator<EndGestionCommand>
    {
        private readonly IAuthRepository _authRepository;
        private readonly IGestionService _gestionService;
        private readonly IActiveGestionService _activeGestionService;
        private readonly IGestionReportService _gestionReportService;
        public EndGestionCommandValidator(
            IAuthRepository authRepository, 
            IGestionService gestionService,
            IActiveGestionService activeGestionService,
            IGestionReportService gestionReportService
            )
        {
            _authRepository = authRepository;
            _gestionService = gestionService;
            _gestionReportService = gestionReportService;
            _activeGestionService = activeGestionService;
            
            RuleFor(c => c.TipificationId).NotNull();
            RuleFor(c => c.AgentId).NotNull();
            RuleFor(c => c.GestionId)
                .NotNull()
                .Must(_activeGestionService.CheckAtiveGestionById).WithMessage("La gestion no existe")
                .Must(_gestionReportService.AllowEndGestion).When(c => c.Recovery == false).WithMessage("La gestion ya fue tipificada");


        }

     
    }
}
