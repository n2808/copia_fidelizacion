﻿using Application.Interfaces;
using Application.Interfaces.Reschedule;
using Application.ViewModel;
using AutoMapper;
using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModel;
using Core.Models.Operation;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Commands
{
    public class EndGestionCommand : IRequest
    {
        public int GestionId { get; set; }
        public int TipificationId { get; set; }
        public Guid AgentId { get; set; }
        public Guid? RescheduleId { get; set; }
        public bool Recovery { get; set; } = false;
        public EndGestionViewModel Gestion { get; set; }
        public OffersViewModel Offers { get; set; }
        public ManualBaseViewModel? ManualBase { get; set; }
    }


    public class EndGestionCommandHandler : IRequestHandler<EndGestionCommand>
    {

        private readonly IActiveGestionService _activeGestionService;
        private readonly IGestionReportsOffersService _gestionReportsOffersService;
        private readonly ITipificationService _tipificationService;
        private readonly IRescheduleService _rescheduleService;
        private readonly IGestionRescheduleService _gestionRescheduleService;
        private readonly IBackOfficeManagementsService _managementsBackOfficeService;
        private readonly IMapper _mapper;

        public EndGestionCommandHandler(
            IGestionService gestionService,
            IRescheduleService rescheduleService,
            IActiveGestionService activeGestionService,
            ITipificationService tipificationService,
            IGestionReportsOffersService gestionReportsOffersService,
            IGestionRescheduleService gestionRescheduleService,
            IBackOfficeManagementsService managementsBackOfficeService,
            IMapper mapper)
        {

            _activeGestionService = activeGestionService;
            _gestionReportsOffersService = gestionReportsOffersService;
            _tipificationService = tipificationService;
            _rescheduleService = rescheduleService;
            _gestionRescheduleService = gestionRescheduleService;
            _managementsBackOfficeService = managementsBackOfficeService;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(EndGestionCommand request, CancellationToken cancellationToken)
        {
            var activeGestion = await _activeGestionService.updateGestion(request);

            var listOffers = await _gestionReportsOffersService.GetOffersByGestionReportsId(activeGestion.Id);

            if (listOffers.Count > 0) await _gestionReportsOffersService.DeleteOffers(listOffers);

            await _gestionReportsOffersService.AddOffers(request.Offers, activeGestion);


            await _rescheduleService.Reschedule(activeGestion);

            //ManagementBackOffice
            await _managementsBackOfficeService.PostManagement(activeGestion);
            // Recovery Gestion.

            if(request.ManualBase.Id != null) await _managementsBackOfficeService.UpdateManagementSecondRing(request.ManualBase, activeGestion.Id, request.TipificationId);

            var tipification = await _tipificationService.GetTipificationById(request.TipificationId);

            var status = tipification.IsContacted == "1" ? true : false;

            if (request.Recovery && request.RescheduleId != null) await _gestionRescheduleService.UpdateGestionReschedule((Guid)request.RescheduleId, request.AgentId, status);           

            return Unit.Value;
        }

    }



}
