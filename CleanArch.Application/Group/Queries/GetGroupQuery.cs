﻿using Application.DTOs.Group;
using Application.Interfaces.Group;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Group.Queries
{
    public class GetGroupQuery : IRequest<List<GroupDto>>
    {
        public int without { get; set; }
    }

    public class GetGroupQueryHandler : IRequestHandler<GetGroupQuery, List<GroupDto>>
    {
        private readonly IGroupService _groupService;

        public GetGroupQueryHandler(IGroupService groupService)
        {
            _groupService = groupService;
        }
        public async Task<List<GroupDto>> Handle(GetGroupQuery request, CancellationToken cancellationToken)
        {
            List<GroupDto> groupDtos = _groupService.GetGroups(request.without);

            return groupDtos;
        }
    }
}
