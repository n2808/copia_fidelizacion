﻿using Application.DTOs.Group;
using Application.Interfaces.Group;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Group.Command
{
    public class PutGroupCommand : IRequest<GroupDto>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string UserUpdate { get; set; }

        public bool Status { get; set; }
    }

    public class PutGroupCommandHandler : IRequestHandler<PutGroupCommand, GroupDto>
    {
        private readonly IGroupService _groupService;
        private readonly IMapper _mapper;

        public PutGroupCommandHandler(IGroupService groupService, IMapper mapper)
        {
            _groupService = groupService;
            _mapper = mapper;
        }

        public async Task<GroupDto> Handle(PutGroupCommand request, CancellationToken cancellationToken)
        {
            Domain.Models.Groups.Group group = _groupService.PutGroup(request);
            return _mapper.Map<GroupDto>(group);
        }
    }
}
