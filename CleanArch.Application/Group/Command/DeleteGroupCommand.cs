﻿using Application.Interfaces.Group;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Group.Command
{
    public class DeleteGroupCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class DeleteGroupCommandHandler : IRequestHandler<DeleteGroupCommand, bool>
    {
        private readonly IGroupService _groupService;

        public DeleteGroupCommandHandler(IGroupService groupService)
        {
            _groupService = groupService;
        }
        public async Task<bool> Handle(DeleteGroupCommand request, CancellationToken cancellationToken)
        {
            _groupService.DeleteGroup(request.Id);

            return true;
        }
    }
}
