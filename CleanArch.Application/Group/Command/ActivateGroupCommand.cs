﻿using Application.Interfaces.Group;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Group.Command
{
    public class ActivateGroupCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class ActivateGroupCommandHandler : IRequestHandler<ActivateGroupCommand, bool>
    {
        private readonly IGroupService _groupService;
        public ActivateGroupCommandHandler(IGroupService groupService)
        {
            _groupService = groupService;
        }

        public async Task<bool> Handle(ActivateGroupCommand request, CancellationToken cancellationToken)
        {
            _groupService.ActivateGroup(request.Id);

            return true;
        }
    }

}
