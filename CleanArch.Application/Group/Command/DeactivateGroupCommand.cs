﻿using Application.Interfaces.Group;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Group.Command
{
    public class DeactivateGroupCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class DeactivateGroupCommandHandler : IRequestHandler<DeactivateGroupCommand, bool>
    {
        private readonly IGroupService _groupService;
        public DeactivateGroupCommandHandler(IGroupService groupService)
        {
            _groupService = groupService;
        }

        public async Task<bool> Handle(DeactivateGroupCommand request, CancellationToken cancellationToken)
        {
            _groupService.DeactivateGroup(request.Id);

            return true;
        }
    }
}
