﻿using FluentValidation;

namespace Application.Group.Command
{
    public class PostGroupCommandValidator : AbstractValidator<PostGroupCommand>
    {
        public PostGroupCommandValidator()
        {
            RuleFor(x => x.Name)
                          .NotEmpty()
                          .WithMessage("El nombre no puede estar vacío")
                          .MaximumLength(255)
                          .WithMessage("La longitud de caracteres no debe ser mayor a 255");

            RuleFor(x => x.DisplayName)
                          .NotEmpty()
                          .WithMessage("El DisplayName no puede estar vacío")
                          .MaximumLength(255)
                          .WithMessage("La longitud de caracteres no debe ser mayor a 255");
           
        }
    }
}
