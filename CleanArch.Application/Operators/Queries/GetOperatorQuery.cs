﻿using Application.DTOs.Operators;
using Application.Interfaces.Operators;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Operators.Queries
{
    public class GetOperatorQuery : IRequest<List<OperatorDto>>
    {
        public int Without { get; set; }

    }

    public class GetOperatorQueryHandler : IRequestHandler<GetOperatorQuery, List<OperatorDto>>
    {
        private readonly IOperatorService _operatorService;
        public GetOperatorQueryHandler(IOperatorService operatorService)
        {
            _operatorService = operatorService;
        }

        public async Task<List<OperatorDto>> Handle(GetOperatorQuery request, CancellationToken cancellationToken)
        {
            List<OperatorDto> operatorDtos = _operatorService.GetOperators(request.Without);

            return operatorDtos;
        }

    }
}
