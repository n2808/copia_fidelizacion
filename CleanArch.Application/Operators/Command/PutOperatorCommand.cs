﻿using Application.DTOs.Operators;
using Application.Interfaces.Operators;
using AutoMapper;
using Domain.Models.Operators;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Operators.Command
{
    public class PutOperatorCommand : IRequest<OperatorDto>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; }
    }

    public class PutOperatorCommandHandler : IRequestHandler<PutOperatorCommand, OperatorDto>
    {
        private readonly IOperatorService _operatorService;
        private readonly IMapper _mapper;
        public PutOperatorCommandHandler(IOperatorService operatorService, IMapper mapper)
        {
            _operatorService = operatorService;
            _mapper = mapper;
        }
        public async Task<OperatorDto> Handle(PutOperatorCommand request, CancellationToken cancellationToken)
        {
            Operator _operator = _operatorService.PutOperator(request);

            return _mapper.Map<OperatorDto>(_operator);

        }
    }
}
