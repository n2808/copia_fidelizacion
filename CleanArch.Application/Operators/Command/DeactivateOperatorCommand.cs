﻿using Application.Interfaces.Operators;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Operators.Command
{
    public class DeactivateOperatorCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class DeactivateOperatorCommandHandler : IRequestHandler<DeactivateOperatorCommand, bool>
    {

        private readonly IOperatorService _operatorService;

        public DeactivateOperatorCommandHandler(IOperatorService operatorService)
        {
            _operatorService = operatorService;

        }

        public async Task<bool> Handle(DeactivateOperatorCommand request, CancellationToken cancellationToken)
        {
            _operatorService.DeactivateOperator(request.Id);

            return true;
        }
    }
}
