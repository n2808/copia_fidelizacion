﻿using Application.DTOs.Operators;
using Application.Interfaces.Operators;
using AutoMapper;
using Domain.Models.Operators;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Operators.Command
{
    public class PostOperatorCommand : IRequest<OperatorDto>
    {    
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; }
    }

    public class PostOperatorCommandHandler : IRequestHandler<PostOperatorCommand, OperatorDto>
    {
        private readonly IOperatorService _operatorService;
        private readonly IMapper _mapper;
        public PostOperatorCommandHandler(IOperatorService operatorService, IMapper mapper)
        {
            _operatorService = operatorService;
            _mapper = mapper;
        }
        public async Task<OperatorDto> Handle(PostOperatorCommand request, CancellationToken cancellationToken)
        {
            Operator _operator = _operatorService.PostOperator(_mapper.Map<Operator>(request));

            return _mapper.Map<OperatorDto>(_operator);
        }
    }
}
