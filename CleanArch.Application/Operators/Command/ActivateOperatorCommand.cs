﻿using Application.Interfaces.Operators;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Operators.Command
{
    public class ActivateOperatorCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class ActivateOperatorCommandHandler : IRequestHandler<ActivateOperatorCommand, bool>
    {

        private readonly IOperatorService _operatorService;

        public ActivateOperatorCommandHandler(IOperatorService operatorService)
        {
            _operatorService = operatorService;

        }

        public async Task<bool> Handle(ActivateOperatorCommand request, CancellationToken cancellationToken)
        {
            _operatorService.ActivateOperator(request.Id);

            return true;
        }
    }
}
