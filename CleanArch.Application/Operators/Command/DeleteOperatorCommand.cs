﻿using Application.Interfaces.Operators;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Operators.Command
{
    public class DeleteOperatorCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class DeleteOperatorCommanddHandler : IRequestHandler<DeleteOperatorCommand, bool>
    {

        private readonly IOperatorService _operatorService;
 
        public DeleteOperatorCommanddHandler(IOperatorService operatorService)
        {
            _operatorService = operatorService;

        }

        public async Task<bool> Handle(DeleteOperatorCommand request, CancellationToken cancellationToken)
        {
            _operatorService.DeleteOperator(request.Id);

            return true;
        }
    }
}
