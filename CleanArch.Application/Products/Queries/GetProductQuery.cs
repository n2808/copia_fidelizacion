﻿using Application.DTOs.Products;
using Application.Interfaces.Products;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Products.Queries
{
    public class GetProductQuery : IRequest<List<ProductDto>>
    {
        public int Without { get; set; }

    }

    public class GetPlanQueryHandler : IRequestHandler<GetProductQuery, List<ProductDto>>
    {
        private readonly IProductService _productService;
        public GetPlanQueryHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<List<ProductDto>> Handle(GetProductQuery request, CancellationToken cancellationToken)
        {
            List<ProductDto> productsDtos = _productService.GetProducts(request.Without);

            return productsDtos;
        }

    }
}
