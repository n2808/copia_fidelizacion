﻿using Application.Interfaces.Products;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Products.Command
{
    public class DeactivateProductCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class DeactivateProductCommandHandler : IRequestHandler<DeactivateProductCommand, bool>
    {
        private readonly IProductService _productService;
        public DeactivateProductCommandHandler(IProductService productService)
        {
            _productService = productService;
        }
        public async Task<bool> Handle(DeactivateProductCommand request, CancellationToken cancellationToken)
        {
            _productService.DeactivateProduct(request.Id);

            return true;
        }
    }
}
