﻿using FluentValidation;

namespace Application.Products.Command
{
    public class PutProductCommandValidator : AbstractValidator<PutProductCommand>
    {
        public PutProductCommandValidator()
        {
            RuleFor(x => x.Name)
                      .NotEmpty()
                      .WithMessage("El nombre no puede estar vacío")
                      .MaximumLength(255)
                      .WithMessage("La longitud de caracteres no debe ser mayor a 255");

            RuleFor(x => x.DisplayName)
                           .NotEmpty()
                           .WithMessage("El campo mostrar nombre no puede estar vacío")
                           .MaximumLength(255)
                           .WithMessage("La longitud de caracteres no debe ser mayor a 255");


            RuleFor(x => x.GeneralLegalText)
                           .NotEmpty()
                           .WithMessage("El campo texto legal general no puede estar vacío");

        }
    }
}
