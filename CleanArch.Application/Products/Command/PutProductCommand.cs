﻿using Application.DTOs.Products;
using Application.Interfaces.Products;
using AutoMapper;
using Domain.Models.Products;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Products.Command
{
    public class PutProductCommand : IRequest<ProductDto>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string GeneralLegalText { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; }
    }

    public class PutProductCommandHandler : IRequestHandler<PutProductCommand, ProductDto>
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        public PutProductCommandHandler(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }
        public async Task<ProductDto> Handle(PutProductCommand request, CancellationToken cancellationToken)
        {
            Product product = _productService.PutProduct(request);

            return _mapper.Map<ProductDto>(product);

        }
    }
}
