﻿using Application.Interfaces.Products;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Products.Command
{
    public class ActivateProductCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class ActivateProductCommandHandler : IRequestHandler<ActivateProductCommand, bool>
    {
        private readonly IProductService _productService;
        public ActivateProductCommandHandler(IProductService productService)
        {
            _productService = productService;
        }
        public async Task<bool> Handle(ActivateProductCommand request, CancellationToken cancellationToken)
        {
            _productService.ActivateProduct(request.Id);

            return true;
        }
    }
}
