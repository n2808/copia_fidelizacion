﻿using Application.DTOs.Products;
using Application.Interfaces.Products;
using AutoMapper;
using Domain.Models.Products;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Products.Command
{
    public class PostProductCommand : IRequest<ProductDto>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string GeneralLegalText { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; }
    }

    public class PostProductCommandHandler : IRequestHandler<PostProductCommand, ProductDto>
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        public PostProductCommandHandler(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }
        public async Task<ProductDto> Handle(PostProductCommand request, CancellationToken cancellationToken)
        {
            Product product = _productService.PostProduct(_mapper.Map<Product>(request));

            return _mapper.Map<ProductDto>(product);
        }
    }
}
