﻿using Application.Interfaces.Products;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Products.Command
{
    public class DeleteProductCommand : IRequest<bool>
    {
        public Guid Id { get; set; }

    }

    public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, bool>
    {

        private readonly IProductService _productService;
        public DeleteProductCommandHandler(IProductService productService)
        {
            _productService = productService;

        }

        public async Task<bool> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            _productService.DeleteProduct(request.Id);

            return true;
        }
    }
}
