﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries.NewCities
{
    public class GetNewCitiesQuery : IRequest<List<NewCityDto>>
    {
        public int without { get; set; }
    }

    public class GetNewCitiesQueryHandler : IRequestHandler<GetNewCitiesQuery, List<NewCityDto>>
    {
        private readonly INewCityService _newCityService;
        public GetNewCitiesQueryHandler(INewCityService newCityService)
        {
            _newCityService = newCityService;
        }
        public async Task<List<NewCityDto>> Handle(GetNewCitiesQuery request, CancellationToken cancellationToken)
        {
            return await _newCityService.GetNewCity(request.without);
        }
    }
}
