﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries.NewCities
{
    public class GetNewCitiesByIdQuery : IRequest<NewCityDto>
    {
        public int Id { get; set; }
    }
    public class GetNewCitiesByIdQueryHandler : IRequestHandler<GetNewCitiesByIdQuery, NewCityDto>
    {
        private readonly INewCityService _newCityService;
        private readonly IMapper _mapper;

        public GetNewCitiesByIdQueryHandler(INewCityService newCityService, IMapper mapper)
        {
            _newCityService = newCityService;
            _mapper = mapper;
        }

        public async Task<NewCityDto> Handle(GetNewCitiesByIdQuery request, CancellationToken cancellationToken)
        {
            var department = await _newCityService.GetNewCityById(request.Id);
            return _mapper.Map<NewCityDto>(department);
        }
    }
}
