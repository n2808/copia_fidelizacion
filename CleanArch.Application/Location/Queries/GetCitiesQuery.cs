﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries
{
    public class GetCitiesQuery : IRequest<List<CityDto>> { }
    

    public class GetCitiesQueryHandler : IRequestHandler<GetCitiesQuery, List<CityDto>>
    {

        private readonly ICityService _cityService;

        public GetCitiesQueryHandler(ICityService cityService)
        {
            _cityService = cityService;
        }
        public async Task<List<CityDto>> Handle(GetCitiesQuery request, CancellationToken cancellationToken)
        {
            return _cityService.GetCities();
        }
    }
}
