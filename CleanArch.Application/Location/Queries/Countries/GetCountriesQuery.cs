﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries.Countries
{
    public class GetCountriesQuery : IRequest<List<CountryDto>>
    {
    }

    public class GetCountriesQueryHandler : IRequestHandler<GetCountriesQuery, List<CountryDto>>
    {
        private readonly ICountryService _countryService;
        public GetCountriesQueryHandler(ICountryService countryService)
        {
            _countryService = countryService;
        }
        public async Task<List<CountryDto>> Handle(GetCountriesQuery request, CancellationToken cancellationToken)
        {
            return await _countryService.GetCountries();
        }
    }
}
