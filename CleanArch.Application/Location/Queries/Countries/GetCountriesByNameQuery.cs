﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using Core.Models.location;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries.Countries
{
    public class GetCountriesByNameQuery : IRequest<List<CountryDepartmentDto>>
    {
        public string Name { get; set; }
    }
    public class GetCountriesByNameQueryHandler : IRequestHandler<GetCountriesByNameQuery, List<CountryDepartmentDto>>
    {
        private readonly ICountryService _countryService;
        private readonly IMapper _mapper;

        public GetCountriesByNameQueryHandler(ICountryService countryService, IMapper mapper)
        {
            _countryService = countryService;
            _mapper = mapper;
        }
        public async Task<List<CountryDepartmentDto>> Handle(GetCountriesByNameQuery request, CancellationToken cancellationToken)
        {
            return await _countryService.GetCountriesByCode(request.Name);
        }
    }
}
