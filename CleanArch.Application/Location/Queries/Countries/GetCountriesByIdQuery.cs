﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries.Countries
{
    public class GetCountriesByIdQuery : IRequest<CountryDto>
    {
        public Guid Id { get; set; }
    }

    public class GetCountriesByIdQueryHandler : IRequestHandler<GetCountriesByIdQuery, CountryDto>
    {
        private readonly ICountryService _countryService;
        private readonly IMapper _mapper;

        public GetCountriesByIdQueryHandler(ICountryService countryService, IMapper mapper)
        {
            _countryService = countryService;
            _mapper = mapper;
        }
        public async Task<CountryDto> Handle(GetCountriesByIdQuery request, CancellationToken cancellationToken)
        {
            var country = await _countryService.GetCountriesById(request.Id);
            return _mapper.Map<CountryDto>(country);
        }
    }
}
