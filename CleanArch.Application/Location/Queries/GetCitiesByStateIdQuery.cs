﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries
{
    public class GetCitiesByStateIdQuery : IRequest<List<CityDto>>
    {
        public int id { get; set; }
    }

    public class GetCitiesByStateIdQueryHandler : IRequestHandler<GetCitiesByStateIdQuery, List<CityDto>>
    {

        private readonly ICityService _cityService;

        public GetCitiesByStateIdQueryHandler(ICityService cityService)
        {
            _cityService = cityService;
        }
       

        public async Task<List<CityDto>> Handle(GetCitiesByStateIdQuery request, CancellationToken cancellationToken)
        {
            return _cityService.GetCitiesByState(request.id);
        }
    }
}
