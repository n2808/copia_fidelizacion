﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries.Departments
{
    public class GetDepartmentsByIdQuery : IRequest<DepartmentDto>
    {
        public int Id { get; set; }
    }

    public class GetDepartmentsByIdQueryHandler : IRequestHandler<GetDepartmentsByIdQuery, DepartmentDto>
    {
        private readonly IDepartmentService _departmentService;
        private readonly IMapper _mapper;

        public GetDepartmentsByIdQueryHandler(IDepartmentService departmentService, IMapper mapper)
        {
            _departmentService = departmentService;
            _mapper = mapper;
        }

        public async Task<DepartmentDto> Handle(GetDepartmentsByIdQuery request, CancellationToken cancellationToken)
        {
            var department = await _departmentService.GetDepartmentById(request.Id);
            return _mapper.Map<DepartmentDto>(department);
        }
    }
}
