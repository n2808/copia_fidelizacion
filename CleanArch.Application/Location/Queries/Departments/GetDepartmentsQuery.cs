﻿using Application.DTOs.location;
using Application.Interfaces.Location;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Location.Queries.Departments
{
    public class GetDepartmentsQuery : IRequest<List<DepartmentDto>>
    {
        public int without { get; set; }
    }

    public class GetDepartmentsQueryHandler : IRequestHandler<GetDepartmentsQuery, List<DepartmentDto>>
    {
        private readonly IDepartmentService _departmentService;
        public GetDepartmentsQueryHandler(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }
        public async Task<List<DepartmentDto>> Handle(GetDepartmentsQuery request, CancellationToken cancellationToken)
        {
            return await _departmentService.GetDepartment(request.without);
        }
    }
}
