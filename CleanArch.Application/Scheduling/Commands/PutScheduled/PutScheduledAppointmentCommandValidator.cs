﻿using CleanArch.Application.Interfaces;
using FluentValidation;
using System;

namespace Application.Scheduling.Commands.PutScheduled
{
    public class PutScheduledAppointmentCommandValidator : AbstractValidator<PutScheduledAppointmentCommand>
    {
        private readonly IActiveGestionService _activeGestionService;

        public PutScheduledAppointmentCommandValidator(IActiveGestionService activeGestionService)
        {
            _activeGestionService = activeGestionService;

            RuleFor(x => x.ActiveGestionId)
                .NotNull()
                .Must(_activeGestionService.CheckAtiveGestionById)
                .WithMessage("La gestión no existe");

            RuleFor(x => x.WorkDay)
            .NotEmpty()
            .WithMessage("La jornada no puede estar vacía");

            RuleFor(x => x.AppointmentDate)
              .NotEmpty().WithMessage("La fecha de Agendamiento no puede estar vacía");


            RuleFor(x => x.NameOfReceive)
                .NotEmpty()
                .WithMessage("El nombre de quien recibe la visita no puede estar vacio");

            RuleFor(x => x.PhoneOfReceive)
                .NotEmpty()
                .WithMessage("El teléfono de quien recibe la visita no puede estar vacio");

            RuleFor(x => x.EmailOfReceive)
                .NotEmpty()
                .WithMessage("El correo de quien recibe la visita no puede estar vacio");
        }
    }
}
