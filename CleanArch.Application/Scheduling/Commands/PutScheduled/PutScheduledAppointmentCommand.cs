﻿using Application.DTOs.Scheduling;
using Application.Interfaces.Scheduling;
using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace Application.Scheduling.Commands.PutScheduled
{
    public class PutScheduledAppointmentCommand :  IRequest<ScheduledAppointmentDto>
    {
        public Guid Id { get; set; }
        public string WorkDay { get; set; }
        public string AppointmentDate { get; set; }
        public string Observation { get; set; }
        public int ActiveGestionId { get; set; }
        public string AccountNumber { get; set; }
        public string CodeFailure { get; set; }
        public string EmailOfReceive { get; set; }
        public string NameOfReceive { get; set; }
        public string IncidenceTD { get; set; }
        public string PhoneOfReceive { get; set; }
        public bool ParentAccount { get; set; }
        public string VisitingDays { get; set; }
        public bool Active { get; set; } = false;
        public string DateOne { get; set; }
        public string? DateTwo { get; set; }
        public string? DateThree { get; set; }
    }

    public  class PutScheduledAppointmentCommandHandler : IRequestHandler<PutScheduledAppointmentCommand, ScheduledAppointmentDto>
    {
        private readonly IScheduledAppointmentService _scheduledAppointment;
        private readonly IMapper _autoMapper;

        public PutScheduledAppointmentCommandHandler(IScheduledAppointmentService scheduledAppointment, IMapper autoMapper)
        {
            _scheduledAppointment = scheduledAppointment;
            _autoMapper = autoMapper;
        }

        public async Task<ScheduledAppointmentDto> Handle(PutScheduledAppointmentCommand request, CancellationToken cancellationToken)
        {          
            var scheduledAppointment = _scheduledAppointment.PutScheduledAppointment(request);
            return _autoMapper.Map<ScheduledAppointmentDto>(scheduledAppointment);
        }
    }
}
