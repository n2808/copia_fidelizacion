﻿using Application.Interfaces.Scheduling;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Scheduling.Commands.DeleteScheduled
{
    public class DeleteScheduledAppointmentByIdCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class GetScheduledAppointmentByIdQueryHandler : IRequestHandler<DeleteScheduledAppointmentByIdCommand, bool>
    {

        private readonly IScheduledAppointmentService _scheduledAppointmentService;

        public GetScheduledAppointmentByIdQueryHandler(IScheduledAppointmentService scheduledAppointmentService)
        {
            _scheduledAppointmentService = scheduledAppointmentService;
        }
        public async Task<bool> Handle(DeleteScheduledAppointmentByIdCommand request, CancellationToken cancellationToken)
        {
            _scheduledAppointmentService.DeleteScheduledAppointment(request.Id);

            return true;
        }
    }
}
