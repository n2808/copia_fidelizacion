﻿using Application.DTOs.Scheduling;
using Application.Interfaces.Scheduling;
using AutoMapper;
using Domain.Models.Scheduling;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Scheduling.Commands.PostScheduled
{

    public class PostScheduledAppointmentCommand : IRequest<ScheduledAppointmentDto>
    {
        public string WorkDay { get; set; }
        public string AppointmentDate { get; set; }
        public string Observation { get; set; }
        public int ActiveGestionId { get; set; }
        public string AccountNumber { get; set; }
        public string CodeFailure { get; set; }
        public string EmailOfReceive { get; set; }
        public string NameOfReceive { get; set; }
        public string IncidenceTD { get; set; }
        public string PhoneOfReceive { get; set; }
        public bool ParentAccount { get; set; }
        public string VisitingDays { get; set; }
        public bool Active { get; set; } = false;
        public string DateOne { get; set; }
        public string?  DateTwo { get; set; }
        public string? DateThree { get; set; }
    }

    public class PostScheduledAppointmentCommandHandler : IRequestHandler<PostScheduledAppointmentCommand, ScheduledAppointmentDto>
    {
        private readonly IScheduledAppointmentService _scheduledAppointment;
        private readonly IMapper _autoMapper;

        public PostScheduledAppointmentCommandHandler(IScheduledAppointmentService scheduledAppointment, IMapper autoMapper)
        {
            _scheduledAppointment = scheduledAppointment;
            _autoMapper = autoMapper;
        }

        public async Task<ScheduledAppointmentDto> Handle(PostScheduledAppointmentCommand request, CancellationToken cancellationToken)
        {
            var scheduledAppointment = _scheduledAppointment
                               .PostScheduledAppointment(_autoMapper.Map<ScheduledAppointment>(request));

            return _autoMapper.Map<ScheduledAppointmentDto>(scheduledAppointment);
        }
    }
}
