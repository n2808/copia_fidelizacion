﻿using Application.DTOs.Scheduling;
using Application.Interfaces.Scheduling;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Scheduling.Queries
{
    public class ScheduledAppointmentQuery : IRequest<List<ScheduledAppointmentDto>> { }

    public class GetScheduledAppointmentQueryHandler : IRequestHandler<ScheduledAppointmentQuery, List<ScheduledAppointmentDto>>
    {

        private readonly IScheduledAppointmentService _scheduledAppointmentService;

        public GetScheduledAppointmentQueryHandler(IScheduledAppointmentService scheduledAppointmentService)
        {
            _scheduledAppointmentService = scheduledAppointmentService;
        }
        public async Task<List<ScheduledAppointmentDto>> Handle(ScheduledAppointmentQuery request, CancellationToken cancellationToken)
        {
            return _scheduledAppointmentService.GetScheduledAppointment();
        }
    }
}
