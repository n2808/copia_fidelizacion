﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.Products
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string GeneralLegalText { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; }
    }
}
