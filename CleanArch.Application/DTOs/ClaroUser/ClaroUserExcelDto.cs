﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.ClaroUser
{
    public class ClaroUserExcelDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Charge { get; set; }
        public string Location { get; set; }
    }
}