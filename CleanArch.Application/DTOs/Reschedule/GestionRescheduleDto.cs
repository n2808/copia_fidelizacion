﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.Reschedule
{
    public class GestionRescheduleDto
    {

        public Guid Id { get; set; }
        public int TipificationId { get; set; }
        public string TipificationCode { get; set; }
        public int SegmentId { get; set; }
        public int GestionId { get; set; }

        public Guid? AgentId { get; set; }

        public int? Count { get; set; }

        public bool status { get; set; }

        public DateTime lastCallAt { get; set; }

        public GestionDto Gestion { get; set; }
    }
}
