﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.SecondRingLoad
{
    public class SecondRingLoadDto
    {
        public int id { get; set; }
        public string account { get; set; }
        public string phone { get; set; }
        public string type { get; set; }
        public string assignedLogin { get; set; }
        public Guid assignedId { get; set; }
        public Guid loadId { get; set; }
        public string loadName { get; set; }
        public bool status { get; set; }
        public string CreatedByName { get; set; }

    }
}
