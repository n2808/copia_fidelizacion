﻿using System;

namespace Application.DTOs.Scheduling
{
    public class ScheduledAppointmentDto
    {
        public Guid Id { get; set; }
        public string WorkDay { get; set; }
        public string AppointmentDate { get; set; }
        public string Observation { get; set; }
        public int ActiveGestionId { get; set; }
        public string AccountNumber { get; set; }
        public string CodeFailure { get; set; }
        public string EmailOfReceive { get; set; }
        public string NameOfReceive { get; set; }
        public string IncidenceTD { get; set; }
        public string PhoneOfReceive { get; set; }
        public bool ParentAccount { get; set; }
        public string VisitingDays { get; set; }
        public bool Active { get; set; } = false;
        public string DateOne { get; set; }
        public string DateTwo { get; set; }
        public string DateThree { get; set; }
    }
}
