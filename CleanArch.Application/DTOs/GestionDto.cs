﻿using Application.DTOs.User;
using Domain.Models.Tipifications;
using System;

namespace Application.DTOs
{
    public class GestionDto
    {
        public int Id { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string CustomerName { get; set; }
        public string Customerdocument { get; set; }
        public string CampaignName { get; set; }
        public string SubCampaignName { get; set; }
        public string SegmentName { get; set; }
        public int SegmentId { get; set; }
        public int SubcampaignId { get; set; }
        public int CampaignId { get; set; }
        public string Phone { get; set; }
        public string Skill { get; set; }
        public string SkillAvaya { get; set; }
        public BackofficeUserDto UserCreated { get; set; }
        public Tipification tipificacion { get; set; }

    }
}
