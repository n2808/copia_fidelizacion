﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.BackOfficeManagements
{
    public class BackOfficeManagementsGroupDto
    {
        public int? SegmentId { get; set; }
        public string SegmentName { get; set; }
        public int? SubcampaignId { get; set; }
        public string SubcampaignName { get; set; }
        public int count { get; set; }
        public string TypeOperation { get; set; }
        public string Process { get; set; }
        public string Operation { get; set; }
    }
}
