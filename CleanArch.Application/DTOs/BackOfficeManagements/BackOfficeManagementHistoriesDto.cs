﻿using Application.DTOs.GestionReports;
using Domain.Models.ManagementsBackoffice;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.BackOfficeManagements
{
    public class BackOfficeManagementHistoriesDto
    {
        public Guid Id { get; set; }
        public string ObservationBack { get; set; }
        public string CreatedByName { get; set; }
        public string UpdatedByName { get; set; }
        public string CreatedAtFormat { get; set; }
        public string UpdateAtFormat { get; set; }
        public string? StatusName { get; set; }

    }
}
