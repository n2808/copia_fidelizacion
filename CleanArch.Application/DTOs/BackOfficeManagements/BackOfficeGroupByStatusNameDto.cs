﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.BackOfficeManagements
{
    public class BackOfficeGroupByStatusNameDto
    {
        public string? Name { get; set; }
        public int Count { get; set; }
    }
}
