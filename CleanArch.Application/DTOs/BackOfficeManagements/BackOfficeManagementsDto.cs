﻿using Application.DTOs.BackOfficeManagements;
using Application.DTOs.GestionReports;
using Application.DTOs.SecondRingLoad;
using Domain.Models.ManagementsBackoffice;
using System;
using System.Collections.Generic;

namespace Application.DTOs.ManagementsBackOffice
{
    public class BackOfficeManagementsDto
    {
        public Guid Id { get; set; }
        public int? ManagementId { get; set; }
        public int? TypificationId { get; set; }
        public string TypificationName { get; set; }
        public string TypeOperation { get; set; }
        public string Operation { get; set; }
        public string Observation { get; set; }
        public Guid? UserId { get; set; }
        public int? TypificationBackId { get; set; }
        public string TypificationNameBack { get; set; }
        public string ObservationBack { get; set; }
        public string ObservationValidator { get; set; }
        public int SegmentId { get; set; }
        public DateTime? InitValidation { get; set; }
        public string InitValidationFormat { get; set; }
        public DateTime? EndValidation { get; set; }
        public string EndValidationFormat { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string CreatedAtFormat { get; set; }
        public DateTime? UpdateAt { get; set; }
        public string UpdateAtFormat { get; set; }
        public string Process { get; set; }
        public int? GestionId { get; set; }
        public int? Count { get; set; }
        public bool Active { get; set; }
        public Domain.Models.Status.Status Status { get; set; }
        public string? StatusName { get; set; }
        public GestionReportsDto Management { get; set; }
        public List<BackOfficeManagementHistoriesDto> Histories { get; set; }

        public SecondRingLoadDto SecondRingLoad { get; set; }

    }

}
