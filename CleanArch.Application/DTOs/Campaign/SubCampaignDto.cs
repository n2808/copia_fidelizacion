﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.Campaign
{
    public class SubCampaignDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public bool IsInbound { get; set; } = false;
        public bool IsOutbound { get; set; } = true;
        public bool IsDigital { get; set; } = false;
        public string Description { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Code { get; set; }
        public int? intId { get; set; }
        public Guid? GuidId { get; set; }
        public int order { get; set; }
        public int CampaignId { get; set; }

        public CampaignDto campaign { get; set; } 

    }
}
