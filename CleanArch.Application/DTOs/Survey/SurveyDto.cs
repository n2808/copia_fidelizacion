﻿using System;
using System.Collections.Generic;

namespace Application.DTOs
{
    public class SurveyDto
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public bool status { get; set; }

        public List<SurveyQuestionDto> SurveyQuestions { get; set; }
    }


}
