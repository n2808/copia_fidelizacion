﻿using System;

namespace Application.DTOs
{
    public class SurveyResponseDto
    {

        public string Name { get; set; }
        public string Description { get; set; }

        public bool status { get; set; }

        public int Order { get; set; } = 1;

        public int SurveyQuestionId { get; set; }

    }


}
