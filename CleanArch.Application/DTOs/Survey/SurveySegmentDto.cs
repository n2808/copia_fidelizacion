﻿using System;

namespace Application.DTOs
{
    public class SurveySegmentDto
    {

        public int SurveyId { get; set; }
        public SurveyDto Survey { get; set; }

        public int SegmentId { get; set; }
        //public Segment Segment { get; set; }

        //public bool status { get; set; } = true;
    }


}
