﻿using System;
using System.Collections.Generic;

namespace Application.DTOs
{
    public class SurveyQuestionDto
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsMultiple { get; set; }


        public Guid TypeId { get; set; }

        public bool status { get; set; }

        public int Order { get; set; } = 1;

        public bool required { get; set; }

        public int SurveyId { get; set; }
        
        public List<SurveyResponseDto> SurveyResponses { get; set; }
    }


}
