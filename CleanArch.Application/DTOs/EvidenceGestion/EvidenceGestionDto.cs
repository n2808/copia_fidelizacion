﻿using Core.Models.Operation;
using Domain.Models.Operation;
using Domain.Models.Tipifications;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Application.DTOs.EvidenceGestion
{
    public class EvidenceGestionDto
    {
        public int GestionReportsId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

        //public int GestionReportsId { get; set; }

        //[ForeignKey("GestionReportsId")]
        //public Domain.Models.Gestion.GestionReports { get; set; }
    }
}
