﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.FetchFaceEvidence
{
    public class FetchFaceEvidenceDto
    {
        public int Id { get; set; }
        public string Document { get; set; }
        public bool Status { get; set; }
        public int ActiveGestionId { get; set; }
        public string URL { get; set; }
        public string UrlPath { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
