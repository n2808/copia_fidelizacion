﻿using System;

namespace Application.DTOs.Operators
{
    public class OperatorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string UserUpdate { get; set; }
        public bool Status { get; set; }
    }
}
