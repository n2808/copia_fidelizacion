﻿using Domain.Models.Operators;
using Domain.Models.Plans;
using Domain.Models.Products;

using System;

namespace Application.DTOs.Quotes
{
    public class QuoteDto 
    {
        public Guid? Id { get; set; }
        public string UserUpdate { get; set; }
        public bool? Status { get; set; }
        public int? SubCampaignId { get; set; }
        public Guid? ProductId { get; set; }
        public Guid? OperatorId { get; set; }
        public Guid? PlanId { get; set; } 
        public  Domain.Models.Operation.SubCampaign SubCampaign { get; set; }     
        public Product Product { get; set; }  
        public Operator Operator { get; set; }     
        public Plan Plan { get; set; }
    }
}
