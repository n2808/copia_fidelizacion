﻿using Application.DTOs.Campaign;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.Skills
{
    public class SkillDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public string Segment { get; set; }
        public int? SubCampaignId { get; set; }
        public Domain.Models.Operation.SubCampaign SubCampaign { get; set; }

    }
}
