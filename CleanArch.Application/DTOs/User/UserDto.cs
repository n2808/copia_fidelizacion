﻿using Application.DTOs.Campaign;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.User
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Document { get; set; }
        public string Names { get; set; }
        public string LastName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public bool Active { get; set; } 
        public string PassWord { get; set; }
        public string BlogNotas { get; set; }
        public string login { get; set; }
        public string Result { get; set; }
        public string RolId { get; set; }
        public List<UserRolDto> UserRol { get; set; }
        public Guid? CoordinadorId { get; set; }
        public User.UserDto Coordinador { get; set; }
        public Guid? GerenteId { get; set; }
        public int? SubCampaignId { get; set; }
        public Domain.Models.Operation.SubCampaign SubCampaign { get; set; }
    }
}
