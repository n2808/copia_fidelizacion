﻿using System;

namespace Application.DTOs.Backoffice
{
    public class BackofficeGestionDto
    {
        public Guid Id { get; set; }
        public DateTime? ChangeDate { get; set; }
        public string ChangeDateText { get; set; }
        public string BeforeTMCode { get; set; }
        public string BeforeValue { get; set; }
        public string BeforeName { get; set; }
        public string NewValue { get; set; }
        public string NewValueTax { get; set; }
        public string NewTMCode { get; set; }
        public string NewName { get; set; }
        public string Account { get; set; }
        public string Observation { get; set; }
        public Guid? TypePlanId { get; set; }
    }
}
