﻿using System;

namespace Application.ServicseDialer.DTOs
{
    public class ServiceDialerDto
    {
        public string Id { get; set; }
        public string VDN { get; set; }
        public string DialerName { get; set; }
        public string DisplayName { get; set; }
        public string DisplayReportName { get; set; }
        public string Description { get; set; }
        public string CallTypeName { get; set; }
        public string Name { get; set; }
        public int CampaignId { get; set; }
        public int? SegmentId { get; set; }

        public bool status { get; set; } = true;


    }
}
