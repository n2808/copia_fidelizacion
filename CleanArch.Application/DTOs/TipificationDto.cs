﻿using System;

namespace Application.DTOs
{
    public class TipificationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameLinked { get; set; }
        public int SubCampaignId { get; set; }
        public string SegmentName { get; set; }
        public string Dialer1Code { get; set; }
        public string Dialer2Code { get; set; }
        public string Report1Code { get; set; }
        public string Report2Code { get; set; }
        public string Report3Code { get; set; }

        public string IsContacted { get; set; }
        public string IsEfective { get; set; }

        public bool? AcceptOffer { get; set; } = false;
        public Int16 GestionNextStatus { get; set; }
        public bool status { get; set; }

        public string priority { get; set; }
        public string Group1 { get; set; }
        public string Group2 { get; set; }
        public string Group3 { get; set; }
        public string Group4 { get; set; }

    }
}
