﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs
{
    public class ClaroUserDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Charge { get; set; }
        public string Location { get; set; }
        public string Region { get; set; }
        public string Ring { get; set; }
        public string Supervisor { get; set; }
        public string IdentificationSupervisor { get; set; }
        public string Coordinator { get; set; }
        public string IdentificationCoordinator { get; set; }
    }
}
