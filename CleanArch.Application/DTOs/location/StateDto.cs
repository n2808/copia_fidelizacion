﻿using System;

namespace Application.DTOs.location
{
    public class StateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Status { get; set; }
        public Guid CountryId { get; set; }       
    }
}
