﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.location
{
    public class DepartmentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Status { get; set; }
        public List<CityDto> Cities { get; set; }

    }
}
