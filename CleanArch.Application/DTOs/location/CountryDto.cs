﻿using System;

namespace Application.DTOs.location
{
    public class CountryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string Code { get; set; }

    }
}
