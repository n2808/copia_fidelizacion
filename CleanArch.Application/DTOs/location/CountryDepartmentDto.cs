﻿using Core.Models.location;
using Domain.Models.location;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.location
{
    public class CountryDepartmentDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public List<City> Cities { get; set; }
        //public string NameCity { get; set; }
        //public string CodeCity { get; set; }

    }
}
