﻿using System;

namespace Application.DTOs.Status
{
    public class StatusDto
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool IsSale { get; set; }
    }
}
