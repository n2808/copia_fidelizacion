﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs
{
    public class SubCategoryTipificationDto
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string TypeCategory { get; set; }
        public bool Active { get; set; }
        public int Code { get; set; }
    }
}
