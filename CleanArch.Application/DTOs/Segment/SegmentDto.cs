﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.Segment
{
    public class SegmentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public bool IsInbound { get; set; } = false;
        public bool IsOutbound { get; set; } = true;
        public bool IsDigital { get; set; } = false;
        public string Description { get; set; }
        public string ValidationScript { get; set; }
        public string LoadScript { get; set; }
        public string Scritp1 { get; set; }
        public string Scritp2 { get; set; }
        public string GreetingDialog { get; set; }
        public string FarewelDialog { get; set; }
        public int SubCampaignId { get; set; }
        public Domain.Models.Operation.SubCampaign SubCampaign { get; set; }
    }
}
