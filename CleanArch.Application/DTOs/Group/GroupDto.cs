﻿using Core.Models.Common;
using System;

namespace Application.DTOs.Group
{
    public class GroupDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string UserUpdate { get; set; }

        public bool Status { get; set; }
    }
}
