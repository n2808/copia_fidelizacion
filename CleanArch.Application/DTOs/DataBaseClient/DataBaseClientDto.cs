﻿using Application.DTOs.Segment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.DTOs.DataBaseClient
{
    public class DataBaseClientDto
    {
        #region Account
        public string Account { get; set; }
        public string UniqueId { get; set; }
        public string AsignedId { get; set; }
        public string CustomerCode { get; set; }
        public string BaseName { get; set; }
        public string ParnerName { get; set; }
        public string AccountType { get; set; }
        public string AccountStatus { get; set; }
        #endregion

        #region GENERAL DATA
        public string Names { get; set; }
        public string LastNames { get; set; }
        public string Status { get; set; }
        public string DocumentType { get; set; }
        public string Document { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string SocialClass { get; set; }
        public string Cut { get; set; }
        #endregion

        #region SERVICE DATA
        public string DialerPhone1 { get; set; }
        public string DialerPhone2 { get; set; }
        public string DialerPhone3 { get; set; }
        public string DialerPhone4 { get; set; }
        public string DialerPhone5 { get; set; }
        public string DialerPhone6 { get; set; }
        public string DialerPhone7 { get; set; }
        public string DialerServiceCode { get; set; }
        public int LoadId { get; set; }
        public string LoadName { get; set; }
        #endregion

    }
}
