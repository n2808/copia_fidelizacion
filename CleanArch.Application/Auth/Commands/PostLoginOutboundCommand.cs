﻿using Application.Core.Exceptions;
using Application.ViewModel.Auth;
using CleanArch.Application.Interfaces.Auths;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Auth.Commands
{
    public class PostLoginOutboundCommand : IRequest<AuthOutBoundViewModel>
    {
        public string Login { get; set; }
        public string ServiceDialer { get; set; }
    }

    public class PostLoginOutboundCommandHandler : IRequestHandler<PostLoginOutboundCommand, AuthOutBoundViewModel>
    {
        private IAuthService _authService;

        public PostLoginOutboundCommandHandler(
            IAuthService authService
        )
        {
            _authService = authService;
        }

        public async Task<AuthOutBoundViewModel> Handle(PostLoginOutboundCommand request, CancellationToken cancellationToken)
        {
            var authData = await _authService.GetAuthOutBound(request);

            if (authData == null)
                throw new BadRequestException("No se ha podido ingresar el token");

            return authData;
        }
    }
}
