﻿using Application.Auth.Commands;
using Application.Gestions.Commands;
using Application.Gestions.Queries.GetInitialGestion;
using CleanArch.Application.Interfaces;
using Domain.Interfaces;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Courses.Commands
{
    public class PostLoginCommandValidator : AbstractValidator<PostLoginCommand>
    {
        private readonly IAuthRepository _authRepository;
        private readonly ISegmentService _segmentService;
        public PostLoginCommandValidator(
            IAuthRepository authRepository,
            ISegmentService segmentService

            )
        {
            _authRepository = authRepository;
            _segmentService = segmentService;
            
            RuleFor(c => c.Login).NotNull();
            RuleFor(c => c.Password).NotNull();
            //RuleFor(c => c.SubCampaignId)
            //    .NotNull();


        }


        
    }
}
