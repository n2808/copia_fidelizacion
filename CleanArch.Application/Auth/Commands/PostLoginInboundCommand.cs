﻿using Application.Core.Exceptions;
using Application.ViewModel.Auth;
using CleanArch.Application.Interfaces.Auths;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Auth.Commands
{
    public class PostLoginInboundCommand : IRequest<AuthInBoundViewModel>
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }


    public class PostLoginInboundCommandHandler : IRequestHandler<PostLoginInboundCommand, AuthInBoundViewModel>
    {
        private IAuthService _authService;

        public PostLoginInboundCommandHandler(
            IAuthService authService
        )
        {
            _authService = authService;
        }

        public async Task<AuthInBoundViewModel> Handle(PostLoginInboundCommand request, CancellationToken cancellationToken)
        {
            var authData = await _authService.GetAuthInbound(request);
            if (authData == null)
                throw new BadRequestException("No se ha podido ingresar el token");

            return authData;
        }
    }
}
