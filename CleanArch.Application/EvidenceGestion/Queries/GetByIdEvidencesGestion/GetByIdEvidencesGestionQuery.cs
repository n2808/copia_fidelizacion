﻿using Application.DTOs.EvidenceGestion;
using Application.Interfaces.EvidenceGestion;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Gestions.Queries.GetByIdEvidencesGestion
{
    public class GetByIdEvidencesGestionQuery : IRequest<List<EvidenceGestionDto>>
    {
        public int GestionReportsId { get; set; }
    }

    public class GetByIdEvidencesGestionQueryHandler : IRequestHandler<GetByIdEvidencesGestionQuery, List<EvidenceGestionDto>>
    {
        private readonly IEvidenceGestionService _evidenceGestionService;
        private readonly IMapper _mapper;

        public GetByIdEvidencesGestionQueryHandler(IEvidenceGestionService evidenceGestionService)
        {
            _evidenceGestionService = evidenceGestionService;
        }
        
        public async Task<List<EvidenceGestionDto>> Handle(GetByIdEvidencesGestionQuery request, CancellationToken cancellationToken)
        {
            List<EvidenceGestionDto> evidenceGestionDtos = _evidenceGestionService.GetByIdEvidencesGestion(request.GestionReportsId);

            return evidenceGestionDtos;
        }
    }
}
