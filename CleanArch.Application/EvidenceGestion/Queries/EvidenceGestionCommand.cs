﻿using Application.DTOs.EvidenceGestion;
using Application.Interfaces.EvidenceGestion;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.EvidenceGestion.Queries
{
    public class EvidenceGestionCommand : IRequest<List<EvidenceGestionDto>>
    {
        public int GestionReportsId { get; set; }
    }

    public class EvidenceGestionCommandHandler : IRequestHandler<EvidenceGestionCommand, List<EvidenceGestionDto>>
    {
        private readonly IEvidenceGestionService _evidenceGestionService;
        private readonly IMapper _mapper;

        public EvidenceGestionCommandHandler(IEvidenceGestionService evidenceGestionService, IMapper mapper)
        {
            _evidenceGestionService = evidenceGestionService;
            _mapper = mapper;
        }

        public async Task<List<EvidenceGestionDto>> Handle(EvidenceGestionCommand request, CancellationToken cancellationToken)
        {
            return await _evidenceGestionService.EvidenceGestion(request);
        }
    }
}
