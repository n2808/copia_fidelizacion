﻿using Application.Common.Response;
using Application.DTOs.SecondRingLoad;
using Application.Interfaces.SecondRingLoad;
using MediatR;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.SecondRingLoad.Command.Post
{
    public class SecondRingLoadExcelCommand : IRequest<ApiResponse<IList<SecondRingLoadDto>>>
    {
        public IFormFile File { get; set; }
    }

    public class SecondRingLoadExcelCommandHandler : IRequestHandler<SecondRingLoadExcelCommand, ApiResponse<IList<SecondRingLoadDto>>>
    {
        private readonly ISecondRingLoad _secondRingLoad;

        public SecondRingLoadExcelCommandHandler(ISecondRingLoad secondRingLoad)
        {
            _secondRingLoad = secondRingLoad;
        }
        public async Task<ApiResponse<IList<SecondRingLoadDto>>> Handle(SecondRingLoadExcelCommand request, CancellationToken cancellationToken)
        {
            return await _secondRingLoad.SaveFile(request.File);
        }
    }
}
