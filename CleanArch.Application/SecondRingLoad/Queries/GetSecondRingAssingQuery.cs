﻿using Application.Interfaces.SecondRingLoad;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.SecondRingLoad.Queries
{
    public class GetSecondRingAssingQuery : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class GetSecondRingAssingQueryHandler : IRequestHandler<GetSecondRingAssingQuery, bool>
    {
        private readonly ISecondRingLoad _secondRingLoad;
        public GetSecondRingAssingQueryHandler(ISecondRingLoad secondRingLoad)
        {
            _secondRingLoad = secondRingLoad;
        }
        public async Task<bool> Handle(GetSecondRingAssingQuery request, CancellationToken cancellationToken)
        {
            await _secondRingLoad.GetSecondsAssign(request.Id);
            return true;
        }
    }
}
