﻿using Application.DTOs.SecondRingLoad;
using Application.Interfaces.SecondRingLoad;
using Application.ViewModel;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.SecondRingLoad.Queries
{
    public class GetSecondRingQuery : IRequest<List<GetSecondRingViewModel>>
    {
        public int without { get; set; }
    }

    public class GetSecondRingQueryHandler : IRequestHandler<GetSecondRingQuery, List<GetSecondRingViewModel>>
    {
        private readonly ISecondRingLoad _secondRingLoad;
        public GetSecondRingQueryHandler(ISecondRingLoad secondRingLoad)
        {
            _secondRingLoad = secondRingLoad;
        }
        public async Task<List<GetSecondRingViewModel>> Handle(GetSecondRingQuery request, CancellationToken cancellationToken)
        {
            return await _secondRingLoad.GetSeconds(request.without);
        }
    }
}
