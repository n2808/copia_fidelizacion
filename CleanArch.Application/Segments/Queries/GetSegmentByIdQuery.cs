﻿using Application.DTOs.Segment;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Queries
{
    public class GetSegmentByIdQuery : IRequest<SegmentDto>
    {
        public int Id { get; set; }
    }

    public class GetSegmentByIdQueryHandler : IRequestHandler<GetSegmentByIdQuery, SegmentDto>
    {
        private readonly ISegmentService _segmentService;
        private readonly IMapper _mapper;

        public GetSegmentByIdQueryHandler(ISegmentService segmentService, IMapper mapper)
        {
            _segmentService = segmentService;
            _mapper = mapper;
        }
        public async Task<SegmentDto> Handle(GetSegmentByIdQuery request, CancellationToken cancellationToken)
        {
            var product = await _segmentService.GetSegmentById(request);
            return _mapper.Map<SegmentDto>(product);
        }
    }

}
