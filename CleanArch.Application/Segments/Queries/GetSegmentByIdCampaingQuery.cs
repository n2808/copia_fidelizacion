﻿using Application.DTOs.Segment;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Queries
{
    public class GetSegmentByIdCampaingQuery : IRequest<List<SegmentDto>>
    {
        public int Id { get; set; }
    }
    public class GetSegmentByIdCampaingQueryHandler : IRequestHandler<GetSegmentByIdCampaingQuery, List<SegmentDto>>
    {
        private readonly ISegmentService _segmentService;

        public GetSegmentByIdCampaingQueryHandler(ISegmentService segmentService)
        {
            _segmentService = segmentService;
        }
        public async Task<List<SegmentDto>> Handle(GetSegmentByIdCampaingQuery request, CancellationToken cancellationToken)
        {
            return await _segmentService.GetSegmentByIdCampaing(request.Id);
        }
    }
}
