﻿using Application.DTOs.Segment;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Queries
{
    public class GetSegmentQuery : IRequest<List<SegmentDto>>
    {
        public int without { get; set; }
    }
    public class GetSegmentQueryCommandHandler : IRequestHandler<GetSegmentQuery, List<SegmentDto>>
    {
        private readonly ISegmentService _segmentService;

        public GetSegmentQueryCommandHandler(ISegmentService segmentService)
        {
            _segmentService = segmentService;
        }
        public async Task<List<SegmentDto>> Handle(GetSegmentQuery request, CancellationToken cancellationToken)
        {
            return await _segmentService.GetSegment(request.without);
        }
    }
}
