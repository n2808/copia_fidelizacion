﻿using Application.DTOs.Segment;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Command
{
    public class PutSegmentCommand : IRequest<SegmentDto>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public bool IsInbound { get; set; } = false;
        public bool IsOutbound { get; set; } = true;
        public bool IsDigital { get; set; } = false;
        public string Description { get; set; }
        public string ValidationScript { get; set; }
        public string LoadScript { get; set; }
        public string Scritp1 { get; set; }
        public string Scritp2 { get; set; }
        public string GreetingDialog { get; set; }
        public string FarewelDialog { get; set; }
        public int SubCampaignId { get; set; }
        public Domain.Models.Operation.SubCampaign SubCampaign { get; set; }
    }

    public class PutSegmentCommandHandler : IRequestHandler<PutSegmentCommand, SegmentDto>
    {
        private readonly ISegmentService _segmentService;
        private readonly IMapper _mapper;

        public PutSegmentCommandHandler(ISegmentService segmentService, IMapper mapper)
        {
            _segmentService = segmentService;
            _mapper = mapper;
        }
        public async Task<SegmentDto> Handle(PutSegmentCommand request, CancellationToken cancellationToken)
        {
            var product = await _segmentService.PutSegment(request);
            return _mapper.Map<SegmentDto>(product);
        }

    }
}
