﻿using Application.DTOs.Segment;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Command.Put
{
    public class PutStatusActivateSegmentByIdCommand : IRequest<SegmentDto>
    {
        public int Id { get; set; }
    }
    public class PutStatusActivateSegmentByIdCommandHandler : IRequestHandler<PutStatusActivateSegmentByIdCommand, SegmentDto>
    {
        private readonly ISegmentService _segmentService;
        private readonly IMapper _mapper;

        public PutStatusActivateSegmentByIdCommandHandler(ISegmentService segmentService, IMapper mapper)
        {
            _segmentService = segmentService;
            _mapper = mapper;
        }
        public async Task<SegmentDto> Handle(PutStatusActivateSegmentByIdCommand request, CancellationToken cancellationToken)
        {
            var segment = await _segmentService.PutStatusActivateSegmentById(request);
            return _mapper.Map<SegmentDto>(segment);
        }
    }
}
