﻿using Application.DTOs.Segment;
using AutoMapper;
using CleanArch.Application.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Command.Put
{
    public class PutStatusDeActivateSegmentByIdCommand : IRequest<SegmentDto>
    {
        public int Id { get; set; }
    }

    public class PutStatusDeActivateSegmentByIdCommandHandler : IRequestHandler<PutStatusDeActivateSegmentByIdCommand, SegmentDto>
    {
        private readonly ISegmentService _segmentService;
        private readonly IMapper _mapper;

        public PutStatusDeActivateSegmentByIdCommandHandler(ISegmentService segmentService, IMapper mapper)
        {
            _segmentService = segmentService;
            _mapper = mapper;
        }
        public async Task<SegmentDto> Handle(PutStatusDeActivateSegmentByIdCommand request, CancellationToken cancellationToken)
        {
            var segment = await _segmentService.PutStatusDeActivateSegmentById(request);
            return _mapper.Map<SegmentDto>(segment);
        }
    }

}
