﻿using Application.DTOs.Segment;
using AutoMapper;
using CleanArch.Application.Interfaces;
using Domain.Models.Operation;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Command.Post
{
    public class PostSegmentCommand : IRequest<SegmentDto>
    {
        public string Name { get; set; }
        public bool Status { get; set; }
        public bool IsInbound { get; set; } = false;
        public bool IsOutbound { get; set; } = true;
        public bool IsDigital { get; set; } = false;
        public string Description { get; set; }
        public string ValidationScript { get; set; }
        public string LoadScript { get; set; }
        public string Scritp1 { get; set; }
        public string Scritp2 { get; set; }
        public string GreetingDialog { get; set; }
        public string FarewelDialog { get; set; }
        public int SubCampaignId { get; set; }
    }

    public class PostSegmentCommandHandler : IRequestHandler<PostSegmentCommand, SegmentDto>
    {
        private readonly ISegmentService _segmentService;
        private readonly IMapper _mapper;

        public PostSegmentCommandHandler(ISegmentService segmentService, IMapper mapper)
        {
            _segmentService = segmentService;
            _mapper = mapper;
        }
        public async Task<SegmentDto> Handle(PostSegmentCommand request, CancellationToken cancellationToken)
        {
            var segment = await _segmentService.PostSegment(_mapper.Map<Segment>(request));
            return _mapper.Map<SegmentDto>(segment);
        }
    }
}
