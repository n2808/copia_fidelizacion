﻿using CleanArch.Application.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Segments.Command
{
    public class DeleteSegmentCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }

    public class DeleteSegmentCommandHandler : IRequestHandler<DeleteSegmentCommand, bool>
    {
        private readonly ISegmentService _segmentService;

        public DeleteSegmentCommandHandler(ISegmentService segmentService)
        {
            _segmentService = segmentService;
        }
        public async Task<bool> Handle(DeleteSegmentCommand request, CancellationToken cancellationToken)
        {
            await _segmentService.DeleteSegment(request.Id);
            return true;
        }
    }
}
