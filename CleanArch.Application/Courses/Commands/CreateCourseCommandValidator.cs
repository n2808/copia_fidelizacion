﻿using CleanArch.Application.ViewModel;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Courses.Commands
{
    public class CreateCourseCommandValidator : AbstractValidator<CourseViewModel>
    {
        public CreateCourseCommandValidator()
        {
            RuleFor(c => c.Name).NotNull().MaximumLength(2).WithMessage("mensaje de pruebea");
            RuleFor(c => c.Description).NotNull().MaximumLength(2).WithMessage("mensaje de pruebea");
        }
    }
}
